﻿# -*- coding: utf-8 -*-
import os, sys
import requests
import re
import xlwt
import imp
import pymysql
import dateparser
import redis
import time
import warnings
import datetime
from datetime import datetime
from datetime import date
import io
warnings.filterwarnings('ignore')
from googletrans import Translator

# reload(sys)
# sys.setdefaultencoding('utf-8')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

base_url = 'http://www.przetargi.egospodarka.pl'
block_re = '<tr>([\w\W]*?)<\/tr>'
title_re = 'Nazwa\s*nadana[\w\W]*?\"\>([^>]*?)<\/span>'
dates_re = 'Daty\s*\:\s*publikacji\s*\/\s*zak[^>]*?enia\"\>([^>]*?)\s*\<br\>([^>]*?)\s*<'
sub_url_re = 'Przedmiot zam[\w\W]*?wienia">\s*<a\s*[\w\W]*?href="([^>]*?)">([^>]*?)<'
title_attribute_re = 'Nazwa\s*nadana[\w\W]*?">([\w\W]*)<\/span'
name_address_re = 'NAZWA I ADRES:([\w\W]*?)<\/p>'
awardProcedure_re = 'zastosowanie_procedury[\w\W]*?\"\>([\w\W]*?)<\/span>'
cpv_code_re = 'kod CPV:[\w\W]*?glowny[\w\W]*?\">([\w\W]*?)\s*<\/TABLE>'
description_re = 'opis\s*przedmiotu[\w\W]*?\:([\w\W]*?)<\/span>'
reference_re = 'numer\_referencyjny\"\>\s*([^>]*?)\s*\<\/span>'
reference_re1 = '(?:<p><b>Nr\:<\/b>|<span id="[^>]*?Ogloszenia">)\s*([^>]*?)\s*<\/'
value_contract_re = 'wartosc_zamowienia_calosc\"\>([^>]*?)<\/span>'
time_read_line = 'Time limit for receipt of tenders or requests to participate:'
d_ra_date_re = 'ctl00\_ContentPlaceHolder1_IV_4_4_data"\>([^>]*?)<\/span>'
d_ra_time_re = 'ContentPlaceHolder1_IV_4_4_godzina">([^>]*?)<\/span>'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
Tender_ID = 529
nxt_re = 'nexpage2[\w\W]*?href\=\"([^>]*?)">nast[\w\W]*?pna[\w\W]*?<\/a>'

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
#print "re----->",Retrive_duplicate


def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'pl'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'pl'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    return translated_sentence


def reg_clean(desc):
    desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')
    desc = desc.replace('&quot;', '')
    desc = re.sub(r'<[^<]*?>', ' ', str(desc))
    desc = re.sub(r'\r\n', '', str(desc), re.I)
    desc = re.sub(r"\\r\\n", '', str(desc), re.I)
    desc = re.sub(r'\t', " ", desc, re.I)
    desc = re.sub(r'\s\s+', " ", desc, re.I)
    desc = re.sub(r"^\s+\s*", "", desc, re.I)
    desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
    desc = re.sub(r"\&ndash\;", "-", desc, re.I)
    desc = re.sub(r"&quot;", "'", desc, re.I)

    return desc


def req_content(url):
    req_url = requests.get(url)
    url_content = req_url.content.decode('utf-8', errors = 'ignore')
    url_content = req_url.content
    return url_content

def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def main():
    global n
    # global main_url
    main_url = 'http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance'
    main_req = requests.get("http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance")
    while True:
        # main_content = req_content(main_url)
		# main_req = requests.get("http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance")
        main_content = main_req.content.decode('latin-1')
        with open("nxt_page1.html","w+") as f:
            f.write(main_content)
        blocks = regxx(block_re,main_content)

        for block in blocks[1:]:
            print ("NO: ", n)
            n = n + 1
            dates = regxx(dates_re,block)

            if dates:
                publication = dates[0][0]
                completion = dates[0][1]
            else:
                publication = ''
                completion = ''

            sub_url = regxx(sub_url_re,block)
            if sub_url:
                sub_urll = sub_url[0][0]
                Ordered_object = sub_url[0][1]
                sub_url_full = base_url + sub_urll
            else:
                Ordered_object = ''
            print ("Sub-URL:", sub_url_full)	
            sub_content1 = requests.get(sub_url_full)
			
            sub_content = sub_content1.content.decode('latin-1')
            # sub_content = str(sub_content1.content)
            with io.open("nxt_page2.html","w+") as f:
                f.write(sub_content)
            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url_full)
            if Duplicate_Check == "N":
                curr_date = dateparser.parse(time.asctime())
                created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                title_attribute = regxx(title_attribute_re,sub_content)
                if title_attribute:
                    title_attribute = title_attribute[0]
                else:
                    title_attribute = ''
                title = re.findall(title_re,sub_content)
                if title:
                    title = title[0]
                    title = reg_clean(title)
                    title = translateText(title)
                    title = title.replace("'", "''")
                else:
                    title = ''
                print ("title ",title)
                name_address = regxx(name_address_re,sub_content)
                print (name_address)
                input("****")
                if name_address:
                    name_address = name_address[0]
                    name_address = reg_clean(name_address)
                    # name_address = translateText(str(name_address))
                    # name_address = name_address.replace("'", "''")
                    # name_address = name_address1.replace("'", "''")
                    # name_address = name_address.replace("'", "''")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("ê","ę")
                    name_address = name_address.replace("¿","ż")
                    name_address = name_address.replace("æ","ć")
                    name_address = name_address.replace("ñ","ń")
                    name_address = name_address.replace("³","ł")
                else:
                    name_address = ''
                with open("nxt_page2.xlsx","a+") as f:
					f.write(1,1,name_address)
        nxt = regxx(nxt_re, main_content)
        if nxt:
            nxt = base_url + nxt[0]
            main_url = nxt
        else:
            print ("<----Over---->")
            break
if __name__== "__main__":
    main_url = 'http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance'

    n = 1
    main()
