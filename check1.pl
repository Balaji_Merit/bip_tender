#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use POSIX qw(strftime);
use Time::Piece;
use BIP_Tender_DB;

# $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);
my $url1="https://www.finditinbirmingham.com/opportunities";
my ($Dash_content_24)=&Getcontent($url1);

open(ts,">Dash_content.html");
print ts "$Dash_content_24";
close ts; 


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.finditinbirmingham.com");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    # $req->header("Content-Type"=> "application/x-www-form-urlencoded");
	# $req->header("Cookie"=> "f=L_eQdC_zVGwFXyRUBAiGzplrWTENxf7rZfM3pbLYLdAaoKjLpSRb0R5LEcIrExBMr-FFrKyhehLQ4kjMHIwZfM51m-JBctz-i2FRhOtcd_w1; __insp_wid=1460515254; __insp_slim=1517577764498; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly93d3cuZmluZGl0aW53b3JjZXN0ZXJzaGlyZS5jb20vYWNjb3VudC9sb2dpbg%3D%3D; __insp_targlpt=TG9naW4gfCBGaW5kaXRpbldvcmNlc3RlcnNoaXJl; _ga=GA1.2.1408162916.1517577440; _gid=GA1.2.1393119484.1517577440; __insp_sid=1257031291; __insp_uid=2517834047");
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		print "Location :: $loc\n";
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
