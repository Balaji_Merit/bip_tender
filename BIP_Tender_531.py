# -*- coding: utf-8 -*-

import requests
import os, sys
import re
import xlwt
from googletrans import Translator
import redis
import time
import json
import pymysql
import xlwt
import imp
import dateparser
import time
import datetime
from datetime import datetime
from datetime import date
import dateutil.parser as dparser
# import urllib3.contrib.pyopenssl

# reload(sys)
# import codecs
# codecs.register(lambda name: codecs.lookup('utf-8') if name == 'cp65001' else None)
# sys.setdefaultencoding("utf-8")
# book = xlwt.Workbook(encoding="UTF-8")
# worksheet = book.add_sheet("data_scrape")
# style = xlwt.XFStyle()
# import redis
# from googletrans import Translator

https_proxy = "https://172.27.137.199:3128"
# Database_Connector = imp.load_source('Database_Connector','D:\BIP_Tender_files\Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
# mysql_cursor.set_character_set('utf8')
# mysql_cursor.execute('SET NAMES utf8;')
# mysql_cursor.execute('SET CHARACTER SET utf8;')
# mysql_cursor.execute('SET character_set_connection=utf8;')


def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	# dt = parse(Input)
	dt=dparser.parse(Input,fuzzy=True)
	FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	return (FormattedDate)
	
def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')

	desc = re.sub(r'<[^<]*?>', ' ', desc)
	desc = re.sub(r'\r\n', '', desc, re.I)
	desc = re.sub(r"\\r\\n", '', desc, re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	return desc 

# conn=pymysql.connect(host='172.27.138.250',port=3306,user='root',password='admin',db='BIP_tender_analysis')
# cur=conn.cursor()
file_name=os.path.basename(__file__)
file_name1 = re.sub(r"BIP_Tender_","",file_name,re.I)
# print file_name1
# raw_input("dddddd")
Tender_ID = re.sub(r".py","",file_name1,re.I)
main_url_query = u"select tender_link from tender_master where id="+str(Tender_ID)
# print Tender_ID
# raw_input("ddddd")
##Redis Connection::::
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)

def redis_connection():
	red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
	return red_connec
	
# def translateText(InputText):
def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'lv'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'lv'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    if isinstance(translated_sentence, str):
        return translated_sentence
    else:	

        return translated_sentence.decode("utf-8")			
    # print "Returning Text :",translated_sentence
    # return translated_sentence
	

def program():
	# main_url = Database_Connector.select_query(mysql_cursor, main_url_query)
	# main_req = requests.get("https://www.iub.gov.lv/iubsearch/pt/_pr/",proxies = proxies)
	# main_cont = main_req.content
	# proxies = {
    # "https" :"https://172.27.137.192:3128",
    # "https":"https://172.27.137.199:3128"
    # }
	# main_req = requests.get("https://info.iub.gov.lv/meklet/pt/_pr/page/1",proxies = proxies)
	# main_cont = main_req.content
	s=requests.session()
	count = 1
	headers1 = {"Host":"info.iub.gov.lv",
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0",
			"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			# "Content-Type":"application/x-www-form-urlencoded",
			# "Referer": "https://www.marches-publics.gouv.fr/?page=entreprise.EntrepriseAdvancedSearch&searchAnnCons",
			"Cookie": "_ga=GA1.3.690759792.1603737041; _gid=GA1.3.1851327829.1603737041; has_js=1; _pk_ref.5.6a91=%5B%22%22%2C%22%22%2C1603737063%2C%22https%3A%2F%2Fwww.iub.gov.lv%2F%22%5D; _pk_ses.5.6a91=1; mtm_consent=1603737063062; _pk_id.5.6a91=b995a8713261291a.1603737063.1.1603737481.1603737063."
			}
	for p in range(1,20):
		url = "https://info.iub.gov.lv/meklet/pt/_pr/page/"+str(p)
		# url = "https://info.iub.gov.lv/meklet/page/"+str(p)
		print (url)
		# res = requests.get(url)
		
		res = s.get(url,verify=False)
		href_list = re.findall('<div class="name"><a href="([^>]*?)"',str(res.content))
		for href in href_list:
			print ("Link:",href)
			Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, href)
			if Duplicate_Check == "N":
				curr_date = dateparser.parse(time.asctime())
				created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
				# re_res = requests.get(href)
				try:
					re_res = requests.get(href,verify=False)
				except Exception as e:
					pass
				# data = re_res.content
				data = re_res.content.decode('utf_8', errors = 'ignore')
				title = re.findall('priek[\w\W]*?mets\s*[\w\W]*?l[\w\W]*?guma[^>]*?<\/h\d{1}>\s*<p>\s*([^>]*?)\s*<\/p>',data)
				# title = re.findall('Iepirkuma līguma nosaukums[^>]*?<\/h\d{1}>\s*<p>\s*([^>]*?)\s*<\/p>\s*<\/div>',data)
				if title:
					title = translateText(title[0])
				else:
					title = ""
				print ("Title",title)	
				awarding_authority = re.findall('<section class="container content">([\w\W]*?)<\/div>\s*<\/div>\s*<div class="tr">',data)
				if awarding_authority:
					# print (awarding_authority[0])
					awarding_authority1 = reg_clean(awarding_authority[0])
					awarding_authority = awarding_authority1.replace('\s+\s+','\s+')
					# awarding_authority = awarding_authority.replace('\Ä\«n','\ī')
					# awarding_authority = awarding_authority.replace('\Ä','\ā')
					# awarding_authority = awarding_authority.replace('\Ä','\ē')
					# awarding_authority = awarding_authority.replace('\Å','\ņ')
					
					
				else:
					awarding_authority = ""
				# awarding_authority = awarding_authority.encode('utf-8', errors = 'ignore')
				# awarding_authority = awarding_authority.decode('latin-1')
				print (awarding_authority)
				location = re.findall('Paredzamā līguma izpildes vieta<\/h4>\s*<p>\s*([^>]*?)\s*<\/p>',data)
				if location:
					location = reg_clean(location[0])
				else:
					location = re.findall('Paredzam[^>]*?guma izpildes vieta<\/h4>\s*<p>\s*([^>]*?)\s*<\/p>',data)
					if location:
						location = reg_clean(location[0])
					else:
						location = re.findall('<h4>[^>]*?pakalpojumu sni[^>]*?nas vai pieg[^>]*?s vieta[^>]*?<[^>]*?>\s*(?:<[^>]*?>\s*|\s*)\s*([^>]*?)\s*<\/',data)
						if location:
							location = reg_clean(location[0])
						else:
							location = re.findall('B[^>]*?arbu ve[^>]*?nas\, pakalpojumu sni[^>]*?nas vai pieg[^>]*?s vieta:\s*<span>([^>]*?)<\/span><\/p>',data)
							if location:
								location = reg_clean(location[0])
							else:
								location = ''
				value = re.findall('Paredzamā līgumcena, bez PVN:\s*<span>([\w\W]*?)<\/span>',data)
				if value:
					value = translateText(value[0])

				else:
					value = ''
				deadline_date1 =  re.findall('<p>Datums\:\s*<span>\s*([^>]*?)\s*<\/span>[^>]*?<span>([^>]*?)<\/span><\/p>',data)
				print (deadline_date1)
				if deadline_date1:
					
					deadline_date = str(deadline_date1[0][0]).replace('&nbsp;', '')
					# deadline_date = deadline_date1[0][0]+" 00:00:00"
					deadline_date = DateFormat(deadline_date)
					print (deadline_date)
					if deadline_date:
						
						# dt = dateparser.parse(deadline_date)
						# deadline_date = DateFormat(deadline_date1[0][0])
						dt = deadline_date
						# dt = datetime.strftime(deadline_date,'%d/%m/%Y %H:%M:%S')
						# print (dt)
						# FormattedDate = (dt.strptime('%Y-%m-%d %H:%M:%S'))
						FormattedDate = dt
						print (FormattedDate)
						# raw_input("Checking2")
						deadline_time = deadline_date1[0][1].strip()
				else:
					deadline_date2 = re.findall('<div class="col c1">\s*J[^>]*?b[^>]*?t\:\s*<br\s*\/>([^>]*?)\s*<i>\(dd\/mm\/gggg\)<\/i>\s*([^>]*?)\s*<\/div>',data)
					if deadline_date2:
						deadline_date = DateFormat(deadline_date2[0][0])
						print (deadline_date)
						FormattedDate = deadline_date
						if deadline_date:
						# deadline_date = str(deadline_date).replace('&nbsp;', '')
						
							# dt = dateparser.parse(deadline_date)
							# dt = deadline_date.strip()
							# FormattedDate = (dt.strftime('%d-%m-%Y %H:%M:%S'))
							# print (FormattedDate)
							# raw_input("Checking")
							deadline_time = deadline_date2[0][1].strip()
						
					else:
						deadline_date = ''
						FormattedDate = ''
						deadline_time = ''
				print (FormattedDate)
				# raw_input("Check")
				deadline_description = 'Date of submission of the offer:'
				orgin = 'European/Non Defence'
				sector = 'European Translation'
				country = 'LV'
				websource = 'Latvia: IUB'
				summary ='The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website:'
				other_information = summary + str(href)
				other_information = translateText(str(other_information))
				print ('other_information::',other_information)
				if FormattedDate != None:
					# /try:
					insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(href), str(), str(orgin), str(sector), str(country), str(websource), str(title), str(title),awarding_authority, str(), str(), str(), str(title), location, str(), str(value), str(deadline_description), FormattedDate, str(deadline_time),str(), str(other_information), str('Y'),str(created_date))
					try:
						val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
						j = 1
						print (insertQuery)
					except:
						j = 0
						pass
				# else:
					# insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(href), str(), str(orgin), str(sector), str(country), str(websource), str(title), str(title),awarding_authority, str(), str(), str(), str(title), str(location), str(), str(value), str(deadline_description), str(), str(),str(), str(other_information), str('Y'),str(created_date))
					
					
				else:
					j = 0
				# print "insertion status :",val
				if j == 1:
					print ("1 Row Inserted Successfully")
				else:
					print ("Insert Failed")
				if count <= 100:
					count = count + 1
				else:
					break
	connection.close()
		
if __name__== "__main__":
	program()


