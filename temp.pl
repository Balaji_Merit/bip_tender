use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Date::Parse;
use POSIX qw(strftime);
use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );


print localtime;
my $fulldate = strftime "%d %m %Y %A", localtime;
print $fulldate;