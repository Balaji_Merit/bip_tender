#### Modules ####
use strict;
use WWW::Mechanize;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### UserAgent Declaration ####
my $mech = WWW::Mechanize->new();

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
$Tender_Url='https://www.finditinworcestershire.com/account/login';
my $User_Name	= 'prabukannan.mani@meritgroup.co.uk';
my $pwd			= 'Merit123';
sleep(500);
$mech->get($Tender_Url);
sleep(500);
$mech->submit_form(
	form_number => 1,
	fields      => { "EmailAddress"			=> $User_Name, "Password"	=> $pwd},
);
sleep(500);
my $Tender_content2 = $mech->content;
open(ts,">Tender_contentchk.html");
print ts "$Tender_content2";
close ts;

if($Tender_content2=~m/<li><a href=([^>]*?)>Opportunities<\/a>/is)
{
	my $Opportunity_Url = 'https://www.finditinworcestershire.com'.$1;
	print "$Opportunity_Url\n";
	$mech->get($Opportunity_Url);
	my $Opportunity_content1 = $mech->content;
	 
	open(ts,">Opportunity_content1.html");
	print ts "$Opportunity_content1";
	close ts;

	while($Opportunity_content1=~m/(<h2>[\w\W]*?)Read\s*more/igs)
	{
		my $block=$1;
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
		if($block=~m/<h2>\s*<a\s*href\=([^>]*?)>\s*([^>]*?)\s*<\/a>[\w\W]*?<span\s*class\=[^>]*?>Location<\/span>\s*<span\s*class\=[^>]*?>([^>]*?)<\/span>[\w\W]*?<span\s*class\=[^>]*?>Value<\/span>\s*<span\s*class\=[^>]*?>\s*\�\s*([^>]*?)\s*<\/span>[\w\W]*?<span\s*class\=[^>]*?>\s*([^>]*?)\s*<\/span>\s*<span\s*class\=[^>]*?>\s*([^>]*?)\s*(\d+\:\d+)<\/span>/is)
		   {
				my $tender_link='https://www.finditinworcestershire.com'.$1;
				$tender_title=$2;
				$tender_title=&clean($tender_title);
				$tender_title=~s/\s*\(\\//igs;
				$Location=$3;
				$Value=$4;
				$Deadline_Description=$5;
				$Deadline_Date=$6;
				$Deadline_Date= &clean($Deadline_Date);
				$Deadline_time=$7;
				print "$tender_link\n";		 
				#########################  Duplicate Check ######################################		 
				my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
				if ($duplicate eq 'Y')
				{
					print "\nAlready Existing this Tender Data\n";
					next;
				}
				else
				{
					print "\nThis is newly Released Tender\n";
				}
				####################################################################################
				
				$mech->get($tender_link);
				my $Tender_content = $mech->content;
				open(ts,">Tender_content_check_26.txt");
				print ts "$Tender_content";
				close ts;
				my $tenderblock;	
			    if($Tender_content=~m/<label[^>]*?Description>Description<\/label>([\w\W]*?)<\/div><\/div><\/div><\/div>/is)
				    {
			   
					   $tenderblock	= ($1);
				    }
					$tenderblock = &BIP_Tender_DB::clean($tenderblock);
				$title_check_duplicate=~s/\s*\(\\//igs;
				# open(ts,">>Opportunity_content1.xls");
				# print ts "$tenderblock\t$tender_title\t$tender_link\t$Deadline_Date\t$Deadline_time\t$Location\t$Value\t$Deadline_Description\n";
				# close ts;
				
				&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure); 
			}						
						 
	}			 
}				

#### To get the tender link ####
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}