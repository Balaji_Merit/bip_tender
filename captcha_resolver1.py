from twocaptchaapi import TwoCaptchaApi
import requests
from datetime import date, timedelta
import re
import os,sys
import pymysql
import time
import json
import pymysql
# import cookielib
# import xlwt
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
# import ConfigParser
import dateutil.parser as dparser
from time import sleep
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
import pytz
# import warnings
# warnings.warn("ignore")
# from urllib.request import urlretrieve
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
api = TwoCaptchaApi('18c788d08a948697708ea27a0207871f')
h = HTMLParser()
block_re = '(<td\s*valign\=\"top\">[\w\W]*?Voir)'
details_block_re = '(<td[\w\W]*?<\/td>)'
sub_detail_re = '(<p[\w\W]*?<\/p>)'
# boamp_blocks_re ='class\="m[^>]*?corps[^>]*?\">([\w\W]*?)<\/tr>'
boamp_blocks_re ='<article class="result-search-avis">([\w\W]*?)<\/article>'
b_link_re = '<td style="display:block; text-align:left">\s*<a href="([^>]*?)">'
title_re = '<dt[^>]*?>Title:<\/dt>\s*<dd[^>]*?>([^>]*?)<\/dd>'
article_re = '<article\s*role\=\"article\">([\w\W]*?)<\/article>'
awardingAuthority_re = '>\s*Buyer\s*Organisation\s*\:\s*<\/dt>\s*<dd[^>]*?>\s*<a\s*href\=\"[^>]*?\"[^>]*?>\s*([^>]*?)\s*<'
awarding_org1_re='>\s*Buyer\s*Organisation\s*\:\s*<\/dt>\s*<dd>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*[^>]*?\s*<'
awarding_org_re='(Organisation\s*Name\s*\(EN\)[^>]*?)\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dl>'
object_contract_re = '<h3>Objet[\w\W]*?<p>([\w\W]*?)</p>'
contract_type='Procurement\s*Type\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>'
desc_re = 'Description\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>'
autho_ref_re = 'class\=\"avis\-ref\">([^>]*?)<\/p>'
CPV_Code_re='CPV\s*Codes\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>'
nuts_code_re='>\s*NUTS\s*codes\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>'
# deadline_date_re = '<h3>Date\s*limite[\w\W]*?<p>([/^\d{2}\/\d{2}\/\d{4}$/]*?)\s+'
deadline_date_re = 'Time\-limit\s*for\s*receipt\s*of\s*tenders[^>]*?\s*<\/dt>\s*<dd[^>]*?>\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s+[^>]*?\s*<\/dd>'
# deadline_date_re = 'Date\s*limite[^>]*?<\/td>[\w\W]*?<td class="txt">(\d{1,2})\/(\d{1,2})\/(\d{4})\s+'
# deadline_time_re = 'Date\s*limite\s*de[^>]*?[^>]*?<\/td>[\w\W]*?<td class="txt">[\w\W]*?\s*(\d{1,2}h\d{2})'
deadline_time_re = 'Time\-limit\s*for\s*receipt\s*of\s*tenders[^>]*?\s*<\/dt>\s*<dd[^>]*?>\s*[^>]*?\s([^>]*?)\s*<\/dd>'
mail_date_re = '[^>]*?\,\s+([\d]{2}\s+[^>]*?\s+[\d]{4})\s+[^>]*?'
award_procedure_re='>\s*Procedure(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>'
value_re='>\s*Estimated\s*Value[^>]*?(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>'
orgin = 'Web'
noticeSector = 'Republic/NI Daily'
Sector = 'Republic/NI Daily'
tender_type = "Normal"
Industry_Sector = "European/Defence"
proofed = "No"
sourcecw = "Contrax Weekly"
origin = "Web"
noticeSector = "Republic/NI Daily"
cy = "GB"
country_code = "United Kingdom"
sourceURL = "eTenders NI (NI)"
pubFlagCW = "No"
authorityDept = ""
authorityAddress2 = ""
authorityAddress3 = ""
Industry_Sector = "European/Defence"
authorityCountry = "GB"
Tender_ID = 20
orgin_Boamp = 'Web'
orgin_place = 'European/Defence'
sector = 'Republic/NI Daily'
country = 'GB'
web_source='eTenders NI (NI)'
dead_line_W_Boamp = '(Time-limit for receipt of tenders[^>]*?)\s*<\/dt>\s*<dd>\s*[^>]*?\s+[^>]*?\s*<\/dd>'
summary_cont = 'For further information regarding the above contract notice please visit: '
# tender_block_re='
mail_block_re='<dt>Contact Point\s*\:\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>'
	    
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate


def reg_clean(cont):
    cont = str(cont).replace('\r','')
    cont = str(cont).replace('\n','')
    cont = str(cont).replace('\t','')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'","''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'","''")
    cont = cont.replace("î","'")
    cont = cont.replace("l'","l''")
    cont = cont.replace("L'","L''")
    cont = cont.replace("d'","d''")
    cont = cont.replace("D'","D''")
    cont = cont.replace("'s","''s")
    cont = cont.replace("du'","du''")
    cont = cont.replace("N'","N''")
    cont = cont.replace("'acheteur","''acheteur")
    cont = cont.replace("'id","''id")
    cont = cont.replace("'b'","'b''")
    cont = cont.replace("'Esp","''Esp")
    cont = cont.replace("'RD","''RD")
    # cont = str(cont).replace("\’","''")
    cont = cont.replace("’","''", re.I)
	# cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''","''")
    cont = cont.replace("'''''","''")
    cont = cont.replace("''''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("\xc3\xa9","é")
    cont = cont.replace("&#39;","''")
    cont = cont.replace("&quot;",'"')
    cont = cont.replace("î","'")

    return cont
def regxx(regx,content):
    cont_data = re.findall(regx,content)
    if cont_data:
        cont_data = cont_data[0] 
    else:
        cont_data = ''
    return h.unescape(cont_data)
	
def Capcha_Convert(capcha_image):
  try:
      
      # global captchacount
      # captchacount += 1
      # print (captchacount)
      Balance = (api.get_balance())
      print ("Balance:", (api.get_balance()))
      '''check balance in 2captcha source'''
      if float(Balance) == 0.0:
          result = 'Empty'
          raise Exception("OperationFailedError: ERROR_ZERO_BALANCE")
      else:
          with open(capcha_image, 'rb') as captcha_file:
              captcha = api.solve(captcha_file)
          try:
              result = captcha.await_result()
              # print (result)
              return result
          except Exception as e:
              print( "Error", str(e))

      
  except Exception as e:
      print( "Error", str(e))
def temp():
	proxies = {
	"http" :"http://172.27.137.199:3128",
	"https":"https://172.27.137.199:3128"
	}
	last_week = date.today()- timedelta(1)
	print ("Today******",last_week)
	# last_week_date1 = (last_week.strftime('%d'))
	# last_week_date2 = (last_week.strftime('%m'))
	# last_week_date3 = (last_week.strftime('%Y')) 
	# for xy in range(1,4):
	home_url="https://etendersni.gov.uk/epps/prepareAdvancedSearch.do?type=cft"
	s=requests.session()
	r=s.get(home_url)
	html=r.content.decode('utf-8', errors = 'ignore')
	html = str(html).replace('\r','')
	html = str(html).replace('\n','')
	html = str(html).replace('\t','')
	print (html)
	capitcha=re.findall('<img id="CAPTCHA"\s*src="([^>]*?)"',html)
	url1="https://etendersni.gov.uk"+str(capitcha[0])
	r1=s.get(url1)
	html1=r1.content.decode('utf-8', errors = 'ignore')
	# print (html1)
	with open ('filename.jpg','wb') as f:
		f.write(r1.content)


	# urllib.request.urlretrieve(url1, "filename.jpg")
	temp=Capcha_Convert("filename.jpg")
	# post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=,&contractType=,&cpcCategory=,0&procedure=,&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=,&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
	# print (post_cont)
	post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=&status=&contractType=&contractType=&cpcCategory=&cpcCategory=&procedure=&procedure=&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=&description=&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
	post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=&status=&contractType=&contractType=&cpcCategory=&cpcCategory=&procedure=&procedure=&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=&description=&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
	# resourceId=&title=&uniqueId=&contractAuthority=&status=,&contractType=,&cpcCategory=,&procedure=,&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=,&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha=2n2ufr
	print (post_cont)
	headers1 = {"Host":"etendersni.gov.uk",
				# "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
				"Content-Type":"application/x-www-form-urlencoded",
				"Connection": "keep-alive",
				# "Accept-Language": "en-US,en;q=0.5",
				# "Accept-Encoding": "gzip, deflate, br",
				# "Cookie": "JSESSIONID=JJVD1pZDcMoySTkwTj21FA__; G8T80W34F34D99I83=!3kQct/YjluGs6dYAf8YbK2rgIYnkWMezX739cI4pJfnWCFyBdAmNyd3LtcdBaW3iFcu+6K0hVR7ZY9pdi6tJwXCb3K4qJPUW3HbS1R4=",
				"Referer":"https://etendersni.gov.uk/epps/prepareAdvancedSearch.do?type=cft"
				}
				
	post_url="https://etendersni.gov.uk/epps/viewCFTSAction.do"			
	get_Req_Content = s.post(post_url, headers=headers1,data=post_cont)
	print ('status_code:', get_Req_Content.status_code)
	print ("fet:",get_Req_Content)

	nxtcontent = get_Req_Content.content.decode('utf-8', errors = 'ignore')
	# with open ('filename.html','w') as f:
		# f.write(nxtcontent)
	b_link = re.findall(b_link_re,nxtcontent)
	print (b_link)
	if len(b_link)==0:
		home_url="https://etendersni.gov.uk/epps/prepareAdvancedSearch.do?type=cft"
		s=requests.session()
		r=s.get(home_url)
		html=r.content.decode('utf-8', errors = 'ignore')
		html = str(html).replace('\r','')
		html = str(html).replace('\n','')
		html = str(html).replace('\t','')
		print (html)
		capitcha=re.findall('<img id="CAPTCHA"\s*src="([^>]*?)"',html)
		url1="https://etendersni.gov.uk"+str(capitcha[0])
		r1=s.get(url1)
		html1=r1.content.decode('utf-8', errors = 'ignore')
	# print (html1)
		with open ('filename.jpg','wb') as f:
			f.write(r1.content)


	# urllib.request.urlretrieve(url1, "filename.jpg")
		temp=Capcha_Convert("filename.jpg")
	# post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=,&contractType=,&cpcCategory=,0&procedure=,&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=,&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
	# print (post_cont)
		post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=&status=&contractType=&contractType=&cpcCategory=&cpcCategory=&procedure=&procedure=&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=&description=&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
		# post_cont="resourceId=&title=&uniqueId=&contractAuthority=&status=&status=&contractType=&contractType=&cpcCategory=&cpcCategory=&procedure=&procedure=&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=&description=&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha="+str(temp)
	# resourceId=&title=&uniqueId=&contractAuthority=&status=,&contractType=,&cpcCategory=,&procedure=,&publicationFromDate=&publicationUntilDate=&submissionFromDate=&submissionUntilDate=&description=,&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=&captcha=2n2ufr
		print (post_cont)
		headers1 = {"Host":"etendersni.gov.uk",
				# "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
				"Content-Type":"application/x-www-form-urlencoded",
				"Connection": "keep-alive",
				# "Accept-Language": "en-US,en;q=0.5",
				# "Accept-Encoding": "gzip, deflate, br",
				# "Cookie": "JSESSIONID=JJVD1pZDcMoySTkwTj21FA__; G8T80W34F34D99I83=!3kQct/YjluGs6dYAf8YbK2rgIYnkWMezX739cI4pJfnWCFyBdAmNyd3LtcdBaW3iFcu+6K0hVR7ZY9pdi6tJwXCb3K4qJPUW3HbS1R4=",
				"Referer":"https://etendersni.gov.uk/epps/prepareAdvancedSearch.do?type=cft"
				}
				
		# post_url="https://etendersni.gov.uk/epps/viewCFTSAction.do"
		post_url="https://etendersni.gov.uk/epps/quickSearchAction.do"
			
		get_Req_Content = s.post(post_url, headers=headers1,data=post_cont)
		# get_Req_Content = requests.get("https://etendersni.gov.uk/epps/quickSearchAction.do?searchSelect=4&current=true&d-3680175-p=2&captcha="+str(capitcha[0]))
		print ('status_code:', get_Req_Content.status_code)
		print ("fet:",get_Req_Content)

		nxtcontent = get_Req_Content.content.decode('utf-8', errors = 'ignore')
	# with open ('filename.html','w') as f:
		# f.write(nxtcontent)
		b_link = re.findall(b_link_re,nxtcontent)
		# b_link = ["/epps/cft/prepareViewCfTWS.do?resourceId=3551504","/epps/cft/prepareViewCfTWS.do?resourceId=3554443","/epps/cft/prepareViewCfTWS.do?resourceId=3560474"]
		if len(b_link)>1:
			# b_link = ["/epps/cft/prepareViewCfTWS.do?resourceId=3551504","/epps/cft/prepareViewCfTWS.do?resourceId=3554443","/epps/cft/prepareViewCfTWS.do?resourceId=3560474"]
			print (b_link)
			for link1 in b_link:
				b_link1="https://etendersni.gov.uk"+str(link1)
				print (b_link1)
				Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, b_link1)
				if Duplicate_Check == "N":
					tender_con=s.get(b_link1)
					link_content = tender_con.content.decode('utf-8', errors = 'ignore')
					with open ('tender_content.html','w') as f:
						f.write(link_content)
					title=regxx(title_re,link_content)
					if title:
						title1=reg_clean(title)
					deadlinedate = regxx(deadline_date_re,link_content)
					# if deadlinedate:
						# deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
						# dt = dateparser.parse(deadlinedate)
						# deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
					# else:
						# deadlinedate=None
					deadlinetime = regxx(deadline_time_re,link_content)
					deadline_desc = regxx(dead_line_W_Boamp,link_content)
					if deadline_desc:
						deadline_desc = deadline_desc
					else:
						deadline_desc = 'Time-limit for receipt of tenders or requests to participate:'
					awardingAuthority1=regxx(awardingAuthority_re,link_content)	
					print (awardingAuthority1)
					if awardingAuthority1:
						
						awardingAuthority1 = reg_clean(awardingAuthority1)
					contract_type1=regxx(contract_type,link_content)	
					if contract_type1:
						contract_type1 = reg_clean(contract_type1)
					else:
						contract_type1 = ''
					CPV_Code=regxx(CPV_Code_re,link_content)	
					if CPV_Code:
						CPV_Code = reg_clean(CPV_Code)
						
					desc=regxx(desc_re,link_content)	
					if desc:
						desc = reg_clean(desc)
					else:
						desc = ''					
					nuts_code=regxx(nuts_code_re,link_content)	
					if nuts_code:
						nuts_code = reg_clean(nuts_code)
					else:
						nuts_code = ''	
					award_procedure=regxx(award_procedure_re,link_content)	
					if award_procedure:
						award_procedure = reg_clean(award_procedure)
					else:
						award_procedure = ''
					# tender_block=regxx(tender_block_re,link_content)	
					# if tender_block:
						# tender_block = reg_clean(tender_block)
					# else:
					tender_block = ''
					value1=regxx(value_re,link_content)		
					if value1:
						value2 = reg_clean(value1)
					else:
						value2 = ''	
					mail1=regxx(mail_block_re,link_content)		
					if mail1:
						mail2 = reg_clean(mail1)
					else:
						mail2 = ''							
					award_auth_link=regxx(awarding_org1_re,link_content)		
					if award_auth_link:
						award_auth_link = "https://etendersni.gov.uk"+str(award_auth_link[0])
						award_content=s.get(award_auth_link)
						award_org_content = award_content.content.decode('utf-8', errors = 'ignore')
						award_org_name=regxx(awarding_org_re,link_content)
						if award_org_name:
							award_org_name=reg_clean(award_org_name[0])
					else:
						award_auth_link = ''
						award_org_name = ''	
					summary_cont = 'For further information on the above contract please visit Web: ' +' '+ str(b_link1)
					curr_date = datetime.now(pytz.timezone('Europe/London'))
					created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
					awardingAuthority2='Buyer Organisation : '+str(awardingAuthority1)+', '+str(award_org_name)
					deadlinedate = regxx(deadline_date_re,link_content)
					# sector='Republic/NI Daily'
					# tender_type='Normal'
					# industry_sector='Other'
					proofed='No'
					if deadlinedate:
						deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
						dt = dateparser.parse(deadlinedate)
						deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
						insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,nuts_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,Intend,BIP_Non_UK,Tender_type,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link1,tender_block, orgin_Boamp, sector, country, web_source,title1, title1, awardingAuthority2, award_procedure, contract_type1, CPV_Code,nuts_code,desc, str(), str(), value2, deadline_desc, deadlinedate, deadlinetime,summary_cont, b_link1, 'N','N','Normal',created_date)
						print (insertQuery)
					else:	
						insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,nuts_code,description,location,authority_reference,value,deadline_description,deadline_time,sent_to,other_information,Intend,BIP_Non_UK,Tender_type,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link1,tender_block, orgin_Boamp, sector, country, web_source,title1, title1, awardingAuthority2, award_procedure, contract_type1, CPV_Code,nuts_code,desc, str(), str(), value2, deadline_desc, deadlinetime,summary_cont, b_link1, 'N','N','Normal',created_date)
						print (insertQuery)
									
					Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
						# row_s2 = row_s2 + 1
						# book.save('my_book_v9.xls')
				else:
					print ("Duplicate data found ")
	if len(b_link)>1:
		# b_link = ["/epps/cft/prepareViewCfTWS.do?resourceId=3551504","/epps/cft/prepareViewCfTWS.do?resourceId=3554443","/epps/cft/prepareViewCfTWS.do?resourceId=3560474"]
		print (b_link)
		for link1 in b_link:
			b_link1="https://etendersni.gov.uk"+str(link1)
			print (b_link1)
			Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, b_link1)
			if Duplicate_Check == "N":
				tender_con=requests.get(b_link1)
				link_content = tender_con.content.decode('utf-8', errors = 'ignore')
				# with open ('tender_content.html','w') as f:
					# f.write(link_content)
				title=regxx(title_re,link_content)
				if title:
					title1=reg_clean(title)
				# deadlinedate = regxx(deadline_date_re,link_content)
				# if deadlinedate:
					# deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
					# dt = dateparser.parse(deadlinedate)
					# deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
				# else:
					# deadlinedate=None
				deadlinetime = regxx(deadline_time_re,link_content)
				deadline_desc = regxx(dead_line_W_Boamp,link_content)
				if deadline_desc:
					deadline_desc = deadline_desc
				else:
					deadline_desc = 'Time-limit for receipt of tenders or requests to participate:'
				awardingAuthority1=regxx(awardingAuthority_re,link_content)	
				print (awardingAuthority1)
				if awardingAuthority1:
					
					awardingAuthority1 = reg_clean(awardingAuthority1)
				contract_type1=regxx(contract_type,link_content)	
				if contract_type1:
					contract_type1 = reg_clean(contract_type1)
				else:
					contract_type1 = ''
				CPV_Code=regxx(CPV_Code_re,link_content)	
				if CPV_Code:
					CPV_Code = reg_clean(CPV_Code)
					
				desc=regxx(desc_re,link_content)	
				if desc:
					desc = reg_clean(desc)
				else:
					desc = ''					
				nuts_code=regxx(nuts_code_re,link_content)	
				if nuts_code:
					nuts_code = reg_clean(nuts_code)
				else:
					nuts_code = ''	
				award_procedure=regxx(award_procedure_re,link_content)	
				if award_procedure:
					award_procedure = reg_clean(award_procedure)
				else:
					award_procedure = ''
				# tender_block=regxx(tender_block_re,link_content)	
				# if tender_block:
					# tender_block = reg_clean(tender_block)
				# else:
				tender_block = ''
				value1=regxx(value_re,link_content)		
				if value1:
					value2 = reg_clean(value1)
				else:
					value2 = ''	
				award_auth_link=regxx(awarding_org1_re,link_content)		
				if award_auth_link:
					award_auth_link = "https://etendersni.gov.uk"+str(award_auth_link[0])
					award_content=s.get(award_auth_link)
					award_org_content = award_content.content.decode('utf-8', errors = 'ignore')
					award_org_name=regxx(awarding_org_re,link_content)
					if award_org_name:
						award_org_name=reg_clean(award_org_name[0])
				else:
					award_auth_link = ''
					award_org_name = ''	
				summary_cont = 'For further information on the above contract please visit Web: ' +' '+ str(b_link1)
				curr_date = datetime.now(pytz.timezone('Europe/London'))
				created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
				awardingAuthority2='Buyer Organisation : '+str(awardingAuthority1)+', '+str(award_org_name)
				deadlinedate = regxx(deadline_date_re,link_content)
				if deadlinedate:
					deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
					dt = dateparser.parse(deadlinedate)
					deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
					insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,nuts_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,Intend,BIP_Non_UK,Tender_type,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link1,tender_block, orgin_Boamp, sector, country, web_source,title1, title1, awardingAuthority2, award_procedure, contract_type1, CPV_Code,nuts_code,desc, str(), str(), value2, deadline_desc, deadlinedate, deadlinetime,summary_cont, b_link1, 'N','N','Normal',created_date)
					print (insertQuery)
				else:	
					insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,nuts_code,description,location,authority_reference,value,deadline_description,deadline_time,sent_to,other_information,Intend,BIP_Non_UK,Tender_type,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link1,tender_block, orgin_Boamp, sector, country, web_source,title1, title1, awardingAuthority2, award_procedure, contract_type1, CPV_Code,nuts_code,desc, str(), str(), value2, deadline_desc, deadlinetime,summary_cont, b_link1, 'N','N','Normal',created_date)
					print (insertQuery)
				# insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,nuts_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,Intend,BIP_Non_UK,Tender_type,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link1,tender_block, orgin_Boamp, sector, country, web_source,title1, title1, awardingAuthority2, award_procedure, contract_type1, CPV_Code,nuts_code,desc, str(), str(), value2, deadline_desc, deadlinedate, deadlinetime,summary_cont, b_link1, 'N','N','Normal',created_date)
				# print (insertQuery)
						
				Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
					# row_s2 = row_s2 + 1
					# book.save('my_book_v9.xls')
			else:
				print ("Duplicate data found ")
		
    # next_page=re.findall('<p class="pagination-index">Page 1 sur\s*([^>]*?)<\/p>',main_content)			
				
	# return (temp)
# if __name__== "__main__":

	# print ("UR in python code")
temp()
	# return (cntent)