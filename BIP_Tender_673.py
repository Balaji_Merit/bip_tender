# -*- coding: utf-8 -*-
import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import re
import imp
import pymysql
import time
from dateutil.parser import parse
# import cookielib
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
h = HTMLParser()

Database_Connector = imp.load_source('Database_Connector', 'C:/BIP/Temp/Database_Connector1.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
block_re = 'Award\s*Notice\s*<\/span>\s*<\/span><a\s*href="([^>]*?)">'
tender_link_re = '<a[^>]*?ref="([^>]*?)"\s*[^>]*?>'
title_re = '<h1[^>]*?>([^>]*?)<\/h1>'
awarding_authority_re = 'Customer info</h3><div class="field-content views-field-pre"><pre>([\w\W]*?)<\/pre><\/div><\/div>'
address1_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p>'
city_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p><p>[\w\W]*?<\/p>'
zipcode_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p>'
contact_re = '<h6><strong>Contact</strong></h6><p>([\w\W]*?)<\/p>'
authority_email = 'Email<\/strong><\/h6><p><a href="mailto:([^>]*?)"\s*[^>]*?>'
telephone_re = 'Telephone<\/strong><\/h6><p>([^>]*?)<\/p>'
fax_re = 'Fax<\/strong><\/h6><p>([^>]*?)<\/p>'
authorityWebAddress_re = 'Internet address(es)[\w\W]*?Main address[\w\W]*?<p><a[^>]*?>([^>]*?)<\/a><\/p>'
contract_type_re = 'Contract Award Procedure \(Procurement Strategy\)\s*:\s*([^>]*?)\s*GSIN '
award_procedure_re = '<p>([^>]*?)\s*procedure<\/p>'
cpv_re = 'Main CPV code[\w\W]*?<ul><li>([\w\W]*?)<\/li><\/ul>'
desc_re = 'Description<\/h3>[\w\W]*?<p>([\w\W]*?)<\/h3><div class="field-content"><p>'
ref1 = 'Contract number<\/strong>[\w\W]*?<span class="field-content">([^>]*?)\s*<\/span><\/dd>'
ref2 = 'Reference number<\/strong>[\w\W]*?<span class="field-content">([^>]*?)\s*<\/span><\/dd>'
ref3 = 'Solicitation number<\/strong>[\w\W]*?<span class="field-content">([^>]*?)\s*<\/span><\/dd>'
duration_months_re = 'Duration in months<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
start_re = 'Start<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
end_re = 'End<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
value_re = 'Value<\/strong>[\w\W]*?<\/abbr>\s*([^>]*?)\s*<\/span>'
amount_re = 'Value<\/strong>[\w\W]*?<\/abbr>\s*([^>]*?)\s*<\/span>'
nuts_code_re = 'NUTS codes</strong><\/h6><p>([\w\W]*?)<\/p>'
deadline_date_re = 'Time limit for receipt[\w\W]*?<\/span><\/strong>[\w\W]*?Date<\/strong><\/h6><p>([^>]*?)<\/p>'
deadline_time_re = 'Time limit for receipt[\w\W]*?<\/span><\/strong>[\w\W]*?Local time<\/strong><\/h6><p>([^>]*?)<\/p>'
dead_line_desc_re = 'Time limit for receipt of projects or requests to participate'
other_information_re = '(Communication<\/span><\/strong><\/h[\w\W]*?)<h2 class="share-this-notice">'
orgin = 'Web'
noticeSector = 'Worldwide/Non Defence'
Sector = 'Worldwide/Non Defence'
tender_type = "Normal"
Industry_Sector = "Other"
proofed = "No"
sourcecw = "Contrax Weekly"
origin = "Web"
noticeSector = "Worldwide/Non Defence"
cy = "CAN"
country_code = "Canada"
sourceURL = "Public Works and Government Services Canada"
pubFlagCW = "No"
authorityDept = ""
authorityAddress2 = ""
authorityAddress3 = ""
Industry_Sector = "Worldwide/Non Defence"
authorityCountry = "CAN"
tender_block_re = '<h1[\w\W]*?>([\w\W]*?)<h3 class="views-label views-label-nothing-2">'
supplier = 'Supplier Information<\/h3><div class="field-content views-field-pre"><pre>([\w\W]*?)<\/pre><\/div>'
d_award = 'Contract award date<\/strong>[\w\W]*?<span class="field-content">([^>]*?)<\/span><\/dd>'
tendersreceived = '<p>Number of tenders received:\s*([^>]*?)\s*</p>'
Tender_ID = 673
Retrive_duplicate = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor, Tender_ID)
print ("re----->",Retrive_duplicate)
last_page_re = 'Next<span class=\'standard-paginate-detail\'>2 of (\d+)\s*<'

def reg_clean(cont):
    cont = str(cont).replace('\r', '')
    cont = str(cont).replace('\n', '')
    cont = str(cont).replace('\t', '')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'", "''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'", "''")
    cont = cont.replace("î", "'")
    cont = cont.replace("l'", "l''")
    cont = cont.replace("L'", "L''")
    cont = cont.replace("d'", "d''")
    cont = cont.replace("D'", "D''")
    cont = cont.replace("'s", "''s")
    cont = cont.replace("du'", "du''")
    cont = cont.replace("N'", "N''")
    cont = cont.replace("'acheteur", "''acheteur")
    cont = cont.replace("'id", "''id")
    cont = cont.replace("'b'", "'b''")
    cont = cont.replace("'Esp", "''Esp")
    cont = cont.replace("'RD", "''RD")
    # cont = str(cont).replace("\’","''")
    cont = cont.replace("’", "''", re.I)
    # cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''", "''")
    cont = cont.replace("'''''", "''")
    cont = cont.replace("''''", "''")
    cont = cont.replace("'''", "''")
    cont = cont.replace("'''", "''")
    cont = cont.replace("\xc3\xa9", "é")
    cont = cont.replace("&#39;", "''")
    cont = cont.replace("&quot;", '"')
    cont = cont.replace("î", "'")
    cont = cont.replace("'", "''")
    cont = cont.replace("''''", "''")
    return cont


def req_content(url):
    try:
        req_url = requests.get(url, proxies={
            "http": "http://172.27.137.192:3128",
            "https": "http://172.27.137.192:3128"
        })
        url_content = req_url.content.decode('utf_8', errors='ignore')
    except Exception as e:
        url_content = ''
    return url_content


def timeFormat(Input):
    print ("Time Before Format :: ", Input)
    time2 = re.findall(
        '(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)', str(Input))
    # print time1[0][0]
    if time2:
        if int(time2[0][0]) < 12:
            print (int(time2[0][0]))
            if time2[0][1]:
                Formattedtime = str(
                    int(time2[0][0])+12) + ":"+str(int(time2[0][1]))+":00"
            if int(time2[0][0]) > 12:
                Formattedtime = str(
                    int(time2[0][0]))+":"+str(int(time2[0][1]))+"00"
  
        else:
            if int(time2[0][0]) == 12:
                Formattedtime = str(int(time2[0][0])) + ":00:00"
            if int(time2[0][0]) > 12:
                Formattedtime = str(
                    int(time2[0][0])) + ":"+str(int(time2[0][1]))+":00"
   
   

    time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)', str(Input))

    if time2:
            if int(time2[0][0]) <= 12:
                Formattedtime = str(int(time2[0][0])) + ":00:00"

                    # Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
    # dt=dparser.parse(Input,fuzzy=True)
    # Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
    print (Formattedtime)
    return (Formattedtime)


def DateFormat(Input):
    print ("DATE Before Format :: ", Input)
    try:
        dt = parse(Input)
        # dt = dparser.parse(Input, fuzzy=True)
        FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
        print ("FormattedDate:: ",FormattedDate)
    except Exception as e:
        deadline_date1 = dateparser.parse(time.asctime())
        deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
        FormattedDate = deadline_date1
    return (FormattedDate)


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return val


def regxx(regx, content):
    cont_data = re.findall(regx, content)
    if cont_data:
        cont_data = cont_data[0]
    else:
        cont_data = ''
    return h.unescape(cont_data)


def start(main_url):
    proxies = {
        "http": "https://172.27.137.192:3128",
        "https": "https://172.27.137.192:3128"
    }
    s = requests.session()
    last_week = date.today() - timedelta(1)
    last_week_temp = date.today()
    print ("Today******", last_week)
    last_week_date1 = (last_week.strftime('%d'))
    last_week_date2 = (last_week.strftime('%m'))
    last_week_date3 = (last_week.strftime('%Y'))
    last_week_temp1 = (last_week_temp.strftime('%d'))
    last_week_temp2 = (last_week_temp.strftime('%m'))
    last_week_temp3 = (last_week_temp.strftime('%Y'))
    s = requests.session()
    for page in range(0,16):
        main_content1 = requests.get("https://buyandsell.gc.ca/procurement-data/search/site?page="+str(page)+"&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_award&f%5B1%5D=dds_facet_date_published%3Adds_facet_date_published_7day")	        
    
        # s = requests.session()
        main_content = main_content1.content.decode('utf_8', errors='ignore')
    # with open("temp96.html", 'w') as f:

        # f.write(main_content)

        blocks = re.findall(block_re, main_content)
        for tender_url in blocks:
        # tender_url = re.findall(tender_link_re, block)[0]
        # tender_url = tender_url.replace("?origin=SearchResults&p=1", "")
		
            print (tender_url)
            Retrive_duplicate = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor, Tender_ID)
            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, tender_url)
            if Duplicate_Check == "N":
                tender_content1 = requests.get(tender_url)
                tender_content = tender_content1.content.decode('utf_8', errors='ignore')
                title = re.findall(title_re, tender_content)[0]
                title = title.replace("'","''")			
				# if "Framework" not in title:
					# title = str(title) + " - Framework Agreement"\
                authority_split=[]	
                address1=''
                city=''
                postal_code=''
                names=''
                Salutation = ''
                first_name = ''
                last_name = ''			
                authority_name1 = re.findall(awarding_authority_re, tender_content)
                print (authority_name1)
                if authority_name1:
                    authority_name1 = authority_name1[0].replace("'","''")
                    authority_split = authority_name1.split("\n")
                    print (authority_split)
                    if len(authority_split)==7:
                        authority_name = authority_split[0]
                        address1 = authority_split[1]
                        address2 = authority_split[2]
                        city = authority_split[3]
                        county = authority_split[4]
                        postal_code = authority_split[5]
                        country = authority_split[6]
                    if len(authority_split)==6:
                        authority_name = authority_split[0]
                        address1 = authority_split[1]
                        # address2 = authority_split[2]
                        city = authority_split[2]
                        county = authority_split[3]
                        postal_code = authority_split[4]
                        country = authority_split[5]
                    if len(authority_split)==5:
                        authority_name = authority_split[0]
                        # address1 = authority_split[1]
                        # address2 = authority_split[2]
                        city = authority_split[1]
                        county = authority_split[2]
                        postal_code = authority_split[3]
                        country = authority_split[4]
                    if len(authority_split)==1:
                        authority_name = authority_split[0]
                        # address1 = authority_split[1]
                        # address2 = authority_split[2]
                        # city = authority_split[1]
                        # county = authority_split[2]
                        # postal_code = authority_split[3]
                        # country = authority_split[4]						
                else:
                    authority_name=''
                contract_type = re.findall(contract_type_re, tender_content)
                if contract_type:
                    contract_type = "Contract type: "+str(contract_type[0])
                else:
                    contract_type = ''

                description = re.findall(desc_re, tender_content)
                if description:
                    description = reg_clean(description[0])
                else:
                    description = ''
                publication_reference = re.findall(ref1, tender_content)
                if publication_reference:
                    publication_reference = "Contract number : "+publication_reference[0]
                else:
                    publication_reference = ''
                reference_no = re.findall(ref2, tender_content)
                if reference_no:
                    reference_no = "Reference Number: "+reference_no[0]
                else:
                    reference_no = ''
                solicit_reference_no = re.findall(ref3, tender_content)
                if reference_no:
                    solicit_reference_no = "Solicitation Number: "+solicit_reference_no[0]
                else:
                    solicit_reference_no = ''	
                authority_reference = str(publication_reference) + str(reference_no) + str(solicit_reference_no)
                authority_reference = authority_reference.strip()
                authority_reference = authority_reference.replace("\s*$", '')
				
                value = re.findall(value_re, tender_content)
                if value:
                    value = value[0]
                else:
                    value = ''
                amount = re.findall(amount_re, tender_content)
                if amount:
                    amount = amount[0]
                    currency = 'CAD'
                else:
                    amount = ''
                    currency = ''
				
                tender_block = re.findall(tender_block_re, tender_content)[0]
                if tender_block:
                    tender_block = reg_clean(tender_block)
                else:
                    tender_block = ''					
                other_information = "For further information regarding the above contract notice please visit: "+tender_url
                curr_date = dateparser.parse(time.asctime())
                created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                contractor_details = re.findall(supplier, tender_content)
                if contractor_details:
                    contractor_details = reg_clean(contractor_details[0])
                else:
                    contractor_details=''
                nooftenders = re.findall(tendersreceived, tender_content)
                if nooftenders:
                    nooftenders=nooftenders[0]
                else:
                    nooftenders=0
                e_mail = ''	
                telephone = ''
                fax = ''
                authorityWebAddress = ''
                contract_type = ''
                procedure_type1 = ''
                cpv_code = ''
                duration_months = ''
                location=''
                d_RApost=''
                nuts_code=''

                D_Award = re.findall(d_award, tender_content)
                if D_Award:
                    D_Award=DateFormat(D_Award[0])


                    insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,cpv_code,description,proofed,location,authority_reference,no_of_months,value,currency,amount,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type,supplier,D_award,tendersReceived,Awarded_Status) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'Public Works and Government Services Canada',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,cpv_code,description,proofed,location,authority_reference,duration_months,value,currency,amount,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Awarded',contractor_details,D_Award,nooftenders,'Y')
                else:
                    insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,cpv_code,description,proofed,location,authority_reference,no_of_months,value,currency,amount,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type,supplier,tendersReceived,Awarded_Status) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'Public Works and Government Services Canada',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,cpv_code,description,proofed,location,authority_reference,duration_months,value,currency,amount,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Awarded',contractor_details,nooftenders,'Y')
					
						
                print (insertQuery)
                Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
    
		
if __name__ == "__main__":
    last_week = date.today()
    # last_week = date.today()
    last_week_date1 = (last_week.strftime('%d/%m/%Y'))
    print (last_week_date1)
    # curr_date1 = date.today() - timedelta(1)
    curr_date1 = date.today()
    created_date1 = (curr_date1.strftime('%d.%m.%Y'))
    print (created_date1)
    # raw_input("check")
    main_url = 'https://www.boamp.fr/avis/liste'
    n = 1
    start(main_url)
