#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use POSIX 'strftime';
#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# my $Tender_Url="http://www.thetraininggateway.com/view-enquiries";
# my $TenderName="Digital Marketplace";
# my $Tender_ID="70";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=2;
my $sourcecw="Contrax Weekly";
my $origin="Web";
my $noticeSector="UK Other";
my $cy="GB";
my $country_code="United Kingdom";
my $sourceURL="The Training Gateway";
my $pubFlagCW="No";
my $authorityDept="";
my $authorityAddress2="";
my $authorityAddress3="";
my $Industry_Sector="Other";
my $authorityCountry="GB";
#### To get the tender link ####
top:
# while ($Tender_content1=~m/url=([^>]*?)\&text/igs)
while ($Tender_content1=~m/<a href="([^>]*?)">Find out more/igs)
{
	my $tender_link = "https://www.thetraininggateway.com/training-member-area/view-opportunities/".($1);
	my ($Tender_content)=&Getcontent($tender_link);
	$Tender_content = decode_utf8($Tender_content); 
	
		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag1,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		my ($authorityName,$tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$defence,$Location,$Deadline_Description,$Deadline_Date,$authorityAddress2,$authorityAddress21,$authorityAddress3,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$authorityPostcode,$authorityFax,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$awardProcedure,$nc,$trackerRef,$aa,$nuts,$origPublicationDate,$proofed,$exported,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$currency,$amount_min,$amount_max,$pr,$value1);
	if ($Tender_content=~m/<h1[^>]*?>([^>]*?)<\/h1>/is)
	{
		$tender_title  = &clean($1);
	}
	# if ($Tender_content=~m/<span>Organisation the work is for<\/span>\s*<\/td>\s*<td  class="summary-item-field">\s*<span>([^>]*?)<\/span>/is)
	# {
		# $Awarding_Authority  = &clean($1);
	# }
	# if ($Tender_content=~m/Region:<\/div>\s*<div class="Column Column2">([^>]*?)<\/div>/is)
    if ($Tender_content=~m/Region<\/strong><br\s*\/>([^>]*?)<\/p><\/div>/is)
	{
		$Location  = &clean($1);
		$Location  = &BIP_Tender_DB::clean($Location);
	}
		if ($Tender_content=~m/Ref\s*\#<\/strong><br\s*\/>([^>]*?)<\/p><\/div>/is)
	{
		$Authority_Reference  = &clean($1);
	}
		
	# if ($Tender_content=~m/Response deadline:<\/div>\s*<div class="Column Column2">([^>]*?)<\/div>/is)
    if ($Tender_content=~m/Deadline<\/strong><br\s*\/>([^>]*?)<\/p><\/div>/is)
	{
		# $Deadline_Description = &clean($1);
		$Deadline_Date = &clean($1);
		# $Deadline_time = $3;
	}
		$Deadline_Description = "Response deadline:";
		# print "After the Formatting : $date_formatted\n";
		# print "Step 7\n";
		# return ($date_formatted);
	
	# if ($Tender_content=~m/(Primary business sector:[\w\W]*?)<div class="clear"><\/div>[\w\W]*?<span id=[\w\W]*?>([^>]*?)<\/span>/is)
    if ($Tender_content=~m/Sector<\/strong><br\s*\/>([^>]*?)<\/p><\/div>/is)
	{
		$other_information  = $1;
			$other_information=&clean($other_information);
	$other_information= &BIP_Tender_DB::clean($other_information);
	}

		# if($Tender_content=~m/<div class="Summary">([\w\W]*?)<\/div>/is)
        if($Tender_content=~m/<\/div>\s*<\/div>\s*<div class="right">([\w\W]*?)<p class="colouredbox">/is)
	{
	$Description=$1;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	# $Description=~s/\-//igs;
	}
	#Duplicate check
    #### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	
	my ($tenderblock);
	if ($Tender_content=~m/<h1[^>]*?>([\w\W]*?)<p class="colouredbox">/is)
	{
		$tenderblock = $1;
	}
		# $tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# $Tender_content
	$tenderblock=&clean($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	print $tender_link;
	print $tender_title;
	print $Description;
	print $Location;
	
		# open (RE,">>Digital.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
	# &BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$other_information);
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$other_information,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag1,$other_information,$BIP_Non_UKFlag);
my $Sector='UK Other';
		my $tender_type="Normal";
		my $Industry_Sector="Other";
		my $proofed="No";
		my $cy="GB";
		my ($Address,$Email,$Telephone);
		my $d_RApost="For further information regarding the above contract notice please visit: ".$tender_link;
		&Insert_Tender_Data_new($dbh,$Tender_ID,$tender_link,$sourcecw,$Awarding_Authority,$tenderblock,$origin,$noticeSector,$authorityDept,$country_code,$cy,$sourceURL,$Industry_Sector,$tender_title,$authorityName,$Address,$authorityAddress2,$authorityAddress3,$defence,$pubFlagCW,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$Telephone,$authorityFax,$Email,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$nc,$trackerRef,$awardProcedure,$Contract_Type,$Deadline_Date1,$pr,$CPV_Code,$aa,$origPublicationDate,$Description,$proofed,$exported,$Location,$Authority_Reference,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$value1,$currency,$amount_min,$amount_max,$Deadline_Description,$Deadline_time,$d_RApost,$nuts,$authorityPostcode);
}



  if($count<=15)
     {
        my $nexturl="https://www.thetraininggateway.com/training-member-area/view-opportunities/?page=".$count;
        $nexturl=~s/\&amp\;/&/igs;
		my ($Tender_content_next)=&Getcontent($nexturl);
		$Tender_content_next = decode_utf8($Tender_content_next);  
		$Tender_content1=$Tender_content_next;
        $count=$count+1;
        goto top;
     }		

# if($count<=15)
# {
# my ($ViewState1,$Eventvalidation1,$EVENTTARGET1);
# my $y='0'.$i if($i<10);
# if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
# {
	# $ViewState1=uri_escape($1);
# }
# if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
# {
	# $Eventvalidation1=uri_escape($1);
# }
# if($Tender_content=~m/javascript:__doPostBack\(\'([^>]*?)\'\,\'\'\)\">\s*[\d]/is)
# {
	# $EVENTTARGET1="ucSummaryList%24ucPagingBottom%24btnNext";
# }
# my $Search_po_Content1=
# '__EVENTTARGET=ucSummaryList%24ucPagingBottom%24btnNext&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState1.'&Header%24txtSearch=&ucSummaryList%24hidType=Tenders&ucSummaryList%24hidPageIndex='.$count.'&ucSummaryList%24tbKeyword=&ucSummaryList%24ddlSectors=0&ucSummaryList%24ddlAdded=0&ucSummaryList%24ddlDeadline=0&ucSummaryList%24ucPagingTop%24hidCurrentPage='.$count.'2&ucSummaryList%24ucPagingBottom%24hidCurrentPage='.$count.'&__EVENTVALIDATION='.$Eventvalidation1;
# my $Referer1='http://www.thetraininggateway.com/view-enquiries/';
# my $post_url1='http://www.thetraininggateway.com/view-enquiries/';


# my ($Tender_content2)=&Postcontent($post_url1, $Referer1, $Search_po_Content1);
# $Tender_content2 = decode_utf8($Tender_content2); 
# $Tender_content1 = $Tender_content2; 
# $count=$count+1;
# goto top;
# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.thetraininggateway.com");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	
	# my $Host=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.thetraininggateway.com");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "http://www.thetraininggateway.com/view-enquiries/");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\Γ\Ç\Ä//igs;
	
	decode_entities($Clean);
	return $Clean;
}


sub Insert_Tender_Data_new()
{
	my $dbh=shift;
my $Tender_ID=shift;
my $tender_link=shift;
my $sourcecw=shift;
my $Buyer=shift;
my $tenderblock=shift;
my $origin=shift;
my $noticeSector=shift;
my $authorityDept=shift;
my $country_code=shift;
my $cy=shift;
my $sourceURL=shift;
my $Industry_Sector=shift;
my $tender_title=shift;
my $authorityName=shift;
my $Address=shift;
my $authorityAddress2=shift;
my $authorityAddress3=shift;
my $defence=shift;
my $pubFlagCW=shift;
my $authorityTown=shift;
my $authorityCounty=shift;
my $online_only=shift;
my $frameworkAgreement=shift;
my $Telephone=shift;
my $authorityFax=shift;
my $Email=shift;
my $temp_description_export=shift;
my $authorityWebAddress=shift;
my $authorityProfileURL=shift;
my $authorityContactSalutation=shift;
my $authorityContactFirstName=shift;
my $authorityContactLastName=shift;
my $authorityPosition=shift;
my $temp_site_export=shift;
my $nc=shift;
my $trackerRef=shift;
my $awardProcedure=shift;
my $Contract_Type=shift;
my $Deadline_Date1=shift;
my $pr=shift;
my $CPV_Code=shift;
my $aa=shift;
my $origPublicationDate=shift;
my $Description=shift;
my $proofed=shift;
my $exported=shift;
my $Location=shift;
my $Authority_Reference=shift;
my $autoModifyDate=shift;
my $autoModifyTime=shift;
my $no_of_months=shift;
my $contract_start_date=shift;
my $contract_end_date=shift;
my $value1=shift;
my $currency=shift;
my $amount_min=shift;
my $amount_max=shift;
my $Deadline_Description=shift;
my $Deadline_time=shift;
my $d_RApost=shift;
my $nuts=shift;
my $authorityPostcode=shift;
$contract_end_date=&BIP_Tender_DB::Date_Formatting($contract_end_date,$Tender_ID);
$contract_start_date=&BIP_Tender_DB::Date_Formatting($contract_start_date,$Tender_ID);
$origPublicationDate=&BIP_Tender_DB::Date_Formatting($origPublicationDate,$Tender_ID);

	# my $bip_non_uk1				= shift;
	# my $tender_type1			=shift;
	my $tender_type1;
	# $InTendFlag1 = 'N' if ($InTendFlag1 eq '');
	# print "\nInTendFlag	:: $InTendFlag1\n";
	my $bip_non_uk1 = 'N';
$sourcecw=~s/\'/\'\'/igs;
$Buyer=~s/\'/\'\'/igs;
$tenderblock=~s/\'/\'\'/igs;
$origin=~s/\'/\'\'/igs;
$noticeSector=~s/\'/\'\'/igs;
$authorityDept=~s/\'/\'\'/igs;
$country_code=~s/\'/\'\'/igs;
$cy=~s/\'/\'\'/igs;
$sourceURL=~s/\'/\'\'/igs;
$Industry_Sector=~s/\'/\'\'/igs;
$tender_title=~s/\'/\'\'/igs;
$authorityName=~s/\'/\'\'/igs;
$Address=~s/\'/\'\'/igs;
$authorityAddress2=~s/\'/\'\'/igs;
$authorityAddress3=~s/\'/\'\'/igs;
$defence=~s/\'/\'\'/igs;
$pubFlagCW=~s/\'/\'\'/igs;
$authorityTown=~s/\'/\'\'/igs;
$authorityCounty=~s/\'/\'\'/igs;
$online_only=~s/\'/\'\'/igs;
$frameworkAgreement=~s/\'/\'\'/igs;
$Telephone=~s/\'/\'\'/igs;
$authorityFax=~s/\'/\'\'/igs;
$Email=~s/\'/\'\'/igs;
$temp_description_export=~s/\'/\'\'/igs;
$authorityWebAddress=~s/\'/\'\'/igs;
$authorityProfileURL=~s/\'/\'\'/igs;
$authorityContactSalutation=~s/\'/\'\'/igs;
$authorityContactFirstName=~s/\'/\'\'/igs;
$authorityContactLastName=~s/\'/\'\'/igs;
$authorityPosition=~s/\'/\'\'/igs;
$temp_site_export=~s/\'/\'\'/igs;
$nc=~s/\'/\'\'/igs;
$trackerRef=~s/\'/\'\'/igs;
$awardProcedure=~s/\'/\'\'/igs;
$Contract_Type=~s/\'/\'\'/igs;
$Deadline_Date1=~s/\'/\'\'/igs;
$pr=~s/\'/\'\'/igs;
$CPV_Code=~s/\'/\'\'/igs;
$aa=~s/\'/\'\'/igs;
$origPublicationDate=~s/\'/\'\'/igs;
$Description=~s/\'/\'\'/igs;
$proofed=~s/\'/\'\'/igs;
$exported=~s/\'/\'\'/igs;
$Location=~s/\'/\'\'/igs;
$Authority_Reference=~s/\'/\'\'/igs;
$autoModifyDate=~s/\'/\'\'/igs;
$autoModifyTime=~s/\'/\'\'/igs;
$no_of_months=~s/\'/\'\'/igs;
$contract_start_date=~s/\'/\'\'/igs;
$contract_end_date=~s/\'/\'\'/igs;
$value1=~s/\'/\'\'/igs;
$currency=~s/\'/\'\'/igs;
$amount_min=~s/\'/\'\'/igs;
$amount_max=~s/\'/\'\'/igs;
$Deadline_Description=~s/\'/\'\'/igs;
$Deadline_time=~s/\'/\'\'/igs;
$d_RApost=~s/\'/\'\'/igs;
$nuts=~s/\'/\'\'/igs;
$authorityPostcode=~s/\'/\'\'/igs;
my $other_information;
	# $tender_type1=~s/\'/\'\'/igs;
	# $tender_type1='';
	if (length($tenderblock)>30000)
	{
		$tenderblock = substr $tenderblock, 0, 30000;
	}	
		if ($Tender_ID==63)
	{
		$other_information = 'Please Click the "View Tenders" Section on the left side of the website for view the above tender. Click the "Further Details" button to view the Contract Notice. Please note you must be a registered user in order to access the tender documentation. Please register and login above website to participate in any available opportunities, download Tender documentation, submit your replies, and communicate with our buyers online. If you have already registered and are a returning user, simply log in on the side panel.';
	}	
	my $Sent_to = &d_Rapost_Value_Concatenation($tender_link,$Tender_ID);
	my $tender_type1="Normal";



		my $insertQuery;
	if($Tender_ID==69)
	{
	#$insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,created_date,nuts_code,award_procedure,supplier,D_award,tendersReceived,authoritypostcode,Site,price,Awarded_Status,InTend,other_information,BIP_Non_UK,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$tenderblock\',\'$Origin\',\'$Sector\',\'$TenderName\',\'$Industry_Sector\',\'$tender_title\',\'$title_check_duplicate\',\'$Awarding_Authority\',\'$Contract_Type\',\'$CPV_Code\',\'$Description\',\'$Location\',\'$Authority_Reference\',\'$Value\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date\')<1 then null else \'$Deadline_Date\' end),\'$Deadline_time\',\'$Sent_to\',NOW(),\'$Nuts_Code\',\'$Award_procedure\',\'$Supplier\',\'$D_award\',\'$tendersReceived\',\'$authoritypostcode\',\'$Site\',\'$price\',\'$Award_status\',\'$InTendFlag1\',\'$other_information\',\'$bip_non_uk1\',\'$tender_type1\')";
	$insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,origPublicationDate,description,proofed,exported,location,authority_reference,autoModifyDate,autoModifyTime,no_of_months,contract_start_date,contract_end_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$Buyer\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$origPublicationDate\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$autoModifyDate\',\'$autoModifyTime\',\'$no_of_months\',\'$contract_start_date\',\'$contract_end_date\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date1\')<1 then null else \'$Deadline_Date1\' end),\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	
	print "Query issue";
	}
	else
		{
	# $insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,created_date,nuts_code,award_procedure,supplier,D_award,tendersReceived,authoritypostcode,Site,price,Awarded_Status,InTend,BIP_Non_UK,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$tenderblock\',\'$Origin\',\'$Sector\',\'$TenderName\',\'$Industry_Sector\',\'$tender_title\',\'$title_check_duplicate\',\'$Awarding_Authority\',\'$Contract_Type\',\'$CPV_Code\',\'$Description\',\'$Location\',\'$Authority_Reference\',\'$Value\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date\')<1 then null else \'$Deadline_Date\' end),\'$Deadline_time\',\'$Sent_to\',NOW(),\'$Nuts_Code\',\'$Award_procedure\',\'$Supplier\',\'$D_award\',\'$tendersReceived\',\'$authoritypostcode\',\'$Site\',\'$price\',\'$Award_status\',\'$InTendFlag1\',\'$bip_non_uk1\',\'$tender_type1\')";
	# $insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,origPublicationDate,description,proofed,exported,location,authority_reference,autoModifyDate,autoModifyTime,no_of_months,contract_start_date,contract_end_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$authorityName\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$origPublicationDate\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$autoModifyDate\',\'$autoModifyTime\',\'$no_of_months\',\'$contract_start_date\',\'$contract_end_date\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',\'$Deadline_Date1\',\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	$insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,other_information,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,description,proofed,exported,location,authority_reference,no_of_months,contract_start_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$authorityName\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$no_of_months\',\'$Deadline_Date1\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',\'$Deadline_Date1\',\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	print $insertQuery;
	}

	my $sth = $dbh->prepare($insertQuery);
	if($sth->execute())
	{
		print "tender_data InserQuery :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		open(ERR,">>Failed_Query_InsertAgain.txt");
		print ERR $insertQuery."\n";
		close ERR;
		$dbh=&DbConnection();
		&send_mail_InsertFailed($insertQuery,$Tender_ID);
	}
}
sub d_Rapost_Value_Concatenation()
{
	my $Tender_link	= shift;
	my $Tender_ID	= shift;
	
	my $sendtoo;
	if ($Tender_ID==1)
	{
		$sendtoo="For further information regarding the above contract notice please visit $Tender_link";
	}
		if (($Tender_ID==69) || ($Tender_ID==70) || ($Tender_ID==73) || ($Tender_ID==74))
	{
		$sendtoo="For further information regarding the above contract notice please visit $Tender_link";
	}
	if ($Tender_ID==6)
	{
		$sendtoo="Please log in or register at the following portal Web: $Tender_link to participate";
	}
	if ($Tender_ID==10)
	{
		$sendtoo="Note: This is a CompeteFor opportunity. Opportunities may be with both public and private sector businesses. To apply, please click the following link to register on the CompeteFor portal Web: $Tender_link";
	}
	if ($Tender_ID==14)
	{
		$sendtoo="You must register or log in at Web: $Tender_link to participate in this tender.";
	}
	if (($Tender_ID==16)||($Tender_ID==17)||($Tender_ID==19)||($Tender_ID==20)||($Tender_ID==21)||($Tender_ID==65)||($Tender_ID==27)||($Tender_ID==29)||($Tender_ID==31)||($Tender_ID==34)||($Tender_ID==38)||($Tender_ID==41)||($Tender_ID==42)||($Tender_ID==44)||($Tender_ID==51)||($Tender_ID==54)||($Tender_ID==55)||($Tender_ID==28)||($Tender_ID==67))
	{
		$sendtoo="For further information regarding the above contract notice please visit: $Tender_link";
	}
	if ($Tender_ID==22)
	{
		$sendtoo="To register your interest in the above opportunity please visit: $Tender_link";
	}
	if ($Tender_ID==24)
	{
		$sendtoo="For further information regarding the above contract notice please visit: http://www.finditinbirmingham.com/";
	}
	if ($Tender_ID==26)
	{
		$sendtoo="For further information regarding the above contract notice please visit: http://www.finditinworcestershire.com/";
	}
	if (($Tender_ID==32)||($Tender_ID==36)||($Tender_ID==60))
	{
		$sendtoo="Please log in or register at the following portal Web: $Tender_link to participate.";
	}
	if (($Tender_ID==47)||($Tender_ID==68))
	{
		$sendtoo="For further information/documentation regarding the above contract notice please visit $Tender_link";
	}
	if (($Tender_ID==49)||($Tender_ID==52)||($Tender_ID==58))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
		if (($Tender_ID==4))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
			if (($Tender_ID==63))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
	if (($Tender_ID==41))
	{
		$sendtoo="For further information regarding the above contract notice please visit: $Tender_link";
	}
	if ($Tender_ID>500)
	{
		$sendtoo="For further information and instructions in the above contract notice please visit website: $Tender_link";
	}
	if ($Tender_ID==513)
	{
		$sendtoo="For further information and instructions in the above contract notice please visit website: https://www.lgtenders.co.nz/";
	}
	return $sendtoo;
}		

sub send_mail_InsertFailed()
{
	my $insertQuery	= shift;
	my $tender_ID	= shift;
	
	my $MAILDETAILS = &ReadIniFile('MAILDETAILS');
	my %MAILDETAILS	= %{$MAILDETAILS};
	
	
	my $fulldate = strftime "%d %m %Y %A %H:%M:%S", localtime;

	my $subject	= "BIP Tender - InsertFailed - $tender_ID - $fulldate";
	my $host   	= $MAILDETAILS{'host'}; 
	my $from 	= $MAILDETAILS{'from'};
	my $user 	= $MAILDETAILS{'user'};
	my $to		= $MAILDETAILS{'to_insert_failed'};
	my $pass	= $MAILDETAILS{'password'};
	my $body	= "Hi team,<br><br>Please find the Below query has not inserted<br><br>Insert Query :: $insertQuery<br><br>"."Thanks,<br>MERIT IT";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
		
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	eval {$msg->send;};
	if($@)
	{
		print "\n\n>>>>>>>>Mail Failed>>>>>>>>\n\n";
	}
	else
	{
		print "\n\n>>>>>>>>Mail Sent>>>>>>>>\n\n";
	}
}

sub ReadIniFile()
{
	my $INIVALUE = shift;
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Tender.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}

sub Date_Formatting()
{
	my $date 		= shift;
	my $Company_ID	= shift;
	$date=~s/([A-Za-z]\s*)\.\s*/$1/igs;
	# print "\nBefore the Formatting : $date\n";
	# if ($Company_ID=~m/\b139\b|\b138\b/is)
	# {
		# print "Step 1\n";
		# if ($date=~m/^\s*([\d]+)(?:\/|\-|\.)([\d]+)(?:\/|\-|\.)([\d]{2,4})/is) #04.31.1990
		# {
			# my $YY = $3;
			# my $MM = $1;
			# my $DD = $2;
			# $DD = "0$DD" if (length($DD)==1);
			# $MM = "0$MM" if (length($MM)==1);
			# if ($MM>12)
			# {
				# my $DD1=$MM;
				# my $MM1=$DD;
				# my $date_formatted = "$YY-$MM1-$DD1";
				# return ($date_formatted);
			# }
			# else
			# {
				# my $date_formatted = "$YY-$MM-$DD";
				# return ($date_formatted);
			# }	
		# }
		
	# }
	if ($date=~m/^\s*([\d]+)\-([\d]+)\-([\d]{4})/is) #27-04-1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 2\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*([\d]+)\.([\d]+)\.([\d]{4})/is) #27.04.1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 3\n";
		return ($date_formatted);
		
	}
	elsif ($date=~m/^\s*([\d]+)\/([\d]+)\/([\d]{4})/is)	#27/04/1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 4\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*([\d]{4})\.([\d]+)\.([\d]+)/is) #2015.10.22
	{
		my $YY = $1;
		my $MM = $2;
		my $DD = $3;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 5\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*[^>]*?([\d]+)\/([\d]+)\/([\d]{2})/is) #27-04-16
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		$YY = "20$YY" if ((length($MM)==2));
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 6\n";
		return ($date_formatted);
	}
	else #Monday, 23rd december 2015
	{
		my $dt = ParseDate($date);
		my $date_formatted;
		if ($dt=~m/([\d]{4})([\d]{2})([\d]{2})/is)
		{
			my $YY = $1;
			my $MM = $2;
			my $DD = $3;
			$DD = "0$DD" if (length($DD)==1);
			$MM = "0$MM" if (length($MM)==1);
			# $date_formatted = "$1-$2-$3";
			$date_formatted = "$YY-$MM-$DD";
		}
		# print "After the Formatting : $date_formatted\n";
		# print "Step 7\n";
		return ($date_formatted);
	}
	# print "Step never\n";
}