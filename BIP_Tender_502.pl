#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url ="https://www.tenders.wa.gov.au/watenders/tender/search/tender-search.do?action=advanced-tender-search-open-tender";
# $Tender_Url ="https://www.tenders.wa.gov.au/watenders/tender/search/tender-search.do?CSRFNONCE=750D51374C452F91928FACD0B916DF00&action=advanced-tender-search-new-tenders";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
top:
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=1;
# my ($Tender_content1,$Tender_Url1);
	# if($Tender_content2=~m/href='([^>]*?)' title="List Tenders new this week">/is)
	# {
	# $Tender_Url1="https://www.tenders.wa.gov.au".$1;
# $Tender_content1=&Getcontent($Tender_Url1);
# $Tender_content1 = decode_utf8($Tender_content1);  
# open(ts,">Tender_content5022.html");
# print ts "$Tender_content1";
# close ts;
	# }
#### To get the tender link ####

	while($Tender_content1=~m/<a[^>]*?href="([^>]*?tender-search-open-tender)"\s*[^>]*?>/igs)
	{
	my $tender_link="https://www.tenders.wa.gov.au".$1;
	$tender_link=~s/\&amp\;/&/igs;
	my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference);
	my $tender_title;
	my ($Tender_content2,$Referer)=&Getcontent($tender_link);
	next if($tender_link=~m/44676/is);
	if($Tender_content2=~m/<th colspan="1" class="h2">([^>]*?)<br>/is)
	{
	$tender_title=$1;
	}
	if($Tender_content2=~m/>Issued\s*by\s*([^>]*?)<\/span>\s*<\/th>/is)
	{
	$Awarding_Authority=$1;
	}
	
	my ($Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	
	if($Tender_content2=~m/Region\/s:<\/span><\/td>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is)
		{
			$Location=$1;
			$Location=~s/\s*<br\s*\/>\s*/,/igs;
			
		}
		print $Location;
		$Location=&BIP_Tender_DB::clean($Location);
	if ($Tender_content2=~m/Description\s*<\/th>[\w\W]*?<textarea[^>]*?>([\w\W]*?)<\/table>/is)
	{
		$Description=$1;
		$Description=&BIP_Tender_DB::clean($Description);
	}
	my $block;
	my $temp1="";
	if($Tender_content2=~m/<span class="LIST_TITLE">Other Contacts<\/span><br>([\w\W]*?)<\/table>\s*<\/tr>\s*<\/table>/is)
	{
	$block=$1;
	while($block=~m/<tr>([\w\W]*?)<\/tr>/igs)
	{
	
	$temp1=$temp1."|".&clean($1);
	}
	
	}
	if($Tender_content2=~m/Number:<\/[\w\W]*?td>\s*([^>]*?)\s*<\/td>/is)
	{
			$Authority_Reference=$1;
	}
		if($Tender_content2=~m/Closes at[^>]*?,\s*([^>]*?)\s*at\s*([^>]*?(?:PM|AM|am|pm))/is)
		{
			$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1);
			$Deadline_time=&clean($2);
			if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:pm|PM)/is)
			{
			my $hh=$1;
			$Deadline_time=($hh+12).":".$2 if($hh != 12);
			$Deadline_time=$hh.":".$2 if($1 == 12);
			}
			if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
			{
			my $hh=$1;
			$Deadline_time=$hh.":".$2 if($hh != 12);
			$Deadline_time="00:".$2 if($hh == 12);
			}
			$Deadline_time=~s/am//igs;
			$Deadline_time=~s/AM//igs;
			$Deadline_time=~s/am//igs;
			$Deadline_time=~s/AM//igs;
			
			# $Deadline_Description="Closing Date :";
			$Deadline_Description="Responses";
		}
		$Awarding_Authority=$Awarding_Authority."|".$temp1;
		$Awarding_Authority==~s/||/|/igs;
		$Awarding_Authority==~s/|\s*$//igs;

# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if ($Tender_content2=~m/<\/script>([\w\W]*?)<\!--\s*Closing tags opened in header.vm\s*\-->/is)
	{
		$tenderblock = $1;
	}
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	if ($Tender_content2=~m/<th colspan="2" class="h2">(Site Visit[\w\W]*?)<\/table>/is)
	{
		$otherInformation =  &BIP_Tender_DB::clean($1);
	}
	# $otherInformation=
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		$cy='AU';
		# sleep(10);
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html");
	$req->header("Host"=> "www.tenders.wa.gov.au");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	decode_entities($Clean);
	return $Clean;
}