#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print "$month1 \n";
my $year1		= strftime "%y", localtime;
print $year1;
my $date2=$date1-1;
$date2="0$date2" if(length($date2)==1);
# print $date2;
my $month2=$month1;
# $month1 = "0$month1" if (length($month1)==1);
$month2="0$month2" if(length($month2)==1);
my $year2=$year1;
$year2="20$year2";
#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
# exit;

#### To get the tender link ####
if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>View current opportunities and notices<\/a>/is)
{
	my $View_current_pan =&Urlcheck($1);
	my ($View_current_content)=&Getcontent($View_current_pan);
	$View_current_content = decode_utf8($View_current_content);  
	open(ts,">View_current_content.html");
	print ts "$View_current_content";
	close ts;
	# print "you are in if loop";
	# exit;
	my $url1 = "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL";
	my ($View_current_content1)=&Getcontent1($url1);
	my $post_url = 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do';
	my $Host	= 'crowncommercialservice.bravosolution.co.uk';
	# my $post_con = "userAct=search&oppList=GLOBAL&firstPublishingDate_FILTER_OPERATOR=EQUAL&firstPublishingDate_FILTER=".$date1."%2F".$month1."%2F20".$year1."&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=2";
	my $post_con = "userAct=search&oppList=GLOBAL&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=AFTER&firstPublishingDate_FILTER=01%2F01%2F2001&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=1";

	my $Host	= 'crowncommercialservice.bravosolution.co.uk';
	my ($View_current_content2)=&Postcontent($post_url,$post_con,$Host,$post_url);
		open(ts,">View_current_content114.html");
		
	print ts "$View_current_content2";
	close ts;
	top:
	while ($View_current_content2=~m/<td[^>]*?\"col[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?>\s*<a[^>]*?onclick\=\"javascript:goToDetail\(\&\#39\;([\d]+)\&\#39\;[^>]*?>([\w\W]*?)<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>/igs)
	{
		my $Organisation	= ($1);	
		my $tender_no		= ($2);
		my $tender_title	= &clean($3);
		my $Work_Category	= ($4);
		my $Listing_Deadline= ($5);
		
		my $tender_link	= 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&oppList=GLOBAL';
		# my $tender_link	= 'https://www.capitalesourcing.com/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB&_ncp='.$ncp;
		
		my ($Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		if ($Listing_Deadline=~m/^\s*([^>]*?)\s([^>]*?)\s*$/is)
		{
			$Deadline_Date = $1;
			$Deadline_time = $2;
		}
		# print "Deadline Date :: $Deadline_Date\n";
		# print "Deadline Time :: $Deadline_time\n";
		
		#Duplicate check
		#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my ($Tender_content)=&Getcontent($tender_link,);
		$Tender_content = decode_utf8($Tender_content);  
		open(ts,">Tender14_content.html");
		print ts "$Tender_content";
		close ts;
		# exit;
		
		if ($Tender_content=~m/>\s*Project\s*Title\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $title_temp = &clean($1);
		}
		if ($Tender_content=~m/>\s*Project\s*Code\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Authority_Reference = &clean($1);
		}
		if ($Tender_content=~m/>\s*Project\s*Description\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Description = &clean($1);
		}
		if ($Tender_content=~m/>\s*Work\s*Category\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Contract_Type = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Listing\s*Deadline)\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Deadline_Description = ($1);
			my $Deadline_Date_temp = &clean($2);
		}
		if ($Tender_content=~m/>\s*Buyer\s*Details\s*<\/h3>\s*([\w\W]*?)\s*<\/ul>/is)
		{
			$Awarding_Authority = ($1);
			$Awarding_Authority =~s/\s*<\/div>\s*<div[^>]*?>\s*/ : /igs;
			$Awarding_Authority = &clean($Awarding_Authority);
		}
		
		my $tenderblock;
		if ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/table>\s*<\/div>\s*<\/form>/is)
		{
			$tenderblock = ($1);
		}
		elsif ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/form>\s*<\/div>\s*<\/div>/is)
		{
			$tenderblock = ($1);
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# my $tender_type="Normal";
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
	}
	if ($View_current_content2=~m/value\=\s*([\d]+)\s*\;[^>]*?>\s*\&gt\;\s*</is)
	{
		my $nexpage_number = $1;
		my $post_url = 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do';
		my $post_cont= "userAct=search&oppList=GLOBAL&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=AFTER&firstPublishingDate_FILTER=01%2F01%2F2001&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=".$nexpage_number;
		# .$nexpage_number ;
# ;
		
		my $Host	= 'crowncommercialservice.bravosolution.co.uk';
		($View_current_content)=&Postcontent($post_url,$post_cont,$Host,$post_url,$cookie);
		$View_current_content2 = decode_utf8($View_current_content);  
		open(ts,">TenderNext14_content.html");
		print ts "$View_current_content";
		close ts;
		print "\nPageNo :: $nexpage_number\n";
		goto top;
	}
}	


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"crowncommercialservice.bravosolution.co.uk");
	$req->header("Referer"=>"https://crowncommercialservice.bravosolution.co.uk/web/login.shtml");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do");
	$req->header("Cookie"=> "$Cookie_JSESSIONID");
	$req->header("Connection"=> "keep-alive");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>|<td>|<\/td>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"crowncommercialservice.bravosolution.co.uk");
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=CURRENT&_ncp=1517573764198.823-1");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}