# -*- coding: utf-8 -*-

import requests
import os, sys
import re
import xlwt
from googletrans import Translator
import redis
import time
import json
import pymysql
import xlwt
import imp
import dateparser
import time
import datetime
import ssl
from datetime import date
# import urllib3.contrib.pyopenssl
import time,re,requests
import datetime as DT
import xlwt
import random
import sys
import sys
from lxml import html
# reload(sys)
# sys.setdefaultencoding("utf-8")
from six.moves.html_parser import HTMLParser
book = xlwt.Workbook(encoding="UTF-8")
worksheet = book.add_sheet("data_scrape")
style = xlwt.XFStyle()
import redis
from googletrans import Translator


# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc = desc.replace('Â','')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r"'", "''", str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	return desc 


file_name=os.path.basename(__file__)
file_name1 = re.sub(r"BIP_Tender_","",file_name,re.I)
print (file_name1)
# raw_input("dddddd")
Tender_ID = re.sub(r".py","",file_name1,re.I)
main_url_query = u"select tender_link from tender_master where id="+str(Tender_ID)
# print Tender_ID
# raw_input("ddddd")
##Redis Connection::::
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate

def redis_connection():
	red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
	return red_connec

def translateText1(InputText):
	 url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
	 payload = """{'q': '{}',
			   'target': 'en'
			   'source' : 'lt'}""".format(InputText)
	 s = requests.post(url, data=payload)
	 value = s.json()
	 translated_sentence = value['data']['translations'][0]['translatedText']
	 return translated_sentence
             
	
def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'lt'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'lt'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    # if isinstance(translated_sentence, str):
        # return translated_sentence
    # else:	

        # return translated_sentence.decode("utf-8")
	

    return translated_sentence
	
	
def program():
	proxies = {
	"http" :"http://172.27.137.192:3128",
	"https":"http://172.27.137.192:3128"
	}
	main_url = Database_Connector.select_query(mysql_cursor, main_url_query)
	# main_req = requests.get(main_url[0],verify=False,proxies=proxies)
	main_req = requests.get(main_url[0],verify=False)
	main_cont = str(main_req.content)
	count = 1
	href_list = re.findall('<div class="notice-search-item">[\w\W]*?<a href="([\w\W]*?)"',str(main_req.content))
	title1 = re.findall('<div class="notice-search-item">[\w\W]*?<a href="[\w\W]*?">([^>]*?)<\/a>',str(main_req.content))
	# print str(title1[0])
	# raw_input("Title list")
	for i,href in enumerate(href_list):
		# print i
		# raw_input("enter")
		proxies = {
		"http" :"http://172.27.137.199:3128",
		"https":"http://172.27.137.199:3128"
		}
		address_data = []
		telephone = []
		email_data = []
		re_url = "https://cvpp.eviesiejipirkimai.lt" + str(href)
		print (re_url)
		
		# authorityref = href.replace("/Notice/Details/","")
		Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, re_url)
		title = str(title1[i])
		# print title
		# raw_input("Check")
		if Duplicate_Check == "N":
			curr_date = dateparser.parse(time.asctime())
			created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			# re_res = requests.get(re_url,verify=False)
			# re_res = requests.get(re_url,headers={'Host': 'cvpp.eviesiejipirkimai.lt','User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0','Content-Type' : 'text/html; charset=utf-8', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Language': 'en-US,en;q=0.5'},verify=False,proxies=proxies)
			re_res = requests.get(re_url,headers={'Host': 'cvpp.eviesiejipirkimai.lt','User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0','Content-Type' : 'text/html; charset=utf-8', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Language': 'en-US,en;q=0.5'},verify=False)
			# data = re_res.content.decode('utf_8', errors = 'ignore')
			# data = data.decode('latin-1')
			data = re_res.content.decode('utf_8', errors = 'ignore')
			title = re.findall('avadinimas<\/h3>[\w\W]*?<div class="eps-text">([\w\W]*?)<\/div>', data)
			if title:
				title = translateText(title)
			else:
				title = ""
			if isinstance(title, str):
				title =title
			else:
				title =title.decode("utf-8")		
											# name and address block starts
			name = re.findall('<div class="eps-text">Asmuo r[^>]*?ams:\s*([\w\W]*?)\s*<\/div>', data)
			if name:
				name = name[0]
			else:
				name = ""

			addr = re.findall('<div class="eps-sub-section-body">([\w\W]*?)<div class="nuts">',data)
			if addr:
				# address = re.findall('<div class="eps-sub-section-body">([\w\W]*?) <div class="eps-section">',addr[0])
			   awarding_authority = addr[0]
			   awarding_authority = re.sub(r'<[^<]*?>', ' ', awarding_authority)
			   awarding_authority = re.sub(r'\s+\s+', ' ', awarding_authority)
			   awarding_authority = awarding_authority.replace('Ã…Â³', 'ų')
			   awarding_authority =  awarding_authority.replace('Ã…Â¡', 'š')
			   awarding_authority =  awarding_authority.replace('Ã„â€”', 'ė')
			   awarding_authority =  awarding_authority.replace('Ã…Â¡Ã„Â®', 'šĮ')
			   awarding_authority =  awarding_authority.replace('Å¡','š')
			   awarding_authority =  awarding_authority.replace('Â','š')
			   # awarding_authority =  awarding_authority.replace('','š')
			   # awarding_authority =  awarding_authority.replace('\'','')
			else:
				awarding_authority = ""
			# awarding_authority=awarding_authority.decode("utf-8")	
			authorityref =  re.findall('<div class="eps-text">Nuorodos numeris:\s*([^>]*?)\s*<\/div>',data)
			if authorityref:
				authorityref = authorityref[0]
			else:
				authorityref = ""
			award_procedure =  re.findall('Proced[^>]*?s tipas[\w\W]*?<div class="eps-sub-section-body">([\w\W]*?)<\/div>',data)
			if award_procedure:
				award_procedure = translateText(award_procedure[0])
			else:
				award_procedure = ""

			cpv_nos =  re.findall('Pagrindinis BV[^>]*?kodas[\w\W]*?<span style="color:red">([\w\W]*?)<\/div>',data)
			if cpv_nos:
				# cpv_nos =  cpv_nos[0].replace('<[^>]*?>','')
				# cpv_nos =  cpv_nos.replace("\s+\s+"," ")
				cpv_nos = translateText(reg_clean(cpv_nos[0]))
			else:
				cpv_nos = ""
			if isinstance(cpv_nos, str):
				cpv_nos =cpv_nos
        # return translated_sentence
    # else:	
			else:
        # return translated_sentence.decode("utf-8")	
				cpv_nos =cpv_nos.decode("utf-8")
			short_description =  re.findall('Trumpas apra[^>]*?mas[^>]*?:<\/div>\s*([\w\W]*?)<\/div>',data)
			if short_description:
				short_description = translateText(reg_clean(short_description[0]))
			else:
				short_description = title
			if isinstance(short_description, str):
				short_description =short_description
			else:
				short_description =short_description.decode("utf-8")	
			value = re.findall('<h3>Numatoma bendra vert[^>]*?<\/h3>\s*<\/div>\s*<\/div>\s*<div class="eps-sub-section-body">([^>]*?)<\/div>',data)
			if value:
				value = translateText(reg_clean(value[0]))
			else:
				value =  re.findall('Bendra pirkimo ver[^>]*?Vert([\w\W]*?)\s*<\/div>',data)
				if value:
					value = value[0]
				else:
					value = ""
				
			site = re.findall('Pagrindinė įgyvendinimo vieta:\s*<div class="eps-text">\s*([^>]*?)\s*<\/div>',data)
			if site:
				site = translateText(reg_clean(site[0]))
			else:
				site = ""
			# if isinstance(site, str):
				# site =site
			# else:
				# site =site.decode("utf-8")
			# print (site)	
			d_RA =  re.findall('<div class="eps-text">\s*Data\:\s*([^>]*?)\s*<\/div>',data)
			if d_RA:
				d_RA = reg_clean(d_RA[0])
				deadline_date = d_RA
				dt = dateparser.parse(deadline_date)
				FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
			else:
				d_RA = ""
				FormattedDate = ''

			deadline_time =  re.findall('<div class="eps-text">Vietos laikas\:\s*([^>]*?)\s*<\/div>',data)
			if deadline_time:
				deadline_time1 = deadline_time[0].strip()
				deadline_time = deadline_time1
				# print deadline_time
				# raw_input("Check")
			else:
				deadline_time = ""

			deadline_description = "Deadline for receipt of tenders or requests to participate:"
			orgin = 'European/Non Defence'
			sector = 'European Translation'
			country = 'LT'
			websource = 'Lithuania: Central Public Procurement Portal'
			summary ='The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website:'
			other_information = summary + str(re_url)
			other_information = translateText (str(other_information))
			# print 'other_information::',other_information
			if FormattedDate:
				insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, re_url, str(), orgin, sector, country, websource, title, title,awarding_authority, award_procedure, str(), cpv_nos, short_description, site, authorityref, value, deadline_description, FormattedDate, deadline_time,str(), other_information, 'Y',created_date)
				insertQuery = insertQuery.replace("'b'","'")
				insertQuery = insertQuery.replace("'',","',")
				insertQuery = insertQuery.replace(",',",",'',")
				insertQuery = insertQuery.replace(",'',',",",'','',")
				print (insertQuery)
				# raw_input("Check.................")
			# else:
				# insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(re_url), str(), str(orgin), str(sector), str(country), str(websource),title, str(title), awarding_authority, award_procedure, str(), str(cpv_nos),short_description, site, str(authorityref), str(value), str(), str(other_information), str('Y'), str(created_date))
				val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
			# print "insertion status :", val
			# if val:
				# print ("1 Row Inserted Successfully")
			# else:
				# print ("Insert Failed")
			count = count+1
			if count > 100:
				break
	connection.close()
if __name__== "__main__":
	# main_url = "http://www.iub.gov.lv/iubsearch/pt/_pr/

	program()