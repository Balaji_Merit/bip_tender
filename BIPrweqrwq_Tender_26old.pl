#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
# http://www.finditinbirmingham.com/Login.aspx?ReturnUrl=%2fOpportunity
my ($EVENTVALIDATION,$VIEWSTATEGENERATOR,$VIEWSTATE) = &post_parameters($Tender_content1);

my $postContent = '__VIEWSTATE='.$VIEWSTATE.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR.'&__EVENTTARGET=&__EVENTARGUMENT=&__EVENTVALIDATION='.$EVENTVALIDATION.'&ctl00%24hidReference=&ctl00%24MemberLogin1%24txtusername=&ctl00%24MemberLogin1%24txtpassword=&ctl00%24ContentPlaceHolder1%24MemberLogin1%24txtusername=kaviyarasan.palanivel%40meritgroup.co.uk&ctl00%24ContentPlaceHolder1%24MemberLogin1%24txtpassword=Merit$123&ctl00%24ContentPlaceHolder1%24MemberLogin1%24btnLogin.x=21&ctl00%24ContentPlaceHolder1%24MemberLogin1%24btnLogin.y=7';



my $host = 'www.finditinworcestershire.com';
my $ref  = 'Referer: http://www.finditinworcestershire.com/Login.aspx';
my ($Tender_content2)=&Postcontent($Tender_Url,$postContent,$host,$ref);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content26_v1.html");
print ts "$Tender_content2";
close ts;

my $Opportunity_Url = 'http://www.finditinworcestershire.com/Opportunities.aspx/';

my ($Opportunity_content1)=&Getcontent($Opportunity_Url);
$Opportunity_content1 = decode_utf8($Opportunity_content1);  
open(ts,">Opportunity_content1.html");
print ts "$Opportunity_content1";
close ts;


#### To get the tender link ####
top:
my $block;
if ($Opportunity_content1=~m/<div[^>]*?supplyList[^>]*?>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
{
	$block = $1;
}
else
{	
	$block=$Opportunity_content1;
}
while ($block=~m/hypView\"\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*(?:(?:<\/strong>|<\/p>)?\s*<p>\s*(Closing\s*Date)\s*(?:\:)?\s*([^>]*?)\s*<)?/igs)
{
	my $tender_link = &Urlcheck($1);
	my $temp_title  = $2;
	my $temp_date_desc = $3;
	my $temp_date = $4;
	
	my ($Tender_content)=&Getcontent($tender_link);
	$Tender_content = decode_utf8($Tender_content);  

	my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
	if ($Tender_content=~m/<h1[^>]*?>([\w\W]*?)<\/h1>/is)
	{
		$tender_title 	= &clean($1);
	}
	if ($Tender_content=~m/(Closing\s*Date)\s*(?:\:)?<[^>]*?>\s*([\w\W]*?)<\/span>/is)
	{
		$Deadline_Description 	= ($1);
		$Deadline_Date 	= &clean($2);
	}
	$Deadline_Date = $temp_date if ($Deadline_Date eq '');
	$Deadline_Description = $temp_date_desc if ($Deadline_Description eq '');
	$tender_title = $temp_title if ($tender_title eq '');
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	if ($Tender_content=~m/Location\s*(?:\:)?<[^>]*?>\s*([\w\W]*?)<\/span>/is)
	{
		$Location	= &clean($1);
	}
	if ($Tender_content=~m/Estimated\s*Value\s*(?:\:)?<[^>]*?>\s*([\w\W]*?)<\/span>/is)
	{
		$Value	= &clean($1);
	}

	my $tenderblock;
	if ($Tender_content=~m/<\/h1>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
	{
		$tenderblock	= ($1);
	}
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	
	&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
}

if ($Opportunity_content1=~m/javascript:__doPostBack\(\'([^>]*?)\'[^>]*?>\s*Next/is)
{
	my $nextpage = uri_escape($1);
	print "NExtPAge	:: $nextpage\n";
	my ($EVENTVALIDATION1,$VIEWSTATEGENERATOR1,$VIEWSTATE1,$hidReference1) = &post_parameters($Opportunity_content1);
	my $nextPostContent = 'ctl00%24ContentPlaceHolder1%24AllOpportunities1%24scriptManager=ctl00%24ContentPlaceHolder1%24AllOpportunities1%24updPanel%7C'.$nextpage.'&__EVENTTARGET='.$nextpage.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE1.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR1.'&__EVENTVALIDATION='.$EVENTVALIDATION1.'&ctl00%24hidReference=82fb4738-300c-46cb-9f99-1b8b25aded6e&ctl00%24ContentPlaceHolder1%24AllOpportunities1%24dropSort=date_added&ctl00%24ContentPlaceHolder1%24AllOpportunities1%24dropOrder=DESC&__ASYNCPOST=true&';
	open (PO,">Opportunity_postcontent.txt");
	print PO "$nextPostContent";
	close PO;
	my $host1 = 'www.finditinworcestershire.com';
	($Opportunity_content1)=&Postcontent($Opportunity_Url,$nextPostContent,$host1,$Opportunity_Url);
	$Opportunity_content1 = decode_utf8($Opportunity_content1); 
	open(ts,">Opportunity_content1.html");
	print ts "$Opportunity_content1";
	close ts;
	
	goto top;
}

sub post_parameters()
{
	my $contentsdfs = shift;
	my ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$VIEWSTATE_Value,$hidReference);
	if($contentsdfs=~m/__VIEWSTATE\"\s[^>]*?value\=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATE_Value=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s[^>]*?value\=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATEGENERATOR_Value=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s[^>]*?value\=\"([^>]*?)\"\s*\/>/is)
	{
		$EVENTVALIDATION_Value=uri_escape($1);
	}
	if($contentsdfs=~m/hidReference[^>]*?\"\s[^>]*?value\=\"([^>]*?)\"/is)
	{
		$hidReference=$1;
	}
	return ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$VIEWSTATE_Value,$hidReference);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"www.capitalesourcing.com");
	# $req->header("Referer"=>"https://www.capitalesourcing.com/web/login.shtml");
	$req->header("Content-Type"=> "text/html; charset=utf-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->header("Cookie"=> "	
_ga=GA1.2.1300722153.1466489177; f=ejo5fRPmAGpcUUAAAQiLrAYKAh6ZKQi0rMoS4kVnOb8fYzItxd4bouAUbieucYee02u2JEp5eeisw0iEGcev5yF7LUmNqUoTMZenEF9KJxY1
; .AspNet.ApplicationCookie=O-WWa9hWwvVpkp6_e3ASvtGdGzsWScL-BWNECQGILyOHm5y9Fa8maPeHRRaRYwE8uodykT2a
qjucmBOWjgk4HIEYdLUIEKLgFGcQEsSFTUErre769v0W-jrlLyayU_1PoKgH-zndBnqbN2MbsDP_-mUikPOakFVLAGXVL3KyblfU
HnnC-oY9GSDQ443QW_0nMlI_T5wNJwO4wn7meR3zza0B10E2o-VoniXP-jge9pBayQAbgrPIbU1YweEldziFRRxcoojawWRY1EiP
kJYzvaVFYCRmeJV8ykrC4uVJ6d3L3pI04j5Mwj0V3uMnGAOc-x8aqe0BKGCwgyIDYd_ai_dz_0pz3CZHpMqxd2XV6GhzkzfMAKgN
GrgBhsujPvhOv5o5gAQtZQsM0gLiZQlEsMbiOKRM72OJrai5gXXGRCNs6kfwryyNUtLnDgp3h0shtSfarRQVwv5LUdzCh4Hf8NFb
MXqqdp8936OQCE9rXmpZkYaiyEwyt8gljUC-ofbwyskjtlVfrk6tYtr7Q0QGzbt3OHcVTogTp6CitCHQgNFv2yvtJKpw-NU82-r1
xMYBCaBkhT-E0ymyyINpmGl1u3w_qR0354CVP9GRFTny1OI");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}