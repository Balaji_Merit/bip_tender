import requests
import os, sys
import re
import xlwt
from googletrans import Translator
import redis
import time
import json
import pymysql
import xlwt
import imp
import dateparser
from datetime import datetime
from datetime import date
# reload(sys)
# sys.setdefaultencoding('utf-8')
import warnings
warnings.filterwarnings('ignore')

# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:\Mohammed\python\Work\January_2018\BIP\BIP_Tender_Completed_11012018\Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:\BIP_Tender_files\Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


title_re = '<h4>\s*([\w\W]*?)\s*<\/h4>'
contracting_re = '<div class="chapter-headline">\s*([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>'
contracting_re1 = '<div class="chapter-headline">\s*(Name, Adressen und Kontaktstelle[\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<hr\s*\/>'
description_re = '<h1>Ausschreibungsdetails<\/h1>\s*<p><\/p>\s*<\/div>\s*<[\w\W]*?>\s*<\/div>\s*<[\w\W]*?>\s*\s*<[\w\W]*?>\s*\s*<[\w\W]*?>\s*<h4>\s*([\w\W]*?)\s*<\/h4>'
site_re = '<label>Hauptort\s*der\s*Ausf[\w\W]*?hrung\:<\/label>\s*<p>\s*([\w\W]*?)\s*<\/p>'
site_re1 = '<div class="chapter-headline">(?:\s*Ort der Leistungserbringung\s*|Ausf\&uuml\;hrungsort)<\/div>\s*<div class="chapter-content">\s*([^>]*?)\s*<\/div>'
ref_re = 'Gesch[\w\W]*?ftszeichen\:\s*<\/label>\s*<[^>]*?>\s*\s*<[^>]*?>\s*([^>]*?)\s*<\/p>\s*<\/div>'
date_re = 'Abgabefrist\s*Angebot:<\/span>\s*<\/label>\s*<[^>]*?>\s*<[^>]*?>\s*(\d+\.\d+\.\d+)\s*(\d+\:\d+)<\/p>'
date_re1 = '<td class="result_col_deadline result_type_date">\s*<div>(\d{1,2}\.\d{1,2}\.\d{1,2})\s*(\d{1,2}\:\d{1,2})</div>'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '


##Clean Function:::
def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')

	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
	desc = re.sub(r"\&uuml\;", "u", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"\&Ouml\;ffentlicher Auftraggeber\s*\(Vergabestelle\),\s*a\)\,Hauptauftraggeber \(zur Angebotsabgabe auffordernde Stelle\)\,\s*","", desc, re.I)
	return desc 


file_name=os.path.basename(__file__)
tender_id1 = file_name.replace("BIP_Tender_","")
# Tender_ID = tender_id1.replace(".py","")
Tender_ID = "536"
main_url_query = u"select tender_link from tender_master where id=536"

##Redis Connection::::

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate

def redis_connection():
	red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
	return red_connec
	
def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'de'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'de'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    return translated_sentence

# main_url = 'https://www.evergabe-online.de/search.html?'
# main_url = "https://www.evergabe-online.de/search.html?2&publishDateRange=ALL&category=2"
def program():
	# conn=pymysql.connect(host='172.27.138.250',port=3306,user='root',password='admin',db='BIP_tender_analysis')
	# cur=conn.cursor()
	main_url = Database_Connector.select_query(mysql_cursor, main_url_query)
	# main_req = requests.get(main_url[0])
	# main_cont = main_req.content
	headers={"Host":"www.evergabe-online.de",
		"Content-Type":"Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
		# "Referer":"https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do",
		"Referer": "https://www.evergabe-online.de/search.html?1"}
		# "Cookie": "JSESSIONID=25R9Pn5p_js2k6NZCShnvOAv9XWBEEbfkRUyN6z9.ogcadm_lb4; VISITORID=ef06300d-4c99-41f8-859b-f6b690b4ed37; VISITOR_ET=1517570403570; _tabSessionId=971ee151-2d28-ac28-1778-1ca41b431317"}
	post_cont="advancedSearchParameters%3AadvancedSearchParameterPanel%3ApublishDateGroup%3ApublishDateGroup_body%3ApublishDateFrom=01.02.2018"	
	# r=requests.post("https://www.evergabe-online.de/search.html?1-4.IBehaviorListener.0-searchForm-advancedSearchParameters-advancedSearchParameterPanel-publishDateGroup-publishDateGroup_body-publishDateFrom",data=keys,headers=headers)
	# content1=r.content
	main_req = requests.post("https://www.evergabe-online.de/search.html?1-4.IBehaviorListener.0-searchForm-advancedSearchParameters-advancedSearchParameterPanel-publishDateGroup-publishDateGroup_body-publishDateFrom",data=post_cont,headers=headers)
	# main_req = requests.get(main_url[0])
	main_cont = main_req.content.decode('utf-8',errors='ignore')
	sub_url_re = '<a class="text-wrap" href="\.([^>]*?)" data-evid="search_list_result">'
	sub_url = re.findall(sub_url_re, main_cont)
	
	
	

	while True:
		sub_url_re = '<a class="text-wrap" href="\.([^>]*?)" data-evid="search_list_result">'
		sub_url = re.findall(sub_url_re, main_cont)
		deadline_date1 = re.findall(date_re1, main_cont, re.I)
		# deadline_time = re.findall(date_re, sub_cont, re.I)

		
		for i,tender_link in enumerate(sub_url):
		# print "No :", count
			base_url = 'https://www.evergabe-online.de' + str(tender_link)
			base_url = re.sub(r'\;jsessionid\=[^>]*?id', '?1&id', str(base_url))
			Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
			Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, base_url)
			print (base_url)
			if Duplicate_Check == "N":
				curr_date = dateparser.parse(time.asctime())
				created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
				sub_req = requests.get(base_url)
				sub_cont = sub_req.content.decode('utf-8',errors='ignore')
				title = re.findall(title_re, sub_cont, re.I)
				if title:
					title = translateText(title[0])
				else:
					title='None'
				awarding_authority = re.findall(contracting_re, sub_cont, re.I)
				awarding_authority1 = re.findall(contracting_re1, sub_cont,re.I )
				if awarding_authority:
					awarding_authority = awarding_authority[0].strip()
					awarding_authority = re.sub(r'<[^>]*?>', ' ', awarding_authority)
					awarding_authority = awarding_authority.replace('\n', '')
					awarding_authority = awarding_authority.replace('                    ', ',')
					awarding_authority = awarding_authority.replace('\&Ouml\;ffentlicher Auftraggeber\s*\(Vergabestelle\),\s*a\)\,Hauptauftraggeber \(zur Angebotsabgabe auffordernde Stelle\)\,\s*','')
					awarding_authority = awarding_authority.strip()
					# awarding_authority = awarding_authority.decode('utf-8',errors='ignore')

					
				else:
					awarding_authority = awarding_authority1
			# awarding_authority = translateText (str(awarding_authority))

			

				description = re.findall(description_re, sub_cont, re.I)
				if description:
					description = description[0]
				else:
					description = ''
				description = translateText (str(description))

				location = re.findall(site_re, sub_cont, re.I)
				if location:
					location = location[0]
				else:
				# location = ''
					location1 = re.findall(site_re1, sub_cont, re.I)
					if location1:
						location = location1[0]
					else:
						location = ''
				location = reg_clean(str(location))

				authority_reference = re.findall(ref_re, sub_cont, re.I)
				if authority_reference:
					authority_reference = authority_reference[0].strip()
				else:
					authority_reference = ''
				authority_reference = translateText (str(authority_reference))

				deadline_description = "Deadline for participation:"
				if deadline_date1[i]:
					deadline_date = deadline_date1[i][0]
					deadline_date = deadline_date.strip()
					deadline_date = str(deadline_date).replace('&nbsp;', '')
					deadline_date = str(deadline_date).replace('.', '/')
					deadline_date = str(deadline_date)+" 00:00:00"
					dt = datetime.strptime(deadline_date,'%d/%m/%y %H:%M:%S')
					# dt = dateparser.parse(deadline_date)
					FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
					FormattedDate = dt
					deadline_time = deadline_date1[i][1]
				else:
					deadline_date = ''
					
				# deadline_date = re.findall(date_re, sub_cont, re.I)
				# deadline_time = re.findall(date_re, sub_cont, re.I)
				# if deadline_date:
					# deadline_date = deadline_date[0][0]
					# deadline_date = deadline_date[0].strip()
					# deadline_date = str(deadline_date).replace('&nbsp;', '')
					# dt = dateparser.parse(deadline_date)
					# FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
					# deadline_time = deadline_time[0][1]
				# else:
					# deadline_date = ''
				# FormattedDate = ''
				# FormattedDate = translateText (str(FormattedDate))
				# print 'Date:::', FormattedDate.strip()
				other_information = summary + str(base_url)
				other_information = translateText (str(other_information))
				orgin = 'European/Non Defence'
				sector = 'European Translation'
				country = 'DE'
				websource = 'Germany: eVergabe'
				if FormattedDate:
					insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, base_url, '', orgin, sector, country, websource, title, title,awarding_authority, '', '', '', description, location, authority_reference, '', deadline_description, FormattedDate, deadline_time,'', other_information, 'Y',created_date)
				else:
					insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(base_url), str(), str(orgin), str(sector), str(country), str(websource),str(title), str(title), awarding_authority, str(), str(), str(), str(description),	str(location), str(authority_reference), str(), str(), str(other_information), str('Y'), str(created_date))
				val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
				
			# print "Insertion status :",val
				if val == 1:
					print ("1 Row Inserted Successfully")
					count1 = count1 + 1
				else:
					print ("Insert Failed")
		next_page = re.findall('<a\s*rel=\"next\"\s*class=\"next\"\s*id\=\"id45\"\s*href\=\"([^>]*?)\"\s*title\=\"Gehe zur n[^>]*?hsten Seite\">',str(main_cont))
		if next_page:
			nxt_pg = next_page[0][1::]
			next_pg_url = "https://www.evergabe-online.de" + nxt_pg
			headers={"Host":"www.evergabe-online.de",
					 "Content-Type":"Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
		# "Referer":"https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do",
					"Referer": "https://www.evergabe-online.de/search.html?1"}
		# "Cookie": "JSESSIONID=25R9Pn5p_js2k6NZCShnvOAv9XWBEEbfkRUyN6z9.ogcadm_lb4; VISITORID=ef06300d-4c99-41f8-859b-f6b690b4ed37; VISITOR_ET=1517570403570; _tabSessionId=971ee151-2d28-ac28-1778-1ca41b431317"}
			keys="advancedSearchParameters%3AadvancedSearchParameterPanel%3ApublishDateGroup%3ApublishDateGroup_body%3ApublishDateFrom=01.02.2018"	
			r=requests.post(next_pg_url,data=keys,headers=headers)
			main_cont=r.content.decode('utf-8',errors='ignore')

	
	connection.close()
	
if __name__== "__main__":
	# main_url = "https://www.evergabe-online.de/search.html?2&publishDateRange=ALL&category=2"
	program()
	
	

