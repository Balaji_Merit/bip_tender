import requests
import re
import os
import sys
import pymysql
import time
import json
import pymysql
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
import configparser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
import dateutil.parser as dparser
from time import sleep
import pytz


Database_Connector = imp.load_source('Database_Connector','C:\\BIP_Tender\\New_Format\\Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
##Clean Function:::
def DuplicateCheck1(Records_DuplicateCheck, URL, title1, deadline_date):
    duplicate = ''
    if len(Records_DuplicateCheck) != 0:
        for row in Records_DuplicateCheck:
            if URL in row:
                print ("duplicate data")
                duplicate = 'Y'
                break
            else:
                duplicate = 'N'
    else:
        duplicate = 'N'

    return (duplicate)
	
def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc =  desc.replace("amp;","")
	desc = desc.replace('\s+\s+', ' ')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r"&#39;","''", str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "''", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	desc = desc.replace('&nbsp;','')
	# title2 =  title2.replace("''","'")
	return desc

def regex_match(regex,content):
	if regex != "None":
		value = re.findall(regex,str(content))
		if len(value) == 0:
			value1 = ""
		else:
			value1 = reg_clean(value[0])
	else:
		value1 = ""
	return (value1)

def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	try:
		dt=dparser.parse(Input,fuzzy=True)
		FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	except Exception as e:
		deadline_date1 = dateparser.parse(time.asctime())
		deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
		FormattedDate = deadline_date1
	return (FormattedDate)

def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	time1 = re.findall('(\d{1,2})\s*(?:Noon|noon|pm|PM)',str(Input))
	print (time1)
	if time1:
		if int(time1[0]) < 12:
			Formattedtime = str(int(time1[0])+12) +":00:00"
		else:
			Formattedtime = str(int(time1[0])) +":00:00"
	print (Formattedtime)	
	time2 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)',str(Input))
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			if time2[0][1]:
				Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+"0:00"
			else:
				Formattedtime = str(int(time2[0][0])+12)  +":00:00"
		else:    
			if int(time2[0][0]) == 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)',str(Input))
        
		if time2:
			if int(time2[0][0]) <= 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			
		else:
			time2 = re.findall('(\d{1,2})\s*(?:pm|PM\s*)',str(Input))
			print (time2)
			if time2:
				if int(time2[0][0]) < 12:
					Formattedtime = str(int(time2[0][0])+12) +":00:00"
			else:		
				time2 = re.findall('(?:Midday|midday|12\s*noon|12.00\s* Noon|12.00\s*noon)',str(Input))
				if time2:
					Formattedtime = "12:00:00"
				else:
					time2 = re.findall('(?:Midnight|midnight)',str(Input))
					if time2:
						Formattedtime = "00:00:00"
					else:
						Formattedtime = Input

	print (Formattedtime)
	return (Formattedtime)

def regex_content_block(regex,content):
# used to take the values block by using regex
	value1=content
	if regex != None:
		revenue = re.findall(regex,str(content))
		return revenue
	else:
		return content

def regex_checking_updating(content,url,tender_id):
	print (tender_id+"\n")
	# print (content)
	config = configparser.ConfigParser()
	config.read('C:\\BIP_Tender\\New_Format\\BIP_Tender_new.ini')
	print (config.sections())
	title=(config.get(str(tender_id),'title'))
	title1 = regex_match(title,str(content))
	print (title1)
	sourcecw=(config.get(str(tender_id),'sourcecw'))
	tender=(config.get(str(tender_id),'tender'))
	origin=(config.get(str(tender_id),'origin'))
	sector=(config.get(str(tender_id),'sector'))
	authorityname=(config.get(str(tender_id),'authorityname'))
	authoritydept=(config.get(str(tender_id),'authoritydept'))
	country_codes_isocountry=(config.get(str(tender_id),'country_codes_isocountry'))
	country=(config.get(str(tender_id),'country'))
	web_source=(config.get(str(tender_id),'web_source'))
	industry_sector=(config.get(str(tender_id),'industry_sector'))
	awarding_authority=(config.get(str(tender_id),'awarding_authority'))
	authorityaddress1=(config.get(str(tender_id),'authorityaddress1'))
	authorityaddress2=(config.get(str(tender_id),'authorityaddress2'))
	authorityaddress3=(config.get(str(tender_id),'authorityaddress3'))
	defence=(config.get(str(tender_id),'defence'))
	pubflagcw=(config.get(str(tender_id),'pubflagcw'))
	authoritytown=(config.get(str(tender_id),'authoritytown'))
	authoritycounty=(config.get(str(tender_id),'authoritycounty'))
	online_only=(config.get(str(tender_id),'online_only'))
	private_sector=(config.get(str(tender_id),'private_sector'))
	frameworkagreement=(config.get(str(tender_id),'frameworkagreement'))
	authoritycountry=(config.get(str(tender_id),'authoritycountry'))
	authoritytelephone=(config.get(str(tender_id),'authoritytelephone'))
	authorityfax=(config.get(str(tender_id),'authorityfax'))
	authorityemail=(config.get(str(tender_id),'authorityemail'))
	temp_description_export=(config.get(str(tender_id),'temp_description_export'))
	authoritywebaddress=(config.get(str(tender_id),'authoritywebaddress'))
	authorityprofileurl=(config.get(str(tender_id),'authorityprofileurl'))
	authoritycontactsalutation=(config.get(str(tender_id),'authoritycontactsalutation'))
	authoritycontactfirstname=(config.get(str(tender_id),'authoritycontactfirstname'))
	authoritycontactlastname=(config.get(str(tender_id),'authoritycontactlastname'))
	authorityposition=(config.get(str(tender_id),'authorityposition'))
	temp_site_export=(config.get(str(tender_id),'temp_site_export'))
	nc=(config.get(str(tender_id),'nc'))
	trackerref=(config.get(str(tender_id),'trackerref'))
	awardprocedure=(config.get(str(tender_id),'awardprocedure'))
	contact_type=(config.get(str(tender_id),'contact_type'))
	earliestdate=(config.get(str(tender_id),'earliestdate'))
	pr=(config.get(str(tender_id),'pr'))
	cpv_code_block=(config.get(str(tender_id),'cpv_code_block'))
	cpv_code=(config.get(str(tender_id),'cpv_code'))
	aa=(config.get(str(tender_id),'aa'))
	origpublicationdate=(config.get(str(tender_id),'origpublicationdate'))
	description=(config.get(str(tender_id),'description'))
	proofed=(config.get(str(tender_id),'proofed'))
	exported=(config.get(str(tender_id),'exported'))
	location=(config.get(str(tender_id),'location'))
	authority_reference=(config.get(str(tender_id),'authority_reference'))
	automodifydate=(config.get(str(tender_id),'automodifydate'))
	automodifytime=(config.get(str(tender_id),'automodifytime'))
	no_of_months=(config.get(str(tender_id),'no_of_months'))
	contract_start_date=(config.get(str(tender_id),'contract_start_date'))
	contract_end_date=(config.get(str(tender_id),'contract_end_date'))
	value=(config.get(str(tender_id),'value'))
	currency=(config.get(str(tender_id),'currency'))
	amount=(config.get(str(tender_id),'amount'))
	amount_min=(config.get(str(tender_id),'amount_min'))
	amount_max=(config.get(str(tender_id),'amount_max'))
	deadline_description=(config.get(str(tender_id),'deadline_description'))
	deadline_date=(config.get(str(tender_id),'deadline_date'))
	deadline_time=(config.get(str(tender_id),'deadline_time'))
	sent_to=(config.get(str(tender_id),'sent_to'))
	other_information=(config.get(str(tender_id),'other_information'))
	nuts_code=(config.get(str(tender_id),'nuts_code'))
	authoritypostcode=(config.get(str(tender_id),'authoritypostcode'))
	supplier=(config.get(str(tender_id),'supplier'))
	d_award=(config.get(str(tender_id),'d_award'))
	tendersreceived=(config.get(str(tender_id),'tendersreceived'))
	price=(config.get(str(tender_id),'price'))
	site=(config.get(str(tender_id),'site'))
	awarded_status=(config.get(str(tender_id),'awarded_status'))
	intend=(config.get(str(tender_id),'intend'))
	bip_non_uk=(config.get(str(tender_id),'bip_non_uk'))
	tender_type=(config.get(str(tender_id),'tender_type'))
	next_page=(config.get(str(tender_id),'next_page'))

	title2 = title1
	# print (title2)
	title2 =  title2.replace("''","'")
	if tender_id=='1174':
		title1=title1[:50]
	print (title1)	
	# print "re----->",Retrive_duplicate with title of the tender
	Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
	Duplicate_Check_Title = Database_Connector.DuplicateCheckTitle(Retrive_duplicate, title2)
	if Duplicate_Check_Title == "N":		
		authorityName1 = regex_match(authorityname,str(content))
		country_codes_isocountry="United Kingdom"
		authorityDept1=regex_match(authoritydept,str(content))
		award_procedure1 = regex_match(awardprocedure,str(content))
		contact_type1 = regex_match(contact_type,str(content))
		cpv_code1 = regex_match(cpv_code,str(content))
		description1 = regex_match(description,str(content))
		location1 = regex_match(location,str(content))
		authority_reference1 = regex_match(authority_reference,str(content))
		authorityAddress1=regex_match(authorityaddress1,str(content))
		authorityAddress2=regex_match(authorityaddress2,str(content))
		authorityAddress3=regex_match(authorityaddress3,str(content))
		defence1=regex_match(defence,str(content))
		pubFlagCW1=regex_match(pubflagcw,str(content))
		authorityTown1=regex_match(authoritytown,str(content))
		authorityCounty1=regex_match(authoritycounty,str(content))
		online_only1=regex_match(online_only,str(content))
		private_sector1=regex_match(private_sector,str(content))
		frameworkAgreement1=regex_match(frameworkagreement,str(content))
		authorityCountry1=regex_match(authoritycountry,str(content))
		authorityTelephone1=regex_match(authoritytelephone,str(content))
		authorityFax1=regex_match(authorityfax,str(content))
		authorityEmail1=regex_match(authorityemail,str(content))
		temp_description_export1=regex_match(temp_description_export,str(content))
		authorityWebAddress1=regex_match(authoritywebaddress,str(content))
		authorityProfileURL1=regex_match(authorityprofileurl,str(content))
		authorityContactSalutation1=regex_match(authoritycontactsalutation,str(content))
		authorityContactFirstName1=regex_match(authoritycontactfirstname,str(content))
		authorityContactLastName1=regex_match(authoritycontactlastname,str(content))
		authorityPosition1=regex_match(authorityposition,str(content))
		temp_site_export1=regex_match(temp_site_export,str(content))
		nc1=regex_match(nc,str(content))
		trackerRef1=regex_match(trackerref,str(content))
		pr1=regex_match(pr,str(content))
		aa1=regex_match(aa,str(content))
		origPublicationDate1=regex_match(origpublicationdate,str(content))
		proofed1=regex_match(proofed,str(content))
		exported1=regex_match(exported,str(content))
		autoModifyDate1=regex_match(automodifydate,str(content))
		autoModifyTime1=regex_match(automodifytime,str(content))
		no_of_months1=regex_match(no_of_months,str(content))
		contract_start_date1=regex_match(contract_start_date,str(content))
		contract_end_date1=regex_match(contract_end_date,str(content))
		currency1=regex_match(currency,str(content))
		amount1=regex_match(amount,str(content))
		amount_min1=regex_match(amount_min,str(content))
		amount_max1=regex_match(amount_max,str(content))
		nuts_code1=regex_match(nuts_code,str(content))
		country_code="United Kingdom"
		authoritypostcode1=regex_match(authoritypostcode,str(content))
		tender1=regex_match(tender,str(content))

		value1 = regex_match(value,str(content))

		if deadline_date != "None":
			print ("U r in Loop")
			deadline_date_re=re.findall(deadline_date,str(content))

			if len(deadline_date_re) == 0 or deadline_date_re[0] == '':
				deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
				# deadline_date1 = dateparser.parse(time.asctime())
				deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
				FormattedDate = DateFormat(deadline_date1)
			else:
				deadline_date1 = reg_clean(deadline_date_re[0])
				deadline_date1 = DateFormat(deadline_date1)
				print (deadline_date1)
				FormattedDate = deadline_date1
		else:
			deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
			deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
			FormattedDate = deadline_date1
		if deadline_time != "None":
			deadline_time_re = re.findall(deadline_time,str(content))
			if len(deadline_time_re) == 0:
				deadline_time1 = ""
			else:
				if tender_id =="1012":
					deadline_time1 = deadline_time_re[0]+" 00:00"
				else:
					deadline_time1 = timeFormat(deadline_time_re[0])
		else:
			deadline_time1 = "00:00:00"
		other_information1 = other_information + str(url)
		curr_date = datetime.now(pytz.timezone('Europe/London'))
		created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))

		insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,private_sector,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,description,proofed,exported,location,authority_reference,no_of_months,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,already_exist,export_flag,created_date,created_by,high_value,authoritypostcode,supplier,D_award,tendersReceived,price,Site,Awarded_Status,InTend,BIP_Non_UK,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format((tender_id), (url),(sourcecw),(authorityName1), (tender1), (origin), (sector), (authorityDept1),(country_code), (country), (web_source), (industry_sector), (title1), (title1),(authorityName1), (authorityAddress1), (authorityAddress2), (authorityAddress3), (defence1), (pubFlagCW1), (authorityTown1), (authorityCounty1), (online_only1), (private_sector1), (frameworkAgreement1), (authorityCountry1), (authorityTelephone1), (authorityFax1), (authorityEmail1), (temp_description_export1), (authorityWebAddress1), (authorityProfileURL1), (authorityContactSalutation1), (authorityContactFirstName1), (authorityContactLastName1),(authorityPosition1), (temp_site_export1), (nc1), (trackerRef1), (award_procedure1), (contact_type1), (FormattedDate), (pr1), (cpv_code1), (aa1), (description1), (proofed1), (exported1), (location1), (authority_reference1), (no_of_months1),  (value1), (currency1), (value1), (amount_min1), (amount_max1), (deadline_description), (FormattedDate), (deadline_time1), (other_information1), (url), (nuts_code),'', '', (created_date),'', '', (authoritypostcode1), '', '', '', '', '', (awarded_status), (intend), (bip_non_uk), (tender_type))
	
		print (insertQuery)
		Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
	
## DB Connections:::

def program():	

	proxies = {"https":"http://172.27.137.192:3128","http":"http://172.27.137.192:3128"}

	main_url_query = "select id,tender_name,tender_link,origin, noticesector,cy from tender_master where id>100 and id<405 and active='Y' and id!=276"
	mysql_cursor.execute(main_url_query)
	records = mysql_cursor.fetchall()
	for item in records:
		tender_id=item[0]
		tender_name=item[1]
		tender_name=tender_name.replace("'","''")
		tender_link=item[2]
		origin=item[3]
		noticesector=item[4]
		cy=item[5]
		print (tender_id)
		Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck1(mysql_cursor,tender_id)
		print (Retrive_duplicate)
		if tender_link:
			tender_base_url= tender_link.replace("Services/Projects.svc/GetProjects?strMode=Current&searchvalue=&bUseSearch=false&OrderBy=Title&OrderDirection=ASC&iPage=1&iPageSize=10&bOnlyWithCorrespondenceAllowed=false&iCustomerFilter=0&iOptInStatus=-1&_=145811260482", "home")
			print (tender_base_url)
			s=requests.session()
			r=s.get(tender_base_url,verify = False,proxies=proxies)
			time.sleep(5)
			html=r.content.decode('utf-8', errors='ignore')
			r1=s.get(tender_link,verify = False,proxies=proxies)
			html1=r1.content.decode('utf-8-sig', errors='ignore')
			page_content=json.loads(html1)
			page_count=(page_content['PageCount'])+1
			for i in range(1,page_count):
				tender_link1=tender_link.replace('iPage=1','iPage='+str(i))
				print (tender_link1)
				r2=s.get(tender_link1,verify = False,proxies=proxies)
				# try:
				html2=r2.content.decode('utf-8', errors='ignore')
				block = re.findall('(\{"AdditionalDeliveryText":[\w\W]*?"Wards":""\})', html2)
				print (block)
				for j in block:
					print (j)
					
					title=re.findall('"Title":"([^>]*?)",',str(j))
					description=re.findall('"Description":"([\w\W]*?)","EstAwardDate"',str(j))
					cpv_code=re.findall('"CPV":"([^>]*?)",',str(j))
					award_procedure=re.findall('"CF_AwardedProcedureType":"([^>]*?)",',str(j))
					contact_t=re.findall('"Contact":"([^>]*?)",',str(j))
					authorityName=re.findall('"Customer":"([^>]*?)",',str(j))
					project1=re.findall('"ProjectID":([^>]*?),',str(j))
					authority_reference=re.findall('"Reference":"([^>]*?)",',str(j))
					contactuserid1=re.findall('"MainContactUserID":"([^>]*?)",',str(j))
					category1=re.findall('"Category":"([^>]*?)",',str(j))
					opendate1=re.findall('"OpenDate":"([^>]*?)",',str(j))
					
					award_date1=re.findall('"EstAwardDate":"([^>]*?)",',str(j))
					close_date1=re.findall('"CloseDate":"([^>]*?)",',str(j))
					timestamp12=re.findall('"OriginalDateDocsAvailableUntil":"([^>]*?)",',str(j))
					timestamp1=timestamp12[0]
					temp1=''
					title1=title[0]
					print (title1)
					description1=description[0]
					cpv_code1=cpv_code[0]
					project=project1[0]
					award_procedure1=award_procedure[0]
					contact=contact_t[0]
					authorityName1=authorityName[0]
					authority_reference1=authority_reference[0]
					contactuserid=contactuserid1[0]
					opendate=opendate1[0]
					award_date=award_date1[0]
					close_date=close_date1[0]
					if category1:
						category=category1[0]
					else:
						category=''

					description1=description1.replace("'","'")
					authority_reference1=authority_reference1.replace("'","''")
					cpv_code1=cpv_code1.replace("-0","")
					cpv_code1=re.sub(r"-\d+", "",cpv_code1)
					description1=description1.replace("'","''")
					description1=description1.replace("‘","''")
					description1=description1.replace("’","''")
					description1=description1.replace("\\u000d","")
					description1=description1.replace('“','"') 
					description1=description1.replace('”','"')
					description1=description1.replace("’","'")
					description1=description1.replace("’","'")
					description1=description1.replace("‘","'")
					description1=description1.replace("<br \/>"," ")
					description1 = re.sub(r"<br \/>", " ", str(description1), re.I)
					description1 = re.sub(r"\\u000d", "", str(description1), re.I)
					if len(contact)!=0:
						contact_type = contact.split(" ",1)
						contact1=reg_clean(contact_type[0])
						if len(contact_type)>1:
							contact2=reg_clean(contact_type[1])
						else:
							contact2=""
					else:
						contact1=''
						contact2=''
					authorityName1=authorityName1.replace("'","''")
					tender_url=tender_base_url.replace("home","ProjectManage/")
					tender_url=tender_url+str(project)
					print (tender_url)
					if len(award_date)!=0:
						if '-' in award_date:
							temp2=int(award_date[8:18])
						else:
							temp2=int(award_date[7:17])
						utc_dt1 = datetime.utcfromtimestamp(temp2)
						aware_utc_dt1 = utc_dt1.replace(tzinfo=pytz.utc)
						tz = pytz.timezone('Europe/London')
						dt7=aware_utc_dt1.astimezone(tz)
						dt6=dt7.strftime('%Y-%m-%d')
						temp=dt7.strftime('%d-%m-%Y')
						dt6=dt6+" 00:00:00"
						temp1=temp1+"Award Date: "+temp
					
					print (opendate)
					if len(opendate)!=0:
						if '-' in opendate:
							temp2=int(opendate[8:18])
						else:
							temp2=int(opendate[7:17])
						
						utc_dt1 = datetime.utcfromtimestamp(temp2)
						aware_utc_dt1 = utc_dt1.replace(tzinfo=pytz.utc)
						tz = pytz.timezone('Europe/London')
						dt8=aware_utc_dt1.astimezone(tz)
						dt2=dt8.strftime('%Y-%m-%d')
						temp=dt8.strftime('%d-%m-%Y')
						dt5=dt2+" 00:00:00"
						temp1=temp1+"Contract Start: "+temp
					# else:
						dt2=''
						
					print (dt2)
					
					if len(close_date)>0:
						if '-' in close_date:
							temp3=int(close_date[8:18])
						else:
							temp3=int(close_date[7:17])
						utc_dt2 = datetime.utcfromtimestamp(temp3)
						aware_utc_dt2 = utc_dt2.replace(tzinfo=pytz.utc)
						tz = pytz.timezone('Europe/London')
						dt9=aware_utc_dt2.astimezone(tz)
						dt3=dt9.strftime('%Y-%m-%d')
						dt4=dt3+" 00:00:00"
						temp=dt9.strftime('%d-%m-%Y')
						temp1=temp1+"Contract End: "+temp
					else:
						dt3=''
					print (dt3)
					temp9=int(timestamp1[7:17])
					utc_dt4 = datetime.utcfromtimestamp(temp9)
					aware_utc_dt1 = utc_dt4.replace(tzinfo=pytz.utc)
					tz = pytz.timezone('Europe/London')
					dt10=aware_utc_dt1.astimezone(tz)
					dt=dt10.strftime('%Y-%m-%d')
					year1=dt10.strftime('%Y')
					month1=dt10.strftime('%m')
					date2=dt10.strftime('%d')
					print ("Date in URl"+date2)
					FormattedDate=dt+" 00:00:00"
					deadline_time1=dt10.strftime('%H:%M:%S')
					
					# print (tender_url)
					Duplicate_Check = Database_Connector.DuplicateCheck1(Retrive_duplicate,title1,tender_url,year1,month1,date2)
					print (Duplicate_Check)
					if Duplicate_Check=='N':
						other_information1 = "For further information on the above contract please visit Web: " + str(tender_url)
						print (other_information1)
						title1=title1.replace("'","''")
						curr_date = datetime.now(pytz.timezone('Europe/London'))
						created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
						deadline_description="Date documents can be requested until"
						# category=j.get("Category")
						tender1=description1
						sourcecw="Contrax Weekly"
						intend='Y'
						tender_type="Intend"
						country_code='GB'
						country="United Kingdom"
						industry_sector="Other"
						pubFlagCW1="No"
						authorityCountry1="GB"
						Awarded_Status='N'
						BIP_Non_UK='N'
						
						if len(category)!=0:
							temp1=temp1+"Category: "+str(category)
						if len(award_procedure1)!=0:
							temp1=temp1+"Award Procedure Type: "+str(award_procedure1)
						insertQuery = u"insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,pubFlagCW,authorityCountry,authorityContactFirstName,authorityContactLastName,awardProcedure,earliestDate,cpv_code,description,authority_reference,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date,Awarded_Status,InTend,BIP_Non_UK,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format((tender_id), (tender_url),(sourcecw),(authorityName1), (tender1), (origin), (noticesector),(country_code), (country), (tender_name), (industry_sector), (title1), (title1),(authorityName1), (pubFlagCW1), (authorityCountry1), (contact1), (contact2), (award_procedure1), (FormattedDate), (cpv_code1), (description1), (authority_reference1),  (deadline_description), (FormattedDate), (deadline_time1), (other_information1), (temp1), (created_date),Awarded_Status, (intend), BIP_Non_UK,(tender_type))
						print (insertQuery)

				
						Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
					

					
if __name__== "__main__":

	program()	