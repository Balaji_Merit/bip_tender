use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use FileHandle;
use JSON::Parse 'parse_json';
use URI::Escape;


# my $ua=LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 }, show_progress=>1);
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
# $ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my $Home_Url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';

my ($Content,$Cookie)=&Getcontent($Home_Url);
my $fh=FileHandle->new("gebiz_Home.html",'w');
binmode($fh);
$fh->print($Content);
$fh->close();
print "$Cookie\n";

my ($ViewState,$form_no,$Viewstategenerator,$Eventvalidation);
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}
if($Content=~m/mojarra\.ab[^>]*?\'action\\\'\s*,\s*\\\'([^>]*?)\\\'\s*,\s*\\\'([^>]*?)\\'\)/is)
{
	$form_no=uri_escape($1);
}

my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt179_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt179_searchBarList_HIDDEN-SUBMITTED-VALUE=&contentForm%3Aj_idt194_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt194_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt194_selectManyMenu-HIDDEN-ACTION-INPUT=&contentForm%3Aj_idt198_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt198_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt198_selectManyMenu-HIDDEN-ACTION-INPUT=&contentForm%3Aj_idt202_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt202_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt202_selectManyMenu-HIDDEN-ACTION-INPUT=&javax.faces.ViewState='.$ViewState.'&contentForm%3Aj_idt182=contentForm%3Aj_idt182: undefined';

my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my ($Result_content)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
my $fh=FileHandle->new("gebiz_Search.html",'w');
binmode($fh);
$fh->print($Result_content);
$fh->close();

my ($ViewState,$form_no,$Viewstategenerator,$Eventvalidation);
Again:
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}
while($Result_content=~m/<span\s*id\s*=\s*\"(contentForm\:[^>]*?\:[^>]*?\:[^>]*?)\"\s*>/igs)
{
	my $form_no=uri_escape($1);
	print "form_no: $form_no\n";

	my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&'.$form_no.'='.$form_no.': undefined';

	my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my ($Result_content)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
	my $fh=FileHandle->new("gebiz_Details.html",'w');
	binmode($fh);
	$fh->print($Result_content);
	$fh->close();
}
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}

my ($contentForm1,$contentForm2,$contentForm3);
if($Result_content=~m/<input\s*id\s*=\s*\"([^>]*?)\"\s*type[^>]*?value\s*=\s*\"Next\"[^>]*?mojarra\.ab[^>]*?\'action\\\'\s*,\s*\\\'([^>]*?)\\\'\s*,\s*\\\'([^>]*?)\s+contentForm[^>]*?>/is)
{
	$contentForm1=uri_escape($1);
	$contentForm2=uri_escape($2);
	$contentForm3=uri_escape($3);
	print "$contentForm1\n";
	print "$contentForm2\n";
	print "$contentForm3\n";
}

my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&contentForm%3Aj_idt748_select=0&javax.faces.ViewState='.$ViewState.'&javax.faces.source='.$contentForm1.'&javax.faces.partial.event=click&javax.faces.partial.execute='.$contentForm1.'%20'.$contentForm2.'&javax.faces.partial.render='.$contentForm3.'%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';

my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
($Result_content)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
my $fh=FileHandle->new("gebiz_Next.html",'w');
binmode($fh);
$fh->print($Result_content);
$fh->close();
<stdin>;
goto Again;

sub Getcontent
{
	my $url = shift;
	my $Cookie = shift;

	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	my $res = $ua->request($req);
	my %res=%$res;
	my $headers=$res{'_headers'};
	my %headers=%$headers;
	my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "$Cookie[0]";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
	return ($content,$Cookie);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Referer=shift;
	my $PostContent=shift;
	my $Cookie=shift;
	print "Cookie: $Cookie\n";
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=>"www.gebiz.gov.sg");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");
	$req->header("Cookie"=>"$Cookie");
	# $req->header("Cookie"=>"__cfduid=de27cff753127248db3d7a9ff4787a2f71494575784; wlsessionid=SJX8GoUSnJhqOxxpgHZr1mUU2IVaK8CQ8aeNuXQ7EqRIVbLoW4F7!247358590; BIGipServerPTN2_PRD_Pool=52519072.47873.0000");
	$req->content("$PostContent");
	my $res = $ua->request($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}
