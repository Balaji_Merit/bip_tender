import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import re
import imp
import pymysql
import time
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
# import warnings
# warnings.filterwarnings('ignore')
# reload(sys)

h = HTMLParser()
# sys.setdefaultencoding('utf8')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

###

book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Place")
sheet2 = book.add_sheet("boamp")
sheet1.write(0, 0, "Orgin")
sheet1.write(0, 1, "Title")
sheet1.write(0, 2, "AwardProcedure")
sheet1.write(0, 3, "categories")
sheet1.write(0, 4, "desc")
sheet1.write(0, 5, "deadline_date")
sheet1.write(0, 6, "deadline_time")
Tender_ID = 538

sheet2.write(0, 0, "Orgin")
sheet2.write(0, 1, "Title")
sheet2.write(0, 2, "AwardingAuthority")
sheet2.write(0, 3, "description")
sheet2.write(0, 4, "AuthorityRefNo")
sheet2.write(0, 5, "Deadline_date")
sheet2.write(0, 6, "Deadline_time")
sheet2.write(0, 7, "Link")

orgin_Boamp = 'European/Non Defence'
orgin_place = 'European/Defence'
sector = 'European Translation'
country = 'FR'
dead_line_W_Boamp = 'Deadline for receipt of tenders:'
summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: Tender Page Link'

date_mail_re = 'Date\:\s*([\d\-\s+]*?)\s+([\d\:]*?)\s+GMT'
# gmail_sender_re = 'From:[\w\W]*?gmail\_sendername\">([^>]*?)[\s|<]'
gmail_sender_re = 'From:\s*([^>]*?)[\s*|<]'
block_re = '(<td\s*valign\=\"top\">[\w\W]*?Voir)'
details_block_re = '(<td[\w\W]*?<\/td>)'
sub_detail_re = '(<p[\w\W]*?<\/p>)'
# boamp_blocks_re ='class\="m[^>]*?corps[^>]*?\">([\w\W]*?)<\/tr>'
boamp_blocks_re ='<tr[^>]*?>([\w\W]*?)<\/tr>'
b_link_re = '<a\s*href\="([^>]*?)"'
title_re = '<h1>([^>]*?)<\/h1>'
article_re = '<article\s*role\=\"article\">([\w\W]*?)<\/article>'
awardingAuthority_re = 'class="detail-avis detail-main-1">([\w\W]*?)Objet'
object_contract_re = '<h3>Objet[\w\W]*?<p>([\w\W]*?)</p>'
desc_re = 'class\="detail\-avis\s*detail\-main\-2\">([\w\W]*?)<\/div>'
autho_ref_re = 'class\=\"avis\-ref\">([^>]*?)<\/p>'
# deadline_date_re = '<h3>Date\s*limite[\w\W]*?<p>([/^\d{2}\/\d{2}\/\d{4}$/]*?)\s+'
deadline_date_re = '<h3>Date\s*limite[^>]*?<\/h3>\s*<p>(\d{1,2})\/(\d{1,2})\/(\d{4})\s+'
deadline_time_re = '<h3>Date\s*limite\s*de[^>]*?<\/h3>[\w\W]*?<p>[\w\W]*?\s*(\d{1,2}h\d{2})'
mail_date_re = '[^>]*?\,\s+([\d]{2}\s+[^>]*?\s+[\d]{4})\s+[^>]*?'
###
'''
PCP = Procedure Category Published on
ROO = Reference | Entitled Object Organization
DL = Deadline for submission of plies
'''
EMAIL_ACCOUNT = "bip1@meritgroup.co.uk"
# EMAIL_ACCOUNT = "mohamed.sulaiman@meritgroup.co.uk"
PASSWORD = "11merit11"
# PASSWORD = "Merit$786"
row_s1 = 1
row_s2 = 1

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.login(EMAIL_ACCOUNT, PASSWORD)
mail.list()
# mail.select('BIP')
mail.select('BIP')
result, data = mail.uid('search', None, "ALL") # (ALL/UNSEEN)
i = len(data[0].split())

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)
    
def reg_clean(cont):
    cont = str(cont).replace('\r','')
    cont = str(cont).replace('\n','')
    cont = str(cont).replace('\t','')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'","''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'","''")
    cont = cont.replace("î","'")
    cont = cont.replace("l'","l''")
    cont = cont.replace("L'","L''")
    cont = cont.replace("d'","d''")
    cont = cont.replace("D'","D''")
    cont = cont.replace("'s","''s")
    cont = cont.replace("du'","du''")
    cont = cont.replace("N'","N''")
    cont = cont.replace("'acheteur","''acheteur")
    cont = cont.replace("'id","''id")
    cont = cont.replace("'b'","'b''")
    cont = cont.replace("'Esp","''Esp")
    cont = cont.replace("'RD","''RD")
    # cont = str(cont).replace("\’","''")
    cont = cont.replace("’","''", re.I)
	# cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''","''")
    cont = cont.replace("'''''","''")
    cont = cont.replace("''''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("\xc3\xa9","é")
    cont = cont.replace("&#39;","''")
    cont = cont.replace("&quot;",'"')
    cont = cont.replace("î","'")
	
    # cont = cont.replace("'","''", re.I)
    # cont = str(cont).decode(encoding='UTF-8',errors='strict')
    return cont
def cont_encode(cont):
    cont = "u'"+ cont
    cont = cont.decode('utf-8')
    return cont
    
def req_content(url):
    try:
        req_url = ur.urlopen(url)
        url_content = req_url.read().decode('utf-8',errors='ignore')
        return url_content
    except Exception as e:
        print ('Errors:', e)
        i = 0
        while i < 3:
            try:
                req_url = requests.get(url)
                url_content = req_url.content
                return url_content.decode('utf-8',errors='ignore')
            except:
                i = i +1
        return None

def regxx(regx,content):
    cont_data = re.findall(regx,content)
    if cont_data:
        cont_data = cont_data[0] 
    else:
        cont_data = ''
    return h.unescape(cont_data)


def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec
    
def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()
    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(str(InputText))
                translated_sentence = translated.text

            except Exception as e:
                # print ('Error translate 1**',e)
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'fr'
                               }
                    # Log file to check google translation
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write('\n=================================1============================\n')
                        #dees.write(str(payload))
                        #dees.write('\n==================================1===========================\n')

                    s = requests.post(url, data=payload)
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write(str(s.text))
                        #dees.write('\n------------------------------1--------------------------------\n')

                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    print ('Error translate 2 **', e)
                    #raw_input('***raw_input*****')
    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            # print ("translated_sentence:::",InputText,translated)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:

            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                payload = {'q': str(InputText),
                           'target': 'en',
                           'source' : 'fr'
                            }
                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write('\n=============================2================================\n')
                    #dees.write(str(payload))
                    #dees.write('\n=============================2================================\n')

                s = requests.post(url, data=payload)

                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write(str(s.text))
                    #dees.write('\n----------------------------2----------------------------------\n')
                value = s.json()
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                redis_con.set(InputText, translated_sentence)
            except Exception as e:
                print ('**translate 4 **', e)
                #raw_input('***raw_input*****')

    return translated_sentence  

    
    
def place(body_msg):
    global row_s1
    Tender_ID = 539
    process_id = 539
	
    blocks = re.findall(block_re,body_msg)
    date_mail = regxx(date_mail_re,body_msg)
    for s1,block in enumerate(blocks):
        details = re.findall(details_block_re,block)
        PCP = details[0]
        if PCP:
            PCPs = re.findall(sub_detail_re,PCP)
            #### PCPs = translateText(PCPs)

            procedure = reg_clean(PCPs[0])
            PCPs = translateText(procedure)
            
            categories = reg_clean(PCPs[1])
            CVP_Code = categories
            CVP_Code = translateText(CVP_Code)
            
            Published_on = reg_clean(PCPs[2])
            
        ROO = details[1]
        if ROO:
            ROOs = re.findall(sub_detail_re,ROO)
            reference = reg_clean(ROOs[0])  
            title = reference
            title = translateText(title)
            
            objet = reg_clean(ROOs[1])
            organisme = reg_clean(ROOs[2])
            desc = organisme
            desc = translateText(desc)
        DL = details[2]
        if DL:
            DLs = re.findall(sub_detail_re,DL)
            date = reg_clean(DLs[0])            
            time = reg_clean(DLs[1]) 
        awardProcedure = procedure
        
        # autho_ref = ref_no
        date_mail = date_mail
        
        # sheet1.write(row_s1, 0, orgin_place)
        # sheet1.write(row_s1, 1, title)
        # sheet1.write(row_s1, 2, awardProcedure)
        # sheet1.write(row_s1, 3, CVP_Code)
        # sheet1.write(row_s1, 4, Published_on)
        # sheet1.write(row_s1, 4, ref_no)
        # sheet1.write(row_s1, 4, desc)
        # sheet1.write(row_s1, 5, date)
        # sheet1.write(row_s1, 6, time)
        # row_s1 = row_s1 + 1
        print ("-----place-{}-----".format(s1))
        
        # insertQuery = =  u"insert into "+ str(tender_data) +" (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,try_Cast(? as datetime),try_Cast(? as datetime),?,?,?)"
        
        # Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
        
        # input("^^")
        
        # return process_id,awardProcedure,title,CPV_code,autho_ref,date_mail

def boamp(body_msg):
    global row_s2
    Tender_ID = 538
    process_id = 538
    boamp_blocks = re.findall(boamp_blocks_re,body_msg)

    for s2,boamp_block in enumerate(boamp_blocks):
        print ("-----boamp-{}-----".format(s2))
        b_link = re.findall(b_link_re,boamp_block)
        if b_link:                         
            b_link = b_link[0]

            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, b_link)
            if Duplicate_Check == "N":
                link_content = req_content(b_link)
                # link_content = str(link_content)
                # sheet2.write(row_s2, 7, b_link)
                # sheet2.write(row_s2, 0, orgin_Boamp)
                # article = regxx(article_re,link_content)
                # article = reg_clean(article)
                
                title = regxx(title_re,link_content)
                title = reg_clean(title)
                title = translateText(title)
                title = reg_clean(title)
                # sheet2.write(row_s2, 1, title)
                
                
                awardingAuthority = regxx(awardingAuthority_re,link_content)
                print (awardingAuthority)
                if awardingAuthority:
                # input("************************")
                    awardingAuthority1 = reg_clean(awardingAuthority)
                # print (awardingAuthority)
                # input("************************")
                # sheet2.write(row_s2, 2,h.unescape(awardingAuthority1))
                
                    desc = regxx(desc_re,link_content)
                    desc = reg_clean(desc)
                    desc = translateText(desc)
                    desc = reg_clean(desc)
                # sheet2.write(row_s2, 3, desc)
                
                    autho_ref = regxx(autho_ref_re,link_content)
                    autho_ref = reg_clean(autho_ref)
                
                # sheet2.write(row_s2, 4, autho_ref)
                
                    deadlinedate = regxx(deadline_date_re,link_content)
                    
                    # deadlinedate  = deadlinedate[0].strptime('Y-%m-%d %H:%M:%S')
                    print (deadlinedate)
                    if deadlinedate:
                        deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
                        dt = dateparser.parse(deadlinedate)
                        deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                        # print (deadlinedate)
                # dead_line_time = dead_line_date[0][1]
                # sheet2.write(row_s2, 5, deadlinedate)       
                
                    deadlinetime = regxx(deadline_time_re,link_content)
                    deadlinetime = deadlinetime.replace('h',':')
                    summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
                    summary_cont = summary_cont +' '+ str(b_link)
                    print (summary_cont)
                # sheet2.write(row_s2, 6, deadlinetime)
                    curr_date = dateparser.parse(time.asctime())
                    created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                
					# print (summary_cont)
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link, str(), orgin_Boamp, sector, country, str(),title, title, awardingAuthority1, str(), str(), str(),desc, str(), autho_ref, str(), dead_line_W_Boamp, deadlinedate, deadlinetime,str(), summary_cont, 'Y',created_date)
                    print (insertQuery)
					
                    Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # row_s2 = row_s2 + 1
                # book.save('my_book_v9.xls')
            else:
                print ("Duplicate data found ")
for x in range(i):
    # conn = pymysql.connect(host='172.27.138.250', port=3306, user='root', password='admin', db='BIP_tender_analysis',use_unicode=True, charset="utf8")
    # cur = conn.cursor()
    latest_email_uid = data[0].split()[x]
    result, email_data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    # result, email_data = conn.store(num,'-FLAGS','\\Seen') 
    # this might work to set flag to seen, if it doesn't already
    raw_email = email_data[0][1]
    raw_email_string = raw_email.decode('utf-8')
    email_message = email.message_from_string(raw_email_string)

    # Header Details
    date_tuple = email.utils.parsedate_tz(email_message['Date'])
    if date_tuple:
        local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        local_message_date = "%s" %(str(local_date.strftime("%a, %d %b %Y %H:%M:%S")))
    email_from = str(email.header.make_header(email.header.decode_header(email_message['From'])))
    email_to = str(email.header.make_header(email.header.decode_header(email_message['To'])))
    subject = str(email.header.make_header(email.header.decode_header(email_message['Subject'])))
    # print (local_message_date)
    # Body details
    for part in email_message.walk():
        if part.get_content_type() == "text/html":
            body = part.get_payload(decode=True)
            body_msg = body.decode('UTF-8',errors='ignore')
            # print (body_msg)
            # body_msg = body.decode('UTF-8')
            gmail_sender = re.findall(gmail_sender_re,body_msg,re.I)
            if gmail_sender:
                gmail_sender = gmail_sender[0]
                print (gmail_sender)
            # print (x)
            # input('*************')
            file_name = "Email_"+"dfd"+".html"
			
            output_file = open(file_name, 'w')
            output_file.write("From: %s\nTo: %s\nDate: %s\nSubject: %s\n\nBody: \n\n%s" %(email_from, email_to,local_message_date, subject, body_msg))
            output_file.close()
            dates = re.findall(mail_date_re,local_message_date)
            print ("Gmail message date",local_message_date)
            print ("Date",dates)
            # input("************************")
            # yesterday_date = datetime.now() - timedelta(days=1)
            yesterday_date = date.today() - timedelta(0)
            print ("Yesterdaydate",yesterday_date)
            # input("before***********************") 

            yesterday_date = yesterday_date.strftime('%d %b %Y')
            print ("Yesterdaydate",yesterday_date)
            # input("***********************") 
            print ("Yesterdaydate",yesterday_date)
            print ("Mail date :",dates[0])
            if yesterday_date == dates[0]:
                print ("Equal")
                if gmail_sender == str('PLACE'):
                    # places = place(body_msg)
                    pass
                elif gmail_sender == str('BOAMP'):
                    
                    borders = boamp(body_msg)
                    # print (body_msg)
                else:
                    print ("INVALIED Email -->",gmail_sender)
            else:
                print ("differnt date")        
            # if gmail_sender == str('PLACE'):
                # places = place(body_msg)
                # pass
            # elif gmail_sender == str('BOAMP'):
                # borders = boamp(body_msg)
            # else:
                # print ("INVALIED Email -->",gmail_sender)
        else:
            continue
# book.save('my_book_v10.xls')            
         
            
        