#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="http://www.americasbiz.net/bid_opportunities/";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=1;
my $Tender_content2;
#### To get the tender link ####
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print $month1;
my $year1		= strftime "%y", localtime;
print $year1;
my @state1=("1","2","68","4","3","5","6","7","8","9","10","11","69","12","13","14","15","16","17","18","19","20","21","22","23","71","24","25","26","27","70","28","29","30","31","32","33","34","35","36","37","38","39","65","40","41","42","43","44","45","46","67","47","48","49","50","51");
my $date2=$date1;
$date2="0$date2" if(length($date2)==1);
print $date2;
my $month2=$month1;
# $month1 = "0$month1" if (length($month1)==1);
$month2="0$month2" if(length($month2)==1);
my $tender_category_content1;
# while($Tender_content1=~m/<li><a href="([^>]*?)"><span\s*/igs)
# {
# my $Tender_content3_page=$1;
# my $Tender_content3=&Getcontent($Tender_content3_page);
# while($Tender_content3=~m/<li>\s*<a\s*href="([^>]*?)"\s*title/igs)
			# {
			# my $tender_state_page=$1;
			# my $tender_link=$1; 
			my ($tender_link_content1,$Referer);
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
			# my $tender_link_content2=&Getcontent($tender_state_page);
for(my $i=0;$i<=$#state1;$i++)
{
my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	my $tender_title;
my $tender_link;	
			# while($tender_link_content2=~m/<li>\s*<a\s*href="([^>]*?)"\s*title/igs)
			# {
			# my $tender_day_content2=$1;
			my $tender_day_content2="http://www.americasbiz.net/bid_opportunities/20".$year1."/".$month2."/".$date2."/state/".$state1[$i]."/";
			# my $tender_day_content2="http://www.americasbiz.net/bid_opportunities/2017/10/08/state/".$state1[$i]."/";
			my $tender_link_content2=&Getcontent($tender_day_content2);
			while($tender_link_content2=~m/<td><a\s*href="([^>]*?)">/igs)
			{
			$tender_link=$1;
			$tender_link_content1=&Getcontent($tender_link);
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
			if($tender_link_content1=~m/<h2>([^>]*?)<\/h2>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		if($tender_link_content1=~m/<span class="titlesize red bold">\s*([^>]*?)<\/span>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
					if (length($tender_title)>600)
	{
		$tender_title = substr $tender_title, 0, 500;
	}
		next if($tender_title=~m/\*UPDATED\s*\*/is);
		if($tender_link_content1=~m/Buyer:<\/span>([\w\W]*?)Bid Pkg Source:/is)
		{
		$Awarding_Authority=$1;
		}
		# elsif($tender_link_content1=~m/Organization:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Awarding_Authority=$1;
		# }
		if($tender_link_content1=~m/Owner Solic Number:<\/span><span class="hideDesktop"><br\/><\/span>\s*([^>]*?)\s*<span/is)
		{
		$Authority_Reference=$1;
		# $Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}

		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		# if($tender_link_content1=~m/Procedure:\s*<\/dt>\s*<dd>([^>]*?)<\/dd>/is)
		# {
			# $award_procedure=$1;
			# $award_procedure = &BIP_Tender_DB::clean($award_procedure);
			# $award_procedure=&clean($award_procedure);
		# }
				if (length($Awarding_Authority)>3000)
	{
		$Awarding_Authority = substr $Awarding_Authority, 0, 2900;
	}
		if($tender_link_content1=~m/Scope:<\/span>([\w\W]*?)Notes:<\/span>/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		if($tender_link_content1=~m/Scope:<\/span>([\w\W]*?)<\/table>/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		if($tender_link_content1=~m/Location:<\/span>([\w\W]*?)<\/td><\/tr>/is)
		{
		$Location=$1;
		$Location=&clean($Location);
		$Location=&Trim($Location);
		$Location=&BIP_Tender_DB::clean($Location);
		}
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
		if($tender_link_content1=~m/Bid Date & Time:<\/span><span class="hideDesktop"><br\/><\/span>\s*([^>]*?)\s*<\/td><\/tr>/is)
		{
		my $temp_dt=$1;
	
		if($temp_dt=~m/(\d{1,2})\/(\d{1,2})\/(\d{2})\s+(\d{1,2}\:\d{1,2}\s*(?:pm|PM|am|AM))/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/20".$3);
		
		$Deadline_time=$4;
		}
		elsif($temp_dt=~m/(\d{1,2})\/(\d{1,2})\/(\d{2})/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/20".$3);
		}
		}
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		elsif($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
	
		# if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		# {
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))/is)
		# {
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		# $Deadline_time=$4;
		# }
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
			# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
		
		$Deadline_Description="Bid Date & Time:";
		
		print $tender_link;
		
		

	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/SELECT STATE([\w\W]*?)HERE/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	  
	if($tender_link_content1=~m/<tr><td data-th="Bid Date & Time: ">([\w\W]*?)Click\s*<a\s*href[^>]*?title="List of our other Free web sites administered by NAPC">/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='US';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# }
}
}
# }
# }
# while($Tender_content1=~m/<li><a href="([^>]*?)"><span\s*>/igs)
# {
# my $Tender_Url=$1;
# my ($Tender_content3)=&Getcontent($Tender_Url);
# $Tender_content3 = decode_utf8($Tender_content3);
# $Tender_content1=$Tender_content3;
# goto top;
# }
# }
# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "www.merx.com");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.americasbiz.net");
	# $req->header("Referer"=> "https://www.etenders.gov.eg/Tender/DoSearch?status=3");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.merx4.merx.com");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://sasktenders.ca/content/public/Search.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}