#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use HTTP::Cookies;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
my $cookie = HTTP::Cookies->new(file=>$0."_cookies.txt", autosave => 1);
$ua->cookie_jar($cookie);
# my $cookie_session = $cookie->get_cookie('JSESSIONID');
# my $session_id = $cookie_session->val;
# print "Session ID :: $session_id\n";

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
# my ($Tender_content1)=&Getcontent($Tender_Url);
# my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?utf8=%E2%9C%93&isSearchAndFilter=true&filterOpen=false&s=new+opportunities&commit=Find+opportunities");
# my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?s=&types%5B%5D=aid-funded-business&types%5B%5D=private-sector&types%5B%5D=public-sector&sort_column_name=first_published_at");
my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?utf8=%E2%9C%93&s=&sort_column_name=first_published_at");
# my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?s=new+opportunities&sort_column_name=first_published_at");

$Tender_content1 = decode_utf8($Tender_content1);  


	re:
	# while ($Tender_content1=~m/<a[^>]*?href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/dt>/igs)
    while ($Tender_content1=~m/<a class="title" href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>/igs)
	{
		my $tender_link = "https://opportunities.export.great.gov.uk".$1;
		my $temp_title  = ($2);
		print "#######################################################\n";
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 
		$Tender_content =~s/<\!\-\-[\w\W]*?\-\->//igs;
		open(ts,">Tender_content_22_c.html");
		print ts "$Tender_content";
		close ts;
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		if($Tender_content=~m/<h1[^>]*?>\s*([\w\W]*?)\s*<\/h1>/is)
		{	
			$tender_title = &clean($1);
		}
       
        if($Tender_content=~m/CPV\s*\:\s*\s*([^>]*?)\s*<\/dd>/is)
		{	
			$CPV_Code = &clean($1);
		}
		# if ($Tender_content=~m/<h4[^>]*?>\s*(Response[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
        if ($Tender_content=~m/<dt class='expiry'>\s*(Opportunity closing date)\s*<\/dt>\s*<dd class='expiry'>\s*([^>]*?)\s*<\/dd>/is)
		# if ($Tender_content=~m/<h4[^>]*?>\s*((?:Response|Expiry)[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}
		elsif($Tender_content=~m/<h4[^>]*?>\s*(Expiry\s*date)[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)	
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}		
		print "TenderTitle :: $tender_title\n";
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<\/div>\s*<\/div>/is)
		{
			$Description = ($1);
			$Description =~s/<[^>]*?block\s*value\">[\w\W]*?$//igs;
			$Description =~s/\s*(<p[^>]*?inline\">[^>]*?<\/p>)\s*/$1\; /igs;
			$Description =~s/\s*For\s*(?:further|more)?\s*information[^>]*?<[^>]*?>//igs;
			$Description =~s/\s*Please\s*register\s*your\s*interest[^>]*?<[^>]*?>//igs;
			$Description =~s/\&\#8211\;/-/igs;
			$Description = &clean($Description);
			$Description =~s/\s;/;/igs;
		}
		
		my $DescriptionTemp;
		if ($Tender_content=~m/Essential\s*information<\/h3>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
		{
			$DescriptionTemp = ($1);
			
			$DescriptionTemp =~s/\&\#8211\;/-/igs;
			$DescriptionTemp = &clean($DescriptionTemp);
			$DescriptionTemp =~s/\s;/;/igs;
		}
		
		
		
		# if ($Tender_content=~m/<h4[^>]*?>\s*Value[^>]*?<\/h4>\s*([\w\W]*?)<\/p>/is)
		if ($Tender_content=~m/<dd class='value'>\s*([\w\W]*?)<\/dd>/is)
		{
			$Value = &clean($1);
		}
		# if ($Tender_content=~m/<dt>Market[^>]*?<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is)
        if ($Tender_content=~m/Your guide to exporting\s*<\/dt>[\w\W]*?href=[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/li>/is)
        
        
		{
			$Location = &clean($1);
		}
		elsif ($Tender_content=~m/<h4[^>]*?>\s*Your\s*guide\s*to\s*exporting[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)
		{
			$Location = &clean($1);
		}
		
		my ($tenderblock);
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<\/div>\s*<\/div>/is)
		{
			$tenderblock = $1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		$tenderblock = $DescriptionTemp."\n".$tenderblock if ($DescriptionTemp ne '');

		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
		# exit;
	}	

  if($Tender_content1=~m/<a\s* class="next"\s*rel\=\"next\"\s*href\=\"([^>]*?)\">[^>]*?<\/a>/is)
     {
        my $nexturl="https://opportunities.export.great.gov.uk".$1;
        $nexturl=~s/\&amp\;/&/igs;
		my ($Tender_content_next)=&Getcontent($nexturl);
		$Tender_content_next = decode_utf8($Tender_content_next);  
		$Tender_content1=$Tender_content_next;
        goto re;
     }		

  	 
	
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"opportunities.export.great.gov.uk");
	$req->header("Content-Type"=> "text/html;charset=utf-8");
	# $req->header("Cookie"=> "JSESSIONID=IQWc8wgu5zuaAg6auxZ7zA__");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;

	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->header("Cookie"=> "JSESSIONID=IQWc8wgu5zuaAg6auxZ7zA__");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	decode_entities($Clean);
	return $Clean;
}