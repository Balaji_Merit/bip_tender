#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';
my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# $Tender_Url="http://www.mmd.admin.state.mn.us/process/admin/postings.asp";
# $Tender_ID='527';
# $TenderName ="Pakistan: Sui Southern Gas Company Limited";
my $x=1;
my $y=25;
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
$Tender_Url="http://www.mmd.admin.state.mn.us/process/admin/postings.asp";
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content520.html");
print ts "$Tender_content1";
close ts;
my $count=1;
#### To get the tender link ####
top:

while($Tender_content1=~m/<B><FONT CLASS=bodyMain>([\w\W]*?)<HR NOSHADE SIZE="1">/igs)
{
my $tender_block=$1;

		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		if($tender_block=~m/<B>TITLE[^>]*?<\/B>([^>]*?)<BR>/is)
		{
		$tender_title=$1;
		}
			if($tender_block=~m/<B>PURCHA[^>]*?<\/B>([^>]*?)<BR>/is)
		{
		$Awarding_Authority=$1;
		$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
		$Awarding_Authority=&clean($Awarding_Authority);
		}	


	if($tender_block=~m/REFERENCE NUMBER:[\w\W]*?CLASS=boldRed>([^>]*?)<\/FONT>/is)
		{
		$Authority_Reference="REFERENCE NUMBER: ".&clean($1);
		}
			if($tender_block=~m/<B>SOLICITATION NUMBER:[^>]*?<\/B>([^>]*?)<BR>/is)
		{
		$Authority_Reference=$Authority_Reference." SOLICITATION NUMBER: ".&clean($1);
		}
	if($tender_block=~m/(RESPONSE TO THIS SOLICITATION IS DUE NO LATER THAN)\s*<\/B>\s*([^>]*?)\s*<B>\s*AT\s*<\/B>([^>]*?)\s*Central Time<BR>/is)
		{
		$Deadline_Description=$1;
		$Deadline_Date=$2;
		my $d_date=$3;
		if($Deadline_Date=~m/(\d{1,2})\/(\d{1,2})\/(\d{2,4})/is)
		{
		$Deadline_Date=$2."-".$1."-".$3;
		}
		# $Deadline_Date=&clean($Deadline_Date);
		# $Deadline_Date=~s/\s+\s+/ /igs;
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($Deadline_Date);
		if($d_date=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($d_date=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		}
		# $Deadline_Description="Closing Date:";

	if($tender_block=~m/<B>NOTE[^>]*?<\/B>([^>]*?)<BR>/is)
	{
	$Description=$1;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	# $Description=~s/\-//igs;
	}
$Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
	$Authority_Reference=&clean($Authority_Reference);
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	my $tender_link="http://www.mmd.admin.state.mn.us/process/admin/postings.asp";
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock)=$tender_block;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	$cy="US";
	

		# open (RE,">>Digital527.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
# }
my $Tender_Url1="http://www.mmd.admin.state.mn.us/process/admin/page2List.asp";
my ($Tender_content2)=&Getcontent($Tender_Url1);
$Tender_content2 = decode_utf8($Tender_content2);
while($Tender_content2=~m/Begin pulling data([\w\W]*?)End data stream/igs)
{
my $tender_block=$1;

		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		if($tender_block=~m/<B>TITLE[^>]*?<\/B>([^>]*?)<BR>/is)
		{
		$tender_title=$1;
		}
			if($tender_block=~m/<B>CONTRACTING AGENCY:[^>]*?<\/B>([^>]*?)<BR>/is)
		{
		$Awarding_Authority="CONTRACTING AGENCY:".$1;
		$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
		$Awarding_Authority=&clean($Awarding_Authority);
		}	

			if($tender_block=~m/<B>CONTACT PERSON:[^>]*?<\/B>([^>]*?)\s*<BR><B>CONTACT PHONE:<\/B> ([^>]*?)\s*<BR>/is)
		{
		$Awarding_Authority=$Awarding_Authority." CONTACT PERSON: ".$1;
		$Awarding_Authority=$Awarding_Authority." CONTACT PHONE: ".$2;
		$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
		$Awarding_Authority=&clean($Awarding_Authority);
		}
	if($tender_block=~m/REFERENCE NUMBER:[\w\W]*?class=boldred>([^>]*?)<\/font\s*color>/is)
		{
		$Authority_Reference=&clean($1);
		}
		
	if($tender_block=~m/(RESPONSE TO THIS SOLICITATION IS DUE NO LATER THAN)\s*<\/B>\s*([^>]*?)\s*<b>\s*AT\s*<\/b>([^>]*?)\s*CENTRAL TIME.<BR>/is)
		{
		$Deadline_Description=$1;
		$Deadline_Date=$2;
		my $d_date=$3;
		if($Deadline_Date=~m/(\d{1,2})\/(\d{1,2})\/(\d{2,4})/is)
		{
		$Deadline_Date=$2."-".$1."-".$3;
		}
		# $Deadline_Date=&clean($Deadline_Date);
		# $Deadline_Date=~s/\s+\s+/ /igs;
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($Deadline_Date);
		if($d_date=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($d_date=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		}
		# $Deadline_Description="Closing Date:";

	if($tender_block=~m/DESCRIPTION:([\w\W]*?)<BR>/is)
	{
	$Description=$1;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	# $Description=~s/\-//igs;
	}

my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	my $tender_link="http://www.mmd.admin.state.mn.us/process/admin/postings.asp";
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock)=$tender_block;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	
	$cy="US";

		# open (RE,">>Digital527.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}  

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "esbd.cpa.state.tx.us");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "esbd.cpa.state.tx.us");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://sasktenders.ca/content/public/Search.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\-//igs;
	decode_entities($Clean);
	return $Clean;
}