#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Date::Parse;
use POSIX qw(strftime);
use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

my $fulldate = strftime "%d %m %Y %A", localtime;
my $day = (split(" ",$fulldate))[-1];
print "Day :: $day\n";

my ($day1,$mon1,$year1) = &DAY_Check($day);
# print "Inputs :: $today1<=>$today3\n";
# my $today2 = $today1;
# my $today2 =~s/\-/%2F/igs;
# my $today4 = $today3;
# my $today4 =~s/\-/%2C/igs;

# print $today2;
# <>;
sub DAY_Check()
{
	my $DAYY = shift;
	
	my $today = time;
	print "Today::$today\n";
	
	if ($DAYY=~m/Wednesday|Thursday|Friday|Saturday|Sunday|Monday|Tuesday/is)
	{
		my $yesterday = $today;
		# my $yesterday = $today;
		
		
		my $today1 = strftime "%d", ( localtime($yesterday) );
		my $today2 = strftime "%m", ( localtime($yesterday) );
		my $today3 = strftime "%Y", ( localtime($yesterday) );
		print "$today1 = $today3\n";
		return ($today1,$today2,$today3);
	}
	if ($DAYY=~m/Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday/is)
	{
		my $yesterday = $today;
		my $today1 = strftime "%d-%m-%Y", ( localtime($yesterday) );
		my $today3 = strftime "%Y-%m-%d", ( localtime($yesterday) );
		print "$today1 = $today3\n";
		return ($today1,$today3);
	}
}
# exit;


### Ping the first level link page ####
# my $Tender_Url='https://www.sell2wales.gov.wales/search/Search_MainPageAdv.aspx';
# my $Tender_Url='https://www.sell2wales.gov.wales/search/search_mainpage.aspx?results=true';
my $Tender_Url='https://uk.eu-supply.com/ctm/Supplier/PublicTenders/PublicTenders';
# my $post_cont="SearchFilter.SortField=None&SearchFilter.SortDirection=None&SearchFilter.ShortDescription=&SearchFilter.Reference=&SearchFilter.TenderId=0&SearchFilter.OperatorId=4&Branding=BLUELIGHT&SavedCategoryId=0&SavedUnitAndName=&SearchFilter.PublishType=1&SearchFilter.BrandingCode=&TextFilter=&SearchFilter.FromDate=".$today2."&SearchFilter.ToDate=".$today2."&SearchFilter.ShowExpiredRft=false&SearchFilter.PagingInfo.PageNumber=1&SearchFilter.PagingInfo.PageSize=100";
my $post_cont='SearchFilter.SortField=None&SearchFilter.SortDirection=None&SearchFilter.ShortDescription=&SearchFilter.Reference=&SearchFilter.TenderId=0&SearchFilter.OperatorId=4&Branding=BLUELIGHT&SavedCategoryId=0&SavedUnitAndName=&SearchFilter.PublishType=1&SearchFilter.BrandingCode=&TextFilter=&SearchFilter.FromDate=".$day1."%2F".$mon1."%2F".$year1."&SearchFilter.ToDate=".$day1."%2F".$mon1."%2F".$year1."&SearchFilter.ShowExpiredRft=false&SearchFilter.PagingInfo.PageNumber=1&SearchFilter.PagingInfo.PageSize=100';

# my $Tender_Url='https://www.sell2wales.gov.wales/search/search_mainpage.aspx?results=true';
my $host="uk.eu-supply.com";

my $Tender_content1=&Postcontent($Tender_Url,$post_cont,$host,$Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content_4_1.html");
print ts "$Tender_content1";
close ts;

		#### To get the tender link ####
		top:
		my $flag_check;
		# while ($Tender_content1=~m/<td>\s*([^>]*?)\s*<\/td>\s*<td>\s*<a href="[^>]*?PID=([^>]*?)\&amp[^>]*?" target="_blank">\s*([^>]*?)\s*<\/a>/igs)
		while ($Tender_content1=~m/<td>\s*<div data-toggle="tooltip"[^>]*?>\s*([^>]*?)\s*<\/div>\s*<\/td>\s*<td>\s*<span[^>]*?><\/span>\s*<a[^>]*?href="([^>]*?)"[^>]*?>([^>]*?)<\/a>\s*<\/td>\s*<td>\s*<div[^>]*?>[^>]*?<\/div>\s*<\/td>\s*<td>\s*<div[^>]*?>\s*s*([^>]*?)\s([^>]*?)\s*<\/div>/igs)
		{
			my $Authority_Reference 		 = &BIP_Tender_DB::clean($1);
			my $tender_link = "https://uk.eu-supply.com/ctm".$2;
			my $tender_title = $3;
			
			# my $block		= $3;
			print $tender_link;
			print $tender_title;
			exit;
					# my $tender_title1			= $3;
		my $Deaddate_first_table	= $4;
		my $Deadtime_first_table	= $5;
			# next if ($block=~m/Contract\s*Award\s*Notice/is);
			# next if ($block=~m/Contract Results/is);
			# next if ($block=~m/Award\s*Notice/is);
			# print "TenderLink :: $tender_link\n";
			# next;
            # next if ($tender_title=~m/Planned Maintenance Contract 460 - Llanfachraeth, Tregele, Pentraeth/is);
            # next if ($tender_title=~m/8000 litre milk buffer tank/is);
            # next if($tender_link=~m/SEP006883/is);
			my ($Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Value,$Sent_to,$Location,$Nuts_Code);
			my ($Tender_content)=&Getcontent($tender_link);
			$Tender_content = decode_utf8($Tender_content); 
			print $Tender_content;
			my ($Deadline_Description,$Deadline_Date,$Deadline_time);
			if ($Tender_content=~m/<span class="celllbl">Response deadline\s*[^>]*?<\/span><br>([^>]*?)\s*(\d{1,2}\:\d{2})<\/td>/is)
			{
				$Deadline_Description ="Response deadline";
				$Deadline_Date	= $1;
				$Deadline_time = $2;
			}
			
			#Duplicate check
            my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
			my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
			if ($duplicate eq 'Y')
			{
				print "\nAlready Existing this Tender Data\n";
				$flag_check = 'Y';
				next;
			}
			else
			{
				print "\nThis is newly Released Tender\n";
			}
			
			
			# my ($Tender_content)=&Getcontent($tender_link);
			# $Tender_content = decode_utf8($Tender_content); 
			# open(ts,">Tender_content472.html");
# print ts "$Tender_content";
# close ts;
# exit;
			# my ($Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Sent_to,$Location);
			# if ($Tender_content=~m/<span class="celllbl">Short description<\/span><br>\s*([^>]*?)\s*\-/is)
			# {
				# $Authority_Reference 		 = &BIP_Tender_DB::clean($1);
			# }
			if ($Tender_content=~m/<span class="celllbl">\s*Buyer\s*<\/span><br>([\w\W]*?)<\/td>\s*<\/tr>\s*<tr valign="top">/is)
			{
				$Awarding_Authority 		 = &BIP_Tender_DB::clean($1);
				$Awarding_Authority 		 = &clean($Awarding_Authority);
				# print $Awarding_Authority;
				# <>;
				
			}
			# exit;
			if ($Tender_content=~m/<span class="celllbl">(Main CPV code<\/span><br>[\w\W]*?)<\/table>/is)
			{
				$CPV_Code 		 = &BIP_Tender_DB::clean($1);
				$CPV_Code 		 = &clean($CPV_Code);
			}
			if ($Tender_content=~m/<span class="celllbl">Type of Contract:<\/span><br>\s*([^>]*?)\s*<\/td>/is)
			{
				$Contract_Type 		 = &BIP_Tender_DB::clean($1);
				$Contract_Type 		 = &clean($Contract_Type);
			}
			if ($Tender_content=~m/<span class="celllbl">Detailed description<\/span><br>([\w\W]*?)span class="celllbl">/is)
			{
				$Description 		 = &BIP_Tender_DB::clean($1);
				$Description 		 = &clean($Description);
			}
			# print "Title :: $tender_title\n";
			# if ($Tender_content=~m/>\s*(Deadline\s*Date)\s*(?:\:)?([\w\W]*?)<\/tr>/is)
			# {
				# $Deadline_Description = &clean($1);
				# $Deadline_Date	= &clean($2);
			# }
			# if ($Tender_content=~m/>\s*Deadline\s*Time\s*(?:\:)?([\w\W]*?)<\/tr>/is)
			# {
				# $Deadline_time = &clean($1);
			# }
			# if ($Tender_content=~m/<span id="[^>]*?DeadlineTime">\s*([^>]*?)\s*</is)
			# {
				# $Deadline_time = $1;
			# }
	
			
			my $tenderblock;
			if ($Tender_content=~m/<span class="celllbl">Short description<\/span>([\w\W]*?)<\/table><\/body><\/html>/is)
			{
				$tenderblock = $1;
			}
			# elsif ($Tender_content=~m/Title\s*\:\s*<\/strong>([\w\W]*?)<\/table>\s*<\/div>\s*<\/div>\s*<\/div>/is)
			# {
				# $tenderblock = $1;
			# }
			$tenderblock = &BIP_Tender_DB::clean($tenderblock);

			&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$Nuts_Code);
		}


	
sub post_parameters()
{
	my $contentsdfs = shift;
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
	if($contentsdfs=~m/__VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	if($contentsdfs=~m/Date_calendar_AD\"\s*value\=\"([^>]*?)\"/is)
	{
		$After_calendar_AD=uri_escape($1);
	}
	print "After_calendar_AD :: $After_calendar_AD\n";
	return ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"uk.eu-supply.com");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	# $Clean=~s/\s*/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	# $Clean=~s/^\s*|\s*$//igs;
	# $Clean=~ s/^\s*,|,\s*$//igs;
	# $Clean=~ s/\:\,/:/igs;
	# $Clean=~ s/\.\,/./igs;
	# $Clean=~s/\,\s*/,/igs;
	# $Clean=~s/^\s*\,+//igs;
	# $Clean=~s/\,+\s*$//igs;
	$Clean=~s/\\n/ /igs;
	# $Clean=~s/\s*/\s+/igs;
	decode_entities($Clean);
	return $Clean;
}