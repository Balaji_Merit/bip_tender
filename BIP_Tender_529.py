﻿# -*- coding: utf-8 -*-
import os, sys
import requests
import re
import xlwt
import imp
import pymysql
import dateparser
import redis
import time
import warnings
import datetime
from datetime import datetime
from datetime import date
import io
warnings.filterwarnings('ignore')
from googletrans import Translator

# reload(sys)
# sys.setdefaultencoding('utf-8')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

base_url = 'http://www.przetargi.egospodarka.pl'
block_re = '<tr>([\w\W]*?)<\/tr>'
title_re = 'Nazwa\s*nadana[\w\W]*?\>\s*([\w\W]*?)(?:<\/span>|<b>)'
dates_re = 'Daty\s*\:\s*publikacji\s*\/\s*zak[^>]*?enia\"\>([^>]*?)\s*\<br\>([^>]*?)\s*<'
sub_url_re = 'Przedmiot zam[\w\W]*?wienia\"\>\s*\<a\s*[\w\W]*?href\=\"([^>]*?)\"\>([^>]*?)\<'
title_attribute_re = 'Nazwa\s*nadana[\w\W]*?">([\w\W]*)<\/span'
name_address_re = 'NAZWA I ADRES:([\w\W]*?)<\/p>'
awardProcedure_re = '(?:zastosowanie_procedury|Zam[\w\W]*?wienie podzielone jest na cz)[\w\W]*?\"\>([\w\W]*?)(?:<\/span>|<\/p>)'
cpv_code_re = 'kod CPV\s*:\s*([\w\W]*?)\s*II'
description_re = 'opis\s*(?:przedmiotu|za[\w\W]*?wienia)[\w\W]*?\:([\w\W]*?)(?:<\/span>|<\/p>)'
reference_re = 'numer_referencyjny">\s*([^>]*?)\s*\<\/span>'
reference_re1 = '(?:<p><b>Nr\:<\/b>|<span id="[^>]*?Ogloszenia">|Numer referencyjny[\w\W]*?<p style="padding-left: 20px">)\s*([^>]*?)\s*<\/'
value_contract_re = 'wartosc_zamowienia_calosc\"\>([^>]*?)<\/span>'
time_read_line = 'Time limit for receipt of tenders or requests to participate:'
d_ra_date_re = 'ctl00\_ContentPlaceHolder1_IV_4_4_data"\>([^>]*?)<\/span>'
d_ra_time_re = 'ContentPlaceHolder1_IV_4_4_godzina">([^>]*?)<\/span>'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
Tender_ID = 529
nxt_re = 'nexpage2[\w\W]*?href\=\"([^>]*?)">nast[\w\W]*?pna[\w\W]*?<\/a>'

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
#print "re----->",Retrive_duplicate


def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'pl'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'pl'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    if isinstance(translated_sentence, str):
        return translated_sentence
    else:	

        return translated_sentence.decode("utf-8")



def reg_clean(desc):
    desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')
    desc = desc.replace('&quot;', '')
    # desc = desc.replace('b''', '')	
    desc = re.sub(r'<[^<]*?>', ' ', str(desc))
    desc = re.sub(r'\r\n', '', str(desc), re.I)
    desc = re.sub(r"\\r\\n", '', str(desc), re.I)
    desc = re.sub(r'\t', " ", desc, re.I)
    desc = re.sub(r'\s\s+', " ", desc, re.I)
    desc = re.sub(r"^\s+\s*", "", desc, re.I)
    desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
    desc = re.sub(r"\&ndash\;", "-", desc, re.I)
    desc = re.sub(r"&quot;", "'", desc, re.I)

    return desc
def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	# time1 = re.findall('(\d{1,2})(?:\:\d{2}\s*|\s*)(?:Noon|noon|pm)',str(Input))
	time1 = re.findall('(\d{1,2})\s*(?:Noon|noon|pm|PM|p.m.)',str(Input))
	print (time1)
	if time1:
		if int(time1[0]) < 12:
			Formattedtime = str(int(time1[0])+12) +":00:00"
		else:
			Formattedtime = str(int(time1[0])) +":00:00"
	print (Formattedtime)
	time3 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:GMT|gmt)',str(Input))
	if time3:
		Formattedtime = str(int(time3[0][0])) +":"+str(int(time3[0][1]))+":00"
	time2 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM|p.m.)',str(Input))
	# print time1[0][0]
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			if time2[0][1]:
				Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+"0:00"
			else:
				Formattedtime = str(int(time2[0][0])+12)  +":00:00"
		else:    
			if int(time2[0][0]) == 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)',str(Input))
        
		if time2:
			if int(time2[0][0]) <= 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			
		else:
	
			time2 = re.findall('(?:Midday|midday|12\s*noon|12.00\s* Noon|12.00\s*noon|noon)',str(Input))
			if time2:
				Formattedtime = "12:00:00"
			else:
				time2 = re.findall('(?:Midnight|midnight)',str(Input))
				if time2:
					Formattedtime = "00:00:00"
				else:
					Formattedtime = Input
					# Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
	# dt=dparser.parse(Input,fuzzy=True)
	# Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
	print (Formattedtime)
	return (Formattedtime)

def req_content(url):
    req_url = requests.get(url)
    url_content = req_url.content.decode('utf-8', errors = 'ignore')
    url_content = req_url.content
    return url_content

def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def main():
    global n
    # global main_url
    # main_req = requests.get('http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance')
    # main_req = requests.get("http://www.przetargi.egospodarka.pl/search/status/1/sort/published_desc/offset/15")
    main_req = requests.get("http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance")
    while True:
        # main_content = req_content(main_url)
		# main_req = requests.get("http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance")
        main_content = main_req.content.decode('latin-1')
        with open("nxt_page1.html","w+") as f:
            f.write(main_content)
        blocks = regxx(block_re,main_content)

        for block in blocks[3:]:
            print ("NO: ", n)
            print (block)
            n = n + 1
            dates = regxx(dates_re,block)

            if dates:
                publication = dates[0][0]
                completion = dates[0][1]
            else:
                publication = ''
                completion = ''

            sub_url = regxx(sub_url_re,block)
            print (sub_url)
            if sub_url:
                sub_urll = sub_url[0][0]
                Ordered_object = sub_url[0][1]
                sub_url_full = base_url + sub_urll
            else:
                Ordered_object = ''
                sub_url_full=''
            print ("Sub-URL:", sub_url_full)	
            sub_content1 = requests.get(sub_url_full)
			
            sub_content = sub_content1.content.decode('latin-1')
            # sub_content = str(sub_content1.content)
            with io.open("nxt_page2.html","w+") as f:
                f.write(sub_content)
            Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)	
            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url_full)
            if Duplicate_Check == "N":
                curr_date = dateparser.parse(time.asctime())
                created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                title_attribute = regxx(title_attribute_re,sub_content)
                if title_attribute:
                    title_attribute = title_attribute[0]
                else:
                    title_attribute = ''
                title = re.findall(title_re,sub_content)
                if title:
                    title = title[0]
                    title = reg_clean(title)
                    title = translateText(title)
                    title = title.replace('&quot;', '"')
                    # title = title.replace("'", "''")
                else:
                    title = ''
                print ("title ",title)
                name_address = regxx(name_address_re,sub_content)
                # print (name_address)
                # input("****")
                if name_address:
                    name_address = name_address[0]
                    name_address = reg_clean(name_address)
                    # name_address = translateText(str(name_address))
                    # name_address = name_address.replace("'", "''")
                    # name_address = name_address.replace("'", "''")
                    # name_address = name_address.replace("'", "''")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("ê","ę")
                    name_address = name_address.replace("¿","ż")
                    name_address = name_address.replace("æ","ć")
                    name_address = name_address.replace("ñ","ń")
                    name_address = name_address.replace("³","ł")
                else:
                    name_address = ''
                awardProcedure = regxx(awardProcedure_re,sub_content)
                if awardProcedure:
                    awardProcedure = awardProcedure[0]
                    awardProcedure = reg_clean (awardProcedure)
                    awardProcedure = str(awardProcedure)
                    # awardProcedure = awardProcedure.replace("'", "''")
                else:
                    awardProcedure = ''
                cpv_code = regxx(cpv_code_re,sub_content)
                if cpv_code:
                    cpv_code = cpv_code[0]
                    cpv_code = reg_clean (cpv_code)
                    cpv_code = translateText(cpv_code)
                    # cpv_code = cpv_code.replace("'", "''")
                else:
                    cpv_code = ''
                description = regxx(description_re,sub_content)
                if description:
                    description = description[0]
                    description = reg_clean (description)
                    description = translateText(description)
                    # description = description.replace("'", "''")
                else:
                    description = ''
                reference = regxx(reference_re,sub_content)
                if reference:
                    reference = reference[0]
                    reference = reg_clean(reference)
                    reference = translateText(reference)
                    if len(reference)>0:
                        reference = "Reference No: " + reference.replace("'", "''")
                    # reference = "Reference No: " + str(reference)
                    else:
                        reference = ''
                else:
                    reference = ''
                reference1 = regxx(reference_re1,sub_content)
                if reference1:
                    reference1 = reference1[0]
                    reference1 = reg_clean(reference1)
                    reference1 = translateText(reference1)
                    reference1 = reference1.replace("'", "''")
                else:
                    reference1 = ''
                reference = str(reference1) + str(reference)
                value_contract = regxx(value_contract_re,sub_content)
                if value_contract:
                    value_contract = value_contract[0]
                    value_contract = reg_clean(value_contract)
                    value_contract = translateText(value_contract)
                else:
                    value_contract = ''
                ra_date = regxx(d_ra_date_re,sub_content)
                FormattedDate = ''
                if ra_date:
                    ra_date = ra_date[0]
                    ra_date = reg_clean (ra_date)
                    ra_date = translateText(ra_date)
                    T_dead_line_date = str(ra_date)+" 00:00:00"
                    T_dead_line_date=T_dead_line_date.replace('b', '')
                    T_dead_line_date=T_dead_line_date.replace("'", "")
                    # dt = dateparser.parse(T_dead_line_date)
                    dt = datetime.strptime(T_dead_line_date,'%Y-%m-%d %H:%M:%S')
                    FormattedDate = dt
                    # if dt:
                        # FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                # else:
                    # pass
                d_ra_time = regxx(d_ra_time_re,sub_content)
                if d_ra_time:
                    d_ra_time = d_ra_time[0]
                    d_ra_time = reg_clean(d_ra_time)
                    d_ra_time = translateText(d_ra_time)
                    print (d_ra_time)
                    d_ra_time=d_ra_time.replace('b', '')
                    d_ra_time=d_ra_time.replace("'", "")
                    d_ra_time = timeFormat(d_ra_time)
                summary_link = summary + sub_url_full

                summary_cont = summary + sub_url_full
                orgin = 'European/Non Defence'
                sector = 'European Translation'
                country = 'PL'
                websource = 'Poland: Przetargi'

                if FormattedDate:
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, sub_url_full, str(), orgin, sector, country, websource,title, title, str(name_address), awardProcedure, str(), cpv_code,description, str(), reference, value_contract, time_read_line, FormattedDate,d_ra_time,str(), summary_cont, 'Y',created_date)
                    print (insertQuery)
                else:
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK,deadline_description) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_url_full), str(), str(orgin), str(sector), str(country), str(websource),str(title), str(title), name_address, str(awardProcedure), str(), str(cpv_code),str(description), str(), str(reference), str(value_contract), str(), str(summary_cont), str('Y'),time_read_line)
                    print (insertQuery)
                val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # print "insertion Status :",val
                if val == 1:
                    print ("1 Row Inserted Successfully")
                else:
                    print ("Insert Failed")
        nxt = regxx(nxt_re, main_content)
        if nxt:
            nxt = base_url + nxt[0]
            main_url = nxt
            print (nxt)
        else:
            print ("<----Over---->")
            break
if __name__== "__main__":
    main_url = 'http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance'

    n = 1
    main()
