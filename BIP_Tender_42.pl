#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use JSON::Parse 'parse_json';
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my $Tender_Url = 'https://supplierlive.proactisp2p.com/Account/Login';
# my $Tender_Url = 'https://supplierlive.proactisp2p.com/Account/Login/Opportunities';
my ($Tender_content2)=&Getcontent($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
# open(ts,">Tender_content42_v1.html");
# print ts "$Tender_content2";
# close ts;

my $RequestVerificationToken;
if ($Tender_content2=~m/__RequestVerificationToken[^>]*?value\=\"([^>]*?)\"/is)
{
	$RequestVerificationToken = $1;
}

my $login_url = 'https://supplierlive.proactisp2p.com/Account/Login/UsernameSignIn';
my $login_ref = 'https://supplierlive.proactisp2p.com/Account/Login?s=Manual';
my $login_hos = 'supplierlive.proactisp2p.com';
my $post_cont = '__RequestVerificationToken='.$RequestVerificationToken.'&Username=kaviyarasan.palanivel%40meritgroup.co.uk&Password=Merit%24123&X-Requested-With=XMLHttpRequest';
my $loginCont = &Postcontent($login_url,$post_cont,$login_hos,$login_ref);
open(ts,">TloginCont_42.html");
print ts "$loginCont";
close ts;


my $dashboad_url = 'https://supplierlive.proactisp2p.com/Dashboard?f=True';
my ($dashboad_content)=&Getcontent($dashboad_url);

my $Tender_Url111="https://supplierlive.proactisp2p.com/Opportunities/Search";
my $post_ref 	 ="https://supplierlive.proactisp2p.com/Opportunities/Search";
my $post_hos 	 = 'supplierlive.proactisp2p.com';
my $post_content ='BuyerTitleReference.Text=Search+by+customer+reference%2C+title+or+customer+name...&BuyerTitleReference=&ClosingDate=&AnnouncementType=0&Status=0&RegisteredInterest=0&IncludeExpired=false&X-Requested-With=XMLHttpRequest';
my ($Tender_content2)= &Postcontent($Tender_Url111,$post_content,$post_hos,$Tender_Url111);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content42_v1.html");
print ts "$Tender_content2";
close ts;

# exit;

#### To get the tender link ####

$Tender_content2=parse_json($Tender_content2);
my $Data = $Tender_content2->{"Data"};

my $Eacharray = $Data->{"Data"};
my @Eacharray = @ {$Eacharray};

my $count = 1;
foreach my $key (@Eacharray)
{
	my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
	
	my $Guid 			= $key->{"Guid"};
	my $BuyerName 		= $key->{"BuyerName"};
	my $ClosingDate 	= $key->{"ClosingDate"};
	$Authority_Reference= $key->{"Reference"};
	$tender_title		= $key->{"Title"};
	
	if ($ClosingDate=~m/^([^>]*?)T([^>]*?)$/is)
	{
		$Deadline_Date = $1;
		$Deadline_time = $2;
		$Deadline_Description = 'ClosingDate';
	}
	my $tender_link = 'https://supplierlive.proactisp2p.com/Opportunities/Search/DrillDown?r='.$Guid.'&t=0&ref='.$Authority_Reference;
	print "$count :: $Authority_Reference\n";
	$count++;
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my ($Tender_content,$code1)=&Getcontent($tender_link);
	$Tender_content = decode_utf8($Tender_content);  
	open(ts,">Tender_content42.html");
	print ts "$Tender_content";
	close ts;
	# exit;
	
	my ($BuyerAddress,$CustomerAddress);
	if ($Tender_content=~m/>\s*Delivery\s*Address\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	{
		$BuyerAddress = &clean($1);
		# $BuyerAddress=~s/\s*<br\s*\/>\s*/, /igs;
		# $BuyerAddress=~s/^\s*\,|\,\s*$//igs;
	}
	
	if ($Tender_content=~m/>\s*Purchasing\s*Contact\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	{
		$CustomerAddress = &clean($1);
		# $CustomerAddress =~s/\s*<br\s*\/>\s*/, /igs;
		# $CustomerAddress=~s/^\s*\,|\,\s*$//igs;
	}
	
	$Awarding_Authority = "Customer Name : $BuyerName, "."$BuyerAddress, "."Customer Contact Details : $CustomerAddress";
	# $Awarding_Authority=~s/^\s*\,|\,\s*$//igs;
	# $Awarding_Authority=~s/\,+/,/igs;
	
	if ($Tender_content=~m/>\s*Description\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	{
		$Description = &BIP_Tender_DB::clean($1);
	}

	my $tenderblock=$Description;
	
	&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
	
	
}




# while ($Tender_content2=~m/Guid\"\:\"([^>]*?)\"/igs)
# {
	# my $tender_link = "https://supplierlive.proactisp2p.com/Account/Login/OpportunityDetail?o=".($1);
	# my ($Tender_content,$code1)=&Getcontent($tender_link);
	# $Tender_content = decode_utf8($Tender_content);  
	# open(ts,">Tender_content42.html");
	# print ts "$Tender_content";
	# close ts;
	# next if ($code1!~m/200/is);
	# print "\nWhile coming below 200\n";
	# # $Tender_content=parse_json($Tender_content);
	
	
	
	
	# # my $tender_title=$Tender_content->{"Name"};
	# $tender_title = $1 if ($Tender_content=~m/>\s*Title\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is);
	# if ($Tender_content=~m/>\s*(Closing\s*Date)\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	# {
		# $Deadline_Description = $1;
		# $Deadline_Date = $2;
	# }
	# # my $ClosingDate =$Tender_content->{"ClosingDate"};
	# # if ($ClosingDate=~m/^([^>]*?)T([^>]*?)$/is)
	# # {
		# # $Deadline_Date = $1;
		# # $Deadline_time = $2;
		# # $Deadline_Description = 'ClosingDate';
	# # }
	
	# #Duplicate check
	# my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	# if ($duplicate eq 'Y')
	# {
		# print "\nAlready Existing this Tender Data\n";
		# next;
	# }
	# else
	# {
		# print "\nThis is newly Released Tender\n";
	# }
	# my ($BuyerName,$BuyerAddress,$CustomerAddress);
	# if ($Tender_content=~m/>\s*Customer\s*Name\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	# {
		# $BuyerName = $1;
	# }
	# if ($Tender_content=~m/>\s*Customer\s*Address\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	# {
		# $BuyerAddress = $1;
		# $BuyerAddress=~s/\s*<br\s*\/>\s*/, /igs;
	# }
	
	# if ($Tender_content=~m/>\s*Customer\s*Contact\s*Details\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	# {
		# $CustomerAddress = $1;
		# $CustomerAddress =~s/\s*<br\s*\/>\s*/, /igs;
	# }
	# $Awarding_Authority = "Customer Name : $BuyerName, "."$BuyerAddress, "."Customer Contact Details : $CustomerAddress";
	# $Awarding_Authority=~s/^\s*\,|\,\s*$//igs;
	# $Awarding_Authority=~s/\,+/,/igs;
	
	# if ($Tender_content=~m/Opportunity\s*\|\s*([^>]*?)\s*</is)
	# {
		# $Authority_Reference = $1;
	# }
	# if ($Tender_content=~m/>\s*Description\s*(?:<\/label>)?\s*(?:<br>)?\s*([\w\W]*?)\s*<\/div>/is)
	# {
		# $Description = &BIP_Tender_DB::clean($1);
	# }

	# my $tenderblock=$Description;
	
	# &BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
# }	


sub post_parameters()
{
	my $contentsdfs = shift;
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION);
	if($contentsdfs=~m/__VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	return ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"supplierlive.proactisp2p.com");
	$req->header("X-Requested-With"=>"XMLHttpRequest");
	$req->header("Referer"=>"https://supplierlive.proactisp2p.com/Account/Login");
	$req->header("Content-Type"=> "text/html; charset=utf-8");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$code);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("X-Requested-With"=>"XMLHttpRequest");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\,/:/igs;
	$Clean=~ s/\.\,/./igs;
	decode_entities($Clean);
	return $Clean;
}