#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="https://suppliers.multiquote.com/Unauthenticated/BrowseOpportunities.aspx";
# my $post_cont='ctl00$ScriptManager=ctl00$M$UpdatePanel1|ctl00$M$ContractMenu$BrowseOpportunitiesMenuItem&ctl00_ScriptManager_HiddenField=&__LASTFOCUS=&__EVENTTARGET=ctl00$M$ContractMenu$BrowseOpportunitiesMenuItem&__EVENTARGUMENT=&__VIEWSTATE_SERVER=79fa6a74-a14c-449a-9d5c-69ea9d5a71ed&__VIEWSTATE=&__EVENTVALIDATION=/wEWDwL+raDpAgLPiN7cBQKqx5f7BAL26IX7AgLN7fuSCAKIluJWAqK7ue8GAr+C64EHAoyUk/AJApenw7sGApan6JMKArKys/wFApjIiugKAqPJ+5YGAqnJ+8kOI0uDVNbxmvqTrDB2D+F6nGge7tG5Ax07T5rYwF1RxKg=&ctl00$GUID=&ctl00$M$LoginControl$LogInPanelBar$i0$_UserName=&ctl00$M$LoginControl$LogInPanelBar$i0$_Password=&ctl00$M$LoginControl$LogInPanelBar$i1$SendPasswordAddress=&ctl00$M$LoginControl$LogInPanelBar$i2$RegisterDunsBox=&ctl00$M$LoginControl$LogInPanelBar$i2$RegisterEmailBox=&ctl00_M_LoginControl_LogInPanelBar_ClientState={"expandedItems":["0"],"logEntries":[],"selectedItems":["0"]}&ctl00$M$UserConsentWindow$_Window$C$_ShowWindow=False&ctl00_M_UserConsentWindow__Window_ClientState=&__ASYNCPOST=true';
# my ($VIEWSTATE1,$VIEWSTATEGENERATOR1,$EVENTVALIDATION1,$After_calendar_AD1)=&post_parameters($Tender_content1);
# my $host="suppliers.multiquote.com";
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1110.html");
print ts "$Tender_content1";
close ts;
# exit;
my ($VIEWSTATE1,$VIEWSTATEGENERATOR1,$EVENTVALIDATION1,$After_calendar_AD1)=&post_parameters($Tender_content1);
my $post_cont='ctl00$ScriptManager=ctl00$M$UpdatePanel|ctl00$M$OpportunityGrid$Grid&__LASTFOCUS=&ctl00_ScriptManager_HiddenField=&__EVENTTARGET=ctl00$M$OpportunityGrid$Grid&__EVENTARGUMENT=RowClick;0&__VIEWSTATE_SERVER=5451e2a3-6fd6-4c10-935e-baa6a069d0d2&__VIEWSTATE=&__EVENTVALIDATION=/wEWDAL+raDpAgLPiN7cBQKf1sCXCAKTn8wdAsLNnt4BAqOZkt0LAuXY8d4PAuXYhboIAuXY/dwMAuXY1aYLAoDCt6oLAoDCy4UELw3ASgbMH2RH8AZ08DY7FNXXfwmL8AFVV3S0TK0TODs=&ctl00$GUID=&ctl00$M$SearchBox=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl02$ctl02$RadComboBoxreference=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxreference_ClientState=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl02$ctl02$RadComboBoxstageTypeName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxstageTypeName_ClientState=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl02$ctl02$RadComboBoxresponseStateName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxresponseStateName_ClientState=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl02$ctl02$RadComboBoxtitle=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxtitle_ClientState=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl02$ctl02$RadComboBoxleadOrganisationName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxleadOrganisationName_ClientState=&ctl00$M$OpportunityGrid$Grid$ctl00$ctl03$ctl01$PageSizeComboBox=20&ctl00_M_OpportunityGrid_Grid_ctl00_ctl03_ctl01_PageSizeComboBox_ClientState=&ctl00_M_OpportunityGrid_Grid_rfltMenu_ClientState=&ctl00_M_OpportunityGrid_Grid_rghcMenu_ClientState=&ctl00_M_OpportunityGrid_Grid_ClientState={"selectedIndexes":["0"],"selectedCellsIndexes":[],"unselectableItemsIndexes":[],"reorderedColumns":[],"expandedItems":[],"expandedGroupItems":[],"expandedFilterItems":[],"deletedItems":[],"hidedColumns":[],"showedColumns":[],"scrolledPosition":"0,0","popUpLocations":{},"draggedItemsIndexes":[]}&__ASYNCPOST=true';
my ($Tender_content12)=&Postcontent($Tender_Url,$post_cont);
# $Tender_content12 = decode_utf8($Tender_content12);  
open(ts,">Tender_content1110_1.html");
print ts "$Tender_content12";
close ts;
if ($Tender_content12=~m/ViewOpportunity.aspx%3fa%3d([^>]*?)\|/is)
{
my $tender_link="https://suppliers.multiquote.com/Unauthenticated/ViewOpportunity.aspx?a=".$1;
# $tender_link=~s/\%2f/\//igs;
# $tender_link=~s/\%3d/=/igs;
# $tender_link=~s/\%3f/?/igs;
my ($Tender_content13)=&Getcontent($tender_link);
$Tender_content13 = decode_utf8($Tender_content13);  
open(ts,">Tender_content1110_2.html");
print ts "$Tender_content13";
close ts;
}
exit;
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print "$month1 \n";
my $year1		= strftime "%y", localtime;
print $year1;
my $date2=$date1-1;
$date2="0$date2" if(length($date2)==1);
# print $date2;
my $month2=$month1;
# $month1 = "0$month1" if (length($month1)==1);
$month2="0$month2" if(length($month2)==1);
my $year2=$year1;
$year2="20$year2";
#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
# exit;

#### To get the tender link ####
if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>View current opportunities and notices<\/a>/is)
{
	my $View_current_pan =&Urlcheck($1);
	my ($View_current_content)=&Getcontent($View_current_pan);
	$View_current_content = decode_utf8($View_current_content);  
	open(ts,">View_current_content.html");
	print ts "$View_current_content";
	close ts;
	# print "you are in if loop";
	# exit;
	my $url1 = "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL";
	my ($View_current_content1)=&Getcontent1($url1);
	my $post_url = 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do';
	my $Host	= 'crowncommercialservice.bravosolution.co.uk';
	# my $post_con = "userAct=search&oppList=GLOBAL&firstPublishingDate_FILTER_OPERATOR=EQUAL&firstPublishingDate_FILTER=".$date1."%2F".$month1."%2F20".$year1."&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=2";
	my $post_con = "userAct=search&oppList=GLOBAL&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=AFTER&firstPublishingDate_FILTER=01%2F01%2F2001&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=1";

	my $Host	= 'crowncommercialservice.bravosolution.co.uk';
	my ($View_current_content2)=&Postcontent($post_url,$post_con,$Host,$post_url);
		open(ts,">View_current_content114.html");
		
	print ts "$View_current_content2";
	close ts;
	top:
	while ($View_current_content2=~m/<td[^>]*?\"col[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?>\s*<a[^>]*?onclick\=\"javascript:goToDetail\(\&\#39\;([\d]+)\&\#39\;[^>]*?>([\w\W]*?)<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>/igs)
	{
		my $Organisation	= ($1);	
		my $tender_no		= ($2);
		my $tender_title	= &clean($3);
		my $Work_Category	= ($4);
		my $Listing_Deadline= ($5);
		
		my $tender_link	= 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&oppList=GLOBAL';
		# my $tender_link	= 'https://www.capitalesourcing.com/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB&_ncp='.$ncp;
		
		my ($Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		if ($Listing_Deadline=~m/^\s*([^>]*?)\s([^>]*?)\s*$/is)
		{
			$Deadline_Date = $1;
			$Deadline_time = $2;
		}
		# print "Deadline Date :: $Deadline_Date\n";
		# print "Deadline Time :: $Deadline_time\n";
		
		#Duplicate check
		#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my ($Tender_content)=&Getcontent($tender_link,);
		$Tender_content = decode_utf8($Tender_content);  
		open(ts,">Tender14_content.html");
		print ts "$Tender_content";
		close ts;
		# exit;
		
		if ($Tender_content=~m/>\s*Project\s*Title\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $title_temp = &clean($1);
		}
		if ($Tender_content=~m/>\s*Project\s*Code\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Authority_Reference = &clean($1);
		}
		if ($Tender_content=~m/>\s*Project\s*Description\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Description = &clean($1);
		}
		if ($Tender_content=~m/>\s*Work\s*Category\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Contract_Type = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Listing\s*Deadline)\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Deadline_Description = ($1);
			my $Deadline_Date_temp = &clean($2);
		}
		if ($Tender_content=~m/>\s*Buyer\s*Details\s*<\/h3>\s*([\w\W]*?)\s*<\/ul>/is)
		{
			$Awarding_Authority = ($1);
			$Awarding_Authority =~s/\s*<\/div>\s*<div[^>]*?>\s*/ : /igs;
			$Awarding_Authority = &clean($Awarding_Authority);
		}
		
		my $tenderblock;
		if ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/table>\s*<\/div>\s*<\/form>/is)
		{
			$tenderblock = ($1);
		}
		elsif ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/form>\s*<\/div>\s*<\/div>/is)
		{
			$tenderblock = ($1);
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# my $tender_type="Normal";
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
	}
	if ($View_current_content2=~m/value\=\s*([\d]+)\s*\;[^>]*?>\s*\&gt\;\s*</is)
	{
		my $nexpage_number = $1;
		my $post_url = 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do';
		my $post_cont= "userAct=search&oppList=GLOBAL&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=AFTER&firstPublishingDate_FILTER=01%2F01%2F2001&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=".$nexpage_number;
		# .$nexpage_number ;
# ;
		
		my $Host	= 'crowncommercialservice.bravosolution.co.uk';
		($View_current_content)=&Postcontent($post_url,$post_cont,$Host,$post_url,$cookie);
		$View_current_content2 = decode_utf8($View_current_content);  
		open(ts,">TenderNext14_content.html");
		print ts "$View_current_content";
		close ts;
		print "\nPageNo :: $nexpage_number\n";
		goto top;
	}
}	
sub post_parameters()
{
	my $contentsdfs = shift;
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
	if($contentsdfs=~m/__VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	if($contentsdfs=~m/Date_calendar_AD\"\s*value\=\"([^>]*?)\"/is)
	{
		$After_calendar_AD=uri_escape($1);
	}
	print "After_calendar_AD :: $After_calendar_AD\n";
	return ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
}


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"suppliers.multiquote.com");
	$req->header("Referer"=>"https://suppliers.multiquote.com/Page/Login.aspx?u=true&r=Home.aspx");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "ASP.NET_SessionId=jakzfvk50nv4ylf2a03hywzd");
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"suppliers.multiquote.com");
	$req->header("Referer"=>"https://suppliers.multiquote.com/Unauthenticated/BrowseOpportunities.aspx");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "ASP.NET_SessionId=jakzfvk50nv4ylf2a03hywzd");
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "suppliers.multiquote.com");
	$req->header("X-Requested-With"=>"XMLHttpRequest"); 
	$req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://suppliers.multiquote.com/Unauthenticated/BrowseOpportunities.aspx");
	$req->header("Cookie"=> "ASP.NET_SessionId=v3rbpenrmfph0qhtpodzf22o");
	# $req->header("Connection"=> "keep-alive");


	$req->content('ctl00%24ScriptManager=ctl00%24M%24UpdatePanel%7Cctl00%24M%24OpportunityGrid%24Grid&__LASTFOCUS=&ctl00_ScriptManager_HiddenField=&__EVENTTARGET=ctl00%24M%24OpportunityGrid%24Grid&__EVENTARGUMENT=RowClick%3B0&__VIEWSTATE_SERVER=1f7b8579-32ae-4cf4-8e58-b6e0acb7c5db&__VIEWSTATE=&__EVENTVALIDATION=%2FwEWDAL%2BraDpAgLPiN7cBQKf1sCXCAKTn8wdAsLNnt4BAqOZkt0LAuXY8d4PAuXYhboIAuXY%2FdwMAuXY1aYLAoDCt6oLAoDCy4UELw3ASgbMH2RH8AZ08DY7FNXXfwmL8AFVV3S0TK0TODs%3D&ctl00%24GUID=&ctl00%24M%24SearchBox=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl02%24ctl02%24RadComboBoxreference=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxreference_ClientState=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl02%24ctl02%24RadComboBoxstageTypeName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxstageTypeName_ClientState=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl02%24ctl02%24RadComboBoxresponseStateName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxresponseStateName_ClientState=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl02%24ctl02%24RadComboBoxtitle=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxtitle_ClientState=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl02%24ctl02%24RadComboBoxleadOrganisationName=&ctl00_M_OpportunityGrid_Grid_ctl00_ctl02_ctl02_RadComboBoxleadOrganisationName_ClientState=&ctl00%24M%24OpportunityGrid%24Grid%24ctl00%24ctl03%24ctl01%24PageSizeComboBox=20&ctl00_M_OpportunityGrid_Grid_ctl00_ctl03_ctl01_PageSizeComboBox_ClientState=&ctl00_M_OpportunityGrid_Grid_rfltMenu_ClientState=&ctl00_M_OpportunityGrid_Grid_rghcMenu_ClientState=&ctl00_M_OpportunityGrid_Grid_ClientState=%7B%22selectedIndexes%22%3A%5B%220%22%5D%2C%22selectedCellsIndexes%22%3A%5B%5D%2C%22unselectableItemsIndexes%22%3A%5B%5D%2C%22reorderedColumns%22%3A%5B%5D%2C%22expandedItems%22%3A%5B%5D%2C%22expandedGroupItems%22%3A%5B%5D%2C%22expandedFilterItems%22%3A%5B%5D%2C%22deletedItems%22%3A%5B%5D%2C%22hidedColumns%22%3A%5B%5D%2C%22showedColumns%22%3A%5B%5D%2C%22scrolledPosition%22%3A%220%2C0%22%2C%22popUpLocations%22%3A%7B%7D%2C%22draggedItemsIndexes%22%3A%5B%5D%7D&__ASYNCPOST=true&');
	# print $Post_Content;
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>|<td>|<\/td>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"crowncommercialservice.bravosolution.co.uk");
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=CURRENT&_ncp=1517573764198.823-1");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}