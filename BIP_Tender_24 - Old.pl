#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
$Tender_Url='https://www.finditinbirmingham.com/Login.aspx?ReturnUrl=%2fOpportunity%2f';
my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
# http://www.finditinbirmingham.com/Login.aspx?ReturnUrl=%2fOpportunity
my ($EVENTVALIDATION,$VIEWSTATEGENERATOR,$VIEWSTATE) = &post_parameters($Tender_content1);

# my $postContent = 'ctl00%24ContentPlaceHolder1%24ScriptManager1=ctl00%24ContentPlaceHolder1%24UpdatePanel1%7Cctl00%24ContentPlaceHolder1%24Login1%24Login&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&ctl00%24hidAccType=&ctl00%24upgradeLinkIDHidden=&ctl00%24hidIsLog=0&ctl00%24hidIgnorePopup=0&ctl00%24hidMessageType=0&ctl00%24hidDisplayFreeUpgrade=0&ctl00%24ContentPlaceHolder1%24hidDisplayToolBar=1&ctl00%24ContentPlaceHolder1%24Login1%24UserName=sarahjane.quinn%40bipsolutions.com&ctl00%24ContentPlaceHolder1%24Login1%24Password=chocolate&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24Login1%24Login=Login';
my $postContent ='ctl00%24ContentPlaceHolder1%24ScriptManager1=ctl00%24ContentPlaceHolder1%24UpdatePanel1%7Cctl00%24ContentPlaceHolder1%24Login1%24Login&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&ctl00%24hidAccType=&ctl00%24upgradeLinkIDHidden=&ctl00%24hidIsLog=0&ctl00%24hidIgnorePopup=0&ctl00%24hidMessageType=0&ctl00%24hidDisplayFreeUpgrade=0&ctl00%24ContentPlaceHolder1%24hidDisplayToolBar=1&ctl00%24ContentPlaceHolder1%24Login1%24UserName=sarahjane.quinn%40bipsolutions.com&ctl00%24ContentPlaceHolder1%24Login1%24Password=chocolate&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24Login1%24Login=Login';

my $host = 'www.finditinbirmingham.com';
my $ref  = 'http://www.finditinbirmingham.com/Login.aspx?ReturnUrl=%2fOpportunity';
my ($Tender_content2)=&Postcontent($Tender_Url,$postContent,$host,$ref);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content24_v1.html");
print ts "$Tender_content2";
close ts;

my $Opportunity_Url = 'http://www.finditinbirmingham.com/Opportunity/';
my ($Opportunity_content1)=&Getcontent($Opportunity_Url);
$Opportunity_content1 = decode_utf8($Opportunity_content1);  
open(ts,">Opportunity24_content1.html");
print ts "$Opportunity_content1";
close ts;


#### To get the tender link ####
top:
my $block;
if ($Opportunity_content1=~m/Latest\s*Opportunities<\/h1>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/is)
{
	$block = $1;
}
else
{	
	$block=$Opportunity_content1;
}
while ($block=~m/<a[^>]*?href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*(?:<\/strong>\s*<br\s*\/>\s*(Closing\s*Date)\s*(?:\:)?\s*([^>]*?)\s*<)?/igs)
{
	my $tender_link = &Urlcheck($1);
	my $temp_title  = $2;
	my $temp_date_desc = $3;
	my $temp_date = $4;
	# next;
	next if ($temp_title=~m/^[\d]+$/is);
	
	my ($Tender_content)=&Getcontent($tender_link);
	$Tender_content = decode_utf8($Tender_content);  
	open(ts,">Tender_content61_v1.html");
	print ts "$Tender_content";
	close ts;
	my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
	if ($Tender_content=~m/OpportunityTitle\">([\w\W]*?)<\/h1>/is)
	{
		$tender_title 	= &clean($1);
	}
	if ($Tender_content=~m/(Closing\s*Date)(?:\:)?<\/div>[\w\W]*?lblClosingDate\">\s*([^>]*?)\s*</is)
	{
		$Deadline_Description 	= ($1);
		$Deadline_Date 	= ($2);
	}
	$Deadline_Date = $temp_date if ($Deadline_Date eq '');
	$Deadline_Description = $temp_date_desc if ($Deadline_Description eq '');
	$tender_title = $temp_title if ($tender_title eq '');
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	if ($Tender_content=~m/lblPostedBy\">([\w\W]*?)<\/div>/is)
	{
		$Awarding_Authority	= &clean($1);
	}

	my $tenderblock;
	if ($Tender_content=~m/lblOpportunityTitle\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
	{
		$tenderblock	= ($1);
	}
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	open(ts,">Tender_content61_v1.xls");
	print ts "$tender_link\t$tender_title\t$Deadline_Description\t$Deadline_Date\t$Awarding_Authority\n";
	close ts;
	# &BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
}

if ($Opportunity_content1=~m/SelectedPage\">\s*([\d]+)\s*<\/span>\s*\&nbsp\;\s*<a[^>]*?href\=\"javascript\:__doPostBack\(\&\#39\;([^>]*?)\&\#39\;/is)
{
	my $nextpage = uri_escape($2);
	
	my ($EVENTVALIDATION1,$VIEWSTATEGENERATOR1,$VIEWSTATE1) = &post_parameters($Opportunity_content1);
	# my $nextPostContent = 'ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ScriptManager1=ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24UpdatePanel2%7C'.$nextpage.'&__EVENTTARGET='.$nextpage.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE1.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR1.'&__EVENTVALIDATION='.$EVENTVALIDATION1.'&ctl00%24hidAccType=2&ctl00%24upgradeLinkIDHidden=325488&ctl00%24hidIsLog=1&ctl00%24hidIgnorePopup=0&ctl00%24hidMessageType=0&ctl00%24hidDisplayFreeUpgrade=0&ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ddlSortBy=Closing%20date&ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ddlDirection=ASC&ctl00%24ContentPlaceHolder1%24OpportunityRightArea1%24txtSearch=&__ASYNCPOST=true&';
	my $nextPostContent = 'ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ScriptManager1=ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24UpdatePanel2%7C'.$nextpage.'&__EVENTTARGET='.$nextpage.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE1.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR1.'&__EVENTVALIDATION='.$EVENTVALIDATION1.'&ctl00%24hidAccType=2&ctl00%24upgradeLinkIDHidden=325488&ctl00%24hidIsLog=1&ctl00%24hidIgnorePopup=0&ctl00%24hidMessageType=0&ctl00%24hidDisplayFreeUpgrade=0&ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ddlSortBy=Closing%20date&ctl00%24ContentPlaceHolder1%24SearchOpportunity1%24ddlDirection=ASC&ctl00%24ContentPlaceHolder1%24OpportunityRightArea1%24txtSearch=&__ASYNCPOST=true&';
	my $host1 = 'www.finditinbirmingham.com';
	($Opportunity_content1)=&Postcontent($Opportunity_Url,$nextPostContent,$host1,$Opportunity_Url);
	$Opportunity_content1 = decode_utf8($Opportunity_content1); 
	open(ts,">Opportunity_contentnextt.html");
	print ts "$Opportunity_content1";
	close ts;
	print "NExtPAge	:: $nextpage\n";
	goto top;
}

sub post_parameters()
{
	my $contentsdfs = shift;
	my ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$VIEWSTATE_Value);
	if($contentsdfs=~m/__VIEWSTATE\"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$VIEWSTATE_Value=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$VIEWSTATEGENERATOR_Value=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$EVENTVALIDATION_Value=uri_escape($1);
	}
	return ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$VIEWSTATE_Value);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"www.capitalesourcing.com");
	# $req->header("Referer"=>"https://www.capitalesourcing.com/web/login.shtml");
	$req->header("Content-Type"=> "text/html; charset=utf-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Cookie"=> "ASP.NET_SessionId=2ama0ot4bx053cgqm4jecdev; __utma=248567696.196716935.1456921537.1456921537.1456921537.1; __utmb=248567696.18.10.1456921537; __utmc=248567696; __utmz=248567696.1456921537.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}