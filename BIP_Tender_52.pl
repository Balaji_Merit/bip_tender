#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
# my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# my $ua=LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
$Tender_Url='https://www.suffolksourcing.co.uk/epps/viewCFTSAction.do?searchSelect=1&selectedItem=quickSearchAction.do%3FsearchSelect%3D1title=&uniqueId=&contractAuthority=&status=cft.status.tender.submission&status=&contractType=&contractType=&cpcCategory=&cpcCategory=&procedure=&procedure=&submissionFromDate=&submissionUntilDate=&description=&description=&CPVCodes=&cpvLabels=&estimatedValueMin=&estimatedValueMax=&tenderOpeningFromDate=&tenderOpeningUntilDate=';
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=1;
#### To get the tender link ####
top:
while ($Tender_content1=~m/>\s*[\d]+\s*<\/td>\s*<td>\s*<a[^>]*?href\=\"([^>]*?)\"[^>]*?>/igs)
{
	my $tender_link = &Urlcheck($1);
	my ($Tender_content)=&Getcontent($tender_link);
	$Tender_content = decode_utf8($Tender_content); 
	
	my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	if ($Tender_content=~m/>\s*Title\s*(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is)
	{
		$tender_title 		 = clean($1);
	}
		
	if ($Tender_content=~m/>\s*(Deadline\s*for\s*receipt\s*of\s*tenders[^>]*?)\s*<\/dt>\s*<dd>\s*([^>]*?)(?:\s([^>]*?))?\s*<\/dd>/is)
	{
		$Deadline_Description = &clean($1);
		$Deadline_Date = &clean($2);
		$Deadline_time = $3;
	}
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	if ($Tender_content=~m/>\s*(Name\s*of\s*Contracting\s*Authority[^>]*?)<\/dt>\s*<dd>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*</is)
	{
		my $buyer_keyword	= &clean($1);
		my $Buyer_url		= &Urlcheck($2);
		my $Buyer			= &clean($3);
		$Buyer = $buyer_keyword.$Buyer;
		my ($Buyer_content)=&Getcontent($Buyer_url);
		$Buyer_content = decode_utf8($Buyer_content); 
		my $Organisation;
		if ($Buyer_content=~m/(Organisation\s*Name\s*\(EN\)[^>]*?\s*\:\s*<\/dt>\s*[\w\W]*?)<\/dl>/is)
		{
			$Organisation = &clean($1);
		}
		$Awarding_Authority=$Buyer_url.",".$Buyer.",".$Organisation;
	}
	if ($Tender_content=~m/>\s*Procurement\s*Type\s*(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is)
	{
		$Contract_Type = &clean($1);
	}
	if ($Tender_content=~m/>\s*CPV\s*Codes\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>/is)
	{
		$CPV_Code = &clean($1);
	}
	if ($Tender_content=~m/>\s*Description\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>/is)
	{
		$Description = &clean($1);
	}
	if ($Tender_content=~m/>\s*NUTS\s*codes\s*\:\s*<\/dt>\s*([\w\W]*?)<\/dd>/is)
	{
		$nuts_code = &clean($1);
	}
	if ($Tender_content=~m/>\s*Project\s*Reference(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is)
	{
		$Authority_Reference = &clean($1);
	}
	if ($Tender_content=~m/>\s*Estimated total contract[^>]*?(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is)
	{
		$Value = &clean($1);
	}
	if ($Tender_content=~m/>\s*Procedure(?:\:)?\s*<[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is)
	{
		$award_procedure = &clean($1);
	}
	
	my ($tenderblock);
	if ($Tender_content=~m/<dl[^>]*?>([\w\W]*?)<\/dl>/is)
	{
		$tenderblock = $1;
	}
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);

	&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
}
if ($Tender_content1=~m/Next\"[^>]*?href\=\"([^>]*?)\"/is)
{
	my $nextpagelink = 'https://www.suffolksourcing.co.uk/epps/viewCFTSAction.do'.$1;
	($Tender_content1)=&Getcontent($nextpagelink);
	$Tender_content1 = decode_utf8($Tender_content1); 
	open(ts,">Nextpage_54.html");
	print ts "$Tender_content1";
	close ts;
	print "pageno	:: $count\n";
	$count++;
	goto top;
}
# if ($Tender_content1=~m/goPage2\(\'next\'\)\"\s*title\=\"Next/is)
# {
	# my $masterkey = uri_escape($1) if ($Tender_content1=~m/masterKey\.value\=\"([^>]*?)\"/is);
	# print "MasterKey :: $masterkey\n";
	# my $nextpagelink  = 'https://eprocurement.tfl.gov.uk/epps/pageIterator/advancedSearchCFT.do?subKey=results%5B%27entriesCollection%27%5D&masterKey='.$masterkey.'&paths=results%5B%27entriesCollection%27%5D&currentPath=results%5BentriesCollection%5D&actionKind=next&sortByField=&selectedPos=&hiddenPos=&paging_results_per_page=&paging_go_to_page=&type=cft&quickSearch=null&searchCriteria=null';
	# $Tender_content1=&Getcontent($nextpagelink);
	# $Tender_content1 = decode_utf8($Tender_content1); 
	# open(ts,">Nextpage_54.html");
	# print ts "$Tender_content1";
	# close ts;
	# print "pageno	:: $count\n";
	# $count++;
	# goto top;
# }

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"etendersni.gov.uk");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	decode_entities($Clean);
	return $Clean;
}