#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts =>{SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="http://web.pcc.gov.tw/tps/pss/tender.do?searchMode=eng&searchType=advance";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my $Home_Url='https://vendor.purchasingconnection.ca/Search.aspx';

my ($Content,$Cookie)=&Getcontent($Home_Url);
my $fh=FileHandle->new("CPTU_Home.html",'w');
binmode($fh);
$fh->print($Content);
$fh->close();


my ($ViewState,$CSRFToken,$Viewstategenerator,$Eventvalidation,$eventtarget);

if($Content=~m/href="javascript:__doPostBack\(([^>]*?)\)">[^>]*?<\/a><\/strong><\/td>\s*<\/tr>/is)
{
	$eventtarget=uri_escape($1);
}
if($Content=~m/<input type="hidden" name="__VIEWSTATEGENERATOR" value="([^>]*?)"\s*\/>/is)
{
	$Viewstategenerator=uri_escape($1);
}
# my $eventtarget='Content$Browse$ctl00$Open@14FFC824-89E2-4C4A-957E-FF26D1554BD9';
# $eventtarget=uri_escape($eventtarget);
# top1:
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewState=uri_escape($1);
}
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$Eventvalidation=uri_escape($1);
}

# my $Search_po_Content='__EVENTTARGET='.$eventtarget.'&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&Content_Browse_ctl00_resultsPerPage&__EVENTVALIDATION='.$Eventvalidation;
my $Search_po_Content='__EVENTTARGET=ctl01%3ABrowse%3Actl00%3Aresult%3AOpportunities%3Actl54%3Actl00&__EVENTARGUMENT=&__VIEWSTATE=XEoEtbrNt%2FqAIjEJSKtZuyVRrThO6CFGUGJzXogqPrbwLIsJjZ%2FnwtCYDLE3iiiCFwWHqrxkUWpLs9%2BbTifyhRnzR2E%2FEQYT6H9PRm2iOyvF3HTBYj%2B4NRhUPBiJfp1K4%2BvE6UE%2BoMd%2Bz1HVdU%2FAOJCfdy4vcEX6HgPpUDiKqO73Xw5%2FfRbdHbtJlJpaJocpdJuYUnRh797iOAhCYi8w3yb5jVjx7ZPO0vD5gpqm0NIB3tMSzAYF6BFOBccXYSvB1xjkWlUPUgdsUtNrMxmpXnnbJIQF8FYMPAiuC9DJQHZTmELwnZcevi1PWhsUb89fiaGPapl88jKSN41trF0krYterC%2FHllXS%2BqJGjJQLC9iBIMs5rhO%2BJttIyM2%2BZ3lBTKqY81YTyCIOh9VYmh3XIldTDmVHTmOJxMdADUsJWdymtzfiZbVSr%2BVxst%2FZcQvjBageJVddki2uo8tQAsPAC%2FvyJeJRR6%2FXmhNOajhfFX8CT5TDjdsOzzrLZjMWi5Bi%2FfWgYwBgugze%2FyeBuoMpAYktPEhqKTbmJ8xLvoQDHybg6h5yWEcroq5DwVXVjXiQIDAsI%2FaM9wdxHUFfSAnPcXpY2DG6kWo4eW59VFcAj71O58vrC0gmmZrU4Ma7eESphgXvTpDeHfxqCBqDtJ57Gjc15nZFdHBLl%2FliJZshe2oy5n14XSilagbmwRzvVkNdfZxN8jjGuWPDNMWw9tMcQc63W3WuwjdtLmTcAlwc6G0NvO%2Fy9Nt4WmHR13hP9j8C18CNPbhggi%2B0R5k9LbstQhAqYQoY8m41wjrKafr6VK91YRT5TIb04dLf0GiXlzNO7642MIZIVNlA1BUKFnOL1s%2Bz4sSZbTtEndPJONVy5AmPDjoPXz12VV5tiKfxa2DD2KoFqHuBNHnLo2ssgImsP6ajN22Zqz%2FdvJO5st5f2XirfH9e6kBdzAz7fosWls87PpE8vpzlA%2FgOt6Q5Zkoe1peBTxVFpAtcPROP%2BP1PSYL8SYZNDWykGWvIqDn45nrj4SFxTeabwF5P6zS50zuExjtTKzDP0%2Fct3cuiydLRCQeghjP9diouZhpzXoWvMpoEHG0SiXSoAKUITa1ZyKxu7Sbtqm8hfOUkn9d4ltpv61boQhE7TxB3wfAJBQ2r5%2BHkxuctyu1oSZRQg2bijkcCk1ymtEqzQvVDCMNFhdHuys2P%2BpYNS0dnylEOLRK8aU735KzVmBonZMmH5AuzauGH4QSyy9WeWC9Ku1I1Nho3NORSOscipPXZD%2F6pOJnSFkhC1U3kdVL6tHdNrh51GrPIb3AQTUi8LOHLK49Bp7NEuNhGLl356JUvil9ZA6j%2FO0nkbPcdVWc5ne8xbv1cYhVauSW6OSZYT85hyre05srkF8Ampae8j%2Bc9fyXW3A4ULqSx2ZXL0iIS0Rkr%2BV%2B34jVfAfz86xeEkJOaWRwGGj9OIJ4zkPuOExe%2FdqA3J0xoP496F43duwisN%2B1%2FzzM3NK3pAtW8q1g5qKXJzw3g6ADplUc%3D&__VIEWSTATEGENERATOR=1DC47A59&__EVENTVALIDATION=YpLFw0ciLdwBQfImi2RH%2BF6PJAadPO7XjsxiKe1eQFmY6nGtUh875k6BjR2Jz22bboPZNQgX86D3G17mJ9g1jbjdCUl6BweKQyPsdeRoGP1L9jkzKLPdeUl3sfQ6dcNGw0R%2BKujYATglo8gcvsK932yMalGnCMOxXZlBlYoplTlvApU26mllf0XOBi2dD%2FwHbJIU3G13oZXJvMmGUCZy0A%3D%3D';
# my $Search_po_Content='__EVENTTARGET=ctl01%3ABrowse%3Actl00%3AOpen%40634F7755-0FC9-4CC6-8414-5179767D80A9&__EVENTARGUMENT=&__VIEWSTATE=UTuvlYp0ZczgkXuapeIk0SqA%2BK7NGY%2FBGX1fzQrgiLiXVmi3tbfT6KtAGpQc9KUg9HPlawJ6aD3yomhiHHi1nyLAhQgfxJyEo0BCTIxLSA%2FGTT9q0olFNl7hYHDZmkqtBTmjcVga3QTRDANKjiwCAewM5AZQlKpAUWbl%2BFzYBYou7Gfju2lm30Vohsh51UdchfL8P1O1cdrz3wMT6wl2g49LWQjCAgyWcvZO1Wtv4L3HtAdKvorxOrmG%2FtVz8B37N9su0189o5kNR1NhKM8rXvkIIhFRj2xUlFlthuXe1dDpJBhGid%2Fx3HXUZ7fMxNDzO%2Fpcm80NzYzWb4J34BcdVxnfLHjrAT%2Fdx%2FWv1ed1pOAFcX19LpowDR8GY%2F%2BfQrQj6M0TR1hM0PzUt%2BpGujoRTDqO%2Fadzu8pqlcF204IJtu7n3wmq29c7Hov%2BTMlpEO2L24uWXjraYy51zEuKJHOpwZxRzxAw6SEfbA2Ass41YhUQ0m1nu%2FvTRXCj1xjEv%2FU%2F81l2q%2FuH745uEZS1f5bcgkpsL4HbQz21L47Js%2FyR72bjsCCjmq8ZN8NZ9k4msfRr7yaEWCYvbUFyJBM2mJ%2BZ5elW%2BaWibUWsQnIS82h%2FLDkGsAj%2F90LaIcLD3FrNFp4iudRTCFDRQaGxB0%2FLmeXTUA%3D%3D&ctl01%24Browse%24ctl00%24resultsPerPage=50&__VIEWSTATEGENERATOR=1DC47A59&__EVENTVALIDATION=bvFo4RFG7I3L571ACKNVrVIY2E6Zk6tC1dr6vAhY3kHWB2JD5y2GU%2F5UqPsWfzp%2B0FMgoaHYpQaIu4qcoJ%2Fz1ySPeYaReIDZUNsyLygJCo697YSb8pE1sjEVp5FZ%2B4qh7wKxV0YxCBLzO5khSjfZ75%2FKK8IpG2PDjyRCHQVKEFCGjssRL3JKSyibBk%2BZOdMafmQYIiNkOE2j7E4Oa%2FYflBeNki0SHiDZXZXyC76quJAaxr31lmkVVv%2BY4mmePOBu8H3iOI4b%2FcK%2BWsh8xbaQNTzlKLsy70dV6Cfzbqkr5u81M6AlKf%2FqVep9tlAjSHGJ4DDpHz6vHoksmcZHdimUdRe9YnhpzKskhRkvVRJDKnU%3D';
# my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$ViewState.'&ctl01%24Search%24ctl00%24searchCriteria%24Keywords=&ctl01%24Search%24ctl00%24searchCriteria%24Category=All&ctl01%24Search%24ctl00%24searchCriteria%24ReferenceID=&ctl01%24Search%24ctl00%24searchCriteria%24SolicitationNumber=&ctl01%24Search%24ctl00%24searchCriteria%24Status=Open&ctl01%24Search%24ctl00%24searchCriteria%24PostSelection=All&ctl01%24Search%24ctl00%24searchCriteria%24CloseSelection=All&ctl01%24Search%24ctl00%24searchCriteria%24Jurisdiction=&ctl01%24Search%24ctl00%24searchCriteria%24ResultsPerPage=50&ctl01%24Search%24ctl00%24StartBrowsing.x=53&ctl01%24Search%24ctl00%24StartBrowsing.y=19&__VIEWSTATEGENERATOR='.$Viewstategenerator.'&__EVENTVALIDATION='.$Eventvalidation;
# ctl01%3ABrowse%3Actl00%3A634F7755-0FC9-4CC6-8414-5179767D80A9
my $Referer='https://vendor.purchasingconnection.ca/Browse.aspx';
my $post_url='https://vendor.purchasingconnection.ca/Browse.aspx';
my ($Result_content)=&Postcontent($post_url, $Referer, $Search_po_Content);
my $fh=FileHandle->new("Cptu_Search.html",'w');
binmode($fh);
$fh->print($Result_content);
$fh->close();
my $i=1;
top1:
$i=$i+1;
while($Result_content=~m/a href='javascript:window.open\("([^>]*?)"/igs)
{
my $tender_link="https://vendor.purchasingconnection.ca".$1;
			my $tender_title;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
			# my $tender_day_content2=$1;
			
			# $tender_link_content2=&Getcontent($tender_day_content2);
			# while($tender_link_content2=~m/<td><a\s*href="([^>]*?)">/igs)
			# {
			# $tender_link=$1;
			my $tender_link_content1=&Getcontent($tender_link);

			if($tender_link_content1=~m/<h1>([^>]*?)<\/h1>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		if($tender_link_content1=~m/<h3>(Response Contact:<\/h3>[\w\W]*?)<\/div><div><h3>/is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/<h3>(Organization:<\/h3>[\w\W]*?)Reference Number/is)
		{
		$Awarding_Authority=$1." ".$Awarding_Authority;
		}
		# elsif($tender_link_content1=~m/Organization:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Awarding_Authority=$1;
		# }
		if($tender_link_content1=~m/<h3>Reference Number:<\/h3>([\w\W]*?)<\/td><\/tr>/is)
		{
		$Authority_Reference="Reference Number :".$1;
		# $Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}
		if($tender_link_content1=~m/<h3>Solicitation[^>]*?Number:[^>]*?<\/h3>([\w\W]*?)<\/td><\/tr>/is)
		{
		$Authority_Reference=$Authority_Reference." Solicitation Number: ".$1;
		# $Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}

		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		$Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		$Authority_Reference=&clean($Authority_Reference);
		if($tender_link_content1=~m/Region of Delivery:<\/h3><\/td><td>([^>]*?)<\/td><\/tr>/is)
		{
			$Location=$1;
			# $award_procedure = &BIP_Tender_DB::clean($award_procedure);
			# $award_procedure=&clean($award_procedure);
		}
	
		if($tender_link_content1=~m/<h3>Opportunity Description:<\/h3>([\w\W]*?)<div id="footer">/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/Location:<\/span>([\w\W]*?)<\/td><\/tr>/is)
		# {
		# $Location=$1;
		# $Location=&clean($Location);
		# $Location=&Trim($Location);
		# $Location=&BIP_Tender_DB::clean($Location);
		# }
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
		if($tender_link_content1=~m/Closing[\w\W]*?:<\/h3><\/td><td>([^>]*?)<br>([^>]*?)<\/td>/is)
		{
		my $temp_dt=$1;
		$Deadline_time=$2;
		if($temp_dt=~m/(\d{1,2})\/(\d{1,2})\/(\d{4})/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/".$3);
		
		# $Deadline_time=$4;
		}
		}
		elsif($tender_link_content1=~m/Closing[\w\W]*?:<\/h3><\/td><td>([^>]*?)<\/td>/is)
		{
		my $temp_dt=$1;
		# $Deadline_time=$2;
		if($temp_dt=~m/(\d{1,2})\/(\d{1,2})\/(\d{4})/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/".$3);
		
		# $Deadline_time=$4;
		}
		}
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\d{2}\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		elsif($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\d{2}\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
	
		# if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		# {
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))/is)
		# {
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		# $Deadline_time=$4;
		# }
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
			# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
		
		$Deadline_Description="Time-limit for receipt of tenders:";
		
		print $tender_link;
		
		
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/<body onload=\'window.focus\(\)\'>([\w\W]*?)<\/body>/is)
	{
	$tenderblock=$1;	
	# $tenderblock=~s/\t/ /igs;
	# $tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='CA';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		
}

if($Result_content=~m/<a href="javascript:__doPostBack\(([^>]*?)\)" style="margin-left:1em;;">Next/is)
{
my $event_target=$1;
$event_target=~s/&#39;,//igs;
$event_target=~s/&#39;//igs;
$event_target=uri_escape($event_target);
print $event_target;
if($Result_content=~m/<input type="hidden" name="__VIEWSTATEGENERATOR" value="([^>]*?)"\s*\/>/is)
{
	$Viewstategenerator=uri_escape($1);
}
# my $eventtarget='Content$Browse$ctl00$Open@14FFC824-89E2-4C4A-957E-FF26D1554BD9';
# $eventtarget=uri_escape($eventtarget);
# top1:
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewState=uri_escape($1);
}
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$Eventvalidation=uri_escape($1);
}
my $Referer='http://vendor.purchasingconnection.ca/Browse.aspx';
my $post_url='http://vendor.purchasingconnection.ca/Browse.aspx';

my $Search_po_Content1='__EVENTTARGET='.$event_target.'&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__EVENTVALIDATION='.$Eventvalidation;

print $Search_po_Content1;
my ($Result_content1)=&Postcontent($post_url, $Referer, $Search_po_Content1);
my $fh=FileHandle->new("Cptu_Search1.html",'w');
binmode($fh);
$fh->print($Result_content1);

$fh->close();
$Result_content=$Result_content1;
goto top1;
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=> "vendor.purchasingconnection.ca");
	$req->header("Content-Type"=> "Content-Type: text/html; charset=utf-8");
	# $req->header("Host"=> "www.merx.com");
	$req->header("Referer"=> "http://vendor.purchasingconnection.ca/Browse.aspx");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	
	# $req->header("Referer"=> "https://www.etenders.gov.eg/Tender/DoSearch?status=3");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.merx4.merx.com");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $referer=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "vendor.purchasingconnection.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "http://vendor.purchasingconnection.ca/Browse.aspx");
	# $req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}