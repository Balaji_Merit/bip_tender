# -*- coding: utf-8 -*-
import requests
import re
import xlwt
import sys
import imp
import dateparser
import time
import redis
import warnings
from datetime import date, timedelta
import dateutil.parser as dparser
from time import sleep
import warnings
import pytz
# warnings.filterwarnings('ignore')
# reload(sys)
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


from googletrans import Translator

blocks_re='<tr\s*class="[^>]*?">([\w\W]*?)<\/tr>'
sub_url_id_re = '\{"data\-id"\:\s*([^>]*?)\}'
base_url = 'https://www.enarocanje.si/Obrazci/?id_obrazec='
sub_url1= 'href="([^>]*?)"'
title_re = '<u class="TENDERNOLINK">\s*([^>]*?)<\/u>'
tender_title_re='<th class="h2">\s*<div style="display: inline-block;">\s*([^>]*?)<'
awarding_re='<span class="SUMMARY_SMALL">\s*<br>\s*Issued by\s*([^>]*?)\s*<'
address_block_re = 'Ime\s*in\s*naslovi<\/h5>([\w\W]*?)<h5>'
address_re = '<label>([\w\W]*?)<\/label>(?:[\w\W]*?)([^>]*?)<\/'
cvp_code_re = '<td class="cpv">([^>]*?)</td>'
bref_des_re = 'Kratek\s*opis[\w\W]*?<\/h5>([\w\W]*?)[<h5>|<h4>]'
place_perform_re = 'Kraj\s*izvedbe[\w\W]*?<\/h5>([\w\W]*?)<h5>'
ref_no_re = '<td[^>]*?class="left[^>]*?top"[^>]*?><b>([^>]*?)<\/b>'
read_line_time = 'Time limit for receipt of tenders or requests to participate'
tender_ref_re='<span class="LIST_TITLE">\s*Number:\s*<\/span>\s*<\/td>\s*<td>\s*([^>]*?)\s*<\/td>'
tender_site_re='<span class="LIST_TITLE">Region\/s:<\/span>\s*<\/td>\s*<td>([\w\W]*?)\s*<\/tr>'
tender_desc_re='<textarea[^>]*?name="desc"[^>]*?id="desc">([\w\W]*?)<\/textarea>'
tender_deadline_re='Closes at[^>]*?,\s*(\d{1,2})\s*([^>]*?)\s*\s*(\d{4})\s*\s*at'
tender_deadtime_re='Closes at[^>]*?,\s*[^>]*?\s*at\s*(\d{1,2}\:\d{2}\s*(?:AM|am|PM|pm))'
tender_other_information_re='<th colspan="2" class="h2">(Site Visit[\w\W]*?)<\/table>'
tender_block_re='<th class="h2">([\w\W]*?)Closes at [\w\W]*?<\/span><\/strong>'
tender_authority_re='>Issued\s*by\s*([^>]*?)<\/span>\s*<\/th>'
other_contacts_re='<span class="LIST_TITLE">Other Contacts<\/span><br>([\w\W]*?)<\/table>\s*<\/tr>\s*<\/table>'
# dead_line_date_re = 'Datum\s*sklenitve\s*pogodbe<\/h5>([\d\.\-\w]*?)<'
# dead_line_date_re = 'Rok\s*za\s*sprejemanje\s*ponudnikovih\s*(?:[\w\W]*?)\s+([\d\.]*?)\s+([\d\:\.]*?)[\.\s+\s*]'
# dead_line_date_re = 'Rok\s*za\s*sprejemanje\s*(?:[\w\W]*?)\s+([\d\.\/]*?)&[\w\W]*?([\d\:\.]*?)[\s+\.\<]'
dead_line_date_re = '<span class="SUMMARY_CLOSINGDATE">\s*(\d{1,2})\s*([^>]*?)\s*,\s*(\d{4})\s*'
dead_line_time_re = '<span class="SUMMARY_CLOSINGDATE">\s*\d{1,2}[^>]*?\s*,\s*\d{4}\s*(\d{1,2\:\d{2}[^>]*?)\s*<\/span>'
Tender_ID = 502
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate


def reg_clean(desc):
    desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')
    desc = desc.replace('  ', ' ')
    # desc = re.sub(r"'", "''", str(desc), re.I)
    desc = desc.replace("'","''")
    desc = desc.replace('<[^>]*?>', ' ')
    # desc = desc.replace("'''","''")
    desc = re.sub(r'  ', ' ', str(desc))
    desc = re.sub(r'<[^>]*?>', ' ', str(desc))
    desc = re.sub(r'\r\n', '', str(desc), re.I)
    desc = re.sub(r"\\r\\n", '', str(desc), re.I)
    desc = re.sub(r'\t', " ", desc, re.I)
    desc = re.sub(r'\s\s+', " ", desc, re.I)
    desc = re.sub(r"^\s+\s*", "", desc, re.I)
    desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
    desc = re.sub(r"\&ndash\;", "-", desc, re.I)

    return desc
	
def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	# time1 = re.findall('(\d{1,2})(?:\:\d{2}\s*|\s*)(?:Noon|noon|pm)',str(Input))
	# time1 = re.findall('(\d{1,2})\s*(?:Noon|noon|pm|PM)',str(Input))
	# print (time1)
	# if time1:
		# if int(time1[0]) < 12:
			# Formattedtime = str(int(time1[0])+12) +":00:00"
		# else:
			# Formattedtime = str(int(time1[0])) +":00:00"
	# print (Formattedtime)	
	time2 = re.findall('(\d{1,2})\:(\d{2})\s*(?:Noon|noon|pm|PM)',str(Input))
	# td2= int(time2[0][1])
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			# if time2[0][1]:
			Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			print (Formattedtime)
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})\:(\d{2})\s*(?:am|AM)',str(Input))
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			# if time2[0][1]:
			Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
		else:
			Formattedtime ="00:00:00"
		

					# Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
	# dt=dparser.parse(Input,fuzzy=True)
	# Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
	print (Formattedtime)
	return (Formattedtime)
	
def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	try:
	
		dt=dparser.parse(Input,fuzzy=True)
		
		print (dt)
		FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	except Exception as e:
		deadline_date1 = dateparser.parse(time.asctime())
		deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
		FormattedDate = deadline_date1
	return (FormattedDate)	
	
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def req_content(url):
    req_url = requests.get(url,proxies = {
    "http" :"http://172.27.137.199:3128",
    "https":"http://172.27.137.199:3128","http" :"http://172.27.137.192:3128",
    "https":"http://172.27.137.192:3128"
    },verify=False,headers = {"Host": "www.tenders.wa.gov.au","Cookie": "JSESSIONID=0FBD034109FCB37132EB255D8B82342A; IP=10.32.5.9; _ga=GA1.4.1390052205.1598595593; _gid=GA1.4.565433511.1598595593; _gat_gtag_UA_124171283_1=1; _gat=1","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0"})
    url_content = req_url.content.decode('utf_8', errors='ignore')
    return url_content


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return  val


def regxx(reg,content):
# def regex_match(regex,content):
	# if regex != "None":
	value = re.findall(reg,str(content))
	if len(value) == 0:
		value1 = ""
	else:
		value1 = value[0]
	
	return (value1)

def start(main_url):
	
    main_content = req_content(main_url)
    # print (main_content)
    time.sleep(5)
    blocks=re.findall(blocks_re,str(main_content))
    print (blocks)
    for block in blocks:
        tender_title=regxx(title_re,block)
        if tender_title:
	        print (tender_title)
	        tender_title=reg_clean(tender_title)
	        Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck1(mysql_cursor,str(Tender_ID))
	        Duplicate_Check_Title = Database_Connector.DuplicateCheckTitle(Retrive_duplicate, tender_title)
	        if Duplicate_Check_Title=='N':
	            curr_date = dateparser.parse(time.asctime())
	            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))			
	            awarding_authority1=regxx(awarding_re,block)
	            if awarding_authority1:
	                awarding_authority=reg_clean(awarding_authority1)
	            else:
	                awarding_authority=''
						
	            ref_no = regxx(ref_no_re,block)
	            if ref_no:
	                ref_no=ref_no
	            else:
	                ref_no=''
						
	            dead_line_date = re.findall(dead_line_date_re,block)
	            print (dead_line_date)
	            if dead_line_date:
	                dead_line_date = str(dead_line_date[0][0])+"-"+str(dead_line_date[0][1])+"-"+str(dead_line_date[0][2])
	                FormattedDate=DateFormat(dead_line_date)
						# dt = dateparser.parse(dead_line_date)
						# FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	            dead_line_time=regxx(dead_line_time_re,block)
	            if dead_line_time:
					
	                deadline_time1 = timeFormat(dead_line_time)
	            description=reg_clean(tender_title)	


	            orgin = 'Worldwide/Non Defence'
	            sector = 'Worldwide'
	            country = 'AU'
	            websource = 'Australia: Govt. of West Australia'
	            summary = 'For further information and instructions in the above contract notice please visit website: https://www.tenders.wa.gov.au/watenders/tender/search/tender-search.do?action=advanced-tender-search-open-tender'
					# other_information = summary + sub_url_full
	            other_information = summary
	            sub_url_full='https://www.tenders.wa.gov.au/watenders/tender/search/tender-search.do?action=advanced-tender-search-open-tender'
	            deadline_description = 'Responses'
				# print 'other_information::', other_information
	            cvp_code=''
	            site=''
	            tender_block=reg_clean(block)
	            if dead_line_date:
			# print "FormattedDate",FormattedDate
	                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, sub_url_full,'', orgin, sector, country,websource,tender_title,tender_title,awarding_authority, '', '', cvp_code, description, site,ref_no,'', deadline_description, FormattedDate, dead_line_time, other_information,'','Y', created_date)
	                val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
	                # counter1 = counter1 + 1
	                print (insertQuery)
					
	            else:
	                val = 0
	                print (tender_title)
	                print ("Deadline Date not available")
                # insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(
                    # Tender_ID, sub_url_full,'', orgin, sector, country, websource, title,
                    # title, awarding_authority, '', '', cvp_code, description, site, ref_no,
                    # '', deadline_description, FormattedDate,  dead_line_time, '', other_information, 'Y',
                    # created_date)
                # Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # counter1 = counter1 + 1                
        else:
	        sub_url=regxx(sub_url1,block)
	        sub_url2="https://www.tenders.wa.gov.au"+sub_url
	        sub_url2=sub_url2.replace("amp;","")
	        print (sub_url2)
	        Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(Tender_ID))
	        Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate,sub_url2)
	        if Duplicate_Check == "N":
	            tender_content = req_content(sub_url2)
	            time.sleep(5)
	            print (tender_content)
	            tender_title=regxx(tender_title_re,tender_content)
	            if tender_title:
	                tender_title=reg_clean(tender_title)
	                print (tender_title)
	            else:
	                tender_title=''
	            ref_no=	regxx(tender_ref_re,tender_content)
	            if ref_no:
	                ref_no=ref_no
	            else:
	                ref_no=''
	            site=regxx(tender_site_re,tender_content)
	            if site:
	                site=reg_clean(site)
	            else:
	                site=''
	            description=regxx(tender_desc_re,tender_content)
	            if description:
	                description=reg_clean(description)
	                description=description.replace("'","''")
					# Women's
	            else:
	                description=''
	            dead_line_date=re.findall(tender_deadline_re,tender_content)
	            print (dead_line_date[0][0])
	            if dead_line_date:
	                dead_line_date = str(dead_line_date[0][0])+"-"+str(dead_line_date[0][1])+"-"+str(dead_line_date[0][2])
	                FormattedDate=DateFormat(dead_line_date)
	                print (FormattedDate)
	            else:
	                FormattedDate=''
	                print ("Deadline Date in invalid format")
					
	            dead_line_time=regxx(tender_deadtime_re,tender_content)
	            if dead_line_time:
	                deadline_time1 = timeFormat(dead_line_time)
	            other_information=regxx(tender_other_information_re,tender_content)
	            if other_information:
	                other_information=reg_clean(other_information)
	            else:
	                other_information=''
	            tender_block=regxx(tender_block_re,tender_content)
	            if tender_block:
	                tender_block=reg_clean(tender_block)
	            else:
	                tender_block=''
	            other_contacts=regxx(other_contacts_re,tender_content)
	            if other_contacts:
	                other_contacts=reg_clean(other_contacts)
	            else:
	                other_contacts=''
	            awarding_authority1=regxx(tender_authority_re,tender_content)
	            if awarding_authority1:
	                awarding_authority=reg_clean(awarding_authority1)
	                awarding_authority=awarding_authority.replace("   ","|")
	            else:
	                awarding_authority=''	
					
	            awarding_authority=awarding_authority+" "+other_contacts
	            orgin = 'Worldwide/Non Defence'
	            sector = 'Worldwide'
	            country = 'AU'
	            websource = 'Australia: Govt. of West Australia'
	            summary = 'For further information and instructions in the above contract notice please visit website: '
	            other_information1=summary + sub_url2
	            deadline_description = 'Responses'
	            cvp_code=''
	            curr_date = dateparser.parse(time.asctime())
	            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
	            if dead_line_date:
			# print "FormattedDate",FormattedDate
	                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, sub_url2, '', orgin, sector, country,websource,tender_title,tender_title,awarding_authority, '', '', cvp_code, description, site,ref_no,'', deadline_description, FormattedDate, deadline_time1, other_information1,other_information,'Y', created_date)
	                val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
	                # counter1 = counter1 + 1
	                print (insertQuery)
					
	            else:
	                val = 0
	                print (tender_title)
	                print ("Deadline Date not available")
				
				
			
			

if __name__== "__main__":
	last_week = date.today() - timedelta(7)
	# last_week = date.today()
	last_week_date1 = (last_week.strftime('%d.%m.%Y'))
	print (last_week_date1)
	# curr_date1 = date.today() - timedelta(1)
	curr_date1 = date.today()
	created_date1 = (curr_date1.strftime('%d.%m.%Y'))
	print (created_date1)
	# raw_input("check")
	main_url = 'https://www.tenders.wa.gov.au/watenders/tender/search/tender-search.do?action=advanced-tender-search-open-tender'
	n = 1
	start(main_url)