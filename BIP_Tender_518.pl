use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use FileHandle;
use JSON::Parse 'parse_json';
use URI::Escape;
use DBI;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 }, show_progress=>1);
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
# $ua->proxy('https', 'http://172.27.137.199:3128'); 
# $ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

my ($ID,$Tender_Name,$Tender_weblink,$Origin,$Sector) =  &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

# https://www.tenderlink.com/webapps/jadehttp.dll?WebTender&tenderer=2744.4&alltenders
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";

# my $Home_Url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my $Home_Url="https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml";
# my $Home_Url="https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml";

my ($Content,$Cookie)=&Getcontent($Home_Url);
my $fh=FileHandle->new("gebiz_Home.html",'w');
binmode($fh);
$fh->print($Content);
$fh->close();
print "$Cookie\n";

my ($ViewState,$form_no,$Viewstategenerator,$Eventvalidation);
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState[^>]*?"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}
if($Content=~m/mojarra\.ab[^>]*?\'action\\\'\s*,\s*\\\'([^>]*?)\\\'\s*,\s*\\\'([^>]*?)\\'\)/is)
{
	$form_no=uri_escape($1);
}

# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt179_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt179_searchBarList_HIDDEN-SUBMITTED-VALUE=&contentForm%3Aj_idt194_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt194_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt194_selectManyMenu-HIDDEN-ACTION-INPUT=&contentForm%3Aj_idt198_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt198_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt198_selectManyMenu-HIDDEN-ACTION-INPUT=&contentForm%3Aj_idt202_selectManyMenu_SEARCH-INPUT=&contentForm%3Aj_idt202_selectManyMenu-HIDDEN-INPUT=&contentForm%3Aj_idt202_selectManyMenu-HIDDEN-ACTION-INPUT=&javax.faces.ViewState='.$ViewState.'&contentForm%3Aj_idt182=contentForm%3Aj_idt182: undefined';

# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt172_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt180_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt180_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&contentForm%3Aj_idt183=contentForm%3Aj_idt183: undefined';
# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt172_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt180_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt180_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&contentForm%3Aj_idt183=contentForm%3Aj_idt183: undefined';
# my $Search_po_Content='contentForm=contentForm&contentForm:j_idt160_windowName=&contentForm:j_idt171_listButton2_HIDDEN-INPUT=&contentForm:j_idt174_searchBar_INPUT-SEARCH=&contentForm:j_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&&contentForm:j_idt177=contentForm:j_idt177: undefined';
my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt160_windowName=multipleWindowBlocker_WindowTab-1&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt179_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt179_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState=-3842243109951969913%3A2697395684652885410&contentForm%3Aj_idt182=contentForm%3Aj_idt182';
# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt160_windowName=multipleWindowBlocker_WindowTab-1&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt179_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt179_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&contentForm%3Aj_idt182=contentForm%3Aj_idt182';

my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my ($Result_content)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
my $fh=FileHandle->new("gebiz_Search.html",'w');
binmode($fh);
$fh->print($Result_content);
$fh->close();
# exit;
# my $Tender_ID=518;
# my $TenderName="GEBIZ";
my $no_of_records;
if($Result_content=~m/([^>]*?)\s*opportunities\s*found/is)
{
$no_of_records=$1;
}
print $no_of_records;
# exit;
for(my $i=1;$i<=$no_of_records;$i++)
{
my ($ViewState,$form_no,$Viewstategenerator,$Eventvalidation);
Again:
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}
while($Result_content=~m/<span\s*id\s*=\s*\"(contentForm\:[^>]*?\:[^>]*?\:[^>]*?)\"\s*>/igs)
{
	my $form_no=uri_escape($1);
	print "form_no: $form_no\n";
	
	# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&'.$form_no.'='.$form_no.': undefined';
	my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt160_windowName=&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&javax.faces.source=contentForm%3Aj_idt701%3Aj_id15%3Aj_id20_commandLink-HIDDEN-BUTTON&javax.faces.partial.event=click&javax.faces.partial.execute=contentForm%3Aj_idt701%3Aj_id15%3Aj_id20_commandLink-HIDDEN-BUTTON%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';
	# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt160_windowName=&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&javax.faces.source=contentForm%3Aj_idt701%3Aj_id15%3Aj_id20_commandLink-HIDDEN-BUTTON&javax.faces.partial.event=click&javax.faces.partial.execute=contentForm%3Aj_idt701%3Aj_id15%3Aj_id20_commandLink-HIDDEN-BUTTON%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';

	# contentForm=contentForm&contentForm%3Aj_idt172_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt175_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt175_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&javax.faces.source=contentForm%3Aj_idt696%3Aj_id22%3Aj_id27_commandLink-HIDDEN-BUTTON&javax.faces.partial.event=click&javax.faces.partial.execute=contentForm%3Aj_idt696%3Aj_id22%3Aj_id27_commandLink-HIDDEN-BUTTON%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true

	my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my ($Result_content)=&Post_Method1($post_url, $Referer, $Search_po_Content, $Cookie);
	# my $fh=FileHandle->new("gebiz_Details.html",'w');
	# open(out,">gebiz_Details.html");
		# print out $Result_content;
		# close out;
		my ($tender_title,$Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&'.$form_no.'='.$form_no.'';
	my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt160_windowName=&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&javax.faces.ViewState='.$ViewState.'&javax.faces.source='.$form_no.'&javax.faces.partial.event=click&javax.faces.partial.execute='.$form_no.'%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';
	# my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt199_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt569%3Aj_id24%3AdatatableNameLocation%3AdatatableNameLocation_inputHidden=&javax.faces.ViewState='.$ViewState.'-3548167199722735103&javax.faces.source=contentForm%3Aj_idt430_ajax_ONLOAD-BUTTON&javax.faces.partial.event=click&javax.faces.partial.execute=contentForm%3Aj_idt430_ajax_ONLOAD-BUTTON%20contentForm%3Aj_idt430&javax.faces.partial.render=contentForm%3Aj_idt430%20contentForm%3AnoticeAttId2&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';
	
	my $tender_link="https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml";
	my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
	my ($Result_content1)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
	# print $Result_content1;
	# exit;
	# my $fh=FileHandle->new("gebiz_Details.html",'w');
	
	next if($Result_content1=~m/CORRIGENDUM/is);
	if($Result_content1=~m/<div class="[^>]*?_TITLE-[^>]*?" style=[^>]*?>([^>]*?)<\/div><\/td><\/tr><\/tbody><\/table><\/div><\/td><\/tr>/is)
	{
	$tender_title=$1;
	}
	if($Result_content1=~m/Agency<\/span><\/div>[\w\W]*?<div class="[^>]*?"\s*style="[^>]*?">([^>]*?)<\/div><\/td>/is)
	{
	$Awarding_Authority="Agency: ".$1;

	}
		if($Result_content1=~m/<table style="display: inline-block;">([\w\W]*?)ITEMS\S*TO/is)
	{
	$Awarding_Authority=$Awarding_Authority." ".$1;
	}

	if($Result_content1=~m/WHO TO CONTACT[\w\W]*?<div class="formOutputText_HIDDEN-LABEL outputText_TITLE-BLACK" style="text-align: left;">([\w\W]*?)<\/script><\/table>/is)
	{
	my $temp=$1;
	# print $temp;

	my $var1;
	my $var2;

	if($temp=~m/data-cfemail="([^>]*?)"/is)
	{
	my $mail1=$1;
	print $mail1;
				# open (RE,">>Digital527.txt");
		# print RE "$mail1";
		# close RE;
		# exit;
	$var1=`C:\\Python27\\python.exe email_test.py $mail1`;
	print $var1;
	# exit;
	sleep(20);
	}
	if($temp=~m/data-cfemail="([^>]*?)"[\w\W]*?data-cfemail="([^>]*?)"/is)
	{
	my $mail1=$1;
	
	my $mail2=$2;
	$var1=`C:\\Python27\\python.exe email_test.py $mail1`;
	sleep(10);
	$var2=`C:\\Python27\\python.exe email_test.py $mail2`;
	sleep(10);
	}
	print $var1;
	# exit;
	print $var2;
	$temp=~s/\[email protected\]//igs;
	$Awarding_Authority=$Awarding_Authority." ".$temp." ".$var1." ".$var2;
	$Awarding_Authority=~s/\[email\s*protected\]//igs;
	$Awarding_Authority=~s/layout_RepaintAllLayouts\(\)\;//igs;
	}
		$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
	$Awarding_Authority=&clean($Awarding_Authority);
	$Awarding_Authority=~s/\[email protected\]//igs;
	
		 if($Result_content1=~m/((?:Quotation No\.|Tender No\.)[\w\W]*?)(Reference No.[\w\W]*?)Agency/is)
	{
	$Authority_Reference=&clean($1);
	my $Authority_Reference1=&clean($2);
	if($Authority_Reference1 eq 'Reference No.')
	{
	$Authority_Reference1="";
	}
	$Authority_Reference=$Authority_Reference." ".$Authority_Reference1;
	$Authority_Reference=&clean($Authority_Reference);
	$Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
	}
	
	if($Result_content1=~m/Closing on[\w\W]*?outputText_NAME-BLACK" style="text-align: left;">(\d{1,2})\s*(\w+)\s*(\d{4})<br\s*\/>([^>]*?)<\/div><\/td><\/tr><\/tbody>/is)
	{
		$Deadline_Date=$1."/".$2."/".$3;
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($Deadline_Date);
		$Deadline_time=&clean($4);
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		$Deadline_time=~s/am//igs;
		$Deadline_time=~s/AM//igs;
			
		$Deadline_Description="Closing on";
		}
					if ($Result_content1=~m/<div class="[^>]*?_TITLE-[^>]*?" style=[^>]*?>([^>]*?)<\/div><\/td><\/tr><\/tbody><\/table><\/div><\/td><\/tr>/is)
			{
				$Description = &clean($1);
			}
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	
	if($Result_content1=~m/(Site\s*Briefing[^>]*?)<\/a>/is)
	{
	$otherInformation=&clean($1);
	}
	my ($tenderblock);
	
	
	if($Result_content1=~m/BACK-BLUE"\s*\/>([\w\W]*?)<input type="[^>]*?" value="Back to Search Results/is)
	{
	$tenderblock=$1;
		open (RE,">>Digital526.txt");
		print RE $tenderblock;
		close RE;
	}
	$tenderblock=~s/\"\s*style="" class="commandButton_BACK-BLUE"\s*\/>//igs;
	$tenderblock=~s/\!function[\w\W]*?n\=0\;nlayout_RepaintAllLayouts\(\)\;//igs;
	$tenderblock=~s/layout_RepaintAllLayouts\(\)\;/ /igs;
	# $tenderblock=~s/function commandLink_JsRedirec[^>]*?EmailPrint OPEN//igs;
	# $tenderblock=~s/loadingScreen_Spinner\([\w\W]*?contentForm:j_id[^>]*?\)\;ajax_doAjaxCall\(\'\'contentForm:j_id[^>]*?\)//igs;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	$tenderblock=~s/^\s*//igs;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	# print $tenderblock;
	# open (RE,">>Digital526.txt");
	# print RE "\n$tenderblock";
	# close RE;
	# exit;
		# if (length($tenderblock)>24000)
	# {
		# $tenderblock = substr $tenderblock, 0, 24000;
	# }
	$cy="SG";
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s+[^>]*?\/>/is)
{
	$ViewState=uri_escape($1);
	print "$ViewState\n";
}

my ($contentForm1,$contentForm2,$contentForm3);
if($Result_content=~m/<input\s*id\s*=\s*\"([^>]*?)\"\s*type[^>]*?value\s*=\s*\"Next\"[^>]*?mojarra\.ab[^>]*?\'action\\\'\s*,\s*\\\'([^>]*?)\\\'\s*,\s*\\\'([^>]*?)\s+contentForm[^>]*?>/is)
{
	$contentForm1=uri_escape($1);
	$contentForm2=uri_escape($2);
	$contentForm3=uri_escape($3);
	print "$contentForm1\n";
	print "$contentForm2\n";
	print "$contentForm3\n";
}

my $Search_po_Content='contentForm=contentForm&contentForm%3Aj_idt171_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt174_searchBar_INPUT-SEARCH=&contentForm%3Aj_idt174_searchBarList_HIDDEN-SUBMITTED-VALUE=&contentForm%3Aj_idt748_select=0&javax.faces.ViewState='.$ViewState.'&javax.faces.source='.$contentForm1.'&javax.faces.partial.event=click&javax.faces.partial.execute='.$contentForm1.'%20'.$contentForm2.'&javax.faces.partial.render='.$contentForm3.'%20contentForm%20dialogForm&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';

my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
($Result_content)=&Post_Method($post_url, $Referer, $Search_po_Content, $Cookie);
my $fh=FileHandle->new("gebiz_Next.html",'w');
binmode($fh);
$fh->print($Result_content);
$fh->close();
}
# <stdin>;
# goto Again;

sub Getcontent
{
	my $url = shift;
	my $Cookie = shift;

	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	my $res = $ua->request($req);
	my %res=%$res;
	my $headers=$res{'_headers'};
	my %headers=%$headers;
	my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "$Cookie[0]";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
	return ($content,$Cookie);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Referer=shift;
	my $PostContent=shift;
	my $Cookie=shift;
	print "Cookie: $Cookie\n";
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=>"www.gebiz.gov.sg");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");
	$req->header("Referer"=>"https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml");
	$req->header("Cookie"=>"$Cookie");
	$req->header("Cookie"=>"test; __cfduid=dcf668df2c61f57a86d249d54d4aeec451568388207; wlsessionid=M1Rs6SlLdE1BG5vbQjgDcJngVl7s8VwaV6EHjy_BgTHaZxwV0nJF!-1982554788");
	$req->content("$PostContent");
	my $res = $ua->request($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}

sub Post_Method1()
{
	my $post_url=shift;
	my $Referer=shift;
	my $PostContent=shift;
	my $Cookie=shift;
	print "Cookie: $Cookie\n";
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=>"www.gebiz.gov.sg");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");
	$req->header("Referer"=>"https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml");
	$req->header("Cookie"=>"$Cookie");
	$req->header("Cookie"=>"test; __cfduid=dcf668df2c61f57a86d249d54d4aeec451568388207; wlsessionid=M1Rs6SlLdE1BG5vbQjgDcJngVl7s8VwaV6EHjy_BgTHaZxwV0nJF!-1982554788; BIGipServerPTN2_PRD_Pool=18964640.47873.0000");
	# Cookie: test; __cfduid=dcf668df2c61f57a86d249d54d4aeec451568388207; wlsessionid=M1Rs6SlLdE1BG5vbQjgDcJngVl7s8VwaV6EHjy_BgTHaZxwV0nJF!-1982554788

	$req->content("$PostContent");
	my $res = $ua->request($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}
sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}