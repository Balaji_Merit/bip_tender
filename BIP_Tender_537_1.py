# -*- coding: utf-8 -*-
import requests
import re
import xlwt
import  sys
from googletrans import Translator
import json
import redis
import time
import imp
import pymysql
import dateparser
import warnings
# warnings.filterwarnings('ignore')




# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

# regex
block_re = '<tr class="[odd|even]*?">([\w\W]*?)<\/tr>'
sub_link_re = '<a\s*href\=\"([^>]*?)\"[^>]*?>'
base_url = 'https://afd.dgmarket.com'
permission_re = '<TITLE>([^>]*?)</TITLE>'
awardingAuthority_block_re = '<tr>([\w\W]*?)<\/tr>'
# awardingAuthority_data = '<td[\w\W]*?>([^>]*?)<\/font>'
awardingAuthority_data = '<tr>[^>]*?<td[^>]*?>\s*([\w\W]*?)\s*<\/td>[\w\W]*?<td[^>]*?>\s*([\w\W]*?)\s*<\/td>'
name_of_package_re = 'notice\-title\">\s*<[\w\W]*?>([^>]*?)<'
buyer_re = 'Acheteur[\w\W]*?<\/td>\s*<td[\w\W]*?>[\w\W]*?>([^>]*?)<[\w\W]*?\/td>'
adderss_re = 'Adresse[\w\W]*?<\/td>\s*<td[\w\W]*?>([\w\W]*?)</td>'
telephone_re = 'T[\w\W]*?phone[\w\W]*?<\/td>\s*<td[\w\W]*?>([\w\W]*?)<\/td[\w\W]*?>'
email_re = "onmouseover\=\"javascript\:this\.href\=\(\'mail\'\+\'to\:\'\+\'([^>]*?)'\+\'([^>]*?)'\+\'([^>]*?)'"
desc_re = 'fixwhitespace\-wrapper\">([\w\W]*?)<\/div>'
country_re = 'Pays[\w\W]*?<\/td>\s*<td[\w\W]*?>[\w\W]*?>([^>]*?)<[\w\W]*?\/td>'
contract_no_re = "Num[\w\W]*?ro\s*de\s*l\'avis/du\s*contrat[\w\W]*?<\/td>\s*<td[\w\W]*?>([^>]*?)<[\w\W]*?\/td>"
read_dead_line= 'Deadline (local time):'
# dead_lin_re = 'limite[\w\W]*?<\/td>\s*<td[\w\W]*?>([^>]*?)<[\w\W]*?\/td>'
datas = ['adresse','téléphone','acheteur','adresse électronique','site web']
dead_line_w = 'Deadline (local time)'
dead_line_re = 'Date\s*limite[\w\W]*?<td[\w\W]*?>\s*([\w\W\s\d\,\/]*?)\s*\-\s*([\d\:]*?)\s*<\/td>'
next = 'https://afd.dgmarket.com/tenders/brandedNoticeList.do?buyerId=&referenceNo=&keywords=&locationISO=&fundingAgencyId=&sub=&updatedId=&status=&deepP=&noticeType=&d-446978-s=p&d-446978-p=2&d-446978-n=1&selPageNumber='
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '

Tender_ID = 537
page_no = 1
######



Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)

def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText): 
    redis_con = redis_connection()
    tempText = InputText
    InputText = InputText.encode('utf8', 'ignore')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()
    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(str(InputText))
                translated_sentence = translated.text

            except Exception as e:
                # print 'Error translate 1**',e
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'ES'
                               }
                    '''Log file to check google translation'''
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write('\n=================================1============================\n')
                        #dees.write(str(payload))
                        #dees.write('\n==================================1===========================\n')

                    s = requests.post(url, data=payload)
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write(str(s.text))
                        #dees.write('\n------------------------------1--------------------------------\n')

                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    print ('Error translate 2 **', e)
                    #raw_input('***raw_input*****')
    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            # print "translated_sentence:::",InputText,translated
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:

            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                payload = {'q': str(InputText),
                           'target': 'en',
                           'source' : 'fr'
                            }
                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write('\n=============================2================================\n')
                    #dees.write(str(payload))
                    #dees.write('\n=============================2================================\n')

                s = requests.post(url, data=payload)

                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write(str(s.text))
                    #dees.write('\n----------------------------2----------------------------------\n')
                value = s.json()
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                redis_con.set(InputText, translated_sentence)
            except Exception as e:
                print ('**translate 4 **', e)
                #raw_input('***raw_input*****')


    return translated_sentence


def req_content(url):
    req_url = requests.get(url)
    url_content = req_url.content.decode('utf_8', errors = 'ignore')
    return url_content

def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def clear(val):
    val = re.sub(r'<[^>]*?>', ' ', val)
    val = val.replace('&nbsp;','')
    val = val.replace('%23','.')
    val = val.replace('\t',' ')
    val = val.replace('\n', '')
    val = val.replace('\s+\s+', ' ')
    val = str(val).replace('^\s+\s*', '')
    return val
def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	try:
	
		dt=dparser.parse(Input,fuzzy=True)
		FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	except Exception as e:
		deadline_date1 = dateparser.parse(time.asctime())
		deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
		FormattedDate = deadline_date1
	return (FormattedDate)

def main():
    global main_url
    n = 1
    # conn = pymysql.connect(host='172.27.138.250', port=3306, user='root', password='admin', db='BIP_tender_analysis',use_unicode=True, charset="utf8")
    # cur = conn.cursor()
    page = 1
    count = 1
    while True:
        main_content = req_content(main_url)
        blocks = regxx(block_re,main_content)
        for index ,block in enumerate(blocks):
            # data_dict[index] = {}
            sub_content = ''
            name_of_package = ''
            buyer = ''
            address = ''
            telephone = ''
            email = ''
            awardingAuthority = ''
            country= ''
            dead_line_date = ''
            dead_line_time = ''
            contract_no= ''
            summary_w = ''
            sub_link = regxx(sub_link_re,block)
            if sub_link:
                print ("No.",n)
                n = n+1
                sub_link = sub_link[0]
                sub_full_link = base_url + sub_link
                Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_full_link)
                if Duplicate_Check == "N":
                    if n<=50:
                        curr_date = dateparser.parse(time.asctime())
                        created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                        permission = regxx(permission_re,main_content)
                        if permission != 'No Permission':
                            sub_content = req_content(sub_full_link)
						# sub_content = sub_content.decode('utf_8', errors = 'ignore')
                            name_of_package = regxx(name_of_package_re,sub_content)
                            if name_of_package:
                                name_of_package = name_of_package[0]
                                T_name_of_package = translateText(str(name_of_package))

                            else:
                                T_name_of_package = ''
                            buyer = regxx(buyer_re,sub_content)
                            if buyer:
                                buyer = buyer[0]
                            else:
                                buyer = ''
                            address = regxx(adderss_re, sub_content)
                            if address:
                                address = address [0]
                                address  = clear(address)
                                address = address
                            else:
                                address = ''
                            telephone = regxx(telephone_re, sub_content)
                            if telephone:
                                telephone = telephone[0]
                                telephone  = clear(telephone)
                                telephone = telephone

                            else:
                                telephone = ''

                            email = regxx(email_re, sub_content)
                            if email:
                                email0 = email [0][0]
                                email1  = email[0][1]
                                email2 = email[0][2]
                                email = email0 + email1 + email2
                                email = clear(email)
                            else:
                                email = ''
                        # awardingAuthority = buyer + address str(telephone) str(email)
                            if buyer or address:
                                awardingAuthority = buyer + address +telephone +email
                                T_awardingAuthority = awardingAuthority
                            else:
                                T_awardingAuthority = ''
                            desc = regxx(desc_re,sub_content)
                            if desc:
                                desc = desc[0]
                                desc = re.sub(r'<[^>]*?>', '', desc)

                                len_desc = len(desc)
                                if len_desc < 40000:
                                    desc = desc.strip()

                                    desc = translateText(str(desc))
                                else:
                                    desc = ''
                            else:
                                desc = ''
                            country = regxx(country_re, sub_content)
                            if country:
                                country = country [0]
                                country  = clear(country).strip()
                                T_country = translateText(str(country))
                            else:
                                T_country = ''
                            dead_line = regxx(dead_line_re,sub_content)

                            if dead_line:
                                dead_line = dead_line[0]
                                print ("deadline :",dead_line)
                                dead_line_date = dead_line[0]
                                print ("deadline date:",dead_line_date)
                                T_dead_line_date = translateText(dead_line_date)
                                print ("translatedd date :",T_dead_line_date)
                                dt = dateparser.parse(T_dead_line_date)
                                FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                                dead_line_time = dead_line [1]

                            else:
                                FormattedDate = ''
                                dead_line_time = ''
                            contract_no = regxx(contract_no_re, sub_content)
                            if contract_no:
                                contract_no = contract_no[0]
                                contract_no  = clear(contract_no).strip()
                                T_contract_no = translateText(str(contract_no))

                            else:
                                T_contract_no = ''
                            print ("")
                            summary_cont = summary + sub_full_link
                            orgin = 'European/Defence'
                            sector = 'European Translation'
                            country = 'FR'
                            websource = 'France: Agence Francaise de Developpement'
                            T_name_of_package=T_name_of_package.replace("'","''").strip()
                            T_awardingAuthority=T_awardingAuthority.replace("'","''")
                            desc=desc.replace("'","''").strip()
                            T_country=T_country.replace("'","''").strip()
                            T_contract_no=T_contract_no.replace("'","''").strip()
                        #dead_line_w=dead_line_w.replace("'","''").strip()

                            if dead_line:
                                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, sub_full_link, str(), orgin, sector, country, websource,T_name_of_package,T_name_of_package, T_awardingAuthority, str(), str(), str(),desc, T_country, T_contract_no, str(), dead_line_w, str(FormattedDate), str(dead_line_time),str(), summary_cont, 'Y',created_date)
                            else:
                                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_full_link), str(), str(orgin), str(sector), str(country),str(websource), str(T_name_of_package), str(T_name_of_package),T_awardingAuthority, str(), str(), str(), str(desc), str(T_country),str(T_contract_no), str(),str(), str(summary_cont), str('Y'),str(created_date))
                            val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                            if val == 1:
                                print ("1 Row Inserted Successfully")
                            else:
                                print ("Insert Failed")
                            summary_w = summary + sub_full_link

                        else:
                            print ("page error(404)",sub_full_link)
                    else:
                        print ("Duplicate data")
        page = page + 1
        nxt_url = next + str(page)
        main_url = nxt_url
        print ("Nxt URl :",main_url)

if __name__== "__main__":
    main_url = 'http://afd.dgmarket.com/tenders/brandedNoticeList.do'
    main()