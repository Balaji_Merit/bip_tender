use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
# use JSON::Parse 'parse_json';
use LWP::Simple; # from CPAN
# use JSON qw( decode_json ); # from CPAN
use Encode qw( decode_utf8 );

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my @origin=("Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","European/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence","Worldwide/Defence");

my @noticeSector=("Worldwide","European","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","European","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","Worldwide","European","Worldwide","Worldwide","Worldwide","Worldwide");

my @sourceURL1=("UNDP: Asia Pacific Regional Centre in Bangkok","UNDP: Europe and CIS","UNDP: United Nations Development Programme","UNDP: United Nations Development Programme Afghanistan","UNDP: United Nations Development Programme Albania","UNDP: United Nations Development Programme Algeria","UNDP: United Nations Development Programme Angola","UNDP: United Nations Development Programme Argentina","UNDP: United Nations Development Programme Bahrain","UNDP: United Nations Development Programme Bangladesh","UNDP: United Nations Development Programme Barbados","UNDP: United Nations Development Programme Benin","UNDP: United Nations Development Programme Bosnia and Herzegovina","UNDP: United Nations Development Programme Botswana","UNDP: United Nations Development Programme Burkina Faso","UNDP: United Nations Development Programme Burundi","UNDP: United Nations Development Programme Cambodia","UNDP: United Nations Development Programme Cameroon","UNDP: United Nations Development Programme China","UNDP: United Nations Development Programme Comoros","UNDP: United Nations Development Programme Croatia","UNDP: United Nations Development Programme Cuba","UNDP: United Nations Development Programme Democratic Republic of the Congo","UNDP: United Nations Development Programme Ecuador","UNDP: United Nations Development Programme Egypt","UNDP: United Nations Development Programme Ethiopia","UNDP: United Nations Development Programme Gabon","UNDP: United Nations Development Programme Gambia","UNDP: United Nations Development Programme Georgia","UNDP: United Nations Development Programme Ghana","UNDP: United Nations Development Programme Guinea","UNDP: United Nations Development Programme Guinea-Bissau","UNDP: United Nations Development Programme Haiti","UNDP: United Nations Development Programme Indonesia","UNDP: United Nations Development Programme Iraq","UNDP: United Nations Development Programme Ivory Coast","UNDP: United Nations Development Programme Kuwait","UNDP: United Nations Development Programme Lebanon","UNDP: United Nations Development Programme Libya","UNDP: United Nations Development Programme Macedonia","UNDP: United Nations Development Programme Malawi","UNDP: United Nations Development Programme Mali","UNDP: United Nations Development Programme Montenegro","UNDP: United Nations Development Programme Morocco","UNDP: United Nations Development Programme Myanmar","UNDP: United Nations Development Programme Namibia","UNDP: United Nations Development Programme Nepal","UNDP: United Nations Development Programme Niger","UNDP: United Nations Development Programme Nigeria","UNDP: United Nations Development Programme Paraguay","UNDP: United Nations Development Programme Romania","UNDP: United Nations Development Programme Rwanda","UNDP: United Nations Development Programme Saudi Arabia","UNDP: United Nations Development Programme Sierra Leone","UNDP: United Nations Development Programme Somalia","UNDP: United Nations Development Programme South Africa","UNDP: United Nations Development Programme Sri Lanka","UNDP: United Nations Development Programme Swaziland","UNDP: United Nations Development Programme Tanzania","UNDP: United Nations Development Programme Timor Leste","UNDP: United Nations Development Programme Togo","UNDP: United Nations Development Programme Tunisia","UNDP: United Nations Development Programme Turkmenistan","UNDP: United Nations Development Programme UAE","UNDP: United Nations Development Programme Ukraine","UNDP: United Nations Development Programme Uruguay","UNDP: United Nations Development Programme Venezuela","UNDP: United Nations Development Programme Yemen","UNDP: United Nations Development Programme Zimbabwe");

my @url1=("http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=THA&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;rgn_id_c=RER&amp;style_type=listing","http://procurement-notices.undp.org/index.cfm","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=AFG&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ALB&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ALG&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ANG&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ARG&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BAH&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BGD&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BAR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BEN&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BIH&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&cty_id_c=BOT&style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=BKF&amp;style_type=table","http://www.bi.undp.org/content/burundi/fr/home/operations/procurement.html","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=CMB&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=CMR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=CPR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=COI&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=CRO&amp;style_type=table","http://www.cu.undp.org/content/cuba/es/home/operations/procurement.html","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ZAI&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ECU&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=EGY&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ETH&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GAB&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GAM&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GEO&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GHA&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GUI&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=GBS&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=HAI&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=INS&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=IRQ&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=IVC&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=KUW&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=LEB&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=LIB&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MCD&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MLW&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MLI&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MTN&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MOR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=MYA&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=NAM&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=NEP&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=NER&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=NIR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=PAR&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&rgn_id_c=RER&style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=RWA&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SAU&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SIL&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SOM&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SAF&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SRL&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=SWA&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=URT&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=TIM&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=TOG&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=TUN&amp;style_type=table","http://www.tm.undp.org/content/turkmenistan/en/home/operations/procurement.html","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=UAE&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=UKR&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=URU&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=VEN&amp;style_type=table","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=YEM&amp;style_type=listing","http://public-components.undp.org/?comp=proc_notices&amp;cty_id_c=ZIM&amp;style_type=table");
my ($sno,$origin,$noticeSector,$cy,$sourceURL,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation);
my $filename="UNDP_test.txt";
open(out,">>$filename");
print out "S.No\tTender ID\tSource Name\torigin\tnoticeSector\tcy\tsourceURL\thigh_value\ttitle\tawardingAuthority\tawardProcedure\tcontractType\tcpvNos\tdescription\tsite\tauthorityRefNo\tvalue\tcw_Radeadline\td_RA\td_Ratime\td_Rapost\totherInformation\n";
close out;
	my $tender_link;
	my @links1;
	my @cy;
	my $sno=0;
	my $sno1=1;
	for(my $i=0;$i<=$#url1;$i++)
	{
		
	my @cy;
	my $sno=0;
	my $sourceURL=$sourceURL1[$i];
	my $url=$url1[$i];
	next if($i==2);
	my ($Home_content,$Referer)=&Getcontent($url);
	open(out,">undp1.html");
	print out "$Home_content";
	close out;
	
	if($i==22)
	{$cy="CONGO, DEM. REPUBLIC";
	}
	if($Home_content=~m/"duty_station_cty":\s*\[([\w\W]*?)\]/is)
	{
	
	my $cy=$1;
	@cy=split(",",$cy);
	
	}
	if($Home_content=~m/"link"\s*:\s*\[([\w\W]*?)\]/is)
	{
	my $links=$1;
	# print $links;
	
	@links1=split(",",$links);
	}
	foreach $tender_link(@links1)
	{
	$tender_link=~s/\"//igs;
	
	my ($Home_content1,$Referer1)=&Getcontent1($tender_link);
		if($Home_content1=~m/<th colspan="3">([\w\W]*?)<\/strong><\/th>/is)
		{
			$title=$1;
			$title=&clean($title);
		}
		if($Home_content1=~m/Office\s*:<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$awardingAuthority=&clean($1);
		}
		if($Home_content1=~m/Deadline :<\/td><td  colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$d_RA=$1;
			$cw_Radeadline="Deadline :";
		}
		if($Home_content1=~m/Reference Number :<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$authorityRefNo=$1;
		}
		if($Home_content1=~m/Overview\s*:\s*<p>([\w\W]*?)<\/td><\/tr>\s*<\/table>/is)
		{
			$description="Overview : ".$1;
			$description=&clean($description);
		}
		$d_Rapost="For further information and instructions in the above contract notice please visit website: ".$tender_link;
		my $y=$i+1;
		if($i==22)
	{
		$cy="CONGO, DEM. REPUBLIC";
		open(out,">>$filename");
		print out "$sno1\t$y\t$sourceURL\t$origin[$i]\t$noticeSector[$i]\t$cy\t$sourceURL\t$high_value\t$title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\n";
	}	
	else{
		open(out,">>$filename");
		print out "$sno1\t$y\t$sourceURL\t$origin[$i]\t$noticeSector[$i]\t$cy[$sno]\t$sourceURL\t$high_value\t$title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\n";
		close out;
		}
		$sno=$sno+1;
		$sno1=$sno1+1;
	
	}
	}
	my $sourceURL=$sourceURL1[2];
	my $url=$url1[2];
	my $y=3;
	my ($Home_content,$Referer)=&Getcontent1($url);
	open(out,">undp.html");
	print out "$Home_content";
	close out;

	while($Home_content=~m/href="([^>]*?notice_id[^>]*?)">[\w\W]*?<\/a><\/td>\s*<td\s*>[^>]*?<\/td>\s*<td\s*>([^>]*?)\s*<\/td>/igs)
	{
	my $tender_link="http://procurement-notices.undp.org/".$1;
	my $cy=$2;
	
	my ($Home_content1,$Referer1)=&Getcontent1($tender_link);
		if($Home_content1=~m/<th colspan="3">([\w\W]*?)<\/strong><\/th>/is)
		{
			$title=$1;
			$title=&clean($title);
		}
		if($Home_content1=~m/Office\s*:<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$awardingAuthority=&clean($1);
		}
		if($Home_content1=~m/Deadline :<\/td><td  colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$d_RA=$1;
		}
		if($Home_content1=~m/Reference Number :<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$authorityRefNo=$1;
		}
		if($Home_content1=~m/Overview\s*:\s*<p>([\w\W]*?)<\/td><\/tr>\s*<\/table>/is)
		{
			$description="Overview : ".$1;
			$description=&clean($description);
		}
		$d_Rapost="For further information and instructions in the above contract notice please visit website: ".$tender_link;
		
		open(out,">>$filename");
		print out "$sno1\t$y\t$sourceURL\t$origin[2]\t$noticeSector[2]\t$cy\t$sourceURL\t$high_value\t$title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\n";
		close out;
		$sno1=$sno1+1;
		# ($origin,$noticeSector,$cy,$sourceURL,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation)="";
	}
sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/&nbsp;/ /igs;
	$values=~s/&amp;/&/igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\–\s*/ - /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/[^[:print:]]+//igs;
	decode_entities($values);
	return($values);
}


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/plain; charset=UTF-8");
	$req->header("Host"=> "public-components.undp.org");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "procurement-notices.undp.org");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}