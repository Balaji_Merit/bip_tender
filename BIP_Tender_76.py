# -*- coding: utf-8 -*-
import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import re
import imp
import pymysql
import time
from dateutil.parser import parse
# import cookielib
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
h = HTMLParser()
# warnings.filterwarnings('ignore')
# reload(sys)
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector', 'C:/BIP/Temp/Database_Connector1.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


block_re = '<div class="search-result">([\w\W]*?)<\/div>\s*<\/dl>\s*<\/div>'
tender_link_re = '<a[^>]*?ref="([^>]*?)"\s*[^>]*?>'
title_re = '<h1[^>]*?>([^>]*?)<\/h1>'
awarding_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>([\w\W]*?)<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p>'
address1_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p>'
city_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p><p>[\w\W]*?<\/p>'
zipcode_authority_re = 'Name[^>]*?addresses[^>]*?<\/span><\/strong><\/h4><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>[\w\W]*?<\/p><p>([\w\W]*?)<\/p>'
contact_re = '<h6><strong>Contact</strong></h6><p>([\w\W]*?)<\/p>'
authority_email = 'Email<\/strong><\/h6><p><a href="mailto:([^>]*?)"\s*[^>]*?>'
telephone_re = 'Telephone<\/strong><\/h6><p>([^>]*?)<\/p>'
fax_re = 'Fax<\/strong><\/h6><p>([^>]*?)<\/p>'
authorityWebAddress_re = 'Internet address(es)[\w\W]*?Main address[\w\W]*?<p><a[^>]*?>([^>]*?)<\/a><\/p>'
contract_type_re = 'Type of contract<\/span><\/strong><\/h5><p>([^>]*?)<\/p>'
award_procedure_re = '<p>([^>]*?)\s*procedure<\/p>'
cpv_re = 'Main CPV code[\w\W]*?<ul><li>([\w\W]*?)<\/li><\/ul>'
desc_re = 'Short description<\/span>[\w\W]*?<p>([\w\W]*?)<\/p>'
ref1 = '(Publication reference:\s*[^>]*?)\s*<'
ref2 = 'Reference number<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
duration_months_re = 'Duration in months<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
start_re = 'Start<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
end_re = 'End<\/strong><\/h6><p>\s*([^>]*?)\s*<\/p>'
value_re = 'Estimated total value<\/span><\/strong><\/h5><p>\s*([^>]*?)\s*<\/p>'
amount_re = 'Estimated total value<\/span><\/strong><\/h5><p>\s*Value excluding VAT:\s*\£([^>]*?)\s*<\/p>'
nuts_code_re = 'NUTS code[^>]*?</strong><\/h6><p>([\w\W]*?)<\/p>'
deadline_date_re = 'Time limit for receipt[\w\W]*?<\/span><\/strong>[\w\W]*?Date<\/strong><\/h6><p>([^>]*?)<\/p>'
deadline_time_re = 'Time limit for receipt[\w\W]*?<\/span><\/strong>[\w\W]*?Local time<\/strong><\/h6><p>([^>]*?)<\/p>'
dead_line_desc_re = 'Time limit for receipt of projects or requests to participate'
other_information_re = '(Communication<\/span><\/strong><\/h[\w\W]*?)<h2 class="share-this-notice">'
orgin = 'Web'
noticeSector = 'UK Other'
Sector = 'UK Other'
tender_type = "Normal"
Industry_Sector = "Other"
proofed = "No"
sourcecw = "Contrax Weekly"
origin = "Web"
noticeSector = "UK Other"
cy = "GB"
country_code = "United Kingdom"
sourceURL = "FTS"
pubFlagCW = "No"
authorityDept = ""
authorityAddress2 = ""
authorityAddress3 = ""
Industry_Sector = "Other"
authorityCountry = "GB"
tender_block_re = '<h1[\w\W]*?>([\w\W]*?)<h2 class="share-this-notice">'
Tender_ID = 76
Retrive_duplicate = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor, Tender_ID)
print ("re----->",Retrive_duplicate)
last_page_re = 'Next<span class=\'standard-paginate-detail\'>2 of (\d+)\s*<'

def reg_clean(cont):
    cont = str(cont).replace('\r', '')
    cont = str(cont).replace('\n', '')
    cont = str(cont).replace('\t', '')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'", "''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'", "''")
    cont = cont.replace("î", "'")
    cont = cont.replace("l'", "l''")
    cont = cont.replace("L'", "L''")
    cont = cont.replace("d'", "d''")
    cont = cont.replace("D'", "D''")
    cont = cont.replace("'s", "''s")
    cont = cont.replace("du'", "du''")
    cont = cont.replace("N'", "N''")
    cont = cont.replace("'acheteur", "''acheteur")
    cont = cont.replace("'id", "''id")
    cont = cont.replace("'b'", "'b''")
    cont = cont.replace("'Esp", "''Esp")
    cont = cont.replace("'RD", "''RD")
    # cont = str(cont).replace("\’","''")
    cont = cont.replace("’", "''", re.I)
    # cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''", "''")
    cont = cont.replace("'''''", "''")
    cont = cont.replace("''''", "''")
    cont = cont.replace("'''", "''")
    cont = cont.replace("'''", "''")
    cont = cont.replace("\xc3\xa9", "é")
    cont = cont.replace("&#39;", "''")
    cont = cont.replace("&quot;", '"')
    cont = cont.replace("î", "'")
    cont = cont.replace("'", "''")
    cont = cont.replace("''''", "''")
    return cont


def req_content(url):
    try:
        req_url = requests.get(url, proxies={
            "http": "http://172.27.137.192:3128",
            "https": "http://172.27.137.192:3128"
        })
        url_content = req_url.content.decode('utf_8', errors='ignore')
    except Exception as e:
        url_content = ''
    return url_content


def timeFormat(Input):
    print ("Time Before Format :: ", Input)
    time2 = re.findall(
        '(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)', str(Input))
    # print time1[0][0]
    if time2:
        if int(time2[0][0]) < 12:
            print (int(time2[0][0]))
            if time2[0][1]:
                Formattedtime = str(
                    int(time2[0][0])+12) + ":"+str(int(time2[0][1]))+":00"
            if int(time2[0][0]) > 12:
                Formattedtime = str(
                    int(time2[0][0]))+":"+str(int(time2[0][1]))+"00"
  
        else:
            if int(time2[0][0]) == 12:
                Formattedtime = str(int(time2[0][0])) + ":00:00"
            if int(time2[0][0]) > 12:
                Formattedtime = str(
                    int(time2[0][0])) + ":"+str(int(time2[0][1]))+":00"
   
   

    time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)', str(Input))

    if time2:
            if int(time2[0][0]) <= 12:
                Formattedtime = str(int(time2[0][0])) + ":00:00"

                    # Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
    # dt=dparser.parse(Input,fuzzy=True)
    # Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
    print (Formattedtime)
    return (Formattedtime)


def DateFormat(Input):
    print ("DATE Before Format :: ", Input)
    try:
        dt = parse(Input)
        # dt = dparser.parse(Input, fuzzy=True)
        FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
        print ("FormattedDate:: ",FormattedDate)
    except Exception as e:
        deadline_date1 = dateparser.parse(time.asctime())
        deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
        FormattedDate = deadline_date1
    return (FormattedDate)


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return val


def regxx(regx, content):
    cont_data = re.findall(regx, content)
    if cont_data:
        cont_data = cont_data[0]
    else:
        cont_data = ''
    return h.unescape(cont_data)


def start(main_url):
    proxies = {
        "http": "https://172.27.137.199:3128",
        "https": "https://172.27.137.199:3128"
    }
    s = requests.session()
    last_week = date.today() - timedelta(1)
    last_week_temp = date.today()
    print ("Today******", last_week)
    last_week_date1 = (last_week.strftime('%d'))
    last_week_date2 = (last_week.strftime('%m'))
    last_week_date3 = (last_week.strftime('%Y'))
    last_week_temp1 = (last_week_temp.strftime('%d'))
    last_week_temp2 = (last_week_temp.strftime('%m'))
    last_week_temp3 = (last_week_temp.strftime('%Y'))
    # estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis[]=1&dateparutionmin=28/08/2020&dateparutionmax=28/08/2020&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis[]=1&dateparutionmin=27/08/2020&dateparutionmax=27/08/2020&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",headers={"Host": "www.boamp.fr","Content-Type": "application/x-www-form-urlencoded","Referer": "https://www.boamp.fr/recherche/avancee"},proxies=proxies,verify=False)
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",proxies=proxies,verify=False)
    # main_content1 = requests.post("https://www.find-tender.service.gov.uk/Search/Results", data="keywords=&stage%5B1%5D=1&stage%5B2%5D=1&form_type%5B1%5D=1&form_type%5B2%5D=1&form_type%5B3%5D=1&form_type%5B4%5D=1&form_type%5B5%5D=1&form_type%5B6%5D=1&form_type%5B7%5D=1&form_type%5B8%5D=1&form_type%5B12%5D=1&form_type%5B13%5D=1&form_type%5B14%5D=1&form_type%5B15%5D=1&form_type%5B16%5D=1&form_type%5B17%5D=1&form_type%5B18%5D=1&form_type%5B19%5D=1&form_type%5B20%5D=1&form_type%5B21%5D=1&form_type%5B22%5D=1&form_type%5B23%5D=1&form_type%5B24%5D=1&form_type%5B25%5D=1&form_type%5B26%5D=1&form_type%5B27%5D=1&minimum_value=&maximum_value=&published_from=" +last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&published_to="+last_week_temp1+"%2F"+last_week_temp2+"%2F"+last_week_temp3+"&closed_from=&closed_to=&form_token=77zvJun3nvZS65ZhZkmsB78t3Eq6Y949&adv_search=", headers={"Host": "www.find-tender.service.gov.uk", "Referer": "https://www.find-tender.service.gov.uk/Search/Results", "Origin": "https://www.find-tender.service.gov.uk", "Cookie": "FT_AUTH=9jlmifndlkogequjr6iamf9ph4; FT_PAGE_TIMEOUT=1614337747720", }, proxies={"http": "http://172.27.137.192:3128", "https": "http://172.27.137.192:3128"}, verify=False)
    main_content1 = requests.post("https://www.find-tender.service.gov.uk/Search/Results", data="keywords=&stage%5B1%5D=1&stage%5B2%5D=1&form_type%5B1%5D=1&form_type%5B2%5D=1&form_type%5B3%5D=1&form_type%5B4%5D=1&form_type%5B5%5D=1&form_type%5B6%5D=1&form_type%5B7%5D=1&form_type%5B8%5D=1&form_type%5B12%5D=1&form_type%5B13%5D=1&form_type%5B14%5D=1&form_type%5B15%5D=1&form_type%5B16%5D=1&form_type%5B17%5D=1&form_type%5B18%5D=1&form_type%5B19%5D=1&form_type%5B20%5D=1&form_type%5B21%5D=1&form_type%5B22%5D=1&form_type%5B23%5D=1&form_type%5B24%5D=1&form_type%5B25%5D=1&form_type%5B26%5D=1&form_type%5B27%5D=1&minimum_value=&maximum_value=&published_from=28%2F05%2F2021&published_to=29%2F05%2F2021&closed_from=&closed_to=&form_token=hHEJvSRSZH9sEfrqt9U96243H3Bd29XS&adv_search=", headers={"Host": "www.find-tender.service.gov.uk", "Referer": "https://www.find-tender.service.gov.uk/Search/Results", "Origin": "https://www.find-tender.service.gov.uk", "Cookie": "FT_COOKIES_PREFERENCES_SET=1; _ga=GA1.4.1341880559.1618483382; FT_AUTH=ia5f14t4d0rln58011rj29heu7; _gid=GA1.4.291270989.1621956787; FT_PAGE_TIMEOUT=1622267572240; _gat_UA-87418217-2=1; _gat_UA-145652997-1=1" }, verify=False)
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",proxies=proxies,verify=False)
    #main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",headers={"Host": "www.boamp.fr","Referer": "https://www.boamp.fr/avis/liste","Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; eZSESSID1335d27b6a308a5b0435f39d334a4390=q6ugd6ocqf1drpdh7m9t762th5"},proxies=proxies,verify=False)
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=

    s = requests.session()
    main_content = main_content1.content.decode('utf_8', errors='ignore')
    # with open("temp96.html", 'w') as f:

        # f.write(main_content)

    blocks = re.findall(block_re, main_content)
    for block in blocks:
        tender_url = re.findall(tender_link_re, block)[0]
        tender_url = tender_url.replace("?origin=SearchResults&p=1", "")
        print (tender_url)
        Retrive_duplicate = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor, Tender_ID)
        Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, tender_url)
        if Duplicate_Check == "N":
            tender_content1 = requests.get(tender_url)
            tender_content = tender_content1.content.decode('utf_8', errors='ignore')
            title = re.findall(title_re, tender_content)[0]
            title = title.replace("'","''")			
            # if "Framework" not in title:
                # title = str(title) + " - Framework Agreement"
            address1=''
            city=''
            postal_code=''
            names=''
            Salutation = ''
            first_name = ''
            last_name = ''			
            authority_name = re.findall(awarding_authority_re, tender_content)
            if authority_name:
                authority_name = authority_name[0].replace("'","''")
                address1 = re.findall(address1_authority_re, tender_content)[0]
                address1 = address1.replace("'","''")
                city = re.findall(city_authority_re, tender_content)[0]
                postal_code = re.findall(zipcode_authority_re, tender_content)[0]
                contact_name = re.findall(contact_re, tender_content)
                Salutation = ''
                first_name = ''
                last_name = ''
                if len(city)>=50:
                    city = city[0:50]
                if len(postal_code)>=15:
                    postal_code = ''
				
                if contact_name:
                    names = contact_name[0].split(" ")
                    last_name = names[-1].strip()
                    if "Miss" in names[0]:
                        Salutation = names[0]
                    if "Ms" in names[0]:
                        Salutation = names[0]
                    if "Mr" in  names[0]:
                        Salutation = names[0]
                    if len(Salutation) == 0:
                        for i in range(0, len(names) - 1):
                            first_name = first_name + ' ' + names[i]
                    else:
                        for i in range(1, len(names) - 1):
                            first_name = first_name + ' ' + names[i]
                    first_name = first_name.replace("^\s*", '')
                    first_name = first_name.replace("\s*$", '')
                    first_name = first_name.strip()
                    print (first_name)
            email1 = re.findall(authority_email, tender_content)
            if email1:
                e_mail = email1[0]
            else:
                e_mail = ''
            telephone = re.findall(telephone_re, tender_content)
            if telephone:
                telephone = telephone[0]
            else:
                telephone = ''
            fax = re.findall(fax_re, tender_content)
            if fax:
                fax = fax[0]
            else:
                fax = ''
            authorityWebAddress = re.findall(authorityWebAddress_re, tender_content)
            if authorityWebAddress:
                authorityWebAddress = authorityWebAddress[0]
            else:
                authorityWebAddress = ''
            contract_type = re.findall(contract_type_re, tender_content)
            if contract_type:
                contract_type = "Contract type: "+str(contract_type[0])
            else:
                contract_type = ''
            procedure_type = re.findall(award_procedure_re, tender_content)
            if len(procedure_type)==1:
				
                for x in range(0, len(procedure_type)):
                    procedure_type1 = procedure_type[x] + ", "
                procedure_type1 = procedure_type1.strip()
                procedure_type1 = procedure_type1[:-1]
            else:
                procedure_type1 = ''
            cpv_code = re.findall(cpv_re, tender_content)
            if cpv_code:
                cpv_code = cpv_code[0]
            else:
                cpv_code = ''
            description = re.findall(desc_re, tender_content)
            if description:
                description = reg_clean(description[0])
            else:
                description = ''
            publication_reference = re.findall(ref1, tender_content)
            if publication_reference:
                publication_reference = publication_reference[0]
            else:
                publication_reference = ''
            reference_no = re.findall(ref2, tender_content)
            if reference_no:
                reference_no = "Reference Number: "+reference_no[0]
            else:
                reference_no = ''
            authority_reference = str(publication_reference) + str(reference_no)
            authority_reference = authority_reference.strip()
            authority_reference = authority_reference.replace("\s*$", '')
            duration_months = re.findall(duration_months_re, tender_content)
            if duration_months:
                duration_months = duration_months[0]
            else:
                duration_months = ''

            end_date = re.findall(end_re, tender_content)
            if end_date:
                end_date = DateFormat(end_date[0])
            else:
                end_date=''

            value = re.findall(value_re, tender_content)
            if value:
                value = value[0]
            else:
                value = ''
            amount = re.findall(amount_re, tender_content)
            if amount:
                amount = amount[0]
                currency = 'GBP'
            else:
                amount = ''
                currency = ''
            nuts_code = re.findall(nuts_code_re, tender_content)
            if nuts_code:
                nuts_code = nuts_code[0]
            else:
                nuts_code = ''
			# nuts_code = nuts_code[0:50]	
            location = "Place of performance :" + str(nuts_code)
            nuts_code = nuts_code[0:50]	
            deadline_date = re.findall(deadline_date_re, tender_content)
            if deadline_date:
                deadline_date = DateFormat(deadline_date[0])
            else:
                deadline_date = ''
            start_date = re.findall(start_re, tender_content)
            if start_date:
                start_date = DateFormat(start_date[0])
            else:
                start_date = deadline_date
            deadline_time = re.findall(deadline_time_re, tender_content)
            if deadline_time:
                deadline_time = timeFormat(deadline_time[0])
            else:
                deadline_time = '00:00'
            other_information = re.findall(other_information_re, tender_content)
            if other_information:
                other_information = reg_clean(other_information[0])[0:20000]
            else:
                other_information = ''
            tender_block = re.findall(tender_block_re, tender_content)
            if tender_block:
                tender_block = reg_clean(tender_block[0])[0:20000]
            else:
                tender_block = ''					
            d_RApost = "For further information regarding the above contract notice please visit: "+tender_url
            curr_date = dateparser.parse(time.asctime())
            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
            end_date = re.findall(end_re, tender_content)
            if end_date:
                end_date = DateFormat(end_date[0])
                # insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,contract_end_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, sourceURL, authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,'1',cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,start_date,end_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,contract_end_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,start_date,end_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
            else:
                deadline_date = re.findall(deadline_date_re, tender_content)
                if deadline_date:
                    deadline_date = DateFormat(deadline_date[0])
                    # insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, sourceURL, authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,'1',cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,deadline_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                    insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,deadline_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                else:
                    # insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,cpv_code,description,proofed,location,authority_reference,no_of_months,value,currency,amount,deadline_description,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, sourceURL, authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,'1',cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,cpv_code,description,proofed,location,authority_reference,duration_months,value,currency,amount,dead_line_desc_re,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                    insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,cpv_code,description,proofed,location,authority_reference,no_of_months,value,currency,amount,deadline_description,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,cpv_code,description,proofed,location,authority_reference,duration_months,value,currency,amount,dead_line_desc_re,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                    
            print (insertQuery)
				
            Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
    last_page = re.findall(last_page_re, main_content)[0]
    for page in range(2,(int(last_page)+1)):
        url="https://www.find-tender.service.gov.uk/Search/Results?&page="+str(page)
        print (url)
        main_content2=s.get(url, headers={"Host": "www.find-tender.service.gov.uk", "Referer": "https://www.find-tender.service.gov.uk/Search/Results", "Origin": "https://www.find-tender.service.gov.uk", "Cookie": "FT_COOKIES_PREFERENCES_SET=1; _ga=GA1.4.1341880559.1618483382; FT_AUTH=ia5f14t4d0rln58011rj29heu7; _gid=GA1.4.291270989.1621956787; FT_PAGE_TIMEOUT=1622008829085; _gat_UA-87418217-2=1; _gat_UA-145652997-1=1" }, proxies={"http": "http://172.27.137.192:3128", "https": "http://172.27.137.192:3128"}, verify=False)
        main_content1 = main_content2.content.decode('utf_8', errors='ignore')


        blocks = re.findall(block_re, main_content1)
        for block in blocks:
            tender_url = re.findall(tender_link_re, block)[0]
            tender_url = tender_url.replace("?origin=SearchResults&p="+str(page), "")
            print (tender_url)
            Retrive_duplicate = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor, Tender_ID)
            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, tender_url)
            if Duplicate_Check == "N":
                tender_content1 = requests.get(tender_url)
                tender_content = tender_content1.content.decode('utf_8', errors='ignore')
                title = re.findall(title_re, tender_content)[0]
                title = title.replace("'","''")
                # if "Framework" not in title:
                    # title = str(title) + " - Framework Agreement"
                authority_name = re.findall(awarding_authority_re, tender_content)
                address1 = re.findall(address1_authority_re, tender_content)
                # address1 = address1.replace("'","''")
                city = re.findall(city_authority_re, tender_content)
                postal_code = re.findall(
                zipcode_authority_re, tender_content)
                if authority_name:
                    authority_name = authority_name[0]				
                    authority_name = authority_name.replace("'","''")
                    address1 = address1[0].replace("'","''")
                    address1 = address1[0:150]
                    city = city[0]
                    city = city[0:50]
                    postal_code = postal_code[0]
                else:
                    authority_name=''
                    address1=''
                    city = ''
                    postal_code = ''

                contact_name = re.findall(contact_re, tender_content)
                Salutation = ''
                first_name = ''
                last_name = ''
                if contact_name:
                    names = contact_name[0].split(" ")
                    last_name = names[-1].strip()
                    if "Miss" in names[0]:
                        Salutation = names[0]
                    if "Ms" in names[0]:
                        Salutation = names[0]
                    if "Mr" in  names[0]:
                        Salutation = names[0]
                    if len(Salutation) == 0:
                        for i in range(0, len(names) - 1):
                            first_name = first_name + ' ' + names[i]
                    else:
                        for i in range(1, len(names) - 1):
                            first_name = first_name + ' ' + names[i]
                    first_name = first_name.replace("^\s*", '')
                    first_name = first_name.replace("\s*$", '')
                    first_name = first_name.strip()
                    print (first_name)
                else:
                    first_name = ''
                    last_name = ''
                email1 = re.findall(authority_email, tender_content)
                if email1:
                    e_mail = email1[0]
                else:
                    e_mail = ''
                telephone = re.findall(telephone_re, tender_content)
                if telephone:
                    telephone = telephone[0]
                else:
                    telephone = ''
                fax = re.findall(fax_re, tender_content)
                if fax:
                    fax = fax[0]
                else:
                    fax = ''
                authorityWebAddress = re.findall(authorityWebAddress_re, tender_content)
                if authorityWebAddress:
                    authorityWebAddress = authorityWebAddress[0]
                else:
                    authorityWebAddress = ''
                contract_type = re.findall(contract_type_re, tender_content)
                if contract_type:
                    contract_type = "Contract type: "+str(contract_type[0])
                else:
                    contract_type = ''
                procedure_type = re.findall(award_procedure_re, tender_content)
                if len(procedure_type)==1:
					
                    for x in range(0, len(procedure_type)):
                        procedure_type1 = procedure_type[x] + ", "
                    procedure_type1 = procedure_type1.strip()
                    procedure_type1 = procedure_type1[:-1]
                else:
                    procedure_type1 = ''
                cpv_code = re.findall(cpv_re, tender_content)
                if cpv_code:
                    cpv_code = cpv_code[0]
                else:
                    cpv_code = ''
                description = re.findall(desc_re, tender_content)
                if description:
                    description = reg_clean(description[0])
                else:
                    description = ''
                publication_reference = re.findall(ref1, tender_content)
                if publication_reference:
                    publication_reference = publication_reference[0]
                else:
                    publication_reference = ''
                reference_no = re.findall(ref2, tender_content)
                if reference_no:
                    reference_no = "Reference Number: "+reference_no[0]
                else:
                    reference_no = ''
                authority_reference = str(publication_reference) + str(reference_no)
                authority_reference = authority_reference.strip()
					
                duration_months = re.findall(duration_months_re, tender_content)
                if duration_months:
                    duration_months = duration_months[0]
                else:
                    duration_months = ''

                end_date = re.findall(end_re, tender_content)
                if end_date:
                    end_date = DateFormat(end_date[0])

                value = re.findall(value_re, tender_content)
                if value:
                    value = value[0]
                else:
                    value = ''
                amount = re.findall(amount_re, tender_content)
                if amount:
                    amount = amount[0]
                    currency = 'GBP'
                else:
                    amount = ''
                    currency = ''
                nuts_code = re.findall(nuts_code_re, tender_content)
                if nuts_code:
                    nuts_code = reg_clean(nuts_code[0])
                else:
                    nuts_code = ''
                location = "Place of performance :" + str(nuts_code.strip())
                nuts_code = nuts_code[0:50]
                deadline_date = re.findall(deadline_date_re, tender_content)
                if deadline_date:
                    deadline_date = DateFormat(deadline_date[0])

                start_date = re.findall(start_re, tender_content)
                if start_date:
                    start_date = DateFormat(start_date[0])
                else:
                    start_date = deadline_date
                deadline_time = re.findall(deadline_time_re, tender_content)
                if deadline_time:
                    deadline_time = timeFormat(deadline_time[0])
                else:
                    deadline_time = '00:00'
                other_information = re.findall(other_information_re, tender_content)
                if other_information:
                    other_information = reg_clean(other_information[0])
                else:
                    other_information = ''
                tender_block = re.findall(tender_block_re, tender_content)
                if tender_block:
                    tender_block = reg_clean(tender_block[0])
                else:
                    tender_block = ''
                # tender_block = tender_block[0:5000]
                # other_information = other_information[0:5000]				
                d_RApost = "For further information regarding the above contract notice please visit: "+tender_url
                curr_date = dateparser.parse(time.asctime())
                created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                end_date = re.findall(end_re, tender_content)
                if end_date:
                    end_date = DateFormat(end_date[0])
                    insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,contract_end_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,start_date,end_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                else:
                    deadline_date = re.findall(deadline_date_re, tender_content)
                    if len(deadline_date)>0:
                        deadline_date = DateFormat(deadline_date[0])
                        insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,earliestDate,cpv_code,description,proofed,location,authority_reference,no_of_months,contract_start_date,value,currency,amount,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,deadline_date,cpv_code,description,proofed,location,authority_reference,duration_months,deadline_date,value,currency,amount,dead_line_desc_re,deadline_date,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                    else:
                        insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,authorityAddress1,pubFlagCW,authorityTown,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,awardProcedure,contact_type,cpv_code,description,proofed,location,authority_reference,no_of_months,value,currency,amount,deadline_description,deadline_time,sent_to,other_information,nuts_code,created_date,authoritypostcode,Site,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, tender_url, "Contrax Weekly", authority_name, tender_block, origin, noticeSector, country_code, cy, 'FTS',Industry_Sector,title,title,address1,pubFlagCW,city,cy,telephone,fax,e_mail,title,authorityWebAddress,Salutation,first_name,last_name,procedure_type1,contract_type,cpv_code,description,proofed,location,authority_reference,duration_months,value,currency,amount,dead_line_desc_re,deadline_time,d_RApost,other_information,nuts_code,created_date,postal_code,location,'Normal')
                    
                print (insertQuery)
					
                Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
			# ro
 
		
if __name__ == "__main__":
    last_week = date.today()
    # last_week = date.today()
    last_week_date1 = (last_week.strftime('%d/%m/%Y'))
    print (last_week_date1)
    # curr_date1 = date.today() - timedelta(1)
    curr_date1 = date.today()
    created_date1 = (curr_date1.strftime('%d.%m.%Y'))
    print (created_date1)
    # raw_input("check")
    main_url = 'https://www.boamp.fr/avis/liste'
    n = 1
    start(main_url)
