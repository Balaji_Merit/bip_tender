#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = 527;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
$ua->proxy('http', 'http://172.27.137.192:3128'); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';
my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# $Tender_Url="http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&agencyFlag=true&_=1515433177467";
# $Tender_Url="http://www.txsmartbuy.com/ShopFlow/services/SP.ss?filter=T&_=1515686835508";
$Tender_Url="http://esbd.cpa.state.tx.us/sagencybid.cfm?startrow=26&endrow=50&ag_num=All&orderby=Agency";



# $Tender_ID='527';
# $TenderName ="Pakistan: Sui Southern Gas Company Limited";
my $x=1;
my $y=25;
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	
# $Tender_Url="www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&agencyFlag=true&_=1515433177467";
#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content520.html");
print ts "$Tender_content1";
close ts;
my $count=1;
#### To get the tender link ####
top:

# while($Tender_content1=~m/<a href="([^>]*?)"\s*>[^>]*?<\/a>\s*<\/font>/igs)
# while($Tender_content1=~m/"solicitationId":"([^>]*?)"/igs)
while($Tender_content1=~m/<strong><a href="([^>]*?)">/igs)
{
# my $tender_link = "http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&solNum=".$1."&_=1515432398422";
my $Tender_page="http://esbd.cpa.state.tx.us/".$1;
print $Tender_page;
my $Tender_page="http://esbd.cpa.state.tx.us/".$1;
my ($Tender_content2)=&Getcontent($Tender_page);
$Tender_content2 = decode_utf8($Tender_content2);

	# while($Tender_content2=~m/"solicitationId":"([^>]*?)"/igs)
	# {
	# my $tender_link="http://esbd.cpa.state.tx.us/".$1;
	
	# my ($tender_link_content1,$Referer)=&Getcontent($tender_link);

		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		# next if($tender_link_content1=~m/Addendum/is);
		if($Tender_content2=~m/<TR><TD bgcolor="E8F3F4"><font color="black" face="Verdana,Arial,Helvetica,Sans-serif" size="4">\s*([^>]*?)\s*<\/td><\/tr>/is)
		{
		$tender_title=$1;
		}
			if($Tender_content2=~m/<B>Agency Requisition Number:<\/b>\s*([^>]*?)\s*<\/td><\/tr>/is)
		{
		$Authority_Reference=&clean($1);
		$Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		}
		# my $tender_link = "http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&solNum=".$Authority_Reference."&_=1515432398422";
		# my $tender_link = "http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&solNum=".$Authority_Reference."&_=1515686835520";
		# my $referer = "http://www.txsmartbuy.com/sp/".$1;
		# my ($tender_link_content1,$Referer)=&Getcontent($tender_link,$Authority_Reference);
			if($Tender_content2=~m/<B>Agency:<\/b>\s*([^>]*?)\s*<TR>/is)
		{
		$Awarding_Authority=$1;
		}	
	if($Tender_content2=~m/<B>Contact Information:<\/b><\/td><\/tr>([\w\W]*?)<\/table>/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".$1;
		}	
		
	$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
	$Awarding_Authority=&clean($Awarding_Authority);
	
	# if($block=~m/"solicitationId":"([^>]*?)"/is)
		# {
		# $Authority_Reference=&clean($1);
		# }
		
	if($Tender_content2=~m/<B>Open Date:<\/b>\s*([^>]*?)\s*<\/td>/is)
		{
		my $d_date=$1;
		if($d_date=~m/(\d{1,2})\/(\d{1,2})\/(\d{4})/is)
		{
		$Deadline_Date=$2."/".$1."/".$3;
		}
		elsif($d_date=~m/(\d{1,2})\/(\d{1,2})\/(\d{2})/is)
		{
		$Deadline_Date=$2."/".$1."/20".$3;
		# $Deadline_Date=$1;
		}
		$Deadline_Date=&clean($Deadline_Date);
		# $Deadline_Date=~s/\s+\s+/ /igs;
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($Deadline_Date);
		}
		my $temp_time;
		if($Tender_content2=~m/<B>Open Date:<\/b>\s*([^>]*?)\s*<\/td>/is)
		{
		$temp_time = $1;
		if($temp_time=~m/(\d{1,2})\:(\d{1,2})\s*(?:PM|pm)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($temp_time=~m/(\d{1,2})\:(\d{1,2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		# if($temp_time=~m/(\d{1,2})\:(\d{1,2})(?:\:00|\s*)/is)
		# {
		# $Deadline_time=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
		
		# }
		$Deadline_Description="Closing Date:";

	# if($tender_link_content1=~m/<strong>NOTE:<\/strong>([\w\W]*?)WOSG FOOTER/is)
	# {
	$Description=$Tender_content2;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	# $Description=~s/\-//igs;
	# }

my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$Tender_page,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
		# if($tender_link_content1=~m/<TR><TD bgcolor="[^>]*?"><font color="black" face="[^>]*?">([\w\W]*?)WOSG FOOTER/is)
	# {
	$tenderblock=$Tender_content2;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	# }
	
$cy='US';
		# open (RE,">>Digital527.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# exit;
}
}


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $auth_ref = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "www.txsmartbuy.com");
	# $req->header("Accept"=> "*/*");
	# $req->header("X-Requested-With"=> "XMLHttpRequest");

	
	# $req->header("Referer"=> "http://www.txsmartbuy.com/sp/".$auth_ref);
	$req->header("Cookie"=> "CFID=83428230; CFTOKEN=46407382; TS014ef742=01c008e7daa84f9d2f031ef24e545df293cd13a07c305a00ab179a480f95314af4d176cae74aee36915353758493a34af39e7ebfb5edba1cf2f4ba498d0d00da89c7ef10687632b29a9788c7422714a37c7cb9e63a");
	

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "esbd.cpa.state.tx.us");
	$req->header("Referer"=> "http://esbd.cpa.state.tx.us/sagencybid.cfm?startrow=1&endrow=25&ag_num=All&orderby=Agency");
	$req->header("Cookie"=> "CFID=83428230; CFTOKEN=46407382; TS014ef742=01c008e7daa84f9d2f031ef24e545df293cd13a07c305a00ab179a480f95314af4d176cae74aee36915353758493a34af39e7ebfb5edba1cf2f4ba498d0d00da89c7ef10687632b29a9788c7422714a37c7cb9e63a");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://sasktenders.ca/content/public/Search.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\-//igs;
	decode_entities($Clean);
	return $Clean;
}