from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

import requests
import re
import os,sys
import pymysql
import time
import json
import pymysql
# import xlwt
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
import pytz
# import ConfigParser
import dateutil.parser as dparser
from time import sleep

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
import pymysql
import glob
import email.utils
import dateparser
import time
from datetime import datetime, timedelta

# import codec
# warnings.filterwarnings('ignore')
# reload(sys)
# sys.setdefaultencoding('utf-8')


Database_Connector = imp.load_source('Database_Connector','C:\\BIP_Tender\\UK_Scripts\\Database_Connector1.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
tender_id=518
origin="Worldwide/Non Defence"
sector=	"Worldwide"
country="SG"
websource="Singapore Government"
def reg_clean(desc):
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc =  desc.replace("s'","s''")
	desc = desc.replace('\s+\s+', ' ')
	desc = desc.replace('layout_RepaintAllLayouts();', ' ')
	desc = desc.replace('\t', ' ')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\{[\w\W]*?\}', ' ', str(desc))
	desc = re.sub(r"&#39;","''", str(desc))
	# desc = re.sub(r"'","''", str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "''", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	desc = desc.replace('&nbsp;','')
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc =  desc.replace("''''","''")
	for i in range(1,10):
		desc = desc.replace('\s+\s+', ' ')
	desc = desc.replace('\t', ' ')
	return desc

def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	# dt = parse(Input)
	dt=dparser.parse(Input,fuzzy=True)
	FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	return (FormattedDate)
	
def decodeEmail(e):
    de = ""
    k = int(e[:2], 16)
    # print k    
    
    for i in range(2, len(e)-1, 2):
        de += chr(int(e[i:i+2], 16)^k)
        # print de
        
    return de	
def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	# time1 = re.findall('(\d{1,2})(?:\:\d{2}\s*|\s*)(?:Noon|noon|pm)',str(Input))
	time1 = re.findall('(\d{1,2})\s*(?:Noon|noon|pm|PM)',str(Input))
	print (time1)
	if time1:
		if int(time1[0]) < 12:
			Formattedtime = str(int(time1[0])+12) +":00:00"
		else:
			Formattedtime = str(int(time1[0])) +":00:00"
	print (Formattedtime)	
	time2 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)',str(Input))
	# print time1[0][0]
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			if time2[0][1]:
				Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+"0:00"
			else:
				Formattedtime = str(int(time2[0][0])+12)  +":00:00"
		else:    
			if int(time2[0][0]) == 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)',str(Input))
        
		if time2:
			if int(time2[0][0]) <= 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			
		else:
			time2 = re.findall('(\d{1,2})\s*(?:pm|PM\s*)',str(Input))
			print (time2)
			if time2:
				if int(time2[0][0]) < 12:
					Formattedtime = str(int(time2[0][0])+12) +":00:00"
			else:		
				time2 = re.findall('(?:Midday|midday|12\s*noon|12.00\s* Noon|12.00\s*noon)',str(Input))
				if time2:
					Formattedtime = "12:00:00"
				else:
					time2 = re.findall('(?:Midnight|midnight)',str(Input))
					if time2:
						Formattedtime = "00:00:00"
					else:
						Formattedtime = Input
					# Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
	# dt=dparser.parse(Input,fuzzy=True)
	# Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
	print (Formattedtime)
	return (Formattedtime)
	
def sendEmail(query):
	## Testing 
	msg_toAddress = 'balaji.ekambaram@meritgroup.co.uk'
	# msg_ccAddress = 'vinothkumar.rajendren@meritgroup.co.uk , balaji.ekambaram@meritgroup.co.uk'
	# curr_date = dateparser.parse(time.asctime())
	# created_date = (curr_date.strftime('%d-%m-%Y'))
	ToAddress = ['balaji.ekambaram@meritgroup.co.uk']
	# CcAddress = ['vinothkumar.rajendren@meritgroup.co.uk','balaji.ekambaram@meritgroup.co.uk']
	# previous_date=datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
	# today_date=datetime.strftime(datetime.now(), '%Y-%m-%d')
	# Normal_tenders = "select id,tender_name,tender_link,origin, noticesector, cy from tender_master a left join (select process_id from tender_data where cast(tender_data.created_date as date) >= '20190901' and cast(tender_data.created_date as date) <= '20190930') b on a.id = b.process_id where a.id is not null and ifnull(b.process_id,'') = '' and a.active = 'Y';"
	# print (Normal_tenders)
	# records=select_query(Normal_tenders)
	msg = MIMEMultipart()
	fromaddr = 'autoemailsender@meritgroup.co.uk'
	msg['From'] = email.utils.formataddr(('Insert error - Tender ID: 518', fromaddr))
	msg['To'] = msg_toAddress
	# msg['Cc']= msg_ccAddress
	created_date1 = (curr_date.strftime('%b %d, %Y'))
	# total_count=0
	msg['Subject'] = "BBIP Tender - InsertFailed - 518 -"+str(created_date1)
	body1 = """<p>Hi team,

				Please find the Below query has not inserted <br>
				<br>
				
				Insert QUERY : """+str(query)+"""
				<br>
				<br>
			<p><em>Note: This is an automatically generated, please don't reply to this email id.</em></p>
			<p>Thanks &amp; Regards,<br />Merit Software Support.</p>"""
	msg.attach(MIMEText(body, 'html'))
	server = smtplib.SMTP('74.80.234.196', 25)
	server.ehlo()
	server.starttls()
	user='meritgroup'
	password='sXNdrc6JU'
	server.login(user, password)
	text = msg.as_string()
	toaddr = ToAddress
	server.sendmail(fromaddr, toaddr, text)
	print('Email sent successfully.')
	server.quit()		
browser = webdriver.Chrome()
browser.maximize_window()
wait = WebDriverWait(browser, 30)
browser.get('https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml')
# driver = webdriver.Chrome(executable_path="chromedriver.exe", )
# driver.get("https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml")
wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, "#contentForm\:j_id41 > a")))


opportunites = browser.find_element_by_css_selector('#contentForm\:j_id41 > a')
opportunites.click()
next_page = True

while next_page:
    # url="https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml?origin=opportunities"
    wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, 'a[type="link"].commandLink_TITLE-BLUE')))
    links = browser.find_elements_by_css_selector('a[type="link"].commandLink_TITLE-BLUE')
    next_page = browser.find_element_by_css_selector('input.formRepeatPagination2_NAVIGATION-BUTTON[value="Next"]')
    time.sleep(30)
    
    for i in range(0, len(links)):
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, 'a[type="link"].commandLink_TITLE-BLUE')))
        links = browser.find_elements_by_css_selector('a[type="link"].commandLink_TITLE-BLUE')
        browser.execute_script("arguments[0].click();", links[i])
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, '[value="Back to Search Results"]')))
        
        page_content = browser.page_source
        with open ('nxt_page12.html','w',encoding='utf-8') as f:
            f.write(page_content)

        url=re.findall('Click on the link below to view the opportunity: %0D([^>]*?)%26origin=email%0D%0DRegards&cc=\'',page_content)
        if url:
            url1=url[0]
        Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
        Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, url1)	
        title = re.findall('<div class="[^>]*?_TITLE-[^>]*?" style=[^>]*?>([^>]*?)<\/div>',page_content)
        if title:
            title = reg_clean(title[0])
            title1=title
            print ("Name:",title)
		# Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))	
        # Duplicate_Check_Title = Database_Connector.DuplicateCheckTitle(Retrive_duplicate, title)
        if Duplicate_Check == "N":
            reference_number=re.findall('((?:Quotation No\.|Tender No\.|Qualification No\.)[\w\W]*?)Reference No\.[\w\W]*?Agency',page_content)
            reference_number1=re.findall('(?:Quotation No\.|Tender No\.|Qualification No\.)[\w\W]*?(Reference No\.[\w\W]*?)Agency',page_content)
            deadline_date=re.findall('Closing on[\w\W]*?outputText_NAME-BLACK" style="text-align: left;">([^>]*?)<br',page_content)
            deadline_time=re.findall('Closing on[\w\W]*?outputText_NAME-BLACK" style="text-align: left;">\d{1,2}\s*\w+\s*\d{4}<br[^>]*?>([^>]*?)<\/div>',page_content)
            # description=re.findall('<label class="Caption">\s*(Short Description[\w\W]*?<label class="Caption">\s*Description\s*[\w\W]*?)<\/textarea>',page_content)
            description=re.findall('<div class="[^>]*?_TITLE-[^>]*?" style=[^>]*?>([^>]*?)<\/div>',page_content)
            agency=re.findall('<span>Agency<\/span><\/label><\/div><\/div><\/div><div[^>]*?><div[^>]*?>([^>]*?)<\/div>',page_content)
            organization=re.findall('WHO TO CONTACT\s*<\/div>([\w\W]*?)ITEMS\s*TO',page_content)
            buyer_name=re.findall('Awarding Agency<\/span>[\w\W]*?<div class="[^>]*?"\s*style="[^>]*?">([^>]*?)<\/div><\/td>',page_content)
			
            customer_name=re.findall('<span class="Required">Customer<\/span>\s*[\w\W]*?value="([^>]*?)"',page_content)
            other_information=re.findall('(Site\s*Briefing[^>]*?)<\/a>',page_content)
            contact_type1=re.findall('<span>Procurement Method</span></div></div></td><td style="vertical-align: top;"><div class="formOutputText_VALUE-DIV " style="text-align: left;">([^>]*?)<\/div><\/td>',page_content)
            # if title:
                # title = title[0]
                # title1=title
                # print ("Name:",title)
				
            if organization:
                organization = reg_clean(organization[0])
				# print ("Name:",organization)
                organization =organization.replace("(For all queries pertaining to this quotation, please seek clarification from the person(s) listed below.) ","")
            if buyer_name:
                # buyer_name = reg_clean(buyer_name[0])
                buyer_name=""
            else:
                buyer_name=""
            if agency:
                agency_name=reg_clean(agency[0])
            else:
                agency_name=""
            email=""	
            block_email=re.findall('WHO TO CONTACT[\w\W]*?<div class="formOutputText_HIDDEN-LABEL outputText_TITLE-BLACK" style="text-align: left;">([\w\W]*?)<\/script><\/table>',page_content)
            print (block_email)
            if block_email:
                email1=re.findall('data-cfemail="([^>]*?)"',block_email)
                for i in email1:
                    email=email+" "+str(decodeEmail(i))
                email=reg_clean(email)	
            if customer_name:
                customer_name = reg_clean(customer_name[0])	
            awarding_authority1=str(agency_name)+" "+str(organization)+" "+str(email)	
            if reference_number:
                authority_reference1 = reg_clean(reference_number[0])
            else:
                authority_reference1 =""
            print ("Reference:",authority_reference1)
            if reference_number1:
                authority_reference2 = reg_clean(reference_number1[0])
                if authority_reference2 == "Reference No.":
                    authority_reference2=""
                print ("Reference:",authority_reference2)
            else:
                authority_reference2=""
            authority_reference1=authority_reference1+" "+ authority_reference2		
            print (deadline_date)			
            if deadline_date:
                
                FormattedDate = DateFormat(deadline_date[0])
				
            # if contact_type1:
                # contact_type1 = reg_clean(contact_type1[0])
            contact_type1=""	
            if deadline_time:
                deadline_time1 = timeFormat(deadline_time[0])
					# print ("Deadline Time:",deadline_time1)
            awarding_authority1=reg_clean(awarding_authority1)
            if description:
                description1=reg_clean(description[0])
            if other_information:
                other_information2=reg_clean(other_information[0])
            else:
                other_information2=""
            tender_block=re.findall('<span class="breadcrumb_TEXT">Overview<\/span><\/div>([\w\W]*?)<script type="text/javascript">',page_content)
            if tender_block:
                tender_block=reg_clean(tender_block[0])
					# print (description1)
            description1=reg_clean(description1)		
            
            deadline_description="Closing on"
            value1=''
            location1=''
            cpv_code1=''
            award_procedure1=''
            BIP_Non_UK='Y'
            other_information1 = "For further information and instructions in the above contract notice please visit website: "+str(url1)
            curr_date = datetime.now(pytz.timezone('Europe/London'))
            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
            if "CORRIGENDUM" not in page_content:
                try:
                    insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date,BIP_Non_UK) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url1), str(tender_block), str(origin), str(sector), str(country), str(websource), str(title1), str(title1),str(awarding_authority1), str(award_procedure1), str(contact_type1), str(cpv_code1), str(description1), str(location1), str(authority_reference1), str(value1), str(deadline_description), str(FormattedDate), str(deadline_time1),str(other_information1), str(other_information2), str(created_date),'Y')
                    print (insertQuery)
                    Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
                except Exception as e:	
                    print (e)
                    insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date,BIP_Non_UK) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url1), str(tender_block), str(origin), str(sector), str(country), str(websource), str(title1), str(title1),str(awarding_authority1), str(award_procedure1), str(contact_type1), str(cpv_code1), str(description1), str(location1), str(authority_reference1), str(value1), str(deadline_description), str(FormattedDate), str(deadline_time1),str(other_information1), str(other_information2), str(created_date),'Y')
                    sendEmail(insertQuery)
        # Process data here
        back_to_results = browser.find_element_by_css_selector('[value="Back to Search Results"]')
        back_to_results.click()
    wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, 'input.formRepeatPagination2_NAVIGATION-BUTTON[value="Next"]')))
    next_page = browser.find_element_by_css_selector('input.formRepeatPagination2_NAVIGATION-BUTTON[value="Next"]')
    next_page.click()
    next_disabled_page = browser.find_elements_by_css_selector('input.formRepeatPagination2_NAVIGATION-BUTTON[value="Next"][disabled="disabled"]')
    if next_disabled_page:
        next_page = False


