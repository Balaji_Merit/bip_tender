#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="https://www.iadb.org/en/procurement-notices-search";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
my %Months_To_Text=("01" => "jan", "02" => "feb", "03" => "mar", "04" => "apr", "05" => "may", "06" => "jun", "07" => "jul", "08" => "aug", "09" => "sep", "10" => "oct", "11" => "nov", "12" => "dec");
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content671.html");
print ts "$Tender_content1";
close ts;
my $count=1;
my $Tender_content2;
#### To get the tender link ####
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print $month1;
my $year1		= strftime "%y", localtime;
print $year1;
for (my $ddt=1;$ddt<=$date1;$ddt++)
{
my @state1=("1","2","68","4","3","5","6","7","8","9","10","11","69","12","13","14","15","16","17","18","19","20","21","22","23","71","24","25","26","27","70","28","29","30","31","32","33","34","35","36","37","38","39","65","40","41","42","43","44","45","46","67","47","48","49","50","51");
my $date2=$ddt;
$date2=$date2-1;
$date2="0$date2" if(length($date2)==1);
my $month2=$month1;
# $month1 = "0$month1" if (length($month1)==1);
$month2="0$month2" if(length($month2)==1);
my $tender_category_content1;
my $n_month =  $Months_To_Text{$month2}; 
print $n_month;

my $date3 = $date2."-".$n_month."-20".$year1;
print $date3;
# for (my $i=0;$i<5;$i++)
# {
# my $Tender_Url1 = "https://www.ungm.org/Public/Notice/Search";
# my $post_cont = '{"PageIndex":'.$i.',"PageSize":15,"Title":"","Description":"","Reference":"","PublishedFrom":"'.$date3.'","PublishedTo":"'.$date3.'","DeadlineFrom":"","DeadlineTo":"","Countries":[],"Agencies":[],"UNSPSCs":[],"NoticeTypes":[],"SortField":"DatePublished","SortAscending":false,"isPicker":false,"NoticeTASStatus":[],"NoticeDisplayType":null,"NoticeSearchTotalLabelId":"noticeSearchTotal"}';
# my ($Tender_content2)=&Postcontent($Tender_Url1,$post_cont);
# $Tender_content2 = decode_utf8($Tender_content2);  
# open(ts,">Tender_content670_1.html");
# print ts "$Tender_content2";
# close ts;
# while($Tender_content2=~m/data-noticeid="([^>]*?)"\s*class/igs)
while($Tender_content2=~m/<td><a href="(\/en[^>]*?)">[^>]*?<\/a><\/td>/igs)
{
my $tender_link = "https://www.iadb.org".$1;
# my $block=$1;
# print $block;
# my $Tender_page="http://esbd.cpa.state.tx.us/".$1;
# my ($Tender_content2)=&Getcontent($Tender_page);
# $Tender_content2 = decode_utf8($Tender_content2);
print $i;
	# while($Tender_content2=~m/"solicitationId":"([^>]*?)"/igs)
	# {
	# my $tender_link="https://www.ungm.org/Public/Notice/".$1;
	
	my ($tender_link_content1,$Referer)=&Getcontent($tender_link);

		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		# next if($tender_link_content1=~m/Addendum/is);
		if($tender_link_content1=~m/<span class="title">([^>]*?)<\/span>/is)
		{
		$tender_title=&BIP_Tender_DB::clean($1);
		}
			if($tender_link_content1=~m/<span class="label">\s*Reference\s*\:\s*<\/span>\s*<span class="value">([^>]*?)<\/span>/is)
		{
		$Authority_Reference=&clean($1);
		$Authority_Reference="Reference: ".$Authority_Reference;
		}
		# my $tender_link = "http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&solNum=".$Authority_Reference."&_=1515432398422";
		# my $tender_link = "http://www.txsmartbuy.com/app/site/hosting/scriptlet.nl?script=206&deploy=1&solNum=".$Authority_Reference."&_=1515686835520";
		# my $referer = "http://www.txsmartbuy.com/sp/".$1;
		# my ($tender_link_content1,$Referer)=&Getcontent($tender_link,$Authority_Reference);
			if($tender_link_content1=~m/<div class="ungm-panel">\s*<div class="title">\s*<span class="highlighted">\s*([^>]*?)\s*<\/span>/is)
		{
		$Awarding_Authority=$1;
		}	
	if($tender_link_content1=~m/<div class="ungm-panel">\s*<div class="ungm-list-item ungm-background">\s*([\w\W]*?)\s*<\/div>\s*<\/div>\s*<\/div>/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".&clean($1);
		$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
		}	
		
	# $Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
	# $Awarding_Authority=&clean($Awarding_Authority);
	
	# if($tender_link_content1=~m/"solicitationId":"([^>]*?)"/is)
		# {
		# $Authority_Reference=&clean($1);
		# }
		
	if($tender_link_content1=~m/Deadline on:<\/span>\s*<span class="value">\s*([^>]*?)\s*\d{1,2}:\d{1,2}/is)
		{
		my $d_date=$1;
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($d_date);
		}
		my $temp_time;
		if($tender_link_content1=~m/Deadline on:<\/span>\s*<span class="value">\s*[^>]*?\s*(\d{1,2}:\d{1,2})/is)
		{
	
		$Deadline_time = $1;
		 }
		
	if($tender_link_content1=~m/Deadline on:<\/span>\s*<span class="value">\s*[^>]*?\s*\d{1,2}:\d{1,2}\s*([^>]*?)\s*<\/span>/is)
		{
		$Deadline_Description=&BIP_Tender_DB::clean($1);
		$Deadline_Description=&clean($Deadline_Description);
}

$Deadline_Description="Deadline: ".$Deadline_Description;
	if($tender_link_content1=~m/<div class="title">Description<\/div>\s*([\w\W]*?)\s*<\/div>/is)
	{
	$Description=$1;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	# $Description=~s/\-//igs;
	}

my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
		if($tender_link_content1=~m/<span class="title">([\w\W]*?)<div class="footer">/is)
	{
	$tenderblock=$1;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	}
	
	# if($tender_link_content1=~m/<div class= "unspscSelector">\s*([\w\W]*?)\s*<div class="footer">/is)
	# {
	# $CPV_Code=$1;
	# $CPV_Code = &BIP_Tender_DB::clean($CPV_Code);
	# $CPV_Code=&clean($CPV_Code);
	# }
	
	if($tender_link_content1=~m/<span class="label">Beneficiary country\(ies\)\:<\/span>\s*<span class="value">\s*([^>]*?)\s*<\/span>/is)
	{
	$cy=&clean($1);
	$Location = "Beneciary country(ies): ".&clean($1);
	# $CPV_Code = &BIP_Tender_DB::clean($CPV_Code);
	# $CPV_Code=&clean($CPV_Code);
	}
# $cy='US';
		# open (RE,">>Digital527.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# exit;
}
}
}
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "www.merx.com");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.ungm.org");
	# $req->header("Referer"=> "https://www.etenders.gov.eg/Tender/DoSearch?status=3");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.merx4.merx.com");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.ungm.org");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/json");

	$req->header("Referer"=> "https://www.ungm.org/Public/Notice");
	# $req->header("Cookie"=> "ASP.NET_SessionId=m1h2f0o4aezfjzj3smy1hoga; _ga=GA1.2.649488281.1532349243; _gid=GA1.2.1741626307.1532349243; UNGM.UserPreferredLanguage=en");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/&#39;/''/igs;
	# Lao People&#39;s Democratic Republic
	$Clean=~s/Expand all Collapse all\s*//igs;
	# decode_entities($Clean);
	return $Clean;
}