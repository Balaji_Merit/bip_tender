# -*- coding: utf-8 -*-
import requests
import re
import xlwt
import os,sys
import imp
import pymysql
import dateparser
import redis
import time
from googletrans import Translator
import redis
# import cookielib

# reload(sys)
# sys.setdefaultencoding('utf-8')
# Database_Connector = imp.load_source('Database_Connector','Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


file_name=os.path.basename(__file__)
tender_id1 = file_name.replace("BIP_Tender_","")
Tender_ID = tender_id1.replace(".py","")
main_url_query = u"select tender_link from tender_master where id="+str(Tender_ID)

tbody_re = r'(<table\s*id\=\"Taperturas\"[\w\W]*?<\/table>)'
blocks_re = '<tr>([\w\W]*?)<\/tr>'
title_link_re = '<td class="titulo">\s*<a\s*href="([^>]*?)"\s*title="([^>]*?)">'
base_url = 'https://contratacion.aena.es/contratacion/'
sub_title_re = '<th[\w\W]*?>\s*T[\w\W]*?tulo\:\s*</th>\s*<td>\s*([^>]*?)\s*<\/td>'
place_re  = '<th[\w\W]*?>Lugar\:[\w\W]*?<address>\s*([\w\W]*?)\s*<\/address>'
contace_table_re = 'Presentaci[\w\W]*?<table[\w\W]*?Condiciones para la presentaci[\w\W]*?">([\w\W]*?)<\/table>'
infromation_block_re = '<th[\w\W]*?>Consulta\s*de\s*informaci[\w\W]*?n</th>([\w\W]*?)<\/td>'
info_query_re = '<li>\s*([\w\W]*?)\s*</li>'
contract_detail_block_re = 'Licitaciones[\w\W]*?<table[\w\W]*?class\=\"detalles\"[\w\W]*?>([\w\W]*?)<\/table>'
award_form_re = 'Forma\s*adjudicaci[\w\W]*?n\:\s*<\/th>\s*<td>\s*([^>]*?)<\/td>'
file_no_re = 'N[\w\W]*?de expediente\:\s*<\/th>\s*<td>\s*([^>]*?)<\/td>'
amount_tender_re = 'Importe\s*bruto\s*licitaci[\w\W]*?n\:\s*<\/th>\s*<td>\s*([^>]*?)<\/td>'
# deadline_re = 'Fecha[\w\W]*?<td>\s*([\w\W]*?)\s*a\s*las\s*([\w\W]*?)\s*\('
deadline_re = 'Fecha[\w\W]*?<td>\s*([\d\/]+)\s*[^>]*?\s*([\d\:]+)\s*[^>]*?<\/td'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
# Tender_ID = 529
award1='<address>([\w\W]*?)<\/address>'
next_page_re = '<a\s*href\=\"([^>]*?)"[^>]*?>\s*Siguiente\s*<\/a>'
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate
global header

referer=''
sessi = ''
s=requests.session()
cookies_re = '\[\<Cookie\s*JSESSIONID\=([^>]*?)for'
# cookie_file = 'tmp/cookies'
# cj = cookielib.LWPCookieJar(cookie_file)
# s = requests.session()
# s.cookies = cj
def req_content(url):


    # req_url = requests.get(url, headers = {})

    header = {
        "Host": "contratacion.aena.es",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:56.0) Gecko/20100101 Firefox/56.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
    }

    r = s.get(url, headers=header)
    sess = regxx(cookies_re, str(s.cookies))
    url_content = r.content.decode('utf_8', errors='ignore')
    return url_content

def req_content_ses(url,ref):

    referer = url
    ''' regex '''

    cookie_file = 'tmp/cookies'
    cj = cookielib.LWPCookieJar(cookie_file)
    s = requests.session()
    s.cookies = cj
    # req_url = requests.get(url, headers = {})

    r = s.get(url, headers=header)
    sess = regxx(cookies_re,str(s.cookies))

    url_content = r.content.decode('utf_8', errors = 'ignore')


    return url_content


def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def reg_clean(value):
    value = re.sub(r'<[^<]*?>', '', str(value))
    value = re.sub(r'\s+', '', str(value))
    # value = re.sub(r'', '', str(value))
    return  value

def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()
    if check:
        translated_sentence = redis_con.get(InputText)
    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                payload = {'q': str(InputText),
                           'target': 'en',
                           'source' : 'es'
                            }

                s = requests.post(url, data=payload)
                value = s.json()
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                redis_con.set(InputText, translated_sentence)
            except Exception as e:
                print ('Translate Error :', e)
    return translated_sentence


def start():
    global  n
    global  main_url
    while True:
        main_content = req_content(main_url)
        with open('main.html', 'w')as f:
            f.write(main_content)
        tbody = regxx(tbody_re,main_content)
        if tbody:
            tbody = tbody[0]
            blocks = regxx(blocks_re,tbody)
            for enum,block in enumerate(blocks):
                n = n + 1
                title_link = regxx(title_link_re,block)

                if title_link:
                    sub_url = title_link[0][0]
                    main_page_title = title_link[0][1]
                    sub_url_full = base_url + sub_url
                    Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url_full)
                    if Duplicate_Check == "N":
                        curr_date = dateparser.parse(time.asctime())
                        created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))

                        ##########################################
                        sub_content = req_content(sub_url_full)
                        sub_title = regxx(sub_title_re,sub_content)
                        if sub_title:
                            sub_title = sub_title[0]
                        else:
                            sub_title = ''
                        contact_table = regxx(contace_table_re,sub_content)
                        FormattedDate = ''
                        deadline_time = ''
                        info_query = ''
                        awardingAuthority = ''
                        if contact_table:
                            contact_table = contact_table[0]
                            place = regxx(place_re, contact_table)
                            if place:
                                place = place[0]
                            else:
                                place = ''
                            infromation_block = regxx(infromation_block_re,contact_table)
                            if infromation_block:
                                infromation_block = infromation_block[0]
                                info_query_raw = regxx(info_query_re,infromation_block)
                                if info_query_raw:
                                    info_query = []
                                    for data in info_query_raw:
                                        val = reg_clean(data)
                                        info_query.append(val)
                                    info_query = '|'.join(info_query)
                                else:
                                    info_query = ''
                            else:
                                infromation_block = ''
                            info_query_raw1 = regxx(award1,sub_content)
                            if info_query_raw1:
                                award2=info_query_raw1[0]
                                award2=award2.replace('<br/>','|').strip()
                                print ("award:",award2)
                            else:
                                award2=''
                            awardingAuthority = 'AENA '+ award2 + '|'+ place +'|'+ info_query
                            print (awardingAuthority)
                            awardingAuthority = re.sub(r"'", "''", str(awardingAuthority))
                            deadline = regxx(deadline_re, contact_table)
                            if deadline:
                                deadline_date = deadline[0][0]
                                deadline_time = deadline[0][1]
                                dt = dateparser.parse(deadline_date)
                                FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                            else:
                                deadline_date = ''
                                deadline_time = ''
                                FormattedDate = ''
                        else:
                            contact_table = " Contact None"

                        contract_detail_block = regxx(contract_detail_block_re,sub_content)
                        awardProcedure = ''
                        amount_tender = ''
                        if contract_detail_block:
                            contract_detail_block = contract_detail_block[0]

                            award_form = regxx(award_form_re, contract_detail_block)
                            if award_form:
                                award_form = award_form[0]
                            else:
                                award_form = ''
                            awardProcedure = award_form


                            file_no = regxx(file_no_re, contract_detail_block)
                            if file_no:
                                file_no = file_no[0]
                            else:
                                file_no = ''

                            amount_tender = regxx(amount_tender_re, contract_detail_block)
                            if amount_tender:
                                amount_tender = amount_tender[0]
                            else:
                                amount_tender = ''

                            infromation_block = regxx(infromation_block_re,contact_table)
                            if infromation_block:
                                infromation_block = infromation_block[0]
                                info_query_raw = regxx(info_query_re,infromation_block)
                                if info_query_raw:
                                    info_query = []
                                    for data in info_query_raw:
                                        val = reg_clean(data)
                                        info_query.append(val)
                                    info_query = '|'.join(info_query)
                                else:
                                    info_query = ''

                            else:
                                infromation_block = ''
                        n = n + 1
                        summary_cont = summary + sub_url_full
                        orgin = 'European/Defence'
                        sector = 'European Translation'
                        country = 'ES'
                        websource = 'Spain: Aena Suppliers'

                        sub_title = translateText(sub_title)
                        awardingAuthority = re.sub('<[^>]*?>', '', awardingAuthority)
                        # awardingAuthority = translateText(awardingAuthority)
                        awardProcedure = translateText(awardProcedure)
                        amount_tender = amount_tender.replace('&euro;','').strip()
                        if FormattedDate:
                            insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_url_full), str(), str(orgin), str(sector), str(country),str(websource),str(sub_title), str(sub_title), str(awardingAuthority), str(awardProcedure), str(),str(),str(sub_title), str(), str(file_no), str(amount_tender), str("Deadline:"), str(FormattedDate),str(deadline_time),str(), str(summary_cont), str('Y'),str(created_date))
                        else:
                            insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_url_full), str(), str(orgin), str(sector), str(country),str(websource), str(sub_title),str(sub_title), str(awardingAuthority),str(awardProcedure), str(), str(), str(sub_title), str(), str(file_no),str(amount_tender),str(),str(summary_cont), str('Y'),str(created_date))
                        insertQuery = insertQuery.replace("'b'","'")
                        insertQuery = insertQuery.replace("'',","',")
                        insertQuery = insertQuery.replace(",',",",'',")
                        insertQuery = insertQuery.replace(",'',',",",'','',")	
                        val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                        # print ("insertion status :", val)
                        if val == 1:
                            print ("1 Row Inserted Successfully")
                        else:
                            print ("Insert Failed")
                        n = n + 1
        nxt_page = regxx(next_page_re,main_content)
        if nxt_page:
            nxt_page = nxt_page[0]
            next_page = base_url + nxt_page
            main_url = next_page
            print (main_url)
        else:
            print ("--over--")
            break



if __name__== "__main__":
    main_url = 'https://contratacion.aena.es/contratacion/principal?portal=licitaciones'

    n = 1
    start()
