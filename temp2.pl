#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX qw( strftime );
# use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use DateTime;
use DateTime::TimeZone;
use DateTime::Format::Strptime;
use BIP_Tender_DB1;
use JSON::Parse 'parse_json';


# my $deaddate='1548086400000';
my $deaddate='1550232000000';
my $Deadline_Date = strftime("%Y-%m-%d", gmtime($deaddate/1000));
my $Deadline_time = strftime("%H%M", gmtime($deaddate/1000));

print $Deadline_Date;
print $Deadline_time;