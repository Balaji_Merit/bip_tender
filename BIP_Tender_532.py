import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import urllib.parse
import re
import imp
import pymysql
import time
from urllib.parse import quote 
# import cookielib
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
# import urllib.request as ur
# from six.moves.html_parser import HTMLParser
# from googletrans import Translator
# from datetime import date, timedelta
h = HTMLParser()

# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

def reg_clean(cleaning_value):
    # cleaning_value = cleaning_value.replace('\r', '').replace('\n', '')
    cleaning_value = cleaning_value.replace('&#160;', '')
    cleaning_value = cleaning_value.replace("&#39;", "'")
    cleaning_value = cleaning_value.replace("\\r", " ")
    cleaning_value = cleaning_value.replace("\\n", " ")
    cleaning_value = cleaning_value.replace("\s*", " ")

    # cleaning_value = re.sub(r"'b'", "b'", str(cleaning_value), re.I)
    cleaning_value = re.sub(r'<[^<]*?>', ' ', str(cleaning_value))
    cleaning_value = re.sub(r'\r\n', '', str(cleaning_value), re.I)
    cleaning_value = re.sub(r"\\r\\n", '', str(cleaning_value), re.I)
    cleaning_value = re.sub(r'\t', " ", cleaning_value, re.I)
    cleaning_value = re.sub(r'\s\s+', " ", cleaning_value, re.I)
    cleaning_value = re.sub(r"^\s+\s*", "", cleaning_value, re.I)
    cleaning_value = re.sub(r"\s+\s*$", "", cleaning_value, re.I)
    cleaning_value = re.sub(r"\&rsquo\;", "'", cleaning_value, re.I)
    cleaning_value = re.sub(r"\&ndash\;", "-", cleaning_value, re.I)
    cleaning_value = re.sub(r"'", "''", cleaning_value, re.I)
    print (cleaning_value)
    return cleaning_value

tbody_re = '<tbody>([\w\W]*?)<\/tbody>'
block_re = '\{("\@search\.score"[\w\W]*?)\}'
baseUrl = 'https://www.hankintailmoitukset.fi'
sublink_re = '<td>([\d\.\:\-]+)\s+([\d\.\:\-]+)<\/td>\s*<td>\s*<a\s*href\=\"([^>]*?)\"\>([\w\W]*?)\<\/a\>'
name_purchese_re = '"projectTitleNormalized":"([^>]*?)",<\/dd>'
title_re='"title":"([^>]*?)",'
title_re1='"projectTitle:"([^>]*?)",'
id_re='"id"\s*\:\s*"([^>]*?)",'
projectid_re='"procurementProjectId"\s*:\s*\s*([^>]*?)\s*\,'
contracting_enti_re = 'Hankintayksikk[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)<\/td>'
bussiness_id_re = 'Y\-tunnus\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
compitive_office_re = 'Kilpailuttamisesta[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
contact_re = '"contactPerson":"([^>]*?)"'
mailing_re = '"streetAddress":"([^>]*?)","postalCode":"[^>]*?","town":"[^>]*?","country":"[^>]*?"'
town_re = '"streetAddress":"[^>]*?","postalCode":"[^>]*?","town":"([^>]*?)","country":"[^>]*?"'
postal_re = '"streetAddress":"[^>]*?","postalCode":"([^>]*?)","town":"[^>]*?","country":"[^>]*?"'
country_re = '"streetAddress":"[^>]*?","postalCode":"[^>]*?","town":"[^>]*?","country":"([^>]*?)"'
phone_re = '"telephoneNumber":"([^>]*?)"'
email_re = '"email":"([^>]*?)"'
website_re = '"mainUrl":"([^>]*?)"'
pp_re = 'Hankintamenettely\<\/dt\>\s*\<dd\>([^>]*?)\<\/dd\>'
address1='"streetAddress":"([^>]*?)","postalCode":"[^>]*?","town":"[^>]*?","country":"[^>]*?"'
value1='"estimatedValue":\s*([^>]*?)\s*,\s*"'
nutscode_re='"nutsCodes":\["([^>]*?)"\]'
cpv_code_re = '"code":"([^>]*?)",'
desc_re = '"objectDescriptions":"([^>]*?)",'
contract_perform_re = 'p[\w\W]*?asiallinen\s*toteutuspaikka[\w\W]*?<dd>([^>]*?)<\/dd>'
organisation_name_re='"organisationName":"([^>]*?)",'
organisation_address_re='"organisationAddress":"([^>]*?)",'
ref_re = '"referenceNumber":"([^>]*?)",'
tender_date_time = 'Deadline for information, tenders or requests to participate:'
date_re = '"tendersOrRequestsToParticipateDueDateTime":"([^>]*?)T'
time_re='"tendersOrRequestsToParticipateDueDateTime":"[^>]*?T(\d{1,2}:\d{2}:\d{2})'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
nxt = '/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D=&page='
contanct_detail_block_re = '(<table\s*[^>]*?class\=\"CONTACT\">[\w\W]*?<\/table>)'
notice_number_re='"noticeNumber":"([^>]*?)"'
award_procedure_re='"mainType":"([^>]*?)"'
# details_re = '<tr[^>]*?>\s*[^>]*?<[td|dd]+[^>]*?>([^>]*?)<\/[td|dd]+>\s*<[td|dd]+>\s*(?:<a\s*[^>]*?>)?([^>]*?)<\/'
details_re = '<tr[^>]*?>\s*[^>]*?<[td]+[^>]*?>([^>]*?)<\/[td]+>\s*<[td]+>\s*([^>]*?)<\/'
Tender_ID = 532

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'fi'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fi'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    if isinstance(translated_sentence, str):
        return translated_sentence
    else:	

        return translated_sentence.decode("utf-8")
	


def req_content(url):
    # proxies = {
    # "http" :"http://172.27.137.199:3128",
    # "https":"https://172.27.137.199:3128"
    # }
    # req_url = requests.get(url,proxies=proxies)
    req_url = requests.get(url)
    url_content = req_url.content.decode('utf-8', errors='ignore')

    return url_content

def regxx(reg,content):
    value = re.findall(reg,content,flags=re.UNICODE)
    # if cont_data:
        # cont_data = cont_data[0] 
    # else:
        # cont_data = ''
    # return h.unescape(cont_data)
    return h.unescape(value)

def regxx1(reg,content):
    value = re.findall(reg,content,flags=re.UNICODE)
    return h.unescape(value)
    # cont_data = re.findall(reg,content)

    # return h.unescape(cont_data)
def main():
	proxies = {
	"http" :"http://172.27.137.192:3128",
	"https":"https://172.27.137.192:3128"
	}
	last_week = date.today()- timedelta(2)
	today1=date.today()+ timedelta(1)
	print ("Today******",last_week)
	last_week_date1 = (last_week.strftime('%d'))
	last_week_date2 = (last_week.strftime('%m'))
	last_week_date3 = (last_week.strftime('%Y'))
	main_url="https://api.hankintailmoitukset.fi/search/notices?search=*&$filter=isLatest%20eq%20true%20and%20(datePublished%20ge%20"+str(last_week)+"%20and%20datePublished%20le%20"+str(last_week)+")&queryType=full&$top=75&$skip=75&searchMode=all&$orderby=datePublished%20desc"
	main_url="https://api.hankintailmoitukset.fi/search/notices?search=*&$filter=isLatest%20eq%20true%20and%20(datePublished%20ge%202020-01-20)&queryType=full&$top=200&$count=true&searchMode=all&$orderby=datePublished%20desc"
	avoid = "Korjausilmoitus"
	avoid2 = "Ilmoituksen numero EUVL"
	main_content = req_content(main_url)
	# print (main_content)
	blocks = regxx(block_re, main_content)
	for enum_i, block in enumerate(blocks):
		print(block)
		title = regxx(title_re1,block)
		if title:
			title=reg_clean(title[0])
			print (title)
			title=title.replace("'","''")
			title = translateText(str(title))
		else:
			title = ''
		id1 = regxx(id_re,block)
		projectid1 = regxx(projectid_re,block)
		url3="https://api.hankintailmoitukset.fi/web/api/notices/"+str(id1[0])
		url_content3 = req_content(url3)
		title = regxx1(title_re,url_content3)
		if title:
			title=reg_clean(title[0])
			title = translateText(str(title))
			print (title)
		else:
			title = ''
		award_procedure = regxx1(award_procedure_re,block)
		if award_procedure:
			award_procedure=reg_clean(award_procedure[0])
			award_procedure = translateText(str(award_procedure))
		else:
			award_procedure = ''	
		
		sub_url1="https://www.hankintailmoitukset.fi/fi/public/procurement/"+str(projectid1[0])+"/notice/"+str(id1[0])+"/overview"
		print (sub_url1)
		suburl_content = req_content(sub_url1)
		deadlinedate = regxx(date_re,block)
		# print (deadlinedate)
		deadlinetime = regxx(time_re,block)
		if deadlinetime:
			deadlinetime = deadlinetime[0]
			# deadlinetime= translateText(str(desc1))
		else:
			deadlinetime = '00:00:00'
		if deadlinedate:
			deadlinedate=deadlinedate[0]
			FormattedDate = str(deadlinedate[0])+" 00:00:00"
		else:
			deadlinedate=""			
		# title = regxx(time_re,block)
		# dt = dateparser.parse(deadlinedate)
		
		Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url1)
		if Duplicate_Check == "N":
			curr_date = dateparser.parse(time.asctime())
			created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			desc1 = regxx(desc_re, block)
			if desc1:
				desc1 = reg_clean(desc1[0])
				desc1= translateText(str(desc1))
			else:
				desc1 = ''
			url1="https://api.hankintailmoitukset.fi/web/api/procurements/"+str(projectid1[0])+"/notices"
			url2="https://api.hankintailmoitukset.fi/web/api/procurements/"+str(projectid1[0])
			print (url1)
			url_content = req_content(url1)
			url2_content = req_content(url2)
			# print (url_content)		
			summary_cont = summary + sub_url1
			organisation_name=regxx(organisation_name_re, block)
			if organisation_name:
				organisation_name=organisation_name[0]
			else:
				organisation_name=''
		
			print (organisation_name)
			reference=regxx(ref_re, url2_content)
			if reference:
				reference=reference[0]
				reference="Reference: "+str(reference)+" "
			else:
				reference=''
			phone=regxx(phone_re, url_content)
			if phone:
				phone=phone[0]
			else:
				phone=''	
			# print (organisation_name)				
			organisation_address=regxx(organisation_address_re, block)
			if organisation_address:
				organisation_address=organisation_address[0]
			else:
				organisation_address=''
			website=regxx(website_re, url_content)
			if website:
				website=website[0]
			else:
				website=''
			value=regxx(value1, url_content)
			if value:
				value=value[0]
				# if value==0:
					# value=''	
			else:
				value=''
			notice_number=regxx1(notice_number_re, url_content)
			if notice_number:
				notice_number=notice_number[0]
				reference=str(reference)+"Notice Number: "+str(notice_number)
				# if value==0:
					# value=''	
			else:
				notice_number=''
					
			deadlinedate = regxx(date_re,url_content)
			if deadlinedate:
				deadlinedate=deadlinedate[0]
				FormattedDate = str(deadlinedate)+" 00:00:00"
			else:
				FormattedDate=""
			# print (deadlinedate)
			deadlinetime = regxx(time_re,url_content)
			if deadlinetime:
				deadlinetime = deadlinetime[0]
				# deadlinetime= translateText(str(desc1))
			else:
				deadlinetime = '00:00:00'				
			
			email1=regxx(email_re, url_content)
			if email1:
				email1=email1[0]
			else:
				email1=''
			contact_person=regxx(contact_re, url_content)
			if contact_person:
				contact_person=contact_person[0]
			else:
				contact_person=''
			nuts_code=regxx(nutscode_re, url_content)
			if nuts_code:
				nuts_code=nuts_code[0]
			else:
				nuts_code=''	
			cpv_code=regxx(cpv_code_re , url_content)
			if cpv_code:
				cpv_code=cpv_code[0]
			else:
				cpv_code=''		
			awarding_authority=	str(contact_person)+"|"+str(organisation_name) + "|"+str(organisation_address)+ "|"+str(phone)+ "|"+str(website)+"|"+str(email1)
			awarding_authority=awarding_authority.replace("u00e4","ä")
			print (awarding_authority)	
			# print (organisation_address)	
			orgin = 'European/Non Defence'
			sector = 'European Translation'
					
			country = 'FI'
			websource = 'Finland: HILMA'
			insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_url1), str(), str(orgin), str(sector), str(country), str(websource), str(title), str(title),str(awarding_authority), str(award_procedure), str(), str(cpv_code), str(desc1), str(), str(reference), str(), str(tender_date_time), str(FormattedDate), str(deadlinetime),str(value), str(summary_cont),str(nuts_code), str('Y'),str(created_date))
			insertQuery = insertQuery.replace("'b'","'")
			insertQuery = insertQuery.replace("'',","',")
			insertQuery = insertQuery.replace(",',",",'',")
			insertQuery = insertQuery.replace(",'',',",",'','',")
			print (insertQuery)
			try:	
				val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
			# print "insertion status :", val
				if val == 1:
					print ("1 Row Inserted Successfully")
				else:
					print ("Insert Failed")
			except Exception as e:

				pass
				
				
			
		
			
		

	# print (main_content)
if __name__== "__main__":
    # Tender_ID = sys.argv[0]
    # Tender_url = Database_Connector.Retrieve_url(mysql_cursor,Tender_ID)
    main_url = 'https://www.hankintailmoitukset.fi/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D='
    nxt_page_link = 'https://www.hankintailmoitukset.fi/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D=&page='
    n = 1
    main()	