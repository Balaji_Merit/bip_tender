#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX qw( strftime );
# use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use DateTime;
use DateTime::TimeZone;
use DateTime::Format::Strptime;
use BIP_Tender_DB1;
use JSON::Parse 'parse_json';

#### Getting the Input Company ID from the Script Name ####
# my $Input_Tender_ID = $0;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $Input_Tender_ID	= $ARGV[0];
# my $Input_Tender_ID	= '101';

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts =>{SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0");
$ua->proxy('https', 'http://172.27.137.199:3128');
# $ua->proxy('http', 'http://172.27.137.192:3128');

# $ua->proxy('https', 'http://172.27.137.199:3128');
$ua->timeout(30);
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);


### Database Initialization ####
my $dbh = &BIP_Tender_DB1::DbConnection();
my $Input_Table='tender_master';

### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB1::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB1::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date =@$Processed_Date;
my $homeUrl;
if ($Tender_Url=~m/^\s*(http[^>]*?aspx)[^>]*?/is)
{
	$homeUrl = $1."/Home";
	print $homeUrl;

}


my ($HomeContent,$CookieeeeeePOST)=&Getcontent1($homeUrl);
$CookieeeeeePOST=~s/\;[^>]*?$/;/igs;
print ("Coo :: $CookieeeeeePOST\n");


my $StartDate=DateTime->now();
print "StART :: $StartDate\n";
my $ms = (1000 * $StartDate->epoch);
# use Time::HiRes qw(gettimeofday);

# my ($sec,$milli) = gettimeofday;
# my $milli2 = sprintf("%.*s", 3, $milli);
# my $ms = join ('',$sec,$milli2);

#### Ping the first level link page ####

my $pageNo = 4;
my $count = 4;

my ($link1,$link2);
if ($Tender_Url=~m/^\s*(http[^>]*?iPage\=)[\d]+([^>]*?\-1\&_\=)/is)
{
	$link1 = $1;
	$link2 = $2;
	$Tender_Url = "$link1"."$pageNo"."$link2"."$ms";
}


my $Tender_Url = 'https://in-tendhost.co.uk/sesharedservices/aspx/Services/Projects.svc/GetProjects?strMode=Current&searchvalue=&bUseSearch=false&OrderBy=Title&OrderDirection=ASC&iPage=7&iPageSize=10&bOnlyWithCorrespondenceAllowed=false&iCustomerFilter=0&iOptInStatus=-1&_=1567001529000';



my ($Tender_content2)=&Getcontent1($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
# open(ts,">Tender_content10_v1.html");
# print ts "$Tender_content2";
# close ts;

&InfoCollect($Tender_content2);
# $pageNo++;

# $count = $1 if ($Tender_content2=~m/\"PageCount\"\:\s*([\d]+)\,/is);
# print ("Count :: $count");
# foreach my $iPage ($pageNo..$count)
# {
	# print "\nPageNumber :: $iPage\n";
	# my $Tender_Url = "$link1"."$iPage"."$link2"."$ms";
	# print "\nTender_Url :: $Tender_Url\n";
	# my $Tender_Url = 'https://in-tendhost.co.uk/sesharedservices/aspx/Services/Projects.svc/GetProjects?strMode=Current&searchvalue=&bUseSearch=false&OrderBy=Title&OrderDirection=ASC&iPage='.$iPage.'&iPageSize=10&bOnlyWithCorrespondenceAllowed=false&iCustomerFilter=0&iOptInStatus=-1&_='.$ms;
	# my ($Tender_content3)=&Getcontent($Tender_Url);
	# $Tender_content3 = decode_utf8($Tender_content3);  
	# open(ts,">Tender_content10_v1.html");
	# print ts "$Tender_content3";
	# close ts;
	# print "\nPageNumber :: $iPage\n";
	# &InfoCollect($Tender_content3);
# }


sub InfoCollect()
{
	my $Tender_content4 = shift;
	# while ($Tender_content4=~m/(\{\"AdditionalDeliveryText[\w\W]*?\})/igs)
	# {
		# my $block = $1;
		
		
		# my $json = parse_json ($block);
		# my %hashjson = %$json;
		
		my $tenderblock;
		# foreach my $key (keys %hashjson)
		# {
			# my $value = $hashjson{$key};
			# $tenderblock.= "$key : $value\n";
			# print "$key : $value\n";
		# }
	
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		my $Deadline_Description = 'Date documents can be requested until';
		
		my $tender_title	= "SSA UK  Hubs as Hosts";
		my $ProjectID	= "980";
		print "$ProjectID	:: $tender_title\n";
		my $File_Type;
		my $deaddate	= "\/Date(1567378740000+0100)\/";
		$deaddate=~s/\s*\/Date\(//igs;
		$deaddate=~s/\)\///igs;

	my $tz = DateTime::TimeZone->new(name => 'Europe/London' );
 
	# my $dt = DateTime->now();
	# my $offset = $tz->offset_for_datetime($dt);
		$File_Type = 1 if ($deaddate=~m/\+0000/is);
		$File_Type = 0 if ($deaddate=~m/\+0100/is);
		# print $File_Type;
		my $con_link;
		if ($Tender_Url=~m/^\s*(http[^>]*?aspx\/)/is)
		{
			$con_link = $1;
		}
		my $hour;
		my $tender_link		= "$con_link".'ProjectManage/'.$ProjectID;
		# my $Deadline_Date = strftime("%Y-%m-%d %H:%M:%S", localtime($deaddate/1000));
		my $Deadline_Date = strftime("%Y-%m-%d", localtime($deaddate/1000));
		my $Deadline_time = strftime("%H:%M:%S", gmtime($deaddate/1000));
			my $tz = DateTime::TimeZone->new(name => 'Europe/London');
	
# my $s = $Deadline_Date;
# my $p = DateTime::Format::Strptime->new(
  # pattern => "%Y-%m-%d",
  # time_zone => "UTC"
# );

# my $dt = $p->parse_datetime($s);    
# $dt->set_time_zone("Europe/London");
	# my $dt = DateTime->(strftime("%H:%M:%S", gmtime($deaddate/1000)));
	# my $offset = $tz->offset_for_datetime($dt);
		# print DateTime->now();
		# print "The value of time",$dt->set_time_zone("Europe/London");
		# print "\n";
		# <>;
		# exit;
		# print "DeadLineDate :: $Deadline_Date\n";
		# my $hr = strftime("%H", gmtime($deaddate/1000));
		# my $min = strftime("%M", gmtime($deaddate/1000));
		# my $sec = strftime("%S", gmtime($deaddate/1000));

		# $hour= 1+int($hr);
		if ($hour==24)
		{
		$hour="00";
		}
		

		# my $hour= 1+int($hr);
        # my $hour= $hr;
		# my $Deadline_time = $hour.":".$min.":".$sec;
		print $Deadline_time;
		print $Deadline_Date;
		# <>;
		##Duplicate check
		# print $tender_link;
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB1::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my $Authority_Reference	= "BC\/00980";
		my $Description			= "a. The British Council SSA Creative Enterprise Support Programme will provide access to skills, networks, business development services and technical assistance to individuals and organisations to strengthen employability and the entrepreneurship in creative industries in target countries.\u000d<br \/>\u0009b. The programme will run over three financial years investing in programme spend from April 2019 – March 2022 and work in at least 20 countries in Sub Saharan Africa. \u000d<br \/>\u0009c. The programme is intended to enhance access to decent work and spur business growth and job creation potential in the creative economy in SSA and is expected to deliver at individual and organisational levels as follows:\u000d<br \/>\u000d<br \/>•\u0009Individual: increasing young people’s skills, knowledge, networks, and other capacity to access decent work in the creative industries, or to establish or grow enterprises in the creative industries that can create \/ enhance livelihoods for the entrepreneurs themselves and for others. \u000d<br \/>•\u0009Organisational: increasing the capacity of creative organisations and communities (primarily) to provide improved business development services and other support resulting in a more enabling operating environment that favours business growth and sustainability. \u000d<br \/>•\u0009Systemic: increasing the opportunities for creative entrepreneurs by articulating the impact of the creative and cultural industries to national and international stakeholders including funders, governments and formal education providers.\u000d<br \/>Objective and impact\u000d<br \/>\u0009d. The overall objective \/ outcome of the programme is to improve employability and increase the impact of creative enterprises that enhance decent livelihood opportunities for young people\u000d<br \/>            e. The purpose \/ impact of the programme is to contribute to strengthened creative industries in target countries in SSA. \u000d<br \/>Our activity in Southern Africa Arts Cluster aims to improve entrepreneurial and leadership capacity, as well as access points for young people entering creative career paths. Through the three programmes on offer (Creative Enterprise, Festivals and Connections), we are connecting creatives, sharing contemporary art, and growing audiences across the cluster and the United Kingdom. We engage all eight countries in every programme, but not necessarily every project, through open and closed calls, invited guests and artists and collaborative working with and through partners. Embedded throughout the programmes are our Equality, Diversity and Inclusive values, focus on woman, innovative and experimental projects, contemporary work and practices, partnership and collaborative way of working and mobility between the countries.\u000d<br \/>2.2\u0009The purpose and scope of this RFP and supporting documents is to explain in further detail the requirements of the British Council and the procurement process for submitting a tender proposal.";
		my $Contact				= "Linda Khumalo";
		my $Customer			= "Zimbabwe";
		my $Category			= "Non-OJEU";
		my $Process				= "Services";
		my $ContaUserID			= "Tatenda Musekiwa";
		my $CPV_Code			= "";
		# $Description=&BIP_Tender_DB::clean($Description);
		print $Description;
		$Awarding_Authority = $Contact.', '.$Customer.', '.$ContaUserID;
		$Awarding_Authority=~s/^\s*\,|\,\s*$//igs;
		my ($award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag);
		$InTendFlag='Y';
		# open (RE,">>result12.txt");
		# print RE "$ProjectID\t$tender_title\t$Authority_Reference\t$Description\t$Contact\t$Customer\t$Category\t$Process\t$ContaUserID\t$Deadline_Date\t$Deadline_time\t$deaddate\n";
		# close RE;
		&BIP_Tender_DB1::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag);
	}
# }	

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}


sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"in-tendhost.co.uk");
	if ($CookieeeeeePOST != '')
	{
		$req->header("Cookie"=> $CookieeeeeePOST);
	}	
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Content-Type"=> "text/html; charset=ISO_8859-1");
	# $req->header("Content-Type"=> "text/html; charset=ISO_8859-1");
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	
	my $Cookieeeeee=$res->header("Set-Cookie");
	# print "Cookieeeeee :: $Cookieeeeee\n";
	
	my $code=$res->code;
	# print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$Cookieeeeee);
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	# print "$user :: $pass\n";
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"in-tendhost.co.uk");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$req->header("Cookie"=> $CookieeeeeePOST);
	# $req->header("Referer"=> "https://in-tendhost.co.uk/blackcountryportal/aspx/Tenders/Current");
	$req->header("X-Requested-With"=> "XMLHttpRequest");
	$req->header("Content-Type"=> "application/json");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br\s*\/>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\,/:/igs;
	$Clean=~ s/\.\,/./igs;
	$Clean=~s/[^[:print:]]+//igs;
	decode_entities($Clean);
	$Clean=~s/[^[:print:]]+//igs;
	return $Clean;
}
