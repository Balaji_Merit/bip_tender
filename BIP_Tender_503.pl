#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;
#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts =>{SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
# $ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="http://www.cptu.gov.bd/advertisement-notices/notice-search.html";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print $month1;
my $year1		= strftime "%y", localtime;
print $year1;
my $csrf_id;
if($Tender_content1=~m/name="_csrf" value="([^>]*?)">/is)
{
	 $csrf_id=uri_escape($1);
}
my $Search_po_Content="csrf=".$csrf_id."&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=".$date1."%2F".$month1."%2F20".$year1."&issueDateTo=".$date1."%2F".$month1."%2F20".$year1."&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId&_csrf=".$csrf_id;
# _csrf=05f2c503-c6c6-489f-9570-3be7599dbf58&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=03%2F05%2F2018&issueDateTo=10%2F05%2F2018&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId&_csrf=05f2c503-c6c6-489f-9570-3be7599dbf58: undefined

my $Search_po_Content="_csrf=".$csrf_id."&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=&issueDateTo=&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId";
# my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__EVENTVALIDATION='.$Eventvalidation.'&searchText=Search...&cboProcurementType='.$z.'&cboMinistry=0&imgbtn.x=21&imgbtn.y=9';

my $Referer='https://cptu.gov.bd/advertisement-notices/notice-search.html';

my $post_url='https://cptu.gov.bd/advertisement-notices/notice-search.html';
my ($Result_content)=&Postcontent($post_url, $Referer, $Search_po_Content);
open(ts,">Tender_content503.html");
print ts "$Result_content";
close ts;
# exit;
# my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
### Ping the first level link page ####
# my ($ViewState,$Eventvalidation);
# if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
# {
	# $ViewState=uri_escape($1);
# }
# if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
# {
	# $Eventvalidation=uri_escape($1);
# }
# for(my $z=1;$z<=3;$z++)
# {
# my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__EVENTVALIDATION='.$Eventvalidation.'&searchText=Search&cboProcurementType=3&cboMinistry=0&imgbtn.x=12&imgbtn.y=20';

# my $Search_po_Content="csrf=4d2cad24-9753-4945-a59a-4ca4c77ba7cb&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=&issueDateTo=&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId&_csrf=4d2cad24-9753-4945-a59a-4ca4c77ba7cb: undefined";

# my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__EVENTVALIDATION='.$Eventvalidation.'&searchText=Search...&cboProcurementType='.$z.'&cboMinistry=0&imgbtn.x=21&imgbtn.y=9';

# my $Referer='http://www.cptu.gov.bd/advertisement-notices/notice-search.html';
# my $post_url='http://www.cptu.gov.bd/advertisement-notices/notice-search.html';
# my ($Result_content)=&Postcontent($post_url, $Referer, $Search_po_Content);
# my $Search_po_Content='method=search&searchMethod=true&isEngTender=Y&engOrgName=&orgId=&engTenderName=&tenderId=&tenderWay=&tenderStartDate=2017%2F05%2F01&tenderEndDate=2017%2F05%2F11&opdtStartDate=&opdtEndDate=&spdtStartDate=&spdtEndDate=&proctrgCate=&btnQuery=Search';
# my $Referer='http://web.pcc.gov.tw/tps/pss/tender.do?method=goSearch&searchMode=eng&searchType=advance';
# my $post_url='http://web.pcc.gov.tw/tps/pss/tender.do?searchMode=eng&searchType=advance';
# my ($Tender_content1)=&Postcontent($post_url, $Referer, $Search_po_Content);
# my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
# open(ts,">Tender_content521.html");
# print ts "$Tender_content1";
# close ts;
my $count=1;
my $Tender_content2;
#### To get the tender link ####
my $i=1;

my $tender_category_content1;
	top:
			# while($Result_content=~m/<a id="[^>]*?" class="LinkURL" href="(Show[^>]*?)">/igs)
			while($Result_content=~m/<tr[^>]*?id="index_\d{1,10}">\s*<td>\d{1,10}<\/td>\s*<td>\s*<a href="([^>]*?)">/igs)
			{
			my $tender_link="https://cptu.gov.bd".$1;
			print $tender_link;
			# <>;	
			my $tender_title;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
			# my $tender_day_content2=$1;
			
			# $tender_link_content2=&Getcontent($tender_day_content2);
			# while($tender_link_content2=~m/<td><a\s*href="([^>]*?)">/igs)
			# {
			# $tender_link=$1;
			my $tender_link_content1=&Getcontent($tender_link);
			
			# if($tender_link_content1=~m/<span id="lblTenderPkgName" columns="60">([^>]*?)<\/span>/is)
			if($tender_link_content1=~m/Tender Package Name[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		# if($tender_link_content1=~m/<span id="lblMinistry" style="font-weight:bold;">([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Ministry\/Division\s*:\s*<\/th>([\w\W]*?)Procuring Entity Code/is)
		{
		$Awarding_Authority=$1;
		}
		# if($tender_link_content1=~m/<span id="lblAgency" style="font-weight:bold;">([^>]*?)<\/span><\/td>/is)
		# {
		# $Awarding_Authority=$Awarding_Authority." ".$1;
		# }
		if($tender_link_content1=~m/PROCURING\s*ENTITY DETAILS<\/h3>([\w\W]*?)The procuring entity reserves the right to accept or reject all tenders/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".$1;
		}
		if($tender_link_content1=~m/Invitation Reference No[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$Authority_Reference=$1;
		# $Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}

		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		print length($Awarding_Authority);
		
					if (length($Awarding_Authority)>3000)
	{
		$Awarding_Authority = substr $Awarding_Authority, 0, 3000;
	}
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		# if($tender_link_content1=~m/Budget amount<\/th>[\w\W]*?>\s*([^>]*?)\s*<\/td>/is)
		if($tender_link_content1=~m/Tender Lot Information[\w\W]*?<tbody>\s*<tr>\s*<td>[^>]*?<\/td>\s*<td>[^>]*?<\/td>\s*\s*<td>[^>]*?<\/td>\s*<td>([^>]*?)<\/td>/is)
		{
			$Value=$1;
			# $award_procedure = &BIP_Tender_DB::clean($award_procedure);
			# $award_procedure=&clean($award_procedure);
		}
	
		if($tender_link_content1=~m/Brief Description of Goods or Works[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=~s/\\n/ /igs;
		$Description=~s/\s+\s+/ /igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/<span id="lblDistrict"[^>]*?>([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Tender Lot Information[\w\W]*?<tbody>\s*<tr>\s*<td>[^>]*?<\/td>\s*<td>[^>]*?<\/td>\s*<td>([^>]*?)<\/td>/is)
		{
		$Location=$1;
		$Location=~s/\\n/ /igs;
		$Location=~s/\s+\s+/ /igs;
		$Location=&clean($Location);
		$Location=&Trim($Location);
		$Location=&BIP_Tender_DB::clean($Location);
		}
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
		# if($tender_link_content1=~m/<span id="lblSellDate">([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Tender Last Selling Date[\w\W]*?<td>\s*([^>]*?)<\/td>/is)
		{
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{1,2})\-(\w+)\-(\d{4})/is)
		# {
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1);
		
		# $Deadline_time=$4;
		}
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# elsif($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
	
		# if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		# {
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))/is)
		# {
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		# $Deadline_time=$4;
		# }
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
			# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
		
		$Deadline_Description="Tender Last Selling Date:";
		
		print $tender_link;
		
		
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/Ministry\/Division\s*:<\/th>([\w\W]*?)The procuring entity reserves the right to accept or reject all tenders/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\\n/ /igs;
	$tenderblock=~s/\s+\s+/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='BD';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		
 &BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# sleep(int(rand(10))+10);
# }
}

my $page;
for($page=1;$page<50;$page++)
{
my $Search_po_Content="csrf=".$csrf_id."&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=&issueDateTo=&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId&_csrf=".$csrf_id;
my $Search_po_Content="_csrf=".$csrf_id."&query=&procurementTypeId.id=0&ministryId.id=0&agencyId.id=0&procurementMethodId.id=0&issueDateFrom=&issueDateTo=&closingDateFrom=&closingDateTo=&viewResultBy=procurementTypeId";
# my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__EVENTVALIDATION='.$Eventvalidation.'&searchText=Search...&cboProcurementType='.$z.'&cboMinistry=0&imgbtn.x=21&imgbtn.y=9';
		
my $Referer='https://cptu.gov.bd/advertisement-notices/notice-search.html';
my $post_url='https://cptu.gov.bd/advertisement-notices/notice-search.html?page='.$page;
my ($Result_content)=&Postcontent($post_url, $Referer, $Search_po_Content);

my $count=1;
my $Tender_content2;
#### To get the tender link ####
my $i=1;

my $tender_category_content1;
	top:
			# while($Result_content=~m/<a id="[^>]*?" class="LinkURL" href="(Show[^>]*?)">/igs)
			while($Result_content=~m/<tr id="index_\d{1,10}">\s*<td>\d{1,10}<\/td>\s*<td>\s*<a href="([^>]*?)">/igs)
			{
			my $tender_link="http://www.cptu.gov.bd".$1;
				print $tender_link;
			# <>;
			my $tender_title;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
			# my $tender_day_content2=$1;
			
			# $tender_link_content2=&Getcontent($tender_day_content2);
			# while($tender_link_content2=~m/<td><a\s*href="([^>]*?)">/igs)
			# {
			# $tender_link=$1;
			my $tender_link_content1=&Getcontent($tender_link);
			
			# if($tender_link_content1=~m/<span id="lblTenderPkgName" columns="60">([^>]*?)<\/span>/is)
			if($tender_link_content1=~m/Tender Package Name[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		# if($tender_link_content1=~m/<span id="lblMinistry" style="font-weight:bold;">([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Ministry\/Division\s*:\s*<\/th>([\w\W]*?)Procuring Entity Code/is)
		{
		$Awarding_Authority=$1;
		}
		# if($tender_link_content1=~m/<span id="lblAgency" style="font-weight:bold;">([^>]*?)<\/span><\/td>/is)
		# {
		# $Awarding_Authority=$Awarding_Authority." ".$1;
		# }
		if($tender_link_content1=~m/PROCURING\s*ENTITY DETAILS<\/h3>([\w\W]*?)The procuring entity reserves the right to accept or reject all tenders/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".$1;
		}
		if($tender_link_content1=~m/Invitation Reference No[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$Authority_Reference=$1;
		# $Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}

		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
							if (length($Awarding_Authority)>3000)
	{
		$Awarding_Authority = substr $Awarding_Authority, 0, 3000;
	}
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		# if($tender_link_content1=~m/Budget amount<\/th>[\w\W]*?>\s*([^>]*?)\s*<\/td>/is)
		if($tender_link_content1=~m/Tender Lot Information[\w\W]*?<tbody>\s*<tr>\s*<td>[^>]*?<\/td>\s*<td>[^>]*?<\/td>\s*\s*<td>[^>]*?<\/td>\s*<td>([^>]*?)<\/td>/is)
		{
			$Value=$1;
			# $award_procedure = &BIP_Tender_DB::clean($award_procedure);
			# $award_procedure=&clean($award_procedure);
		}
	
		if($tender_link_content1=~m/Brief Description of Goods or Works[\w\W]*?<td>\s*([^>]*?)\s*<\/td>/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=~s/\\n/ /igs;
		$Description=~s/\s+\s+/ /igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/<span id="lblDistrict"[^>]*?>([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Tender Lot Information[\w\W]*?<tbody>\s*<tr>\s*<td>[^>]*?<\/td>\s*<td>[^>]*?<\/td>\s*<td>([^>]*?)<\/td>/is)
		{
		$Location=$1;
		$Location=~s/\\n/ /igs;
		$Location=~s/\s+\s+/ /igs;
		$Location=&clean($Location);
		$Location=&Trim($Location);
		$Location=&BIP_Tender_DB::clean($Location);
		}
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
		# if($tender_link_content1=~m/<span id="lblSellDate">([^>]*?)<\/span><\/td>/is)
		if($tender_link_content1=~m/Tender Last Selling Date[\w\W]*?<td>\s*([^>]*?)<\/td>/is)
		{
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{1,2})\-(\w+)\-(\d{4})/is)
		# {
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1);
		
		# $Deadline_time=$4;
		}
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# elsif($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
	
		# if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		# {
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))/is)
		# {
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		# $Deadline_time=$4;
		# }
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
			# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
		
		$Deadline_Description="Tender Last Selling Date:";
		
		print $tender_link;
		
		
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/Ministry\/Division\s*:<\/th>([\w\W]*?)The procuring entity reserves the right to accept or reject all tenders/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\\n/ /igs;
	$tenderblock=~s/\s+\s+/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='BD';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		
 &BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# sleep(int(rand(10))+10);
# }
}

}
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "www.merx.com");
	# $req->header("Referer"=> "http://web.pcc.gov.tw/tps/pss/tender.do?searchMode=eng&searchType=advance");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "cptu.gov.bd");
	# $req->header("Referer"=> "https://www.etenders.gov.eg/Tender/DoSearch?status=3");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.merx4.merx.com");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $referer=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=>"cptu.gov.bd");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");
	$req->header("Referer"=>"https://cptu.gov.bd/advertisement-notices/notice-search.html");
	$req->content("$Post_Content");
	# $req->header("Cookie"=> "_ga=GA1.3.1410577211.1512398481; JSESSIONID=E14A93DA035003B25BFF2A4FBA1563EA; BIGipServerCPTU-WEB-SERVER-HTTP-Pools=3523880970.20480.0000; _gid=GA1.3.1571852232.1521197792; _gat=1");
	$req->header("Cookie"=> "JSESSIONID=8F54DDC71005C2AEF465CE263F637EA9; CPTU-COOKIE=!bSFdjbouAhYxKiRioOqhQHl7tl682dRZJtJh/01G+VRdZkbi0ymDhCkkT9IJpmu8c8asyhZs4vGpzM8=; _ga=GA1.3.1264208213.1571992522; _gid=GA1.3.507585004.1572250144; _gat=1");

	
# JSESSIONID=A117EC07C20239047F278343893D2437; CPTU-COOKIE=!bSFdjbouAhYxKiRioOqhQHl7tl682dRZJtJh/01G+VRdZkbi0ymDhCkkT9IJpmu8c8asyhZs4vGpzM8=; _ga=GA1.3.1264208213.1571992522; _gid=GA1.3.1519001320.1571992522; _gat=1

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n//igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}