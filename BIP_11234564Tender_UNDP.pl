#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX qw( strftime );
# use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );

use BIP_Tender_DB;
use JSON::Parse 'parse_json';

#### Getting the Input Company ID from the Script Name ####
# my $Input_Tender_ID = $0;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);
# for (my $i=601;$i<670;$i++)
# {
# my $Input_Tender_ID	= $i;
my $Input_Tender_ID	= $ARGV[0];
# my $Input_Tender_ID	= '101';

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(30);
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);


### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

### Retrieve Company URL from Company table ####
for (my $i=601;$i<670;$i++)
{
 # my $i='603';
$Input_Tender_ID=$i;
my ($ID,$Tender_Name,$Tender_weblink,$Origin,$Sector) = &BIP_Tender_DB::RetrieveUrl1($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
# $Tender_ID='602';
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
print $Tender_Url;

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

### Dupe Check for the press release ####
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;


my $StartDate=DateTime->now();
print "StART :: $StartDate\n";
my $ms = (1000 * $StartDate->epoch);
print "ms :: $ms\n";

#### Ping the first level link page ####



if($Tender_ID==603)
{
my ($Tender_content2)=&Getcontent1($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content10_v1.html");
print ts "$Tender_content2";
close ts;
&InfoCollect1($Tender_content2,$Tender_ID,$Origin,$Sector,$TenderName);
}
else
{
my ($Tender_content2)=&Getcontent($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content10_v1.html");
print ts "$Tender_content2";
close ts;
&InfoCollect($Tender_content2,$Tender_ID,$Origin,$Sector,$TenderName);
}
 # }
}

sub InfoCollect()
{
	my $Tender_content4 = shift;
	my $Tender_ID		= shift;
	my $Origin1			= shift;
	my $Sector1			= shift;
	my $sourceURL1		= shift;
	my ($sno,$cy,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation);
	my ($tender_link,$tenderblock,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_Date,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$BIP_Non_UKFlag);
	my @links1;
	my @cy;
	my $cy;
	### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	if($Tender_content4=~m/"duty_station_cty":\s*\[([\w\W]*?)\]/is)
	{
	
	$cy=$1;
	@cy=split(",",$cy);
	
	}
	my @links1;
	my $i=0;
	if($Tender_content4=~m/"link"\s*:\s*\[([\w\W]*?)\]/is)
	{
	my $links=$1;
	@links1=split(",",$links);
	}
	foreach $tender_link(@links1)
	{
	$tender_link=~s/\"//igs;
	
	my ($Home_content1,$Referer1)=&Getcontent1($tender_link);
		if($Home_content1=~m/<th colspan="3">([\w\W]*?)<\/strong><\/th>/is)
		{
			$tender_title=$1;
			$tender_title=&clean($tender_title);
		}
		if($Home_content1=~m/Office\s*:<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Awarding_Authority=&clean($1);
		}
		if($Home_content1=~m/Deadline :<\/td><td  colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Deadline_Date=$1;
			$Deadline_Description="Deadline :";
		}
		if($Home_content1=~m/Reference Number :<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Authority_Reference=$1;
		}
		if($Home_content1=~m/Overview\s*:\s*<p>([\w\W]*?)<\/td><\/tr>\s*<\/table>/is)
		{
			$Description="Overview : ".$1;
			$Description=&clean($Description);
		}
		# $d_Rapost="For further information and instructions in the above contract notice please visit website: ".$tender_link;
		# my $y=$i+1;
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		$BIP_Non_UKFlag="Y";
		if($Home_content1=~m/<h3>View Notice<\/h3>([\w\W]*?)\s*END CONTENT/is)
		{
		$tenderblock=$1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		@cy[$i]=~s/"//igs;
		if($Tender_ID==623)
	{
		$cy="CONGO, DEM. REPUBLIC";
			&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin1,$Sector1,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		# open(out,">>$filename");
		# print out "$sno1\t$y\t$sourceURL\t$origin\t$noticeSector\t$cy[i]\t$sourceURL\t$high_value\t$title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\n";
	}	
	elsif($Tender_ID==636)
	{
	$cy="COTE d'IVOIRE";
	&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin1,$Sector1,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	}
	else{
			&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin1,$Sector1,@cy[$i],$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		}
		$i=$i+1;
	}
	}

sub InfoCollect1()
{
	my $Tender_content4=shift;
	my $Tender_ID		= shift;
		my $Origin1			= shift;
	my $Sector1			= shift;
	my $sourceURL1		= shift;
	my ($tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_Date,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$BIP_Non_UKFlag);
	my ($sno,$origin,$noticeSector,$cy,$sourceURL,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation);
	### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	while($Tender_content4=~m/href="([^>]*?notice_id[^>]*?)">[\w\W]*?<\/a><\/td>\s*<td\s*>[^>]*?<\/td>\s*<td\s*>([^>]*?)\s*<\/td>/igs)
	{
	my $tender_link="http://procurement-notices.undp.org/".$1;
	my $cy=$2;
	
	my ($Home_content1,$Referer1)=&Getcontent1($tender_link);
		if($Home_content1=~m/<th colspan="3">([\w\W]*?)<\/strong><\/th>/is)
		{
			$tender_title=$1;
			$tender_title=&clean($tender_title);
		}
		if($Home_content1=~m/Office\s*:<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Awarding_Authority=&clean($1);
		}
		if($Home_content1=~m/Deadline :<\/td><td  colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Deadline_Date=$1;
			$Deadline_Description="Deadline :";
		}
		if($Home_content1=~m/Reference Number :<\/td><td colspan="2">([^>]*?)\s*<\/td>/is)
		{
			$Authority_Reference=$1;
		}
		if($Home_content1=~m/Overview\s*:\s*([\w\W]*?)<\/td><\/tr>\s*<\/table>/is)
		{
			$Description="Overview : ".$1;
			$Description=&BIP_Tender_DB::clean($Description);
		}
		if($Home_content1=~m/<h3>View Notice<\/h3>([\w\W]*?)\s*END CONTENT/is)
		{
		$tenderblock=$1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		my $BIP_Non_UKFlag="Y";
		my $InTendFlag='N';
		# open(out,">>$filename");
		# print out "$sno1\t$y\t$sourceURL\t$origin[2]\t$noticeSector[2]\t$cy\t$sourceURL\t$high_value\t$title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\n";
		# close out;
		&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		# $i=$i+1;
		# $sno1=$sno1+1;
		# ($origin,$noticeSector,$cy,$sourceURL,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation)="";
	}
}

# sub Urlcheck()
# {
	# my $News_link = shift;
	# if($News_link!~m/^http/is)
	# {	
		# my $u1=URI::URL->new($News_link,$Tender_Url);
		# my $u2=$u1->abs;
		# $News_link=$u2;
		# $News_link=~s/\&amp\;/&/igs;
	# }
	# return ($News_link);
# }
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/plain; charset=UTF-8");
	$req->header("Host"=> "public-components.undp.org");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "procurement-notices.undp.org");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/ /igs;
	$Clean=~s/,+/,/igs;
	# $Clean=~s/'/''/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\,/:/igs;
	$Clean=~ s/\.\,/./igs;
	$Clean=~s/[^[:print:]]+//igs;
	decode_entities($Clean);
	$Clean=~s/[^[:print:]]+//igs;
	return $Clean;
}

