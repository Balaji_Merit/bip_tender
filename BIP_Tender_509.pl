#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);

my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

my ($ID,$Tender_Name,$Tender_weblink,$Origin,$Sector) =  &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
# my $TenderName 			= @$Tender_Name[0];
# my $Tender_Url 			= @$Tender_weblink[0];
# my $Tender_ID=523;
# my $TenderName="USA: Georgia Procurement Registry";
# my $Tender_Url="https://www.etenders.gov.eg/Tender/DoSearch?status=3";
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# my $Tender_Url="https://www.jetro.go.jp/cgi-bin/gov/gove010e.cgi";
my $Tender_Url="https://www.jetro.go.jp/view_interface.php?blockId=28528891";

# https://www.tenderlink.com/webapps/jadehttp.dll?WebTender&tenderer=2744.4&alltenders
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my $fulldate = strftime "%d %m %Y %A", localtime;
my $day = (split(" ",$fulldate))[-1];
my $today3 = strftime "%Y-%m-%d", (localtime($day));
my $yesterday = $today3 - 60 * 24 * 60;

my $date1		= strftime "%d", (localtime($today3));
print $date1;
my $month1		= strftime "%m", (localtime($today3));
print $month1;
my $year1		= strftime "%y", (localtime($today3));
print $year1;
# my $StartDate=DateTime->now();
# print "StART :: $StartDate\n";
# my $ms = (1000 * $StartDate->epoch);
# print "ms :: $ms\n";
# $date1='06';
	my $Referer='https://www.jetro.go.jp/en/database/procurement/';
# for(my $i=0;$i<50000;$i=$i+30)	
# {
my $post_url='https://www.jetro.go.jp/view_interface.php?blockId=27911119&current=0&ut=1564495111';
# open(ts,">Buyer_content.html");
# print ts "$Buyer_content";
# close ts;

# my $post_url='http://www.gebiz.gov.sg/ptn/opportunityportal/opportunityDetails.xhtml';
my $i=0;
my $total_records;
my $Home_content=&Getcontent($post_url);
	open(out,">result509.html");
	print out "$Home_content";
	close out;
	# exit;

	if($Home_content=~m/"total":([^>]*?)\s*\,/is)
	{
	$total_records=$1;
	}
	print $total_records;
	my $total_pages=int($total_records)/30;
	my $count=0;
	my $pageno=0;
	
	while($Home_content=~m/\{("xid"[\w\W]*?)\}/igs)
	{
	my $Home_content1=$1;
	while($Home_content1=~m/"xid":([^>]*?)\,"aid":"([^>]*?)"\,/igs)
			{
			my $tender_link='https://www.jetro.go.jp/en/database/procurement/national/articles/'.$1.'/'.$2.'.html';
			my $tender_title;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);

			my $tender_link_content1=&Getcontent($tender_link);

		if($tender_link_content1=~m/<h1>\s*<span class="sub_text">[^>]*?\s*<\/span>\s*([^>]*?)\s*<\/h1>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}	
		# if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(4\)/is)
		
		# {
		# $tender_title=$1;
		# $tender_title=&clean($tender_title);
		# }
	
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(4\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(6\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(7\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/<h1>\s*<span class="sub_text">[^>]*?\s*<\/span>\s*([^>]*?)\s*<\/h1>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_link_content1=~m/\(8\)Contact point[\w\W]*?:([\w\W]*?)</is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/Contact point[\w\W]*?Notice[\w\W]*?:([\w\W]*?)<\/p>/is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/Procurement entity<\/th>\s*<td><span>([\w\W]*?)<\/span><\/td>\s*<\/tr>/is)
		{
		$Awarding_Authority=$1;
		}
		
		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		# $tender_title=$Description;
		if($tender_link_content1=~m/\(3\)\s*(?:\<BR\>|\s*)\s*NATURE[\w\W]*?:([\w\W]*?)\(4\)/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=~s/\\n/ /igs;
		$Description=~s/\s+\s+/ /igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		$Description=$tender_title;
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/<span id="lblDistrict"[^>]*?>([^>]*?)<\/span><\/td>/is)
		# {
		# $Location=$1;
		# $Location=~s/\\n/ /igs;
		# $Location=~s/\s+\s+/ /igs;
		# $Location=&clean($Location);
		# $Location=&Trim($Location);
		# $Location=&BIP_Tender_DB::clean($Location);
		# }
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
	
		if($tender_link_content1=~m/Time[^>]*?limit[^>]*?:\s*([^>]*?)\s*</is)
		{
		my $temp_dt=$1;
		my $temp_dt=&clean($1);
		# $Deadline_time=$1;
		# $$temp_dt=~s/\s*//igs;
		if($temp_dt=~m/(\d{1,2})\s*(\w+)\s*(?:\,|\s*)\s*(\d{4})\s*/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1."-".$2."-".$3." 00:00:00");
		
		# $Deadline_time=$4;
		}
		
		if($temp_dt=~m/(\d{1,2})\s*(?:\.|\:)\s*(\d{2})/is)
		{
		# my $hh=$1;
		$Deadline_time=$1.":".$2;
		}
		}
		$Deadline_time=~s/am//igs;
		$Deadline_time=~s/AM//igs;
		
		$Deadline_Description="TIME LIMIT FOR TENDER :";
		
		# print $tender_link;
		
# $tender_link="https://www.jetro.go.jp/cgi-bin/gov/gove030e.cgi";	
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/<table class="elem_table_basic">([\w\W]*?)<\/table>/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\\n/ /igs;
	$tenderblock=~s/\s+\s+/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='JP';
# $tender_link="https://www.jetro.go.jp/cgi-bin/gov/gove030e.cgi";
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		
		
 &BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# sleep(int(rand(10)));
}
}
# }

my $post_url='https://www.jetro.go.jp/view_interface.php?blockId=27911111&current=0&ut=1564500586';
# open(ts,">Buyer_content.html");
# print ts "$Buyer_content";
# close ts;

# my $post_url='http://www.gebiz.gov.sg/ptn/opportunityportal/opportunityDetails.xhtml';
my $i=0;
my $total_records;
my $Home_content=&Getcontent($post_url);
	open(out,">result509.html");
	print out "$Home_content";
	close out;
	# exit;

	if($Home_content=~m/"total":([^>]*?)\s*\,/is)
	{
	$total_records=$1;
	}
	print $total_records;
	my $total_pages=int($total_records)/30;
	my $count=0;
	my $pageno=0;
	
	while($Home_content=~m/\{("xid"[\w\W]*?)\}/igs)
	{
	my $Home_content1=$1;
	while($Home_content1=~m/"xid":([^>]*?)\,"aid":"([^>]*?)"\,/igs)
			{
			my $tender_link='https://www.jetro.go.jp/en/database/procurement/local/articles/'.$2.'.html';
			my $tender_title;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);

			my $tender_link_content1=&Getcontent1($tender_link);

		if($tender_link_content1=~m/<h1>\s*<span class="sub_text">[^>]*?\s*<\/span>\s*([^>]*?)\s*<\/h1>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		# print $tender_title;
		# <>;
		}	
		# if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(4\)/is)
		
		# {
		# $tender_title=$1;
		# $tender_title=&clean($tender_title);
		# }
	
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(4\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(6\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/\(3\)[\w\W]*?(?:\:|\;)([\w\W]*?)\(7\)/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_title eq "")
		{
		if($tender_link_content1=~m/<h1>\s*<span class="sub_text">[^>]*?\s*<\/span>\s*([^>]*?)\s*<\/h1>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		}
		if($tender_link_content1=~m/\(8\)Contact point[\w\W]*?:([\w\W]*?)</is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/Contact point[\w\W]*?Notice[\w\W]*?:([\w\W]*?)<\/p>/is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/Procurement entity<\/th>\s*<td><span>([\w\W]*?)<\/span><\/td>\s*<\/tr>/is)
		{
		$Awarding_Authority=$1;
		}
		
		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		# $tender_title=$Description;
		if($tender_link_content1=~m/\(3\)\s*(?:\<BR\>|\s*)\s*NATURE[\w\W]*?:([\w\W]*?)\(4\)/is)
		{
		$Description=$1;
		# $Description=~s/\-\-\>//igs;
		$Description=~s/\\n/ /igs;
		$Description=~s/\s+\s+/ /igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		$Description=$tender_title;
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/<span id="lblDistrict"[^>]*?>([^>]*?)<\/span><\/td>/is)
		# {
		# $Location=$1;
		# $Location=~s/\\n/ /igs;
		# $Location=~s/\s+\s+/ /igs;
		# $Location=&clean($Location);
		# $Location=&Trim($Location);
		# $Location=&BIP_Tender_DB::clean($Location);
		# }
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
	
		if($tender_link_content1=~m/Time[^>]*?limit[^>]*?:\s*([^>]*?)\s*</is)
		{
		my $temp_dt=$1;
		my $temp_dt=&clean($1);
		# $Deadline_time=$1;
		# $$temp_dt=~s/\s*//igs;
		if($temp_dt=~m/(\d{1,2})\s*(\w+)\s*(?:\,|\s*)\s*(\d{4})\s*/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1."-".$2."-".$3." 00:00:00");
		
		# $Deadline_time=$4;
		}
		if($temp_dt=~m/(\d{1,2})\s*(?:\.|\:)\s*(\d{2})\s*(?:pm|PM|P\.M\.)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		elsif($temp_dt=~m/(\d{1,2})\s*(?:\.|\:)\s*(\d{2})/is)
		{
		# my $hh=$1;
		$Deadline_time=$1.":".$2;
		}

		if($temp_dt=~m/(\d{1,2})\s*(?:\.|\:)\s*(\d{2})\s*(?:AM|A\.M\.)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		elsif($temp_dt=~m/(\d{1,2})\s*:\s*(\d{2})/is)
		{
		# my $hh=$1;
		$Deadline_time=$1.":".$2;
		}
		$Deadline_time=~s/am//igs;
		$Deadline_time=~s/AM//igs;
		
		$Deadline_Description="TIME LIMIT FOR TENDER :";
		
		# print $tender_link;
		
# $tender_link="https://www.jetro.go.jp/cgi-bin/gov/gove030e.cgi";	
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/<table class="elem_table_basic">([\w\W]*?)<\/table>/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\\n/ /igs;
	$tenderblock=~s/\s+\s+/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='JP';
# $tender_link="https://www.jetro.go.jp/cgi-bin/gov/gove030e.cgi";
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		
		
 &BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# sleep(int(rand(10)));
}
}
}	
# }
sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/&nbsp;//igs;
	$values=~s/&amp;/&/igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\–\s*/ - /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/[^[:print:]]+//igs;
	$values=~s/&nbsp;//igs;
	# decode_entities($values);
	return($values);
}

sub Urlcheck()
{
	my $News_link = shift;
	my $Tender_Url=shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.jetro.go.jp");
	$req->header("Cookie"=> "PHPSESSID=b3e1b3e88677d1e7ff450b5ece9716e3; _ga=GA1.3.2041839269.1561127579; __ulfpc=201906212003016660; _gid=GA1.3.76000979.1564494753");
	$req->header("Referer"=> "https://www.jetro.go.jp/en/database/procurement/national/list.html?_page=1");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");


	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.jetro.go.jp");
	$req->header("Cookie"=> "PHPSESSID=b3e1b3e88677d1e7ff450b5ece9716e3; _ga=GA1.3.2041839269.1561127579; __ulfpc=201906212003016660; _gid=GA1.3.76000979.1564494753");
	$req->header("Referer"=> "https://www.jetro.go.jp/en/database/procurement/local/list.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Post_Method1()
{
	my $post_url=shift;
	my $post_cont=shift;
	# my $referer=shift;
	# my $Keyword=shift;
	# $Keyword=~s/\s+/\%20/igs;
	
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "bs.nakanohito.jp");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "https://www.jetro.go.jp/en/database/procurement/national/list.html?_page=1");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("X-Requested-With"=> "XMLHttpRequest");
	

	$req->header("Accept-Language"=> "en-us,en;q=0.5");
	# $req->header("Location"=> "https://www.gebiz.gov.sg/ptn/opportunityportal/opportunityDetails.xhtml");

	$req->header("Cookie"=> "AP=201906212334067505");
	

	# $req->header("Set-Cookie"=>"NSC_xfc_qfstjtufodf=ffffffff09081f7645525d5f4f58455e445a4a423660;expires=Tue, 02-May-2017 14:54:00 GMT;path=/;httponly");

	$req->content($post_cont);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# open(poc,">POST_PcccC.html");
	# print poc $content;
	# close poc;
	return ($content,$redir_url);
}

sub Post_Method2()
{
	my $post_url=shift;
	my $post_cont=shift;
	# my $referer=shift;
	# my $Keyword=shift;
	# $Keyword=~s/\s+/\%20/igs;
	
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.jetro.go.jp");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://www.jetro.go.jp/cgi-bin/gov/gove020e.cgi");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Encoding"=> "gzip, deflate, br");
	

	$req->header("Accept-Language"=> "en-us,en;q=0.5");
	# $req->header("Location"=> "https://www.gebiz.gov.sg/ptn/opportunityportal/opportunityDetails.xhtml");

	$req->header("Cookie"=> "_ga=GA1.3.1246231193.1490704812; __ulfpc=201703281810129855; _gid=GA1.3.1438270290.1494578949; PHPSESSID=4vnulhvdlcqrd9reofuq5ak9d4");
	

	# $req->header("Set-Cookie"=>"NSC_xfc_qfstjtufodf=ffffffff09081f7645525d5f4f58455e445a4a423660;expires=Tue, 02-May-2017 14:54:00 GMT;path=/;httponly");

	$req->content($post_cont);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# open(poc,">POST_PcccC.html");
	# print poc $content;
	# close poc;
	return ($content,$redir_url);
}


sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n//igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}