from selenium import webdriver
# import chromedriver_binary
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import re
import os,sys
import pymysql
import time
import json
import pymysql
# import xlwt
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
import pytz
# import ConfigParser
import dateutil.parser as dparser
from time import sleep
# import codec
# warnings.filterwarnings('ignore')
# reload(sys)
# sys.setdefaultencoding('utf-8')


Database_Connector = imp.load_source('Database_Connector','C:\\BIP_Tender\\UK_Scripts\\Database_Connector1.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

def reg_clean(desc):
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc = desc.replace('\s+\s+', ' ')
	desc = desc.replace('\t', ' ')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\{[\w\W]*?\}', ' ', str(desc))
	desc = re.sub(r"&#39;","''", str(desc))
	# desc = re.sub(r"'","''", str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "''", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	desc = desc.replace('&nbsp;','')
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	for i in range(1,10):
		desc = desc.replace('\s+\s+', ' ')
	desc = desc.replace('\t', ' ')
	return desc

def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	# dt = parse(Input)
	dt=dparser.parse(Input,fuzzy=True)
	FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	return (FormattedDate)
tender_id=1110
origin="Web"
sector=	"UK Other"
country="GB"
websource="MultiQuote"
	
# driver = webdriver.Chrome()
# capabilities = { 'chromeOptions':  { 'useAutomationExtension': False}}
# driver = webdriver.Chrome(desired_capabilities = capabilities)
# options = Options()
# options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
driver = webdriver.Chrome(executable_path="chromedriver.exe", )
driver.get("https://suppliers.multiquote.com/Unauthenticated/BrowseOpportunities.aspx")

i=0
tdElements = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')
while True:
	try:
		tdElements[i].click()
		i=i+1
	except Exception as e:
		i=0
		next_page = driver.find_elements_by_css_selector('.rgPageNext')
		driver.page_source
		tdElements = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')
		tdElements[i].click()
		i=i+1
	# print (tdElements[i])
	time.sleep(10)
	# i=i+1
	page_content = driver.page_source
	reference_number=re.findall('<input name=[^>]*?value="([^>]*?)"\s*[^>]*?id="ctl00_M_RFQView_RFQCode"[^>]*?>',page_content)
	# print (reference_number[0])
	if reference_number:
		authority_reference1 = reference_number[0]
		url="https://suppliers.multiquote.com/Unauthenticated/ViewRfqOpportunity.aspx?c="+str(authority_reference1)
			

		Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
		Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate,url)

		if Duplicate_Check == "N":
			# page_content = driver.page_source
			view_notice=re.findall('<span class="Text">View Notice<\/span>',page_content)
			if view_notice:
				driver.find_element_by_css_selector('#ctl00_BackButton').click()
			else:	
				title = re.findall('<input name="[^>]*?Title"[^>]*?Title" title="([^>]*?)"\s*style=[^>]*?><\/div>',page_content)
				reference_number=re.findall('<input name=[^>]*?value="([^>]*?)"\s*[^>]*?id="ctl00_M_RFQView_RFQCode"[^>]*?>',page_content)
				deadline_date=re.findall('<input name=[^>]*?value="[^>]*?day,\s*([^>]*?)"\s*[^>]*?ExpiryDate"[^>]*?>',page_content)
				deadline_time=re.findall('<input name="[^>]*?_ExpiryTimeLabel"[^>]*?value="([^>]*?)"',page_content)
				# description=re.findall('<label class="Caption">\s*(Short Description[\w\W]*?<label class="Caption">\s*Description\s*[\w\W]*?)<\/textarea>',page_content)
				description=re.findall('<th scope="col" class="rgHeader" style="white-space:nowrap;">(Item<\/th>[\w\W]*?Description[\w\W]*?)<\/textarea>',page_content)
				organization=re.findall('<label class="Caption">\s*Organisation[\w\W]*?value="([^>]*?)"',page_content)
				buyer_name=re.findall('<span class="Required">Buyer<\/span>[\w\W]*?value="([^>]*?)"',page_content)
				customer_name=re.findall('<span class="Required">Customer<\/span>\s*[\w\W]*?value="([^>]*?)"',page_content)
				contact_type1=re.findall('Agreement Type\s*[\w\W]*?value="([^>]*?)"',page_content)
				if title:
					title = title[0]
					title1=title
					print ("Name:",title)
				
				if organization:
					organization = organization[0]
					# print ("Name:",organization)
				if buyer_name:
					buyer_name = buyer_name[0]
				else:
					buyer_name=""
					# print ("Name:",buyer_name)	
				if customer_name:
					customer_name = customer_name[0]	
				awarding_authority1=str(buyer_name)+" "+str(organization)	
				if reference_number:
					authority_reference1 = reference_number[0]
					print ("Reference:",authority_reference1)
							
				if deadline_date:
					FormattedDate = DateFormat(deadline_date[0])
					# print ("Deadline Date:",FormattedDate)
				if contact_type1:
					contact_type1 = contact_type1[0]
					# print ("Deadline Date:",cpv_code1)	
				if deadline_time:
					deadline_time1 = deadline_time[0]
					# print ("Deadline Time:",deadline_time1)
				awarding_authority1=reg_clean(awarding_authority1)
				if description:
					description1=reg_clean(description[0])
				tender_block=re.findall('<tbody>([\w\W]*?)<\/textarea>',page_content)
				if tender_block:
					tender_block=reg_clean(tender_block[0])
						# print (description1)
				description1=reg_clean(description1)		
				url="https://suppliers.multiquote.com/Unauthenticated/ViewRfqOpportunity.aspx?c="+str(authority_reference1)
				deadline_description="Expiry Date"
				value1=''
				location1=''
				cpv_code1=''
				contact_type1=''
				award_procedure1=''
				other_information1 = "For further information regarding the above contract notice please visit: "+ str(url)
				curr_date = datetime.now(pytz.timezone('Europe/London'))
				created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
				insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url), str(tender_block), str(origin), str(sector), str(country), str(websource), str(title1), str(title1),str(awarding_authority1), str(award_procedure1), str(contact_type1), str(cpv_code1), str(description1), str(location1), str(authority_reference1), str(value1), str(deadline_description), str(FormattedDate), str(deadline_time1),str(other_information1), str(url), str(created_date))
				print (insertQuery)
				Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
					# time.sleep(3)
				# with open('Mydata1.html','a+')as f:
					# f.write(page_content)
				# tdElements2 = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')	
	driver.find_element_by_css_selector('#ctl00_BackButton').click()
	time.sleep(10)
	tdElements = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')
	

next_page = driver.find_elements_by_css_selector('.rgPageNext')
driver.page_source
i=0
tdElements = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')
while True:
	tdElements[i].click()
	print (tdElements[i])
	time.sleep(10)
	i=i+1
	page_content = driver.page_source
	reference_number=re.findall('<input name=[^>]*?value="([^>]*?)"\s*[^>]*?id="ctl00_M_RFQView_RFQCode"[^>]*?>',page_content)
	# print (reference_number[0])
	if reference_number:
		authority_reference1 = reference_number[0]
		url="https://suppliers.multiquote.com/Unauthenticated/ViewRfqOpportunity.aspx?c="+str(authority_reference1)
	else:
		driver.find_element_by_css_selector('#ctl00_BackButton').click()
		i=i+1
	Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
	Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate,url)
	if Duplicate_Check == "Y":
		driver.find_element_by_css_selector('#ctl00_BackButton').click()
		i=i+1
	if Duplicate_Check == "N":
		# page_content = driver.page_source
		view_notice=re.findall('<span class="Text">View Notice<\/span>',page_content)
		if view_notice:
			driver.find_element_by_css_selector('#ctl00_BackButton').click()
		else:	
			title = re.findall('<input name="[^>]*?Title"[^>]*?Title" title="([^>]*?)"\s*style=[^>]*?><\/div>',page_content)
			reference_number=re.findall('<input name=[^>]*?value="([^>]*?)"\s*[^>]*?id="ctl00_M_RFQView_RFQCode"[^>]*?>',page_content)
			deadline_date=re.findall('<input name=[^>]*?value="[^>]*?day,\s*([^>]*?)"\s*[^>]*?ExpiryDate"[^>]*?>',page_content)
			deadline_time=re.findall('<input name="[^>]*?_ExpiryTimeLabel"[^>]*?value="([^>]*?)"',page_content)
			# description=re.findall('<label class="Caption">\s*(Short Description[\w\W]*?<label class="Caption">\s*Description\s*[\w\W]*?)<\/textarea>',page_content)
			description=re.findall('<th scope="col" class="rgHeader" style="white-space:nowrap;">(Item<\/th>[\w\W]*?Description[\w\W]*?)<\/textarea>',page_content)
			organization=re.findall('<label class="Caption">\s*Organisation[\w\W]*?value="([^>]*?)"',page_content)
			buyer_name=re.findall('<span class="Required">Buyer<\/span>[\w\W]*?value="([^>]*?)"',page_content)
			customer_name=re.findall('<span class="Required">Customer<\/span>\s*[\w\W]*?value="([^>]*?)"',page_content)
			contact_type1=re.findall('Agreement Type\s*[\w\W]*?value="([^>]*?)"',page_content)
			if title:
				title = title[0]
				title1=title
				# print ("Name:",title)
			
			if organization:
				organization = organization[0]
				# print ("Name:",organization)
			if buyer_name:
				buyer_name = buyer_name[0]
			else:
				buyer_name=""
				# print ("Name:",buyer_name)	
			if customer_name:
				customer_name = customer_name[0]	
			awarding_authority1=str(customer_name)+" "+str(buyer_name)+" "+str(organization)	
			if reference_number:
				authority_reference1 = reference_number[0]
				print ("Reference:",authority_reference1)
						
			if deadline_date:
				FormattedDate = DateFormat(deadline_date[0])
				# print ("Deadline Date:",FormattedDate)
			if contact_type1:
				contact_type1 = contact_type1[0]
				# print ("Deadline Date:",cpv_code1)	
			if deadline_time:
				deadline_time1 = deadline_time[0]
				# print ("Deadline Time:",deadline_time1)
			awarding_authority1=reg_clean(awarding_authority1)
			if description:
				description1=reg_clean(description[0])
			tender_block=re.findall('<tbody>([\w\W]*?)<\/textarea>',page_content)
			if tender_block:
				tender_block=reg_clean(tender_block[0]).decode('UTF-8',errors=ignore)
					# print (description1)
			description1=reg_clean(description1)		
			url="https://suppliers.multiquote.com/Unauthenticated/ViewRfqOpportunity.aspx?c="+str(authority_reference1)
			deadline_description="Expiry Date"
			value1=''
			location1=''
			cpv_code1=''
			award_procedure1=''
			other_information1 = "For further information regarding the above contract notice please visit: "+ str(url)
			curr_date = datetime.now(pytz.timezone('Europe/London'))
			created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url), str(tender_block), str(origin), str(sector), str(country), str(websource), str(title1), str(title1),str(awarding_authority1), str(award_procedure1), str(contact_type1), str(cpv_code1), str(description1), str(location1), str(authority_reference1), str(value1), str(deadline_description), str(FormattedDate), str(deadline_time1),str(other_information1), str(url), str(created_date))
			print (insertQuery)
			Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
				# time.sleep(3)
			# with open('Mydata1.html','a+')as f:
				# f.write(page_content)
			# tdElements2 = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')	
			driver.find_element_by_css_selector('#ctl00_BackButton').click()
	time.sleep(10)
	tdElements = driver.find_elements_by_css_selector('.rgAltRow td:nth-of-type(2), .rgRow td:nth-of-type(2)')
