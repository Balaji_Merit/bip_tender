#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX qw( strftime );
# use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );

use BIP_Tender_DB;
use JSON::Parse 'parse_json';

#### Getting the Input Company ID from the Script Name ####
# my $Input_Tender_ID = $0;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
my $Input_Tender_ID	= $ARGV[0];
# my $Input_Tender_ID	= '101';
# my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->proxy('http', 'http://172.27.137.192:3128');
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(30);
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);


### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;


my $StartDate=DateTime->now();
print "StART :: $StartDate\n";
my $ms = (1000 * $StartDate->epoch);
print "ms :: $ms\n";

#### Ping the first level link page ####

my $pageNo = 1;
my $count = 1;

my ($link1,$link2);
if ($Tender_Url=~m/^\s*(http[^>]*?iPage\=)[\d]+([^>]*?\-1\&_\=)/is)
{
	$link1 = $1;
	$link2 = $2;
	$Tender_Url = "$link1"."$pageNo"."$link2"."$ms";
}


# my $Tender_Url = 'https://in-tendhost.co.uk/sesharedservices/aspx/Services/Projects.svc/GetProjects?strMode=Current&searchvalue=&bUseSearch=false&OrderBy=Title&OrderDirection=ASC&iPage='.$pageNo.'&iPageSize=10&bOnlyWithCorrespondenceAllowed=false&iCustomerFilter=0&iOptInStatus=-1&_='.$ms;
my ($Tender_content2)=&Getcontent($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content10_v1.html");
print ts "$Tender_content2";
close ts;
exit;
&InfoCollect($Tender_content2);
$pageNo++;

$count = $1 if ($Tender_content2=~m/\"PageCount\"\:\s*([\d]+)\,/is);
foreach my $iPage ($pageNo..$count)
{
	print "\nPageNumber :: $iPage\n";
	my $Tender_Url = "$link1"."$iPage"."$link2"."$ms";
	print "\nTender_Url :: $Tender_Url\n";
	# my $Tender_Url = 'https://in-tendhost.co.uk/sesharedservices/aspx/Services/Projects.svc/GetProjects?strMode=Current&searchvalue=&bUseSearch=false&OrderBy=Title&OrderDirection=ASC&iPage='.$iPage.'&iPageSize=10&bOnlyWithCorrespondenceAllowed=false&iCustomerFilter=0&iOptInStatus=-1&_='.$ms;
	my ($Tender_content3)=&Getcontent($Tender_Url);
	$Tender_content3 = decode_utf8($Tender_content3);  
	open(ts,">Tender_content10_v1.html");
	print ts "$Tender_content3";
	close ts;
	print "\nPageNumber :: $iPage\n";
	&InfoCollect($Tender_content3);
}


sub InfoCollect()
{
	my $Tender_content4 = shift;
	while ($Tender_content4=~m/(\{\"AdditionalDeliveryText[\w\W]*?\})/igs)
	{
		my $block = $1;
		
		
		my $json = parse_json ($block);
		my %hashjson = %$json;
		
		my $tenderblock;
		foreach my $key (keys %hashjson)
		{
			my $value = $hashjson{$key};
			$tenderblock.= "$key : $value\n";
			# print "$key : $value\n";
		}
	
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		my $Deadline_Description = 'Date documents can be requested until';
		
		my $tender_title	= $hashjson{"Title"};
		my $ProjectID	= $hashjson{"ProjectID"};
		print "$ProjectID	:: $tender_title\n";
		
		my $deaddate	= $hashjson{"DateDocsAvailableUntil"};
		$deaddate=~s/\s*\/Date\(//igs;
		$deaddate=~s/\)\///igs;

		my $con_link;
		if ($Tender_Url=~m/^\s*(http[^>]*?aspx\/)/is)
		{
			$con_link = $1;
		}
		
		my $tender_link		= "$con_link".'ProjectManage/'.$ProjectID;
		# my $Deadline_Date = strftime("%Y-%m-%d %H:%M:%S", localtime($deaddate/1000));
		my $Deadline_Date = strftime("%Y-%m-%d", localtime($deaddate/1000));
		my $Deadline_time = strftime("%H:%M:%S", localtime($deaddate/1000));
		print "DeadLineDate :: $Deadline_Date\n";
		
		##Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my $Authority_Reference	= $hashjson{"Reference"};
		my $Description			= $hashjson{"Description"};
		my $Contact				= $hashjson{"Contact"};
		my $Customer			= $hashjson{"Customer"};
		my $Category			= $hashjson{"Category"};
		my $Process				= $hashjson{"Process"};
		my $ContaUserID			= $hashjson{"MainContactUserID"};
		my $CPV_Code			= $hashjson{"CPV"};
		$Description=&clean($Description);
		$Awarding_Authority = $Contact.', '.$Customer.', '.$ContaUserID;
		$Awarding_Authority=~s/^\s*\,|\,\s*$//igs;
		my ($award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag);
		$InTendFlag='Y';
		# open (RE,">>result12.txt");
		# print RE "$ProjectID\t$tender_title\t$Authority_Reference\t$Description\t$Contact\t$Customer\t$Category\t$Process\t$ContaUserID\t$Deadline_Date\t$Deadline_time\t$deaddate\n";
		# close RE;
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag);
	}
}	

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	# print "$user :: $pass\n";
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"in-tendhost.co.uk");
	$req->header("Content-Type"=> "application/json; charset=utf-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\,/:/igs;
	$Clean=~ s/\.\,/./igs;
	$Clean=~s/[^[:print:]]+//igs;
	decode_entities($Clean);
	$Clean=~s/[^[:print:]]+//igs;
	return $Clean;
}

#### To get the tender link ####
# while ($Tender_content2=~m/Guid\"\:\"([^>]*?)\"/igs)
# {
	# my $tender_link = "https://supplierlive.proactisp2p.com/Login/Opportunity?o=".($1);
	# my ($Tender_content)=&Getcontent($tender_link);
	# $Tender_content = decode_utf8($Tender_content);  
	# # open(ts,">Tender_content42.html");
	# # print ts "$Tender_content";
	# # close ts;
	
	# $Tender_content=parse_json($Tender_content);
	
	
	# my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
	# my $tender_title=$Tender_content->{"Name"};
	# my $ClosingDate =$Tender_content->{"ClosingDate"};
	# if ($ClosingDate=~m/^([^>]*?)T([^>]*?)$/is)
	# {
		# $Deadline_Date = $1;
		# $Deadline_time = $2;
		# $Deadline_Description = 'ClosingDate';
	# }
	
	# #Duplicate check
	# my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	# if ($duplicate eq 'Y')
	# {
		# print "\nAlready Existing this Tender Data\n";
		# next;
	# }
	# else
	# {
		# print "\nThis is newly Released Tender\n";
	# }
	
	# my $BuyerName=$Tender_content->{"BuyerName"};
	# my $BuyerAddress=$Tender_content->{"BuyerAddress"};
	# my $BuyerContact=$Tender_content->{"BuyerContact"};
	# $BuyerContact = &clean($BuyerContact);
	# $Awarding_Authority = "Customer Name : $BuyerName, "."$BuyerAddress, "."Customer Contact Details : $BuyerContact";
	
	# my $Authority_Reference=$Tender_content->{"Reference"};
	# my $Description=$Tender_content->{"Description"};
	# my $tenderblock=$Description;
	
	# &BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
# }	