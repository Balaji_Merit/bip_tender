import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import requests
import xlwt
import urllib
import re
import urllib.request as ur
from six.moves.html_parser import HTMLParser
h = HTMLParser()

from googletrans import Translator
###

book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Place")
sheet2 = book.add_sheet("boamp")
sheet1.write(0, 0, "procedure")
sheet1.write(0, 1, "categories")
sheet1.write(0, 2, "Published_on")
sheet1.write(0, 3, "reference")
sheet1.write(0, 4, "ref_no")
sheet1.write(0, 5, "organisme")
sheet1.write(0, 6, "objet")
sheet1.write(0, 7, "deadline_date")
sheet1.write(0, 8, "deadline_time")

sheet2.write(0, 0, "Title")
sheet2.write(0, 1, "AwardingAuthority")
sheet2.write(0, 2, "Desc")
sheet2.write(0, 3, "Autho_ref")
sheet2.write(0, 4, "Deadline_date")
sheet2.write(0, 5, "Deadline_time")
sheet2.write(0, 6, "Link")

date_mail_re = 'Date\:\s*([\d\-\s+]*?)\s+([\d\:]*?)\s+GMT'
gmail_sender_re = 'From:[\w\W]*?gmail\_sendername\">([^>]*?)[\s|<]'
block_re = '(<td\s*valign\=\"top\">[\w\W]*?Voir)'
details_block_re = '(<td[\w\W]*?<\/td>)'
sub_detail_re = '(<p[\w\W]*?<\/p>)'
boamp_blocks_re ='class\=\"m\_[\w\W]*?corpspaire\">([\w\W]*?)<\/tr>'
b_link_re = '<a\s*href\="([^>]*?)"'
title_re = 'class\=\"[\w\W]*?header-detail">[\w\W]*?<h1>([^>]*?)<\/h1>[\w\W]*?<\/header>'
article_re = '<article\s*role\=\"article\">([\w\W]*?)<\/article>'
awardingAuthority_re = 'class="detail-avis detail-main-1">([\w\W]*?)Objet'
object_contract_re = '<h3>Objet[\w\W]*?<p>([\w\W]*?)</p>'
desc_re = 'class\="detail\-avis\s*detail\-main\-2\">([\w\W]*?)<\/div>'
autho_ref_re = 'class\=\"avis\-ref\">([^>]*?)<\/p>'
deadline_date_re = '<h3>Date\s*limite[\w\W]*?<p>([/^\d{2}\/\d{2}\/\d{4}$/]*?)\s+'
deadline_time_re = '<h3>Date\s*limite\s*de[^>]*?<\/h3>[\w\W]*?<p>[\w\W]*?\s*(\d{1,2}h\d{2})'
###
'''
PCP = Procedure Category Published on
ROO = Reference | Entitled Object Organization
DL = Deadline for submission of plies
'''
EMAIL_ACCOUNT = "mohamed.sulaiman@meritgroup.co.uk"
PASSWORD = "Merit$786"
row_s1 = 1
row_s2 = 1

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.login(EMAIL_ACCOUNT, PASSWORD)
mail.list()
mail.select('BIP')
result, data = mail.uid('search', None, "ALL") # (ALL/UNSEEN)
i = len(data[0].split())
def reg_clean(cont):
    cont = str(cont).replace('\r','')
    cont = str(cont).replace('\n','')
    cont = str(cont).replace('\t','')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    # cont = str(cont).decode(encoding='UTF-8',errors='strict')
    return cont
def cont_encode(cont):
    cont = "u'"+ cont
    cont = cont.decode('utf-8')
    return cont
    
def req_content(url):
    try:
        req_url = ur.urlopen(url)
        url_content = req_url.read().decode('utf-8')
        return url_content
    except Exception as e:
        print ('Errors:', e)
        i = 0
        while i < 3:
            try:
                req_url = requests.get(url)
                url_content = req_url.content
                return url_content
            except:
                i = i +1
        return None

def regxx(regx,content):
    cont_data = re.findall(regx,content)
    if cont_data:
        cont_data = cont_data[0] 
    else:
        cont_data = ''
    return h.unescape(cont_data)


def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec
    
def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()
    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(str(InputText))
                translated_sentence = translated.text

            except Exception as e:
                # print ('Error translate 1**',e)
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'fr'
                               }
                    # Log file to check google translation
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write('\n=================================1============================\n')
                        #dees.write(str(payload))
                        #dees.write('\n==================================1===========================\n')

                    s = requests.post(url, data=payload)
                    #with open('descriptoin1.txt', 'a') as dees:
                        #dees.write(str(s.text))
                        #dees.write('\n------------------------------1--------------------------------\n')

                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    print ('Error translate 2 **', e)
                    #raw_input('***raw_input*****')
    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            # print ("translated_sentence:::",InputText,translated)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:

            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                payload = {'q': str(InputText),
                           'target': 'en',
                           'source' : 'fr'
                            }
                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write('\n=============================2================================\n')
                    #dees.write(str(payload))
                    #dees.write('\n=============================2================================\n')

                s = requests.post(url, data=payload)

                #with open ('descriptoin1.txt','a') as dees:
                    #dees.write(str(s.text))
                    #dees.write('\n----------------------------2----------------------------------\n')
                value = s.json()
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                redis_con.set(InputText, translated_sentence)
            except Exception as e:
                print ('**translate 4 **', e)
                #raw_input('***raw_input*****')

    return translated_sentence  

def place(body_msg):
    global row_s1
    process_id = 539
    blocks = re.findall(block_re,body_msg)
    date_mail = regxx(date_mail_re,body_msg)
    for s1,block in enumerate(blocks):
        details = re.findall(details_block_re,block)
        PCP = details[0]
        if PCP:
            PCPs = re.findall(sub_detail_re,PCP)
            # PCPs = translateText(PCPs)

            procedure = reg_clean(PCPs[0])
            # PCPs = translateText(procedure)
            
            categories = reg_clean(PCPs[1])
            # categories = translateText(categories)
            
            Published_on = reg_clean(PCPs[2])
            
        ROO = details[1]
        if ROO:
            ROOs = re.findall(sub_detail_re,ROO)
            reference = reg_clean(ROOs[0])            
            objet = reg_clean(ROOs[1])
            organisme = reg_clean(ROOs[2])
        DL = details[2]
        if DL:
            DLs = re.findall(sub_detail_re,DL)
            date = reg_clean(DLs[0])            
            time = reg_clean(DLs[1]) 
        awardProcedure = procedure
        title = reference
        CPV_code = categories
        # autho_ref = ref_no
        date_mail = date_mail
        sheet1.write(row_s1, 0, procedure)

        sheet1.write(row_s1, 1, categories)

        sheet1.write(row_s1, 2, Published_on)

        sheet1.write(row_s1, 3, reference)
        # sheet1.write(row_s1, 4, ref_no)

        sheet1.write(row_s1, 5, objet)

        sheet1.write(row_s1, 6, organisme)

        sheet1.write(row_s1, 7, date)

        sheet1.write(row_s1, 8, time)
        row_s1 = row_s1 + 1
        print ("-----place-{}-----".format(s1))
        
        # insertQuery = =  u"insert into "+ str(tender_data) +" (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,try_Cast(? as datetime),try_Cast(? as datetime),?,?,?)"
        
        # Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
        
        # input("^^")
        
        # return process_id,awardProcedure,title,CPV_code,autho_ref,date_mail

def boamp(body_msg):
    global row_s2
    process_id = 538

    boamp_blocks = re.findall(boamp_blocks_re,body_msg)

    for s2,boamp_block in enumerate(boamp_blocks):
        print ("-----boamp-{}-----".format(s2))
        b_link = re.findall(b_link_re,boamp_block)
        if b_link:                         
            b_link = b_link[0]

            link_content = req_content(b_link)
            # link_content = str(link_content)
            
            sheet2.write(row_s2, 6, b_link)
            # article = regxx(article_re,link_content)
            # article = reg_clean(article)
            
            title = regxx(title_re,link_content)
            title = reg_clean(title)
            sheet2.write(row_s2, 0, title)
            awardingAuthority = regxx(awardingAuthority_re,link_content)
            awardingAuthority = reg_clean(awardingAuthority)
            sheet2.write(row_s2, 1,h.unescape(awardingAuthority))
            
            desc = regxx(desc_re,link_content)
            desc = reg_clean(desc)
            desc = translateText(desc)
            sheet2.write(row_s2, 2, desc)
            
            autho_ref = regxx(autho_ref_re,link_content)
            autho_ref = reg_clean(autho_ref)
            sheet2.write(row_s2, 3, autho_ref)
            
            deadlinedate = regxx(deadline_date_re,link_content)
            sheet2.write(row_s2, 4, deadlinedate)       
            
            deadlinetime = regxx(deadline_time_re,link_content)
            deadlinetime = deadlinetime.replace('h',':')
            sheet2.write(row_s2, 5, deadlinetime)

            row_s2 = row_s2 + 1
            book.save('my_book_v7.xls')
for x in range(i):
    # conn = pymysql.connect(host='172.27.138.250', port=3306, user='root', password='admin', db='BIP_tender_analysis',use_unicode=True, charset="utf8")
    # cur = conn.cursor()
    latest_email_uid = data[0].split()[x]
    result, email_data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    # result, email_data = conn.store(num,'-FLAGS','\\Seen') 
    # this might work to set flag to seen, if it doesn't already
    raw_email = email_data[0][1]
    raw_email_string = raw_email.decode('utf-8')
    email_message = email.message_from_string(raw_email_string)

    # Header Details
    date_tuple = email.utils.parsedate_tz(email_message['Date'])
    if date_tuple:
        local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        local_message_date = "%s" %(str(local_date.strftime("%a, %d %b %Y %H:%M:%S")))
    email_from = str(email.header.make_header(email.header.decode_header(email_message['From'])))
    email_to = str(email.header.make_header(email.header.decode_header(email_message['To'])))
    subject = str(email.header.make_header(email.header.decode_header(email_message['Subject'])))

    # Body details
    for part in email_message.walk():
        if part.get_content_type() == "text/html":
            body = part.get_payload(decode=True)
            body_msg = body.decode('utf-8')
            gmail_sender = re.findall(gmail_sender_re,body_msg,re.I)
            if gmail_sender:
                gmail_sender = gmail_sender[0]
            file_name = "Email_" + str(x)+"_"+gmail_sender+".html"
            output_file = open(file_name, 'w')
            output_file.write("From: %s\nTo: %s\nDate: %s\nSubject: %s\n\nBody: \n\n%s" %(email_from, email_to,local_message_date, subject, body_msg))
            output_file.close()
                        
            if gmail_sender == str('PLACE'):
                places = place(body_msg)
            elif gmail_sender == str('BOAMP'):
                borders = boamp(body_msg)
            else:
                print ("INVALIED Email -->",gmail_sender)
        else:
            continue
# book.save('my_book_v7.xls')            
         
            
        