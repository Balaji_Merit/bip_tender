# -*- coding: utf-8 -*-
import os, sys
import requests
import re
from googletrans import Translator
import json
import redis
import time
import pymysql
import xlwt
import imp
import dateparser
import warnings
import time
warnings.filterwarnings('ignore')
# reload(sys)

# sys.setdefaultencoding('utf-8')

# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

def reg_clean(desc):
    desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')

    desc = re.sub(r'<[^<]*?>', ' ', str(desc))
    desc = re.sub(r'\r\n', '', str(desc), re.I)
    desc = re.sub(r"\\r\\n", '', str(desc), re.I)
    desc = re.sub(r'\t', " ", desc, re.I)
    desc = re.sub(r'\s\s+', " ", desc, re.I)
    desc = re.sub(r"^\s+\s*", "", desc, re.I)
    desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
    desc = re.sub(r"\&ndash\;", "-", desc, re.I)

    return desc

tbody_re = '<tbody>([\w\W]*?)<\/tbody>'
block_re = '<tr([\w\W]*?)<\/tr>'
baseUrl = 'https://www.hankintailmoitukset.fi'
sublink_re = '<td>([\d\.\:\-]+)\s+([\d\.\:\-]+)<\/td>\s*<td>\s*<a\s*href\=\"([^>]*?)\"\>([\w\W]*?)\<\/a\>'
name_purchese_re = 'Hankinnan\s*nimi\<\/dt\>\s*\<dd\>([^>]*?)<\/dd>'
contracting_enti_re = 'Hankintayksikk[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)<\/td>'
bussiness_id_re = 'Y\-tunnus\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
compitive_office_re = 'Kilpailuttamisesta[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
contact_re = 'Yhteyshenkil[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
mailing_re = 'Postiosoite[\:]*?<(?:\/?dt|\/?td)>\s*<(?:\/?dd|\/?td)>\s*([\w\W]*?)\s*<(?:\/dd|\/td>)'
zipcode_re = 'Postinumero[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
postal_re = 'Postitoimipaikka[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
country_re = 'Maa[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
phone_re = 'Puhelin[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
email_re = 'S[\w\W]*?hk[\w\W]*?postiosoite[\w\W]*?\<\/td\>\s*\<td\>([^>]*?)\<\/td\>'
website_re = 'Internet\-osoite[\w\W]*?\<\/td\>\s*\<td\>[\w\W]*?\"\>([^>]*?)\<\/a>\s*\<\/td\>'
pp_re = 'Hankintamenettely\<\/dt\>\s*\<dd\>([^>]*?)\<\/dd\>'
cvpCode_re = 'class\=\"CPV\"[\w\W]*?\<strong\>[\w\W]*?\<td\>([^>]*?)\<\/td\>'
desc_re = 'Hankinnan[\w\W]*?\<p\>([^>]*?)\<\/p\>'
contract_perform_re = 'p[\w\W]*?asiallinen\s*toteutuspaikka[\w\W]*?<dd>([^>]*?)<\/dd>'

ref_re = 'tai\s*viitenumero[\w\W]*?\<dd\>([^>]*?)\<\/dd\>'
tender_date_time = 'Tenders or requests for participation must be submitted to the contracting entity by no later than'
date_time_re = 'hankintayksik[\w\W]*?lle\s*viimeist[\w\W]*?n<\/dt>\s*<dd>([\d\.\:\-]+)\s+([\d\.\:\-]+)<\/dd'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
nxt = '/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D=&page='
contanct_detail_block_re = '(<table\s*[^>]*?class\=\"CONTACT\">[\w\W]*?<\/table>)'
# details_re = '<tr[^>]*?>\s*[^>]*?<[td|dd]+[^>]*?>([^>]*?)<\/[td|dd]+>\s*<[td|dd]+>\s*(?:<a\s*[^>]*?>)?([^>]*?)<\/'
details_re = '<tr[^>]*?>\s*[^>]*?<[td]+[^>]*?>([^>]*?)<\/[td]+>\s*<[td]+>\s*([^>]*?)<\/'
Tender_ID = 532

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'fi'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fi'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    return translated_sentence
	


def req_content(url):
    proxies = {
    "http" :"http://172.27.137.199:3128",
    "https":"https://172.27.137.199:3128"
    }
    req_url = requests.get(url,proxies=proxies)
    url_content = req_url.content.decode('utf_8', errors='ignore')

    return url_content

def regxx(reg,content):
    value = re.findall(reg,content,flags=re.UNICODE)
    return value

def main():
    proxies = {
    "http" :"http://172.27.137.192:3128",
    "https":"https://172.27.137.192:3128"
    }
    avoid = "Korjausilmoitus"
    avoid2 = "Ilmoituksen numero EUVL"
    global n
    global main_url
    global  page
    page = 1
    while (page < 5):
        main_content = req_content(main_url)
        tbody = regxx(tbody_re,main_content)
        if tbody:
            tbody = tbody[0]
            blocks = regxx(block_re, tbody)
            for enum_i, block in enumerate(blocks):
                # avoid is filter to accept without correction tenders
                if avoid not in block:
                    # sublink reg is filtering the without deadline date of tenders
                    sublink = regxx(sublink_re,block)
                    if sublink:
                        deadlinedate = sublink[0][0]
                        deadlinetime = sublink[0][1]
                        title = sublink[0][3]
                        if title:
                            title = translateText(str(title))
                        else:
                            title = ''
                        dt = dateparser.parse(deadlinedate)
                        FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                        sublink = sublink[0][2]
                        sublink_full = baseUrl + sublink
                        Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sublink_full)
                        if Duplicate_Check == "N":
                            curr_date = dateparser.parse(time.asctime())
                            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                            sub_content = req_content(sublink_full)
                            name_purchese = regxx(name_purchese_re,sub_content)
                            if avoid2 not in sub_content:
                                if name_purchese:
                                    name_purchese = name_purchese[0]
                                    name_purchese = translateText(str(name_purchese))
                                else:
                                    name_purchese = ''
                                contanct_detail_block = regxx(contanct_detail_block_re,sub_content)
                                if contanct_detail_block:
                                    data = regxx(details_re,contanct_detail_block[0])
                                    if data:
                                        datasring = ''
                                        for value in data:
                                            if not "Y-tunnus" in str(value[0]):
                                                datasring = datasring+str(value[0]) + ':' + str(value[1]) + '\n'
                                    else:
                                        datasring = ''
                                    datasring = datasring.strip()
								# if contanct_detail_block[1]:
									# contact_detail2 = regxx(details_re,contanct_detail_block[1])
									# if contact_detail2:
										# datasring2 = ''

										# for value2 in contact_detail2:
											# datasring2 = datasring2+str(value2[0]) + ':' + str(value2[1]) + '\n'
									# else:
                                    datasring2 = ''
                                    datasring2 = datasring2.strip()
                                contact_details = datasring

                                contact_details = str(contact_details)
                                awardProcedure = regxx(pp_re,sub_content)
                                if awardProcedure:
                                    awardProcedure = awardProcedure[0]
                                    awardProcedure = translateText(str(awardProcedure))
                                else:
                                    awardProcedure = ''
                                cvpCode = regxx(cvpCode_re,sub_content)
                                if cvpCode:
                                    cvpCode = ' | '.join(cvpCode)
                                    cvpCode = translateText(str(cvpCode))

                                else:
                                    cvpCode = ''
                                desc = regxx(desc_re, sub_content)
                                if desc:
                                    desc = desc[0]
                                    desc = translateText(str(desc))
                                else:
                                    desc = ''
                                contract_perform = regxx(contract_perform_re, sub_content)
                                if contract_perform:
                                    contract_perform = reg_clean(contract_perform[0])
                                else:
                                    contract_perform = ''
								# contract_perform = translateText(str(contract_perform))
                                ref = regxx(ref_re, sub_content)
                                if ref:
                                    ref = ref[0]
									# ref = translateText(str(ref))
                                else:
                                    ref = ''
								# date_time = regxx(date_time_re, sub_content)
								# if date_time:
									# date = date_time[0][0]
									# date = translateText(str(date))
									# time = date_time[0][1]
								# else:
									# date = ''
									# time = ''
                                summary_cont = summary + sublink_full
                                orgin = 'European/Non Defence'
                                sector = 'European Translation'
                                
                                country = 'FI'
                                websource = 'Finland: HILMA'
								
                                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sublink_full), str(), str(orgin), str(sector), str(country), str(websource), str(title), str(title),str(contact_details), str(awardProcedure), str(), str(cvpCode), str(desc), str(contract_perform), str(ref), str(), str(tender_date_time), str(FormattedDate), str(deadlinetime),str(), str(summary_cont), str('Y'),str(created_date))
                                try:	
                                    val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
								# print "insertion status :", val
                                    if val == 1:
                                        print ("1 Row Inserted Successfully")
                                    else:
                                        print ("Insert Failed")
                                except Exception as e:
            
                                    pass
                    else:
                        tbody = ''
                else:
                    print ("correction block")
        page = page + 1
        nxt_url = nxt_page_link + str(page)
        main_url = nxt_url

    connection.close()
if __name__== "__main__":
    # Tender_ID = sys.argv[0]
    # Tender_url = Database_Connector.Retrieve_url(mysql_cursor,Tender_ID)
    main_url = 'https://www.hankintailmoitukset.fi/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D='
    nxt_page_link = 'https://www.hankintailmoitukset.fi/fi/notice/search/?_s%5B_sent%5D=1&_s%5Bphrase%5D=&_s%5Bcpv%5D=&_s%5Borganisation%5D=&_s%5Bnuts%5D=&_s%5Bpublished_start%5D=&_s%5Bpublished_end%5D=&page='
    n = 1
    main()