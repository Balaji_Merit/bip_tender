#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
# my $dbh = &BIP_Tender_DB::DbConnection();
# my $Input_Table='tender_master';

#### Getting the Input Company ID from the Script Name ####
#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

# my $Tender_Url="http://www.maginternational.org/tenders/tenders/#.UvSm4S78amM";
# my $SourceName="UK: MAG (Mine Action Group)";
# my $Tender_ID="1010";
# my $origin="Web";
# my $noticeSector="UK Other";
print "Tender_ID	: $Tender_ID\n";	
# print "SourceName	: $SourceName\n";	
print "Tender_Url	: $Tender_Url\n";	

### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1); 

	my($ID,$cy,$sourceURL,$high_value,$title,$awardingAuthority,$awardProcedure,$contractType,$cpvNos,$description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$authoritypostcode,$nuts,$tender_title);
	my $Created_Date = localtime;
	#my $Created_Date= $t->ymd;
	
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	my $tender_title;
	my $temp_dt;
	if ($Tender_content1=~m/(?:larger\">\s*[^>]*?\s*|\s*)<strong>\s*([^>]*?)\s*<\/strong>\s*<\/p>/is)
	{
		$tender_title  = &clean($1);
	}
	if ($Tender_content1=~m/larger\">\s*[\w\W]*?\s*<p\s*class\=\"none\">\s*([^>]*?)\s*<strong>/is)
	{
		$temp_dt  = &clean($1);
		
	}
	elsif ($Tender_content1=~m/tbody>[\w\W]*?<p class="larger">\s*[^>]*?\s*<strong>\s*([^>]*?)\s*</is)
	{
		$temp_dt= $1;


	}
	elsif($Tender_content1=~m/Closing date for tender\:\s*([\w\W]*?)<\/p>/is)
	{
	$temp_dt= $1;
	}
	if($temp_dt=~m/(\d{1,2})\s*(\w+)\s*(\d{4})\s*(?:at\s*|\,\s*|by\s*|\s*)\s*(\d{1,2}:\d{1,2}\s*(?:am|pm|\s*))/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1."/".(substr $2, 0, 3)."/".$3);
		# $Deadline_Date=$1."/".(substr $2, 0, 3)."/".$3;
		print $Deadline_Date;
		
		$Deadline_time=$4;
		# print $Deadline_time;
		# exit;
		}

	if ($Tender_content1=~m/larger\">\s*[\w\W]*?\s*<strong>\s*([^>]*?)\s*GMT/is)
	{
		$Deadline_time = &clean($1);
		
	}
			if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM|noon)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		
	if ($Tender_content1=~m/<h\d{1}[^>]*?>Tenders<\/h\d{1}>([\w\W]*?)Sidebar/is)
	{
		$Description = &clean($1);
		
	}
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$Tender_Url,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		$Deadline_Description="Deadline for the Submission:";
		
		print $tender_link;
		
	# open (RE,">>1010.txt");
	# print RE "$ID\t$Tender_ID\t$SourceName\t$origin\t$noticeSector\t$cy\t$sourceURL\t$high_value\t$tender_title\t$awardingAuthority\t$awardProcedure\t$contractType\t$cpvNos\t$description\t$site\t$authorityRefNo\t$value\t$cw_Radeadline\t$d_RA\t$d_Ratime\t$d_Rapost\t$otherInformation\t$authoritypostcode\t$nuts\t$Created_Date\n";
	# close RE;

	my ($tenderblock);
	# if($tender_link_content2=~m/SELECT STATE([\w\W]*?)HERE/is)
	# {
	# $tenderblock=$1;	
	# $tenderblock=~s/\t/ /igs;
	# $tenderblock=~s/\|/ /igs;
	# $tenderblock=&clean($tenderblock);
	# $tenderblock=&Trim($tenderblock);
	# $tenderblock = &BIP_Tender_DB::clean($tenderblock);
	# }
	  
	if($Tender_content1=~m/<h\d{1}[^>]*?>Tenders<\/h\d{1}>([\w\W]*?)Sidebar/is)
	{
	# $tenderblock=$tender_link_content2;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
#######################
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	#$req->header("Host"=>"www.thetraininggateway.com");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	
	# my $Host=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.thetraininggateway.com");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "http://www.thetraininggateway.com/view-enquiries/");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	
	decode_entities($Clean);
	return $Clean;
}