#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
# use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
# use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
# my $Input_Tender_ID = $0;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
# my $dbh = &BIP_Tender_DB::DbConnection();
# my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
# my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= '72';
my $TenderName 			= "Knowledgepool";
my $Tender_Url 			= "https://procurementsolutions.esourcingportal.com/";

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	


### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $Tender_Url1 			= "https://procurementsolutions.esourcingportal.com/current_opportunities.cfm";
my ($Tender_content1)=&Getcontent1($Tender_Url1,$cookie);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content2.html");
print ts "$Tender_content1";
close ts;
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"procurementsolutions.esourcingportal.com");
	# $req->header("Referer"=>"https://crowncommercialservice.bravosolution.co.uk/web/login.shtml");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "JSESSIONID=8CB53C6323F58CFFD1F0929216EADB5C.cfusion; HASESSIONID=8CB53C6323F58CFFD1F0929216EADB5C; b1pi=!5LctpbE3tdgCchXZ8u8TtOtNMLgLyVSVyPO2f8c8vxNyW82GBjKG9f5muq38yM/VpmVC+s5TSUe+iA==; i18next=en");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do");
	$req->header("Cookie"=> "$Cookie_JSESSIONID");
	$req->header("Connection"=> "keep-alive");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>|<td>|<\/td>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}

sub Getcontent1
{
	my $url = shift;
	my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"procurementsolutions.esourcingportal.com");
	$req->header("Referer"=> "https://procurementsolutions.esourcingportal.com/");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "$Cookie_JSESSIONID");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}