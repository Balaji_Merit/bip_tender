#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
top:
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=1;
#### To get the tender link ####

	while($Tender_content1=~m/<a class="TenderOff"\s*href\=\"([^>]*?)\"[^>]*?>/igs)
	{
	my $tender_link=$1;
	my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference);
	my $tender_title;
	my ($Tender_content2,$Referer)=&Getcontent($tender_link);

	if($Tender_content2=~m/<span class="BigTextMenu">\s*<div align="left">([^>]*?)\s*<\/div>/is)
	{
	$tender_title=$1;
	}
	if($Tender_content2=~m/Reference: <\/b><\/span><\/td>\s*<td><span class="Text">([^>]*?)<\/span><\/td>/is)
	{
	$Authority_Reference=$1;
	}
	
	my ($Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	
	if($Tender_content2=~m/State\:\s*<\/b><\/span><\/td>\s*<td>\s*<span class="Text">([^>]*?)<\/span>/is)
		{
			$Location=$1;
		}
	
	if ($Tender_content2=~m/<span class="Paragraph">([\w\W]*?)<div Id="footing">/is)
	{
		$Description=$1;
		$Description=&BIP_Tender_DB::clean($Description);
	}
		
		if($Tender_content2=~m/Closing Date:\s*<\/b><\/span><\/td>\s*<td>\s*<span class="Text">([^>]*?)<\/span>\s*<span class="Text">\&nbsp\;\(([^>]*?)\)<\/span>\s*<\/td>/is)
		{
			$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1);
			$Deadline_time=&clean($2);
			if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
			{
			my $hh=$1;
			$Deadline_time=($hh+12).":".$2 if($hh != 12);
			$Deadline_time=$hh.":".$2 if($1 == 12);
			}
			if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
			{
			my $hh=$1;
			$Deadline_time=$hh.":".$2 if($hh != 12);
			$Deadline_time="00:".$2 if($hh == 12);
			}
			$Deadline_time=~s/am//igs;
			$Deadline_time=~s/AM//igs;
			
			$Deadline_Description="Closing Date :";
		}
		elsif($Tender_content2=~m/Closing Date:\s*<\/b><\/span><\/td>\s*<td>\s*<span class="Text">([^>]*?)<\/span>/is)
		{
			$Deadline_Date=$1;
			# $d_Ratime=&clean($2);
			$Deadline_Description="Closing Date :";
		}
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	# if ($Tender_content4=~m/<span>(Deadline[^>]*?)<\/span>\s*<\/td>\s*<td  class="summary-item-field">\s*<span>([^>]*?)<\/span>/is)
	# {
		# $other_information  = $1.": ".$2;
	# }
	# if ($Tender_content4=~m/<span>(Latest[^>]*?)<\/span>\s*<\/td>\s*<td  class="summary-item-field">\s*<span>([^>]*?)<\/span>/is)
	# {
		# $other_information= $other_information." ".$1.": ".$2;
	# }	
	# if ($Tender_content4=~m/<span>(Expected contract[^>]*?)<\/span>\s*<\/td>\s*<td  class="summary-item-field">\s*<span>([^>]*?)<\/span>/is)
	# {
		# $other_information= $other_information." ".$1.": ".$2;
	# }	
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if ($Tender_content2=~m/<span class="BigTextMenu">\s*<div align="left">([^>]*?)\s*<\/div>([\w\W]*?)<div Id="footing">/is)
	{
		$tenderblock = $1." ".$2;
	}
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
		$cy='AU';
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
# }
if($Tender_content1=~m/<A class="[^>]*?"\s*HREF="([^>]*?)"\s*>Next/is)
	{
	$Tender_Url="http://www.constructionsite.com.au".$1;
	# my ($Tender_content1,$Referer)=&Getcontent($url);
	# open(out,">undp1.html");
	# print out "$Home_content";
	# close out;
	goto top;
}
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/plain; charset=UTF-8");
	$req->header("Host"=> "www.constructionsite.com.au");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	decode_entities($Clean);
	return $Clean;
}