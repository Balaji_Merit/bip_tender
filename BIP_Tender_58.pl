#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
$ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;

#### To get the tender link ####
while ($Tender_content1=~m/InCategoryHyperLink\"\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*</igs)
{
	my $tender_link1 = &Urlcheck($1);
	my $categoryyyy  = $2;
	next if ($categoryyyy=~m/\(Expired\s*Tenders\)/is);
	my ($Tender_content2)=&Getcontent($tender_link1);
	$Tender_content2 = decode_utf8($Tender_content2); 

	while ($Tender_content2=~m/TenderDetailsHyperLink1\"\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*</igs)
	{
		my $tender_link = &Urlcheck($1);
		my $tender_title1=$2;
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 
		open(ts,">Tender_content58.html");
		print ts "$Tender_content";
		close ts;
	
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		if ($Tender_content=~m/CaptionLabel\">\s*([^>]*?)\s*</is)
		{
			$tender_title 		 = &clean($2);
		}
		$tender_title=$tender_title1 if ($tender_title eq '');
		
		if ($Tender_content=~m/>\s*(Submission\s*DeadLine)\s*(?:\:)?\s*<\/span>\s*<[^>]*?expiry_dateLabel\">\s*([^>]*?)\s*</is)
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}
		if ($Tender_content=~m/expiry_timeLabel\">\s*([^>]*?)\s*</is)
		{
			$Deadline_time = &clean($1);
		}
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my ($address,$Contact,$tenderblock);
		if ($Tender_content=~m/>\s*Return\s*Information\s*(?:\:)?([\w\W]*?)<\/li>/is)
		{
			$address = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Contact\s*(?:\:)?[\w\W]*?)<\/li>/is)
		{
			$Contact = &clean($1);
		}
		$Awarding_Authority=$address.', '.$Contact;
		$Awarding_Authority=&clean($1);
		
		if ($Tender_content=~m/>\s*Procedure(?:\:)?([\w\W]*?)<\/li>/is)
		{
			$award_procedure = &clean($1);
		}
		if ($Tender_content=~m/>\s*Further\s*Information(?:\:)?([\w\W]*?)<\/li>/is)
		{
			$Description = &clean($1);
		}
		if ($Tender_content=~m/>\s*Reference\s*Code(?:\:)?([\w\W]*?)<\/li>/is)
		{
			$Authority_Reference = &clean($1);
		}
		if ($Tender_content=~m/>\s*Value(?:\:)?([\w\W]*?)<\/li>/is)
		{
			$Value = &clean($1);
		}
		
		if ($Tender_content=~m/Tender\s*Information<\/span>([\w\W]*?)<\/table>/is)
		{
			$tenderblock = $1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);

		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
	}	
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>'e-services.worcestershire.gov.uk');
	$req->header("Content-Type"=> "text/html; charset=utf-8");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;

	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Referer"=> "currentCulture=en%2DGB; EUSSESSION=8576e973-08e5-4169-87f5-9b97537c3bd5; semanticId=1; EUSSTARTPAGE=etenders%5Fsimple%2Easp; EUSBRAND=%2FPROD%2FETENDERS%5FSIMPLE; EUSCUSTOMCSS=0");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\,\s*\,/,/igs;
	$Clean=~ s/,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}