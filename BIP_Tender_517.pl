#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url='https://buyandsell.gc.ca/';
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my $count=1;
my $Tender_content2;
#### To get the tender link ####
# if($Tender_content1=~m/<p>Click the <a href="([^>]*?)">/is)
# {
# my $url="https://buyandsell.gc.ca/procurement-data/search/site?f[0]=sm_facet_procurement_data:data_data_tender_notice&amp;f[1]=ss_publishing_status:SDS-SS-005";
# my $url="https://buyandsell.gc.ca/procurement-data/search/site?f[0]=sm_facet_procurement_data%3Adata_data_tender_notice&f[1]=dds_facet_date_published%3Adds_facet_date_published_today";
for (my $i=0;$i<15;$i++)
{
# https://buyandsell.gc.ca/procurement-data/search/site?f%5B0%5D=sm_facet_procurement_data%3Atender_notice&f%5B1%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B2%5D=dds_facet_date_published%3Adds_facet_date_published_7day
# my $url="https://buyandsell.gc.ca/procurement-data/search/site?page=".$i."&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B1%5D=dds_facet_date_published%3Adds_facet_date_published_today";
my $url="https://buyandsell.gc.ca/procurement-data/search/site?page=".$i."&f%5B0%5D=sm_facet_procurement_data%3Atender_notice&f%5B1%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B2%5D=dds_facet_date_published%3Adds_facet_date_published_7day";
# my $url= "https://buyandsell.gc.ca/procurement-data/search/site?solrsort=dds_publication_date%20desc&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B1%5D=ss_publishing_status%3ASDS-SS-005";
# my $url="https://buyandsell.gc.ca/procurement-data/search/site?page=1&solrsort=dds_publication_date%20desc&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B1%5D=ss_publishing_status%3ASDS-SS-005&f%5B2%5D=dds_facet_date_published%3Adds_facet_date_published_7day";
# https://buyandsell.gc.ca/procurement-data/search/site?f%5b0%5d=sm_facet_procurement_data:data_data_tender_notice&amp;f[1]=ss_publishing_status:SDS-SS-005
# if($url!~m/^http/is)
# {	
# print "\nhi\n";
# my $u1=URI::URL->new($url,$Tender_Url);
# my $u2=$u1->abs;
# $url=$u2;
# $url=~s/\&amp\;/&/igs;
# }

$Tender_content2=&Getcontent($url);
$Tender_content2 = decode_utf8($Tender_content2);
open(ts,">Tender_content517.html");
print ts "$Tender_content2";
close ts;  
# }
my $count=0;
# top:
# while($Tender_content2=~m/<h2>\s*<a\s*href="([^>]*?)">/igs)
while($Tender_content2=~m/Tender\s*Notice\s*<\/span>\s*<\/span><a\s*href="([^>]*?)">/igs)
	{
		my $tender_link=$1;
		my ($tender_link_content1,$Referer)=&Getcontent($tender_link);
		# print "Balaji";
		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		# next if($tender_link_content1!~m/<strong class="[^>]*?">Amendment date<\/strong><\/dt><dd class='[^>]*?'><span class="[^>]*?">None<\/span><\/dd>/is);
		print $tender_link_content1;
		if($tender_link_content1=~m/<h1[^>]*?>([^>]*?)<\/h1>/is)
		{
		$tender_title=$1;
		}
		print $tender_title;
		if($tender_link_content1=~m/(Procurement entity[\w\W]*?)<\/dd>[\w\W]*?(Contact name[\w\W]*?)<\/dd><\/dl>/is)
		{
		$Awarding_Authority=$1.$2;
		}
		# if($tender_link_content1=~m/Contact:[\w\W]*?\|<\/div>([^>]*?)<br\s*\/>/is)
		# {
		# $Awarding_Authority=$Awarding_Authority.$1;
		# }
		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		if($tender_link_content1=~m/Tendering procedure[\w\W]*?"field-content">([^>]*?)<\/span>/is)
		{
			$award_procedure=$1;
		}
	
		# if($tender_link_content1=~m/Description[\w\W]*?"field-content">([\w\W]*?)Access and terms of use/is)
		if($tender_link_content1=~m/Description\s*<\/h3>([\w\W]*?)<\/article>/is)
		{
		$Description=$1;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		# if($tender_link_content1=~m/Region of delivery[\w\W]*?"field-content">([^>]*?)<\/span>/is)
		if($tender_link_content1=~m/Region of delivery[\w\W]*?<li class="first last">([^>]*?)<\/li>/is)
		{
		$Location=$1;
		}
		
		# if($tender_link_content1=~m/Reference number[\w\W]*?field-content">([^>]*?)<\/span>[\w\W]*?Solicitation number[\w\W]*?field-content">([^>]*?)<\/span>/is)
		if($tender_link_content1=~m/Reference number[\w\W]*?field-content">([^>]*?)<\/div>[\w\W]*?Solicitation number[\w\W]*?field-content">([^>]*?)<\/div>/is)
		{
		$Authority_Reference="Reference number: ".$1.", Solicitation number: ".$2;
		}
		if($tender_link_content1=~m/Date closing[\w\W]*?field-content">([^>]*?)<\/span>/is)
		{
		my $ddate=$1;
		if($ddate=~m/(\d{4})\/(\d{1,2})\/(\d{1,2})\s*(\d{1,2}\:\d{1,2})/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		$Deadline_time=$4;
		}
		}
		$Deadline_Description="Date closing: (Eastern Daylight Time (EDT))";
		
		print $tender_link;
		
		
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
		if($tender_link_content1=~m/<h2[^>]*?>Tender Notice([\w\W]*?)<div class="wb-share"/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='CA';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
}
# }
# $count=$count+1;
# if($Tender_content2=~m/href="([^>]*?)">\s*next/is)
# if($count<15)
# {
# "https://buyandsell.gc.ca/procurement-data/search/site?page=".$count."&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B1%5D=dds_facet_date_published%3Adds_facet_date_published_today"
# my $url=$1;
# $url=~s/\&amp\;/&/igs;
# my ($Tender_content3)=&Getcontent("https://buyandsell.gc.ca/procurement-data/search/site?page=".$count."&f%5B0%5D=sm_facet_procurement_data%3Adata_data_tender_notice&f%5B1%5D=dds_facet_date_published%3Adds_facet_date_published_today");
# $Tender_content3 = decode_utf8($Tender_content3);
# open(ts,">Tender_content517_2.html");
# print ts "$Tender_content3";
# close ts;
# exit;
# my $Tender_content2=$Tender_content3;
# goto top;
# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "buyandsell.gc.ca");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.biznet.ct.gov");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://sasktenders.ca/content/public/Search.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	decode_entities($Clean);
	return $Clean;
}