#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);
$Input_Tender_ID='511';
my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
$ua->proxy('https', 'http://172.27.137.199:3128'); 
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# $Tender_Url='http://www.merx.com/English/NonMember.asp?WCE=Show&TAB=1&PORTAL=MERX&State=1&hcode=tAF08b5PaGBuUkl43CRDCg%3d%3d';
$Tender_Url='https://www.merx.com/public/solicitations/categories';
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;




# my $time1=localtime(time() - 24*60*60);
my $date12		= strftime "%Y-%m-%d", localtime(time()- 24*60*60);
print $date12;
### Ping the first level link page ####

my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content511.html");
print ts "$Tender_content1";
close ts;
# exit;
# my $count=1;
my $Tender_content2;
#### To get the tender link ####
my $Tender_link3;


my ($tender_link_content1,$tender_link_content2,$Referer);
# my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	# my $tender_link;
	my $tender_title;
		
		while($Tender_content1=~m/<a[^>]*?id="solCountGridLinkLastWeek_[^>]*?" href="([^>]*?)"\s*title="[^>]*?"\s*class="mets-command-link">/igs)
			{
			my $tender_link1=$1; 
			# $count = $count + 1;
			# print $count;
			if($tender_link1!~m/^https/is)
			{
			$tender_link1="https://www.merx.com".$tender_link1;
			$tender_link_content2=&Getcontent($tender_link1);
			}
			else
			{
			$tender_link_content2=&Getcontent1($tender_link1);
			}

			# my ($tender_link_content1,$Referer);
			# if($tender_link=~m/^https/is)
			# {
			# $tender_link_content1=&Getcontent1($tender_link);
			# goto proc1;
			# }
	top1:		
	while($tender_link_content2=~m/<a[^>]*?id="searchResult[^>]*?"\s*href="([^>]*?)"\s*[^>]*?>/igs)
{
		my $tender_link="https://www.merx.com".$1;
		my $tender_link_content1=&Getcontent($tender_link);
		my $published_date;
		my $revised_date;
			my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		
		if($tender_link_content1=~m/<strong>Published<\/strong><\/font><\/TD>\s*<td class="ContentListEVEN"[^>]*?>([^>]*?)<\/TD>/is)
		{
		$published_date=$1;
		print $published_date;
		}
		# next if($published_date!~m/$date12/is);
		# next if($published_date!~m/2018-01-29/is);
		if($tender_link_content1=~m/<strong>Revised<\/strong><\/font><\/TD>\s*<td class="ContentListODD">([^>]*?)<\/TD>/is)
		{
		$revised_date=$1;
		}
		next if($revised_date=~m/20/is);	
		# next if($revised_date ne '');
		# {
		# print $revised_date;
		# <>;
		# }
		
			# if($tender_link_content1=~m/<h1[^>]*?>([^>]*?)<\/h1>/is)
		if($tender_link_content1=~m/Title\s*<\/span>\s*<div class="mets-field-body">\s*<p>\s*([^>]*?)\s*<\/p>\s*<\/div>/is)
		{
		$tender_title=$1;
		$tender_title=&BIP_Tender_DB::clean($tender_title);
		print $tender_title;
		# <>;
		}
		if($tender_link_content1=~m/Organization Name<\/strong><\/font><\/TD>\s*<td class="[^>]*?">([\w\W]*?)<\/TD>/is)
		{
		$Awarding_Authority=$1;
		}
		elsif($tender_link_content1=~m/Issuing Organization([\w\W]*?)<\/div>/is)
		{
		$Awarding_Authority=$1;
		}
				
		if($tender_link_content1=~m/<span  class="mets-field-label">\s*Location\s*<\/span>\s*<div class="mets-field-body">\s*<p>\s*([\w\W]*?)<\/p><\/div>/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".$1;
		}
		if($tender_link_content1=~m/(Contact Information[\w\W]*?)<div class="twoColFields">/is)
		{
		$Awarding_Authority=$Awarding_Authority." ".$1;
		}
		
		if($tender_link_content1=~m/Reference[\w\W]*?<td\s*class=[^>]*?>([^>]*?)<\/TD>/is)
		{
		$Authority_Reference="Reference Number :".$1;
		}
		if($tender_link_content1=~m/Reference Number([\w\W]*?)<\/div>/is)
		{
		$Authority_Reference="Reference Number :".$1;
		}
		
		if($tender_link_content1=~m/Solicitation Number[\w\W]*?<td\s*class=[^>]*?>([^>]*?)<\/TD>/is)
		{
		$Authority_Reference=$Authority_Reference." Solicitation Number :".$1;
		}
		if($tender_link_content1=~m/Solicitation Number([\w\W]*?)<\/div>/is)
		{
		$Authority_Reference=$Authority_Reference." Solicitation Number :".$1;
		}
		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		$Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		$Authority_Reference=&clean($Authority_Reference);
		# if($tender_link_content1=~m/Procedure:\s*<\/dt>\s*<dd>([^>]*?)<\/dd>/is)
		# {
			# $award_procedure=$1;
			# $award_procedure = &BIP_Tender_DB::clean($award_procedure);
			# $award_procedure=&clean($award_procedure);
		# }
	
		if($tender_link_content1=~m/Description<\/span>\s*<div class="mets-field-body">\s*([\w\W]*?)<\/span>/is)
		{
		$Description=$1;
		# $Description=~s/content=[^>]*?"\s*\/>//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		elsif($tender_link_content1=~m/Description<\/span>([\w\W]*?)<\/div>/is)
		{
		$Description=$1;
		$Description=~s/mets-field">//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		if($tender_link_content1=~m/Job Location<\/span>\s*<div class="mets-field-body">\s*<p>([^>]*?)<\/p>\s*<\/div>/is)
		{
		$Location=$1;
		}
		elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		{
		$Location=$1;
		$Location=&BIP_Tender_DB::clean($Location);
		$Location=&clean($Location);
		}
		elsif($tender_link_content1=~m/Location\s*<\/span>([\w\W]*?)<\/div>/is)
		{
		$Location=$1;
		$Location=&BIP_Tender_DB::clean($Location);
		$Location=&clean($Location);
		}
		my $dd_des;
		my $temp_dt;
		if($tender_link_content1=~m/Closing Date<\/span>\s*<div class="mets-field-body">\s*<p>([^>]*?)<\/p>\s*<\/div>/is)
		{
		$temp_dt=$1;
		$temp_dt=~s/\&nbsp\;/ /igs;
		print $temp_dt;
		# exit;
		# my $dd_des;
							
		if($temp_dt=~m/([^>]*?)\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))\s*([^>]*?Time[^>]*?)/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		$Deadline_time=$4;
		$dd_des=&clean($5);
		# print $dd_des;
		$Deadline_Description="Closing Date (".$dd_des."):";
		# print $Deadline_Description;
		# <>;
		}
		if($temp_dt=~m/(\w+)\s+(\d{1,2})\s*\,\s*(\d{4})\s*(\d{1,2}\:\d{1,2}\s*(?:AM|PM))\s*([^>]*?ST)/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/".$2);
		
		$Deadline_time=$4;
		$dd_des=&clean($5);
		# print $dd_des;
		$Deadline_Description="Closing Date (".$dd_des."):";
		# print $Deadline_Description;
		# <>;
		}
		# exit;
		if($temp_dt=~m/(\d{4})\/(\d{1,2})\/(\d{1,2})\s*(\d{1,2}\:\d{1,2}\:\d{1,2}\s*(?:AM|PM))\s*([^>]*?)</is)
			{
			$Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
			$Deadline_time=$4;
			$dd_des=&clean($5);
			$Deadline_Description="Closing Date(".$dd_des."):";
			# print $Deadline_Description;
			# <>;
			}
		if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}\s*(?:AM|PM))\s*([^>]*?)</is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		$Deadline_time=$4;
		$dd_des=&clean($5);
		# print $dd_des;
		$Deadline_Description="Closing (".$dd_des."):";
		# print $Deadline_Description;
		# <>;
		}
		
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($Deadline_time=~m/(?:(\d{1,2}):(\d{2})\s*(?:am|AM)|(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:am|AM))/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		}
		if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		{
		$temp_dt=$1;
		if($temp_dt=~m/(\d{4})\/(\d{1,2})\/(\d{1,2})\s*(\d{1,2}\:\d{1,2}\:\d{1,2}\s*(?:AM|PM))\s*([^>]*?)</is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		$Deadline_time=$4;
		$dd_des=&clean($5);
		$Deadline_Description="Closing (".$dd_des."):";
		}
		if($temp_dt=~m/(\w+)\s+(\d{1,2})\s*\,\s*(\d{4})\s*,\s*(\d{1,2}\:\d{1,2})\s*([^>]*?)</is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/".$3);
		$Deadline_time=$4;
		$dd_des=&clean($5);
		$Deadline_Description="Closing (".$dd_des."):";
		}
		$Deadline_Description="Closing (".$dd_des."):";
			# print $Deadline_Description;
			# <>;
		
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		}
		
		$Deadline_time=~s/am//igs;
		$Deadline_time=~s/AM//igs;
		
		$Deadline_Description=&clean($Deadline_Description);
		$Deadline_Description=&BIP_Tender_DB::clean($Deadline_Description);
		print $tender_link;
		
		
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
		if($tender_link_content1=~m/<h1[^>]*?>([\w\W]*?)Back"\s*><\/td>/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	
	elsif($tender_link_content1=~m/<h3 class="content-block-title">\s*Basic Information<\/h3>([\w\W]*?)<a id="descriptionTextReadMore"/is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='CA';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
# exit;
# }
}
# my $search_profile;
# if($tender_category_content1=~m/<input\s*type\s*=\s*"hidden"\s*name="search_profile"\s*value="([^>]*?)"\s*>/is)
# {
	# $search_profile=uri_escape($1);
	# print "$search_profile\n";

# }
# $count1=$count1+1;
if($tender_link_content1=~m/<a data-page-number="[^>]*?" rel='next' data-page-size="25" href="([^>]*?)" title="Go to Next Page " class="next">/is)
{
# my $Tender_Url="http://www.merx.com/English/SUPPLIER_Menu.asp?WCE=ButtonClick&TAB=1&PORTAL=MERX&State=2&revision=&hcode=D6wWmddGRBZUCi03zMrw7w%3d%3d";
		# my $Tender_Url2="https://www.merx.com/English/SUPPLIER_Menu.asp?WCE=Show&TAB=1&PORTAL=MERX&State=2&ACTION=NEXT&rowcount=2000&lastpage=20&FED_ONLY=0&MoreResults=&PUBSORT=2&CLOSESORT=0&hcode=z5IzyYdgZlPtIuu48rSWtA%3d%3d";
		my $Tender_Url2="https://www.merx.com".$1;
		# print $Tender_Url2;
		# <>;

# my $referer="http://www.merx.com/English/SUPPLIER_Menu.Asp?WCE=GOTO&GID=CATSEARCH&CAT=C&TAB=1&hcode=vQu0iaYTsTnvaFSHykRXgQ%3d%3d";
# my $referer="http://www.merx.com/English/SUPPLIER_Menu.asp?WCE=ButtonClick&TAB=1&PORTAL=MERX&State=2&revision=&hcode=D6wWmddGRBZUCi03zMrw7w%3d%3d";


# my $post_content="search_profile=".$search_profile."&KeywordValue=Search...&ospQuickSearch=&SEARCH_BY_CONTEXT=OO&txt_maxPerPage=100&SEARCH_SINCE=ALL";
# my $post_content="search_profile=eNplkUEOwjAMBP%2FDJXC3coEHIOADJnVpRFpHdgLq70lLaaFc7N3xypITYI0WTqQ5JD2SHPFGdrcFs0KwzyLUpfcczLeFM6G45oAJr6jjeEWmxKWPZNW3MdAnMiJw4hOJRwt1shswpYJZYEup1Dv1T5ZKh8Csoc2aGnyUJWaRg%2Br4m89OOXhX4NSF6o6LnbpyFke%2BGgMfyTG2mFwThWsfho1%2FJHLMAUXHm0pg5a%2B5J7kJ5%2FLW5se8TzPDL7wAEXuSkQ%3D%3D&KeywordValue=Search...&ospQuickSearch=&SEARCH_BY_CONTEXT=OO&txt_maxPerPage=100&SEARCH_SINCE=ALL";
# my $post_content="search_profile=eNplkUEOwjAMBP%2FDJXC3coEHIOADJnVpRFpHdgLq70lLaaFc7N3JypIdYI0WTqQ5JD2SHPFGdrfdglkx2GcR6tI7AObbwplQXHPAhFfU8XlFpsSlj2TVtzHQJzIicOITiUcLdbIbMKWCWWBLqdQ79U%2BWSofArKHNmhp8lCFmkYPq%2BJvPTjl4V%2BDUheqOi526chZHvhoDH8kxtphcE4VrH4aJfyRyzAFFx51KYOWvuSe5CedybPNj3quZ4RteX6mSwQ%3D%3D&KeywordValue=Search...&ospQuickSearch=Search...&SEARCH_BY_CONTEXT=OO&txt_maxPerPage=100&SEARCH_SINCE=ALL";
my ($Tender_content3)=&Getcontent($Tender_Url2);
$Tender_content3 = decode_utf8($Tender_content3);
goto top1;
}
}
# }
# }
# $count1=$count+1;
# if($tender_category_content1=~m/onkeypress="NavigatePage\(\'([^>]*?)\'\)\;"><span class="NavLinkStyleLink">2<\/span>/is)
# {
# my $Tender_Url="http://www.merx.com/English/SUPPLIER_Menu.asp?WCE=Show&TAB=1&PORTAL=MERX&State=2&searchtype=remotesearch&ACTION=PAGE2&rowcount=3088&lastpage=309&FED_ONLY=0&MoreResults=&PUBSORT=2&CLOSESORT=0&hcode=4FIhNpo0U2DUQHTx%2b8rPSA%3d%3d";
# my $referer="http://www.merx.com/English/SUPPLIER_Menu.asp?WCE=GOTO&GID=REMOTESEARCH&TAB=1&PORTAL=MERX&hcode=xu%2b3MhJeX2npe2sJVPMRvQ%3d%3d";
# my $referer="http://www.merx.com/English/SUPPLIER_Menu.asp?WCE=ButtonClick&TAB=1&PORTAL=MERX&State=2&revision=&hcode=D6wWmddGRBZUCi03zMrw7w%3d%3d";

# my $post_content="search_profile=eNpdkUtuwzAMRK9SZNmN2j2jTXuAoO0FGJmOhdimQFIJfPvKsvPzhpx5HAgkBKzJww9p7k0PJAc8kf%2F8ALdB8JVFaLRlDu7Zwi%2BhhO4bDY%2Bodbwha%2BJvSuSFBjbSCm7BOoAg0UgieuD0ZgXtdzg2Ow%2Bt%2BXdwpYLjsq17BAeyUs80XVkanVN3DUNW6%2FBSHnYPOauRn%2FndKfcxFLh2oXbkYteunCVQbGrgJjmlAS10SbiNPdX9NiRxyj3Kcm8JbPwxTyQn4Tzf9WKW09z8P%2F%2BvopuA&KeywordValue=Search...&ospQuickSearch=Search...&SEARCH_BY_CONTEXT=OO&txt_maxPerPage=10&SEARCH_SINCE=ALL";
# my $post_content="search_profile=eNplkUEOwjAMBP%2FDJXC3coEHIOADJnVpRFpHdgLq70lLaaFc7N3xypITYI0WTqQ5JD2SHPFGdrcFs0KwzyLUpfcczLeFM6G45oAJr6jjeEWmxKWPZNW3MdAnMiJw4hOJRwt1shswpYJZYEup1Dv1T5ZKh8Csoc2aGnyUJWaRg%2Br4m89OOXhX4NSF6o6LnbpyFke%2BGgMfyTG2mFwThWsfho1%2FJHLMAUXHm0pg5a%2B5J7kJ5%2FLW5se8TzPDL7wAEXuSkQ%3D%3D&KeywordValue=Search...&ospQuickSearch=&SEARCH_BY_CONTEXT=OO&txt_maxPerPage=10&SEARCH_SINCE=ALL";
# my ($Tender_content3)=&Postcontent($Tender_Url,$post_content);
# $Tender_content3 = decode_utf8($Tender_content3);
# $tender_category_content1=$Tender_content3;
# my ($Tender_content3)=&Postcontent($Tender_Url,$post_content);
# $Tender_content3 = decode_utf8($Tender_content3);
# goto top1;
# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.merx.com");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.merx4.merx.com");
	# $req->header("Referer"=> "http://das.ct.gov/cr1.aspx?page=12");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.merx.com");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded");

	$req->header("Referer"=> "https://www.merx.com/English/SUPPLIER_Menu.asp?WCE=ButtonClick&TAB=1&PORTAL=MERX&State=2&revision=&hcode=D6wWmddGRBZUCi03zMrw7w%3d%3d");
	
	# $req->header("Referer"=>$Referer);
	$req->header("Cookie"=> "LangID=1; _ga=GA1.2.128783164.1517296484; _gid=GA1.2.317791283.1517296484; visitor_id24752=165506802; visitor_id24752-hash=3c5cf836cddd9812199bbf4cee6391e7bc2c73f3ffdf736ad4d5caa582b707aa3a71f4370013786aef7778b47f184b5e04591378; UP_COOKIE=V3dNRE4ySzQ3cHNIQ2plN1REVkYxd01FYmdEOHBUclVNZnVxRFo5Rno0Y08xWUQzd240UUZrOThDTEdYTWFZd29ZMHAxUzlqT0JhbwpqVUdLUnN3d3Rsa2hSYXFLWHA5T01LR3M0Z0VyTW1Ca3VIS1RMN2lUOHRKK3lFTWQ3WU8xZE1RSjNaZldKcVRjeDNyaE1jY1g3UT09; __utma=1.128783164.1517296484.1517297077.1517297077.1; __utmz=1.1517297077.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); SERVERID=aa236c21609d08f72c8ff716bfedd162; ASPSESSIONIDCSTQBSAT=JOIDAHDAIGKDEGHDLMPPJMBN; IIS_SESSION=.merx3");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	decode_entities($Clean);
	return $Clean;
}