#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="https://procontract.due-north.com/Opportunities/Index?resetFilter=True&applyFilter=True&p=527b4bbd-5c58-e511-80ef-000c29c9ba21&v=1";

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;
my @nextnumbers;
#### To get the tender link ####
top:
while ($Tender_content1=~m/<tr[^>]*?gridrow[^>]*?>([\w\W]*?)<\/tr>/igs)
{
	my $block = $1;
	if ($block=~m/href\=\"([^>]*?)\"/is)	
	{
		my $tender_link = &Urlcheck($1);
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 
		my ($tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date);
		
		if ($Tender_content=~m/From\s*<\/span>[\w\W]*?>\s*to\s*<\/span>([^>]*?)\s([^>]*?)</is)
		{
			$Deadline_Date = $1;
			$Deadline_time = $2;
		}
		if ($Tender_content=~m/<h1>\s*([^>]*?)\s*<\/h1>/is)
		{
			$tender_title = &clean($1);
		}
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/Categories<\/div>\s*<div class="cell400">\s*<div\s*>([^>]*?)<\/div>/is)
		{
			$Sector = &clean($1);
		}
		my ($Buyer,$Contact,$Email,$Telephone,$Address);
		if ($Tender_content=~m/>\s*Buyer\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Buyer = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Contact)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $contact_keyword = $1;
			$Contact = &clean($2);
			$Contact=$contact_keyword.":".$Contact;
		}
		if ($Tender_content=~m/>\s*(Email)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $Email_keyword = $1;
			$Email = &clean($2);
			$Email=$Email_keyword.":".$Email;
		}
		if ($Tender_content=~m/>\s*(Telephone)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $Telephone_keyword = $1;
			$Telephone = &clean($2);
			$Telephone=$Telephone_keyword.":".$Telephone;
		}
		if ($Tender_content=~m/>\s*Address\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Address = &clean($1);
		}
		$Awarding_Authority=$Buyer.",".$Contact.",".$Email.",".$Telephone.",".$Address;
		$Awarding_Authority=~s/^\s*,|,\s*$//igs;
		if ($Tender_content=~m/>\s*Description\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Description = &BIP_Tender_DB::clean($1);
		}
		if ($Tender_content=~m/>\s*Region\(s\)\s*of\s*supply\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Location = clean($1);
		}
		if ($Tender_content=~m/>\s*Opportunity\s*Id\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Authority_Reference = clean($1);
		}
		if ($Tender_content=~m/>\s*Estimated\s*value\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Value = clean($1);
		}
		if ($Tender_content=~m/<h2>(Expression\s*of\s*interest\s*window)<\/h2>/is)
		{
			$Deadline_Description = $1;
			$Deadline_Description='Expression of Interest End Date';
		}
		if ($Tender_content=~m/Main\s*contract\s*details<\/h2>([\w\W]*?)<\/main>/is)
		{
			$tenderblock = $1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		$Sector='UK Other';
		my $tender_type="Normal";
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time);
	}	
}

if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>\s*Next/is)
{
	my $nextpage = &Urlcheck($1);
	
	my $nextnumber =$1 if ($nextpage=~m/Page\=([\d]+)\&/is);
	exit if ($nextnumber ~~ @nextnumbers);
	push (@nextnumbers,$nextnumber);
	($Tender_content1)=&Getcontent($nextpage);
	$Tender_content1 = decode_utf8($Tender_content1); 
	open(ts,">Tender_contentNext17.html");
	print ts "$Tender_content1";
	close ts;
	print "NextPAge :: $nextpage\n";
	goto top;
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	decode_entities($Clean);
	return $Clean;
}