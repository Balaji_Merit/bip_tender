#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

open (RE,">RESULT.html");
print RE "Tender_ID\tTenderName\tTender_Url\ttender_link\ttender_title\tLocation\ttenderblock\tdeadline_description\tdeadline_date\n";
close RE;

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content)=&Getcontent($Tender_Url);
$Tender_content = decode_utf8($Tender_content);  
open(ts,">Tender_content_BIP1.txt");
print ts "$Tender_content";
close ts;

#### To get the tender link ####
if ($Tender_content=~m/<div[^>]*?view\-content\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
{
	my $tender_block = $1;
	while ($tender_block=~m/<td[^>]*?title\s*\"\s*>\s*<a[^>]*?href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<([\w\W]*?)<\/tr>/igs)
	{
		my $tender_link = &Urlcheck($1);
		my $tender_title= ($2);
		my $block		= $3;
		my $tender_title= &BIP_Tender_DB::clean($tender_title);

		
		my $Location = $1 if($block=~m/region\"\s*>\s*([^>]*?)\s*</is);
		
		my ($Final_content)=&Getcontent($tender_link);
		$Final_content = decode_utf8($Final_content); 
		# open(ts,">Final_content.html");
		# print ts "$Final_content";
		# close ts;
		my $tenderblock;
		
		if($Final_content=~m/node\-content\">([\w\W]*?)<\/div>\s*<\/div>\s*(?:<\/section>)\s*<\/div>/is)
		{
			$tenderblock = $1 
		}
		elsif ($Final_content=~m/<div[^>]*?description\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
		{
			$tenderblock = $1 
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		my ($Deadline_Description,$Deadline_Date);
		if ($Final_content=~m/(Expiry\s*date)\:([\w\W]*?)<\/div>/is)
		{
			$Deadline_Description = $1;
			$Deadline_Date = &BIP_Tender_DB::clean($2);
		}
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my ($Origin,$Sector,$Country,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time);
		
		open (RE,">>RESULT.txt");
		print RE "$Tender_ID\t$TenderName\t$Tender_Url\t$tender_link\t$tender_title\t$Location\t$tenderblock\t$Deadline_Description\t$Deadline_Date\n";
		close RE;
		# my $Checked_Date=DateTime->now();
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time);
		# exit;
	}
}	


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

# sub clean()
# {
	# my $Clean =shift;
	# $Clean=~ s/<br>/ /igs;
	# $Clean=~s/<[^>]*?>//igs;
	# $Clean=~s/^\s*|\s*$//igs;
	# $Clean=~s/\s\s+/ /igs;
	# $Clean=~s/\&nbsp\;/ /igs;
	# $Clean=~s/\&amp\;/&/igs;
	# $Clean=~s/[^[:print:]]+//igs;
	# $Clean=~s/^\s*|\s*$//igs;
	# decode_entities($Clean);
	# return $Clean;
# }