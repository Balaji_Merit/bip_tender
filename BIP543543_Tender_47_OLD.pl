#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Date::Parse;
use POSIX qw(strftime);
use DateTime::Format::Strptime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

my $fulldate = strftime "%d %m %Y %A", localtime;
my $day = (split(" ",$fulldate))[-1];
print "Day :: $day\n";

my ($today1,$today3) = &DAY_Check($day);
print "Inputs :: $today1<=>$today3\n";
my $today2 = $today1;
my $today2 =~s/\-/%2F/igs;
my $today4 = $today3;
my $today4 =~s/\-/%2C/igs;


sub DAY_Check()
{
	my $DAYY = shift;
	
	my $today = time;
	print "Today::$today\n";
	
	if ($day=~m/Wednesday|Thursday|Friday|Saturday/is)
	{
		my $yesterday = $today - 60 * 48 * 60;
		my $today1 = strftime "%d-%m-%Y", ( localtime($yesterday) );
		my $today3 = strftime "%Y-%m-%d", ( localtime($yesterday) );
		print "$today1 = $today3\n";
		return ($today1,$today3);
	}
	if ($day=~m/Sunday|Monday|Tuesday/is)
	{
		my $yesterday = $today - 60 * 72 * 60;
		my $today1 = strftime "%d-%m-%Y", ( localtime($yesterday) );
		my $today3 = strftime "%Y-%m-%d", ( localtime($yesterday) );
		print "$today1 = $today3\n";
		return ($today1,$today3);
	}
}
# exit;


### Ping the first level link page ####
my $Tender_Url='https://www.sell2wales.gov.wales/search/Search_MainPageAdv.aspx';
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;



if ($Tender_content1=~m/>\s*Notice\s*Type\s*(?:\:)?([\w\W]*?)<\/select>/is)
{
	my $Notice_type_block = $1;
	while ($Notice_type_block=~m/<option\s*value\=\"([\d]+)\"[^>]*?>\s*([^>]*?)\s*</igs)
	{
		my $DocType_code = $1;
		my $DocType		 = $2;
		
		next if ($DocType=~m/Contract\s*Award\s*Notices/is);
		
		my ($VIEWSTATE1,$VIEWSTATEGENERATOR1,$EVENTVALIDATION1,$After_calendar_AD1)=&post_parameters($Tender_content1);
		my $urlll = 'https://www.sell2wales.gov.wales/search/Search_MainPageAdv.aspx';
		my $host='www.sell2wales.gov.wales';
		# my $post_content ='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE1.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR1.'&__EVENTVALIDATION='.$EVENTVALIDATION1.'&ctl00%24ContentPlaceHolder1%24txtKeyword=&ctl00%24ContentPlaceHolder1%24txtAuthName=&ctl00%24ContentPlaceHolder1%24cboLocation=1000&ctl00%24ContentPlaceHolder1%24cboDocType=0&ctl00%24ContentPlaceHolder1%24rdAfter='.$today3.'&ctl00%24ContentPlaceHolder1%24rdAfter%24dateInput='.$today2.'&ctl00_ContentPlaceHolder1_rdAfter_dateInput_ClientState=%7B%22enabled%22%3Atrue%2C%22emptyMessage%22%3A%22%22%2C%22validationText%22%3A%22'.$today3.'-00-00-00%22%2C%22valueAsString%22%3A%22'.$today3.'-00-00-00%22%2C%22minDateStr%22%3A%221980-01-01-00-00-00%22%2C%22maxDateStr%22%3A%222099-12-31-00-00-00%22%2C%22lastSetTextBoxValue%22%3A%2222%2F03%2F2016%22%7D&ctl00_ContentPlaceHolder1_rdAfter_calendar_SD=%5B%5B2016%2C3%2C22%5D%5D&ctl00_ContentPlaceHolder1_rdAfter_calendar_AD='.$After_calendar_AD1.'&ctl00_ContentPlaceHolder1_rdAfter_ClientState=&ctl00%24ContentPlaceHolder1%24rdBefore=&ctl00%24ContentPlaceHolder1%24rdBefore%24dateInput=&ctl00_ContentPlaceHolder1_rdBefore_dateInput_ClientState=%7B%22enabled%22%3Atrue%2C%22emptyMessage%22%3A%22%22%2C%22validationText%22%3A%22%22%2C%22valueAsString%22%3A%22%22%2C%22minDateStr%22%3A%221980-01-01-00-00-00%22%2C%22maxDateStr%22%3A%222099-12-31-00-00-00%22%2C%22lastSetTextBoxValue%22%3A%22%22%7D&ctl00_ContentPlaceHolder1_rdBefore_calendar_SD=%5B%5D&ctl00_ContentPlaceHolder1_rdBefore_calendar_AD='.$After_calendar_AD1.'&ctl00_ContentPlaceHolder1_rdBefore_ClientState=&ctl00%24ContentPlaceHolder1%24cmdSearch=Search';
		my $post_content ='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE1.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR1.'&__EVENTVALIDATION='.$EVENTVALIDATION1.'&ctl00%24ContentPlaceHolder1%24txtKeyword=&ctl00%24ContentPlaceHolder1%24txtAuthName=&ctl00%24ContentPlaceHolder1%24cboLocation=1000&ctl00%24ContentPlaceHolder1%24cboDocType='.$DocType_code.'&ctl00%24ContentPlaceHolder1%24rdAfter='.$today3.'&ctl00%24ContentPlaceHolder1%24rdAfter%24dateInput='.$today1.'&ctl00_ContentPlaceHolder1_rdAfter_dateInput_ClientState=%7B%22enabled%22%3Atrue%2C%22emptyMessage%22%3A%22%22%2C%22validationText%22%3A%22%22%2C%22valueAsString%22%3A%22%22%2C%22minDateStr%22%3A%221980-01-01-00-00-00%22%2C%22maxDateStr%22%3A%222099-12-31-00-00-00%22%2C%22lastSetTextBoxValue%22%3A%22%22%7D&ctl00_ContentPlaceHolder1_rdAfter_calendar_SD=%5B%5D&ctl00_ContentPlaceHolder1_rdAfter_calendar_AD='.$After_calendar_AD1.'&ctl00_ContentPlaceHolder1_rdAfter_ClientState=&ctl00%24ContentPlaceHolder1%24rdBefore=&ctl00%24ContentPlaceHolder1%24rdBefore%24dateInput=&ctl00_ContentPlaceHolder1_rdBefore_dateInput_ClientState=%7B%22enabled%22%3Atrue%2C%22emptyMessage%22%3A%22%22%2C%22validationText%22%3A%22%22%2C%22valueAsString%22%3A%22%22%2C%22minDateStr%22%3A%221980-01-01-00-00-00%22%2C%22maxDateStr%22%3A%222099-12-31-00-00-00%22%2C%22lastSetTextBoxValue%22%3A%22%22%7D&ctl00_ContentPlaceHolder1_rdBefore_calendar_SD=%5B%5D&ctl00_ContentPlaceHolder1_rdBefore_calendar_AD='.$After_calendar_AD1.'&ctl00_ContentPlaceHolder1_rdBefore_ClientState=&ctl00%24ContentPlaceHolder1%24cmdSearch=Search';
		my $Tender_content2=&Postcontent($urlll,$post_content,$host,$urlll);
		$Tender_content2 = decode_utf8($Tender_content2);
		open (RE,">Tender_content47.html");
		print RE "$Tender_content2";
		close RE;
		# exit;


		#### To get the tender link ####
		top:
		my $flag_check;
		while ($Tender_content2=~m/<td>\s*<a[^>]*?href\=\"([^>]*?)\"/igs)
		{
			my $tender_link = &Urlcheck($1);
			# print "TenderLink :: $tender_link\n";
			# next;
			
			my ($Tender_content)=&Getcontent($tender_link);
			$Tender_content = decode_utf8($Tender_content); 
			
			my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date);
			if ($Tender_content=~m/>\s*Title(?:\:)?([\w\W]*?)<\/tr>/is)
			{
				$tender_title 		 = &clean($1);
			}
			print "Title :: $tender_title\n";
			if ($Tender_content=~m/>\s*(Deadline\s*Date)\s*(?:\:)?([\w\W]*?)<\/tr>/is)
			{
				$Deadline_Description = &clean($1);
				$Deadline_Date	= &clean($2);
			}
			if ($Tender_content=~m/>\s*Deadline\s*Time\s*(?:\:)?([\w\W]*?)<\/tr>/is)
			{
				$Deadline_time = &clean($1);
			}
			#Duplicate check
			my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
			if ($duplicate eq 'Y')
			{
				print "\nAlready Existing this Tender Data\n";
				$flag_check = 'Y';
				next;
			}
			else
			{
				print "\nThis is newly Released Tender\n";
			}
			my ($official_name,$address1,$address2,$Contact,$Telephone,$Mail,$Fax);
			if ($Tender_content=~m/>\s*(Official\s*Name\s*(?:\:)?[\w\W]*?)<\/td>/is)
			{
				$official_name = &clean($1);
			}
			if ($Tender_content=~m/>\s*Postal\s*Address\s*(?:\:)?([\w\W]*?)<\/tr>([\w\W]*?)<\/tr>/is)
			{
				$address1 = &clean($1);
				$address2 = &clean($2);
			}
			if ($Tender_content=~m/>\s*(For\s*the\s*attention\s*of[\w\W]*?)<\/td>/is)
			{
				$Contact = &clean($1);
			}
			if ($Tender_content=~m/>\s*(Telephone[\w\W]*?)(?:<\/p>|<\/td>)/is)
			{
				$Telephone = &clean($1);
			}
			if ($Tender_content=~m/>\s*(E-Mail[\w\W]*?)(?:<\/p>|<\/td>)/is)
			{
				$Mail = &clean($1);
			}
			if ($Tender_content=~m/>\s*(Fax[\w\W]*?)(?:<\/p>|<\/td>)/is)
			{
				$Fax = &clean($1);
			}
			$Awarding_Authority=$official_name.", ".$address1.", ".$address2.", ".$Contact.", ".$Telephone.", ".$Mail.", ".$Fax;
			$Awarding_Authority=&clean($Awarding_Authority);
			if ($Tender_content=~m/>\s*Notice\s*Type\s*(?:\:)?([\w\W]*?)<\/tr>/is)
			{
				$Contract_Type = &clean($1);
			}
			if ($Tender_content=~m/>\s*Abstract*(?:\:)?([\w\W]*?)<\/tr>/is)
			{
				$Description = &clean($1);
			}
			if ($Tender_content=~m/>\s*Additional\s*Information<\/h3>([\w\W]*?)<\/tr>/is)
			{
				my $Authority_Reference1 = ($1);
				$Authority_Reference = $1 if ($Authority_Reference1=~m/>\s*\(\s*([^>]*?)\s*\)\s*</is);
				# $Authority_Reference=~s/\s*\(\s*|\s*\)\s*//igs;
			}
			my $Nuts_Code;
			if ($Tender_content=~m/<span>NUTS\s*Code([\w\W]*?)<\/td>/is)
			{
				$Nuts_Code = &clean($1);
			}
			
			my $tenderblock;
			if ($Tender_content=~m/Title\s*\:\s*<\/strong>([\w\W]*?)<h2>\s*Further\s*Instructions/is)
			{
				$tenderblock = $1;
			}
			elsif ($Tender_content=~m/Title\s*\:\s*<\/strong>([\w\W]*?)<\/table>\s*<\/div>\s*<\/div>\s*<\/div>/is)
			{
				$tenderblock = $1;
			}
			$tenderblock = &BIP_Tender_DB::clean($tenderblock);

			&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$Nuts_Code);
		}
		# if ($flag_check eq 'Y')
		# {
			# next;
		# }
		if ($Tender_content2=~m/selected\"\s*value\=\"[\d]+\">([^>]*?)<\/option>\s*<option[^>]*?>([^>]*?)</is)
		{
			my $current_page = $1;
			my $next_page	 = $2;
			print "\nNextpage :: $next_page\n";
			my ($VIEWSTATE2,$VIEWSTATEGENERATOR2,$EVENTVALIDATION2)=&post_parameters($Tender_content2);
			my $urlll = 'https://www.sell2wales.gov.wales/search/Search_MainPageAdvResults.aspx';
			my $host='www.sell2wales.gov.wales';
			my $post_content ='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE2.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR2.'&__EVENTVALIDATION='.$EVENTVALIDATION2.'&ctl00%24ContentPlaceHolder1%24NavBar1%24drpPageNo='.$current_page.'&ctl00%24ContentPlaceHolder1%24NavBar2%24drpPageNo='.$current_page.'&ctl00%24ContentPlaceHolder1%24NavBar2%24cmdNext.x=10&ctl00%24ContentPlaceHolder1%24NavBar2%24cmdNext.y=12';
			$Tender_content2=&Postcontent($urlll,$post_content,$host,$urlll);
			open (RE,">Nextpage_47.html");
			print RE "$Tender_content2";
			close RE;
			goto top;	
		}
	}	
}	
	
sub post_parameters()
{
	my $contentsdfs = shift;
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
	if($contentsdfs=~m/__VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($contentsdfs=~m/__VIEWSTATEGENERATOR\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($contentsdfs=~m/__EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	if($contentsdfs=~m/After_calendar_AD\"\s*value\=\"([^>]*?)\"/is)
	{
		$After_calendar_AD=uri_escape($1);
	}
	print "After_calendar_AD :: $After_calendar_AD\n";
	return ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$After_calendar_AD);
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.sell2wales.gov.uk");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/,/igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\,/:/igs;
	$Clean=~ s/\.\,/./igs;
	$Clean=~s/\,\s*/,/igs;
	$Clean=~s/^\s*\,+//igs;
	$Clean=~s/\,+\s*$//igs;
	decode_entities($Clean);
	return $Clean;
}