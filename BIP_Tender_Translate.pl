#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use POSIX 'strftime';
use MIME::Lite;
use MIME::Base64;
use Config::IniFiles;
use Text::CSV;
use Email::Send::SMTP::Gmail;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use JSON::Parse 'parse_json';
use File::Copy;


	# my $var1=`"C:\\Python 3.6\\python.exe" check.py`;
	my $MAILDETAILS = &ReadIniFile('MAILDETAILS');
	my %MAILDETAILS	= %{$MAILDETAILS};
	my $Filename1="C:/BIP/Temp/test_26.xlsx";
	
	print $Filename1;
	# exit;
	my $FILEPATH 	= &ReadIniFile('FILEPATH');
	my $FILEPATH 	= &ReadIniFile('FILEPATH');
	my %FILEPATH	= %{$FILEPATH};
	my $fulldate = strftime "%b %d, %Y %A %H:%M:%S", localtime;
	my $fulldate1 = strftime "%d-%m-%Y", localtime;
	my $Export_Path = "C:/BIP/Temp/";
	my $Filename2="BIP_Non_UK_Translation_$fulldate1.xls";
	print "PAth :: $Export_Path\n";
	rename($Filename1,$Filename2);
	# print "$Filename2";
	# exit;
	my $Filepath1 = "C:/BIP/Temp/$Filename2";
	print $Filepath1;
	my $subject	= "BiP Solutions_Worldwide Translation Sites Scraper Output_$fulldate";
	my $host   	= $MAILDETAILS{'host'}; 
	my $from 	= $MAILDETAILS{'from'};
	my $user 	= $MAILDETAILS{'user'};
	my $to		= $MAILDETAILS{'to_nonuk_trans'};
	my $cc		= $MAILDETAILS{'cc_nonuk_trans'};
	my $pass	= $MAILDETAILS{'password'};
	my $body	= "Greetings from Software support..!<br><br>Please find the Exported file for BIP Non-UK Translation Tenders.<br><br>Note : This is an automatically generated email, please don't reply.<br><br>Have a nice day..!<br><br>"."Thanks &amp; Regards,<br>Merit Software Support.";
	print $body;
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	 
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
        Type     =>'Excel',
        Path     =>"$Filepath1",
        Filename =>"$Filename2"
    );
	
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	
	eval {$msg->send;};
	if($@)
	{
		print "\n\n>>>>>>>>Mail Failed>>>>>>>>\n\n";
		
	}
	else
	{
		print "\n\n>>>>>>>>Mail Sent>>>>>>>>\n\n";
	}
	my $directory="C:/BIP/Temp/";
my $path="//ch1021sf01/BIPSpendAnalysis/2021/Tender_outputs/";
# my $file="REsult_InTendHost_20_11_2018_Tuesday_18_35_00.csv";
 my $source = "$Filepath1";
 my $target = "$path/$Filename2";
copy $source => $target or warn "Copy of $Filename2 failed: $!";
	sub ReadIniFile()
{
	my $INIVALUE = shift;
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Tender.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}