#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url ="https://www.amcham.org.eg/api/tas/get-tenders?isLandingPage=true&pageCount=50&pageNumber=1";
$Tender_Url ="https://www.amcham.org.eg/tas/";

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my $url="https://www.amcham.org.eg/tas/search?typeID=999";
my ($Tender_content2)=&Getcontent($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
open(ts,">Tender_content507.html");
print ts "$Tender_content2";
close ts;
# my $url="https://www.amcham.org.eg/tas/search?typeID=999&";
my ($Tender_content1)=&Getcontent($url);
$Tender_content1 = decode_utf8($Tender_content1);  
my $count=1;
$Tender_content1='{"result":{"searchResult":[{"tenderID":155065,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 300,000","deadlineDate":"2019-11-11T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"TXJ4NGpobXNxUmg2RnlOWjBuRUlNQT09","winnerCompanies":"","title":"Supply 1,500 tons each of rice of grade 1 (of both thin grams & wide gains) of more than 5 % track percentage. Accompanies with 1 Kg sample each = machine Egyptian standard specs","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Cairo","locationID":0,"sectorID":57,"sectorTitle":"Agriculture & Food","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155060,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 140,000 & 230,000","deadlineDate":"2019-11-13T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":"61,","beneficialSectorsIDsList":[61],"beneficialSectors":"Industry,","beneficialSectorsList":["Industry"],"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"MlRLZ2drOWd2TzI3WG1vSDVFdkQ4QT09","winnerCompanies":"","title":"Rehabilitation/ replacement of sanitary drainage facilities at Markaz Ismailiiya & (b) Ismailiyya second district under 2 contracts","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Ismailiya","locationID":0,"sectorID":58,"sectorTitle":"Water & Waste-Water Equipment & Works","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155064,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 34,000","deadlineDate":"2019-11-17T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"OTkzcEluOW55L1J1ZlJyQmI1RkZMUT09","winnerCompanies":"","title":"Supply of medicines","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Cairo","locationID":0,"sectorID":131,"sectorTitle":"Medical - Pharmaceuticals & Laboratory","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155071,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 90,000","deadlineDate":"2019-11-17T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"c2poS09DeTk2ZkN4VlZ6N2NJSU9nZz09","winnerCompanies":"","title":"Paving of some streets at Mamsoura West District","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Dakahliya","locationID":0,"sectorID":56,"sectorTitle":"Construction Projects","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155069,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 200,000","deadlineDate":"2019-11-18T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"UmtWUTdYSTZGWjRjRm9nNDV3RkNNQT09","winnerCompanies":"","title":"Supply of a Quadruple Mass Spectrometer instrument","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Cairo","locationID":0,"sectorID":131,"sectorTitle":"Medical - Pharmaceuticals & Laboratory","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155068,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 10,000 & 60,000","deadlineDate":"2019-11-20T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":"64,","beneficialSectorsIDsList":[64],"beneficialSectors":"Information & Communications Technology,","beneficialSectorsList":["Information & Communications Technology"],"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"c0xIR0htU0dYT2hUcVp5SnErK0Y4QT09","winnerCompanies":"","title":"Supply & erection of (a) glass sheets for the Conferences Hall, also (b) supply & testing of a UPS unit","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Alexandria","locationID":0,"sectorID":61,"sectorTitle":"Industry","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155066,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"N/A","deadlineDate":"2019-11-21T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":"54,","beneficialSectorsIDsList":[54],"beneficialSectors":"Electromechanical Works,","beneficialSectorsList":["Electromechanical Works"],"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"UW92RWtpeUQ5RThhbmdWSzJ5Z0xHZz09","winnerCompanies":"","title":"Request registration of local suppliers, of contractors for construction & building of electrical works, also registration of consultants for various other works","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Giza","locationID":0,"sectorID":56,"sectorTitle":"Construction Projects","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155062,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 50,000","deadlineDate":"2019-11-26T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"cWdtOWp2L0ZPQzdvbzExaTUxSjQ4QT09","winnerCompanies":"","title":"Asphalt paving of roads within Markaz & City of Kafr Saad for completion in 4 months","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Damietta","locationID":0,"sectorID":132,"sectorTitle":"Construction Equipment","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155067,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 680,000 & 39,000","deadlineDate":"2019-11-28T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":"61, 64,","beneficialSectorsIDsList":[61,64],"beneficialSectors":"Industry, Information & Communications Technology,","beneficialSectorsList":["Industry"," Information & Communications Technology"],"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"OHJoZHI3TFJ6ZmVrYVlWRFlkTTArUT09","winnerCompanies":"","title":"Supply of (a) cylindrical shaped fenders for the protection of Alexandria & Dekheila sea ports, (b) maintenance & repair of UPS units including related spare parts","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Alexandria","locationID":0,"sectorID":61,"sectorTitle":"Industry","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155070,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"LE 35,000","deadlineDate":"2019-11-28T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":"54,","beneficialSectorsIDsList":[54],"beneficialSectors":"Electromechanical Works,","beneficialSectorsList":["Electromechanical Works"],"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"OXE0dEVHdDRTa3k2c2E3cHlOWEVPdz09","winnerCompanies":"","title":"Rehabilitation of the cables & switchboards serving Benha railways station","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Kalioubiya","locationID":0,"sectorID":59,"sectorTitle":"Energy","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null},{"tenderID":155063,"subSectorID":0,"tenderCurrency":"LE","tenderCurrencyID":0,"bidBond":"N/A","deadlineDate":"2019-12-16T00:00:00","originalDeadlineDate":null,"oldTenderID":null,"newTenderID":null,"beneficialSectorsIDs":null,"beneficialSectorsIDsList":null,"beneficialSectors":null,"beneficialSectorsList":null,"source":null,"sourceID":0,"isBookmarked":false,"companyName":null,"issuedBy":null,"companyAddress":null,"companyPhone":null,"creationDate":null,"specsFees":null,"description":null,"localInternational":"Local","typeName":null,"financedBy":null,"deadlineDateAll":null,"performanceBond":null,"gSubSectorTitle":null,"gSectorTitle":null,"companyFax":null,"companyEmail":null,"companyNameArabic":null,"companyDivision":null,"tenderType":0,"tenderFinancing":0,"companyID":0,"area":null,"notes":null,"beneficiallySubSectorsIDs":null,"mainBeneficiallySubSectorID":0,"generatedSectorID":0,"generatedSubSectorID":0,"encryptedTenderID":"NzFtVkpGYUFCWW5veWRIV2RBb3BJUT09","winnerCompanies":"","title":"Completion of the revamping of 50 railways crossing. Deadlines to now become 2/162019. <","status":"Active","publishedDate":"2019-11-05T00:00:00","location":"Cairo","locationID":0,"sectorID":56,"sectorTitle":"Construction Projects","medium":null,"userIP":null,"subSectorNames":null,"subSectorIDs":null,"beneficialSectorsSubsectors":null}],"totalCount":73,"remainingCount":62},"status":{"id":1,"value":"DONE","message":"Ok"}}';
#### To get the tender link ####
top:
	while($Tender_content1=~m/tenderID([\w\W]*?)beneficialSectorsSubsectors/igs)
	{
	my $inner_link=$1;
	# my $inner_link="https://www.amcham.org.eg/online_services/tas/".$1;
		# my $tender_link="https://www.amcham.org.eg/tas/tender-details/".$1;
	# $tender_link==~s/\&amp\;/&/igs;
	# my ($Tender_content3)=&Getcontent($tender_link);
	# print $inner_link;
	# my ($Tender_content2)=&Getcontent($inner_link);
	while($inner_link=~m/"encryptedTenderID":"([^>]*?)"/igs)
	{
	my $tender_link="https://www.amcham.org.eg/tas/tender-details/".$1;
	
	$tender_link==~s/\&amp\;/&/igs;
	my ($Tender_content3)=&Getcontent($tender_link);
	$Tender_content3 = decode_utf8($Tender_content3); 
	open(ts,">Tender_content507_3.html");
print ts "$Tender_content3";
close ts;
	
	my ($tender_title,$Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
	
	if($Tender_content3=~m/<title>\s*([^>]*?)\s*-\s*AmCham Egypt\s*<\/title>/is)
	{
	$tender_title=$1;
	print $tender_title;
	}
# exit;
	# if($Tender_content2=~m/(Buyer:<\/span>[\w\W]*?)<td data-th="Bid Pkg/is)
	# {
	# $Awarding_Authority=&BIP_Tender_DB::clean($1);
	# $Awarding_Authority=&clean($Awarding_Authority);
	# }
	$Awarding_Authority="American Chamber of Commerce in Egypt, Egypt. Web: ".$tender_link;
	if($Tender_content3=~m/Tender-Title\-\->([\w\W]*?)<div class="footer d-flex justify-content-between align-items-center">/is)
	{
	$Description=&clean($1);
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	print $Description;
	# exit;
	}
	if($Tender_content3=~m/Deadline\s*:<\/div>[\w\W]*?">\s*([^>]*?)\s*<span>/is)
	{
		$Deadline_Date=$1;
		if($Deadline_Date=~m/(\w+)\s*(\d{1,2})\s*\,\s*(\d{4})/is)
		{
		$Deadline_Date=$2."-".$1."-".$3;
		}
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($Deadline_Date);
		# $Deadline_time=&clean($4);
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
		# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
			
		$Deadline_Description="Deadline";
		# print $Deadline_Date;
		# exit;
		}
	$Deadline_Description="Deadline";
	if($Tender_content3=~m/<\!\-\-Location\-\->([\w\W]*?)\s*<\/span>\s*<\/div>/is)
	{
	$Location=&clean($1);
	$Location=&BIP_Tender_DB::clean($Location);
	print $Location;
	# exit;
	}
	

	 # if($Tender_content2=~m/Owner Solic Number:[\w\W]*?<br\s*\/><\/span>([^>]*?)<span/is)
	# {
	# $Authority_Reference=&clean($1);
	# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
	# }

# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	# if($Tender_content2=~m/<h2>([\w\W]*?Buyer:[\w\W]*?)<\/table>/is)
	# {
	# $tenderblock=&clean($1);
	# }
	$tenderblock = &BIP_Tender_DB::clean($Description);
	$tenderblock=&clean($tenderblock);
	$cy="EG";
		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
}


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html");
	$req->header("Host"=> "www.amcham.org.eg");
	$req->header("Referer"=> "https://www.amcham.org.eg/tas/");
# $req->header("Cookie"=> "ASP.NET_SessionId=pbk5qv2clkdyplemeaikjw3r; UserUniqueKey=7d9f0bce-1e21-474c-99b1-fea9b6c9aa9d; cookiesession1=172CDC20SURIV2N2IXICIRCSJORF744C; _ga=GA1.3.1597878409.1572884611; _gid=GA1.3.865141605.1572884611; _gat_UA-85200123-1=1");
# $req->header("X-Requested-With"=> "XMLHttpRequest");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://sasktenders.ca/content/public/Search.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.273493052.1490704315; ASP.NET_SessionId=waveczm53zsn51sibpqb24i2");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	decode_entities($Clean);
	return $Clean;
}