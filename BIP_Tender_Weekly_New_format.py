import requests
import re
import os,sys
import pymysql
import time
import json
import pymysql
# import xlwt
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
# import ConfigParser
import configparser

import dateutil.parser as dparser
from time import sleep
import warnings
import pytz
warnings.warn("ignore")
# try:
    # from ConfigParser import ConfigParser
# except:
    # from six.moves import ConfigParser

# import codec
# warnings.filterwarnings('ignore')
# reload(sys)
# sys.setdefaultencoding('utf-8')


Database_Connector = imp.load_source('Database_Connector','C:\\BIP_Tender\\New_Format\\Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
##Clean Function:::
def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc =  desc.replace("amp;","")
	desc = desc.replace('\s+\s+', ' ')
	desc = re.sub(r'<blockquote class="twitter-tweet"[^<]*?>[\w\W]*?<\/blockquote>', ' ', str(desc))
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r"&#39;","''", str(desc))
	# desc = re.sub(r"'","''", str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "''", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	desc = desc.replace('&nbsp;','')
	desc = desc.replace('👇','') 
	desc = desc.replace('🇮🇹🇬🇧','') 
	desc = desc.replace('👏','')	
	desc = desc.replace('😊👍','')
	desc = desc.replace('! 🏼⚽️ ','')
	if isinstance(desc, str):
		return desc
	else:	

		return desc.decode("utf-8")
	# desc = desc.replace('<blockquote class="twitter-tweet"[^<]*?>[\w\W]*?<\/blockquote>', ' ')
	# title2 =  title2.replace("''","'")
	# return desc.decode('utf-8', errors='ignore')

def regex_match(regex,content):
	if regex != "None":
		value = re.findall(regex,str(content))
		if len(value) == 0:
			value1 = ""
		else:
			value1 = reg_clean(value[0])
	else:
		value1 = ""
	return (value1)
	
def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	try:
	
		dt=dparser.parse(Input,fuzzy=True)
		FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	except Exception as e:
		deadline_date1 = dateparser.parse(time.asctime())
		deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
		FormattedDate = deadline_date1
	return (FormattedDate)

def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	# time1 = re.findall('(\d{1,2})(?:\:\d{2}\s*|\s*)(?:Noon|noon|pm)',str(Input))
	time1 = re.findall('(\d{1,2})\s*(?:Noon|noon|pm|PM)',str(Input))
	print (time1)
	if time1:
		if int(time1[0]) < 12:
			Formattedtime = str(int(time1[0])+12) +":00:00"
		else:
			Formattedtime = str(int(time1[0])) +":00:00"
	print (Formattedtime)
	time3 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:GMT|gmt)',str(Input))
	if time3:
		Formattedtime = str(int(time3[0][0])) +":"+str(int(time3[0][1]))+":00"
	time2 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)',str(Input))
	# print time1[0][0]
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			if time2[0][1]:
				Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+"0:00"
			else:
				Formattedtime = str(int(time2[0][0])+12)  +":00:00"
		else:    
			if int(time2[0][0]) == 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)',str(Input))
        
		if time2:
			if int(time2[0][0]) <= 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			
		else:
			time2 = re.findall('(\d{1,2})\s*(?:pm|PM\s*)',str(Input))
			print (time2)
			if time2:
				if int(time2[0][0]) < 12:
					Formattedtime = str(int(time2[0][0])+12) +":00:00"
			else:		
				time2 = re.findall('(?:Midday|midday|12\s*noon|12.00\s* Noon|12.00\s*noon|noon)',str(Input))
				if time2:
					Formattedtime = "12:00:00"
				else:
					time2 = re.findall('(?:Midnight|midnight)',str(Input))
					if time2:
						Formattedtime = "00:00:00"
					else:
						Formattedtime = Input
					# Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
	# dt=dparser.parse(Input,fuzzy=True)
	# Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
	print (Formattedtime)
	return (Formattedtime)

def regex_content_block(regex,content):
# used to take the values block by using regex
	value1=content
	if regex != None:
		revenue = re.findall(regex,str(content))
		return revenue
	else:
		return content.decode('utf-8', errors = 'ignore')

def regex_checking_updating(content,url,tender_id):
	print (tender_id+"\n")
	# print (content)
	config = configparser.ConfigParser()
	config.read('C:\\BIP_Tender\\New_Format\\BIP_Tender_new.ini')
	print (config.sections())
	title=(config.get(str(tender_id),'title'))
	title1 = regex_match(title,str(content))
	print (title1)
	sourcecw=(config.get(str(tender_id),'sourcecw'))
	tender=(config.get(str(tender_id),'tender'))
	origin=(config.get(str(tender_id),'origin'))
	sector=(config.get(str(tender_id),'sector'))
	authorityname=(config.get(str(tender_id),'authorityname'))
	authoritydept=(config.get(str(tender_id),'authoritydept'))
	country_codes_isocountry=(config.get(str(tender_id),'country_codes_isocountry'))
	country=(config.get(str(tender_id),'country'))
	web_source=(config.get(str(tender_id),'web_source'))
	industry_sector=(config.get(str(tender_id),'industry_sector'))
	awarding_authority=(config.get(str(tender_id),'awarding_authority'))
	authorityaddress1=(config.get(str(tender_id),'authorityaddress1'))
	authorityaddress2=(config.get(str(tender_id),'authorityaddress2'))
	authorityaddress3=(config.get(str(tender_id),'authorityaddress3'))
	defence=(config.get(str(tender_id),'defence'))
	pubflagcw=(config.get(str(tender_id),'pubflagcw'))
	authoritytown=(config.get(str(tender_id),'authoritytown'))
	authoritycounty=(config.get(str(tender_id),'authoritycounty'))
	online_only=(config.get(str(tender_id),'online_only'))
	private_sector=(config.get(str(tender_id),'private_sector'))
	frameworkagreement=(config.get(str(tender_id),'frameworkagreement'))
	authoritycountry=(config.get(str(tender_id),'authoritycountry'))
	authoritytelephone=(config.get(str(tender_id),'authoritytelephone'))
	authorityfax=(config.get(str(tender_id),'authorityfax'))
	authorityemail=(config.get(str(tender_id),'authorityemail'))
	temp_description_export=(config.get(str(tender_id),'temp_description_export'))
	authoritywebaddress=(config.get(str(tender_id),'authoritywebaddress'))
	authorityprofileurl=(config.get(str(tender_id),'authorityprofileurl'))
	authoritycontactsalutation=(config.get(str(tender_id),'authoritycontactsalutation'))
	authoritycontactfirstname=(config.get(str(tender_id),'authoritycontactfirstname'))
	authoritycontactlastname=(config.get(str(tender_id),'authoritycontactlastname'))
	authorityposition=(config.get(str(tender_id),'authorityposition'))
	temp_site_export=(config.get(str(tender_id),'temp_site_export'))
	nc=(config.get(str(tender_id),'nc'))
	trackerref=(config.get(str(tender_id),'trackerref'))
	awardprocedure=(config.get(str(tender_id),'awardprocedure'))
	contact_type=(config.get(str(tender_id),'contact_type'))
	earliestdate=(config.get(str(tender_id),'earliestdate'))
	pr=(config.get(str(tender_id),'pr'))
	cpv_code_block=(config.get(str(tender_id),'cpv_code_block'))
	cpv_code=(config.get(str(tender_id),'cpv_code'))
	aa=(config.get(str(tender_id),'aa'))
	origpublicationdate=(config.get(str(tender_id),'origpublicationdate'))
	description=(config.get(str(tender_id),'description'))
	proofed=(config.get(str(tender_id),'proofed'))
	exported=(config.get(str(tender_id),'exported'))
	location=(config.get(str(tender_id),'location'))
	authority_reference=(config.get(str(tender_id),'authority_reference'))
	automodifydate=(config.get(str(tender_id),'automodifydate'))
	automodifytime=(config.get(str(tender_id),'automodifytime'))
	no_of_months=(config.get(str(tender_id),'no_of_months'))
	contract_start_date=(config.get(str(tender_id),'contract_start_date'))
	contract_end_date=(config.get(str(tender_id),'contract_end_date'))
	value=(config.get(str(tender_id),'value'))
	currency=(config.get(str(tender_id),'currency'))
	amount=(config.get(str(tender_id),'amount'))
	amount_min=(config.get(str(tender_id),'amount_min'))
	amount_max=(config.get(str(tender_id),'amount_max'))
	deadline_description=(config.get(str(tender_id),'deadline_description'))
	deadline_date=(config.get(str(tender_id),'deadline_date'))
	deadline_time=(config.get(str(tender_id),'deadline_time'))
	sent_to=(config.get(str(tender_id),'sent_to'))
	other_information=(config.get(str(tender_id),'other_information'))
	nuts_code=(config.get(str(tender_id),'nuts_code'))
	authoritypostcode=(config.get(str(tender_id),'authoritypostcode'))
	supplier=(config.get(str(tender_id),'supplier'))
	d_award=(config.get(str(tender_id),'d_award'))
	tendersreceived=(config.get(str(tender_id),'tendersreceived'))
	price=(config.get(str(tender_id),'price'))
	site=(config.get(str(tender_id),'site'))
	awarded_status=(config.get(str(tender_id),'awarded_status'))
	intend=(config.get(str(tender_id),'intend'))
	bip_non_uk=(config.get(str(tender_id),'bip_non_uk'))
	tender_type=(config.get(str(tender_id),'tender_type'))
	next_page=(config.get(str(tender_id),'next_page'))

	title2 = title1
	# print (title2)
	title2 =  title2.replace("''","'")
	if tender_id=='1174':
		title1=title1[:50]
	print (title1)	
	# print "re----->",Retrive_duplicate with title of the tender
	Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
	Duplicate_Check_Title = Database_Connector.DuplicateCheckTitle(Retrive_duplicate, title2)
	if Duplicate_Check_Title == "N":		
		authorityName1 = regex_match(authorityname,str(content))
		country_codes_isocountry="United Kingdom"
		authorityDept1=regex_match(authoritydept,str(content))
		award_procedure1 = regex_match(awardprocedure,str(content))
		contact_type1 = regex_match(contact_type,str(content))
		cpv_code1 = regex_match(cpv_code,str(content))
		description1 = regex_match(description,str(content))
		location1 = regex_match(location,str(content))
		authority_reference1 = regex_match(authority_reference,str(content))
		authorityAddress1=regex_match(authorityaddress1,str(content))
		authorityAddress2=regex_match(authorityaddress2,str(content))
		authorityAddress3=regex_match(authorityaddress3,str(content))
		defence1=regex_match(defence,str(content))
		pubFlagCW1=regex_match(pubflagcw,str(content))
		authorityTown1=regex_match(authoritytown,str(content))
		authorityCounty1=regex_match(authoritycounty,str(content))
		online_only1=regex_match(online_only,str(content))
		private_sector1=regex_match(private_sector,str(content))
		frameworkAgreement1=regex_match(frameworkagreement,str(content))
		authorityCountry1=regex_match(authoritycountry,str(content))
		authorityTelephone1=regex_match(authoritytelephone,str(content))
		authorityFax1=regex_match(authorityfax,str(content))
		authorityEmail1=regex_match(authorityemail,str(content))
		temp_description_export1=regex_match(temp_description_export,str(content))
		authorityWebAddress1=regex_match(authoritywebaddress,str(content))
		authorityProfileURL1=regex_match(authorityprofileurl,str(content))
		authorityContactSalutation1=regex_match(authoritycontactsalutation,str(content))
		authorityContactFirstName1=regex_match(authoritycontactfirstname,str(content))
		authorityContactLastName1=regex_match(authoritycontactlastname,str(content))
		authorityPosition1=regex_match(authorityposition,str(content))
		temp_site_export1=regex_match(temp_site_export,str(content))
		nc1=regex_match(nc,str(content))
		trackerRef1=regex_match(trackerref,str(content))
		pr1=regex_match(pr,str(content))
		aa1=regex_match(aa,str(content))
		origPublicationDate1=regex_match(origpublicationdate,str(content))
		proofed1=regex_match(proofed,str(content))
		exported1=regex_match(exported,str(content))
		autoModifyDate1=regex_match(automodifydate,str(content))
		autoModifyTime1=regex_match(automodifytime,str(content))
		no_of_months1=regex_match(no_of_months,str(content))
		contract_start_date1=regex_match(contract_start_date,str(content))
		contract_end_date1=regex_match(contract_end_date,str(content))
		currency1=regex_match(currency,str(content))
		amount1=regex_match(amount,str(content))
		amount_min1=regex_match(amount_min,str(content))
		amount_max1=regex_match(amount_max,str(content))
		nuts_code1=regex_match(nuts_code,str(content))
		country_code="United Kingdom"
		authoritypostcode1=regex_match(authoritypostcode,str(content))
		tender1=regex_match(tender,str(content))

		value1 = regex_match(value,str(content))
		# if tender_id=='1052':
			# deadline_date_re[0]=str(deadline_date_re[0])+" 2019"
		if deadline_date != "None":
			print ("U r in Loop")
			deadline_date_re=re.findall(deadline_date,str(content))
			# if tender_id=='1052':
				# deadline_date_re[0]=str(deadline_date_re[0])+" 2019"
				# print (deadline_date_re[0])
			if len(deadline_date_re) == 0 or deadline_date_re[0] == '':
				deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
				# deadline_date1 = dateparser.parse(time.asctime())
				deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
				FormattedDate = DateFormat(deadline_date1)
			else:
				deadline_date1 = reg_clean(deadline_date_re[0])
				deadline_date1 = DateFormat(deadline_date1)
				print (deadline_date1)
				FormattedDate = deadline_date1
		else:
			deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
			deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
			FormattedDate = deadline_date1
		if deadline_time != "None":
			deadline_time_re = re.findall(deadline_time,str(content))
			if len(deadline_time_re) == 0:
				deadline_time1 = ""
			else:
				if tender_id =="1012":
					deadline_time1 = deadline_time_re[0]+" 00:00"
				else:
					deadline_time1 = timeFormat(deadline_time_re[0])
		else:
			deadline_time1 = "00:00:00"
		exclusiion_list = ["Leaders","Marketing Assistant","Supervisor","Administration Assistant","Administrative Assistant","Administrator","Assistant Manager","Bookseller","Box Office Manager","Box Office Supervisor","Business Director","Casual Technicians","Communicator","Community Facilitator","Community Manager","Company Manager","Coordinator","Curatorial Assistant","Data and Systems Manager","Development Manager","Driver Controller","Duty Manager","Facilitator","Finance Manager","Freelance Researcher","Guidebook Seller","Head of","Head of Marketing","Head of Development Operations","Head of Marketing & Communications","Head of Operations","Head of Public Programming","IT Director","Junior Producer","Learning Manager","Learning Facilitator","Marketing Officer","Officer","Outside Events Officer","Producer","Production Manager","Programming Manager","Project Development Manager","Public Events Manager","Safeguarding Manage","Sales Director","Seller","Session Support Worker","Show Manager","Studio Manager","Teacher","Technician","Trainee Recording Engineer","Venue Coordinator","Violin Teacher","Wellbeing Officer","Worker","Workshop Trainee","Youth Coordinator","Assistant Manager","Casual Technicians","Casual Technician","Leaders","Outside Events Officer","Duty Manager","Finance Manager","Company Manager","Show Manager","Programming Manager","Project Development Manager","Box Office Manager","Head of Operations","Performing Arts Teaching Assistant","Learning Operations Manager","Finance Assistant","Transition Manager","Trainee Production/Editorial Assistant","Communications Coordinator","Sales and Marketing Director","Marketing Manager","Editorial Assistant","Stage Manager","Customer Service Executive","Junior Press Officer","Assistant Producer","Archivist and Records Manager","Head of Editorial"] 				
		# valid_type = next((True for exclusion_list1 in exclusion_list if exclusion_list1 in description1), False)
		# valid_type = [ele for ele in exclusiion_list if(ele in description1)]
		
		# if bool(valid_type)==False:
		valid_type2 = [ele for ele in exclusiion_list if ele in title1]
		print (valid_type2)
		if bool(valid_type2):
			valid_type1="IC Tenders"
		else:
			valid_type1="Normal"
		# else:
			# valid_type1="IC Tenders"
		other_information1 = other_information + str(url)
		curr_date = datetime.now(pytz.timezone('Europe/London'))
		created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
		# import pdb;pdb.set_trace()
		# insertQuery = "insert into tender_data1 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,private_sector,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,description,proofed,exported,location,authority_reference,no_of_months,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,already_exist,export_flag,created_date,created_by,high_value,authoritypostcode,supplier,D_award,tendersReceived,price,Site,Awarded_Status,InTend,BIP_Non_UK,Tender_Type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url),str(sourcecw),str(authorityName1), str(tender1), str(origin), str(sector), str(authorityDept1),str(country_code), str(country), str(web_source), str(industry_sector), str(title1), str(title1),str(authorityName1), str(authorityAddress1), str(authorityAddress2), str(authorityAddress3), str(defence1), str(pubFlagCW1), str(authorityTown1), str(authorityCounty1), str(online_only1), str(private_sector1), str(frameworkAgreement1), str(authorityCountry1), str(authorityTelephone1), str(authorityFax1), str(authorityEmail1), str(temp_description_export1), str(authorityWebAddress1), str(authorityProfileURL1), str(authorityContactSalutation1), str(authorityContactFirstName1), str(authorityContactLastName1),str(authorityPosition1), str(temp_site_export1), str(nc1), str(trackerRef1), str(award_procedure1), str(contact_type1), str(FormattedDate), str(pr1), str(cpv_code1), str(aa1), str(description1), str(proofed1), str(exported1), str(location1), str(authority_reference1), str(no_of_months1),  str(value1), str(currency1), str(value1), str(amount_min1), str(amount_max1), str(deadline_description), str(FormattedDate), str(deadline_time1), str(other_information1), str(url), str(nuts_code),'', '', str(created_date),'', '', str(authoritypostcode1), '', '', '', '', '', str(awarded_status), str(intend), str(bip_non_uk), str(tender_type))
		
		# data = [tender_id, url,sourcecw,authorityName1, tender1, origin, sector, authorityDept1,country_code, country, web_source, industry_sector, title1, title1,authorityName1, authorityAddress1, authorityAddress2, authorityAddress3, defence1, pubFlagCW1, authorityTown1, authorityCounty1, online_only1, private_sector1, frameworkAgreement1, authorityCountry1, authorityTelephone1, authorityFax1, authorityEmail1, temp_description_export1, authorityWebAddress1, authorityProfileURL1, authorityContactSalutation1, authorityContactFirstName1, authorityContactLastName1,authorityPosition1, temp_site_export1, nc1, trackerRef1, award_procedure1, contact_type1, FormattedDate, pr1, cpv_code1, aa1, description1, proofed1, exported1, location1, authority_reference1, no_of_months1,  value1, currency1, value1, amount_min1, amount_max1, deadline_description, FormattedDate, deadline_time1, other_information1, url, nuts_code,'', '', created_date,'', '', authoritypostcode1, '', '', '', '', '', awarded_status, intend, bip_non_uk, tender_type]
		
		insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,private_sector,frameworkAgreement,authorityCountry,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,description,proofed,exported,location,authority_reference,no_of_months,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,other_information,nuts_code,already_exist,created_date,created_by,high_value,authoritypostcode,supplier,D_award,tendersReceived,price,Site,Awarded_Status,InTend,BIP_Non_UK,Tender_Type,valid_type) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format((tender_id), (url),(sourcecw),(authorityName1), (tender1), (origin), (sector), (authorityDept1),(country_code), (country), (web_source), (industry_sector), (title1), (title1),(authorityName1), (authorityAddress1), (authorityAddress2), (authorityAddress3), (defence1), (pubFlagCW1), (authorityTown1), (authorityCounty1), (online_only1), (private_sector1), (frameworkAgreement1), (authorityCountry1), (authorityTelephone1), (authorityFax1), (authorityEmail1), (temp_description_export1), (authorityWebAddress1), (authorityProfileURL1), (authorityContactSalutation1), (authorityContactFirstName1), (authorityContactLastName1),(authorityPosition1), (temp_site_export1), (nc1), (trackerRef1), (award_procedure1), (contact_type1), (FormattedDate), (pr1), (cpv_code1), (aa1), (description1), (proofed1), (exported1), (location1), (authority_reference1), (no_of_months1),  (value1), (currency1), (value1), (amount_min1), (amount_max1), (deadline_description), (FormattedDate), (deadline_time1), (other_information1), (url), (nuts_code),'', (created_date),'', '', (authoritypostcode1), '', '', '', '', '', (awarded_status), (intend), (bip_non_uk), (tender_type),(valid_type1))
		
		print (insertQuery)
		Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
	
## DB Connections:::

def program():	

	proxies = {"http" :"http://172.27.137.192:3128","https":"https://172.27.137.192:3128"}
	# proxies = {
	# "http" :"http://172.27.137.199:3128",
	# "https":"http://172.27.137.199:3128"
	# }
	# main_url_query = "SELECT id from tender_master where (frequency=DAYNAME(now()) or frequency='Daily')"
	# mysql_cursor.execute(main_url_query)
	# tender_id = mysql_cursor.fetchall()
	# print (tender_id)
	# print "re----->",Retrive_duplicate
	next_page1=True
	config = configparser.ConfigParser()
	config.read('C:\\BIP_Tender\\New_Format\\BIP_Tender_new.ini')
	print (config.sections())
	# for field in config.options(str(Retailer_Name)):
		# globals()[field]= str(config.get(str(Retailer_Name),field))

	# for field in config.options(str(Retailer_Name)):
	# globals()[field]= str(config.get(str(Retailer_Name),field))
	for i in config.sections():
		temp = str(i)
		home_url=str(config.get(str(temp),'home_url'))
		sub_url_regex=(config.get(str(temp),'sub_url_regex'))
		home_url_headers=(config.get(str(temp),'home_url_headers'))
		home_url_block_regex=(config.get(str(temp),'home_url_block_regex'))
		home_url_method=(config.get(str(temp),'home_url_method'))
		base_url=(config.get(str(temp),'base_url'))
		sub_url_method=(config.get(str(temp),'sub_url_method'))
		print ("i************",temp)
		# for field in config.options(str(temp)):
			
			# globals()[field]= str(config.get(str(temp),field))
		
			
		PYTHONHTTPSVERIFY=1
	
	
		Retrive_duplicate  = Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(temp))
		# while next_page1:
		if home_url_method == "None":	
			# time.sleep(5)
		# session = requests.Session()
		# retry = Retry(connect=3, backoff_factor=0.5)
		# adapter = HTTPAdapter(max_retries=retry)
		# session.mount('http://', adapter)
		# session.mount('https://', adapter)

		# session.get(url)
			r=requests.get(eval(home_url),headers=eval(home_url_headers),verify = False,proxies=proxies,allow_redirects=True)
			html=r.content.decode('utf-8', errors='ignore')

		# print html
	
#### if tender url only from the home url content
			if sub_url_regex != "None" and home_url_block_regex == "None":
				sub_url = re.findall(sub_url_regex,str(html))
			# print sub_url
				for j in sub_url:
					print (j)
					if not "http" in str(j):
						str1 = base_url.replace('"','')
						if temp == '1139':
							j = str(str1)+j+".htm"
						else:	
							j = str(str1)+j
						j = j.replace("&amp;","&")
						print (j)
					# print "re----->",Retrive_duplicate with url
					Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, j)
					if Duplicate_Check == "N":
						if sub_url_method == "None":
							r1 = requests.get(str(j),headers=eval(home_url_headers),verify = False,proxies=proxies,allow_redirects=True)
							html1=r1.content.decode('utf-8', errors = 'ignore')
							regex_checking_updating(html1,j,temp)
						
# if tender url is available in a block of home url content								
			if sub_url_regex != "None" and home_url_block_regex != "None":
				home_url_block = re.findall(home_url_block_regex,str(html))
			# print sub_url
				for j in home_url_block:
					sub_url = re.findall(sub_url_regex,str(j))
					if not "http" in str(sub_url):
						str1 = base_url.replace('"','')
						sub_url = sub_url[0].replace("&amp;","&")
						sub_url = sub_url[0].replace("\\/","/")
						sub_url = str(str1)+sub_url
					# print "re----->",Retrive_duplicate with url
					# sub_url = str(str1)+sub_url
					Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url)
					if Duplicate_Check == "N":
						if sub_url_method == "None":
							r1 = requests.get(str(sub_url),headers=eval(home_url_headers), verify = False,proxies=proxies,allow_redirects=True)
							html1=r1.content.decode('utf-8', errors = 'ignore')
							regex_checking_updating(html1,sub_url,temp)
						
# only tender block has been taken from home url content									
			if sub_url_regex == "None" and home_url_block_regex != "None":
				home_url_block1 = re.findall(home_url_block_regex,str(html))
				print (home_url_block1)
			# print (home_url_block1)
				for html1 in home_url_block1:
					html1=html1.replace("\n", "").strip()
					print (html1)
					regex_checking_updating(html1,home_url,temp)	
				# if next_page ==	"None":
					# next_page1=False
				# else:
					# next_page_url = regex_match(next_page,str(html))
					# next_page_url = next_page_url[0]
					# next_page_url = next_page_url.replace("&amp;","&")
					# next_page_url = next_page_url.replace("\\/","/")					
					# if not "http" in str(next_page_url):
						# str1 = base_url.replace('"','')
						# next_page_url = str(str1)+next_page_url
					# home_url=next_page_url
					# next_page1=True	
					
						
	connection.close()
		
if __name__== "__main__":

	program()




