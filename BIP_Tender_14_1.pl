#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = 14;
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->proxy('https', 'http://172.27.137.192:3128');
$ua->timeout(100); 
# $ua->max_redirect(1); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
$Tender_Url="https://e-sourcingni.bravosolution.co.uk/web/login.shtml";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
# my $post_content2="userAct=search&oppList=GLOBAL&filterEnabled=true&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=EQUAL&firstPublishingDate_FILTER=31%2F01%2F2018&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=1";
# my $host1 = "e-sourcingni.bravosolution.co.uk";
### Ping the first level link page ####
# my ($Tender_content1)=&Postcontent($Tender_Url,$post_content2,$host1);
# $Tender_content1 = decode_utf8($Tender_content1);  
# open(ts,">Tender_content14_2.html");
# print ts "$Tender_content1";
# close ts;
# exit;
open RES,">crown.txt";
print RES "ID\tTender ID\tSource Name\torigin\tnoticeSector\tcy\tsourceURL\thigh_value\ttitle\tawardingAuthority\tawardProcedure\tcontractType\tcpvNos\tdescription\tsite\tauthorityRefNo\tvalue\tcw_Radeadline\td_RA\td_Ratime\td_Rapost\totherInformation\tauthoritypostcode\tnuts\tCreated Date\n";
close RES;
#### To get the tender link ####
# if ($Tender_content1=~m/<a href=\s*"([^>]*?)"\s*[^>]*?>Global Opportunities<\/a>/is)
for(my $i=1;$i<=5;$i++)
{
# {
	# my $View_current_pan ="https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL";
	my $View_current_pan ="https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do";
	my $post_content1 = "userAct=order&oppList=GLOBAL&filterEnabled=false&projectInfo_FILTER_OPERATOR=EMPTY&projectInfo_FILTER=&purchasingOrganizationInfo_FILTER_OPERATOR=EMPTY&purchasingOrganizationInfo_FILTER=&firstPublishingDate_FILTER_OPERATOR=EMPTY&firstPublishingDate_FILTER=&firstPublishingDate_FILTER_SELECT=TODAY&firstPublishingDate_FILTER_fromDate_period=&workCategory_FILTER_OPERATOR=EMPTY&workCategory_FILTER=&SearchBox4sayt_workCategory_FILTER=&procurementRoute_FILTER_OPERATOR=EMPTY&procurementRoute_FILTER=&SearchBox4sayt_procurementRoute_FILTER=&searchButtonPressed=true&listManager.pagerComponent.page=".$i."&listManager.orderBy=firstPublishingDate";
	my $Host	= 'e-sourcingni.bravosolution.co.uk';
	my ($View_current_content)=&Postcontent($View_current_pan,$post_content1,$Host);
	# my ($View_current_content)=&Getcontent($View_current_pan);
	$View_current_content = decode_utf8($View_current_content);  
	# open(ts,">View_current_content14_3.html");
	# print ts "$View_current_content";
	# close ts;
	# exit;
	# sleep(10);

	top:
	while ($View_current_content=~m/<td[^>]*?\"col[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?>\s*<a[^>]*?onclick\=\"javascript:goToDetail\(\&\#39\;([\d]+)\&\#39\;[^>]*?>([\w\W]*?)<\/td>\s*<td[^>]*?[^>]*?>\s*[^>]*?\s*<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>\s*<td[^>]*?[^>]*?>\s*([^>]*?)\s*<\/td>/igs)
	{
		my $Organisation	= ($1);	
		my $tender_no		= ($2);
		my $tender_title	= &clean($3);
		my $Work_Category	= ($4);
		my $Listing_Deadline= ($5);
		# my $tender_link	= 'https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&oppList=GLOBAL';
		# my $tender_link	= 'https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB&_ncp=1517466791341.865-2';
		# my $tender_link	= "https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId=".$tender_no."&oppList=GLOBAL";
		my $tender_link	= "https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId=".$tender_no."&userAct=changeLangIndex&language=en_GB&_ncp=1517478911039.803-2";
		# my $tender_link	= 'https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&oppList=GLOBAL';
		# my $tender_link	= 'https://defra.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB&_ncp=1517404553172.803-1';
		# my $tender_link	= 'https://www.capitalesourcing.com/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB&_ncp='.$ncp;
		# my $tender_link	= 'https://defra.bravosolution.co.uk/esop/toolkit/opportunity/opportunityDetail.do?opportunityId='.$tender_no.'&userAct=changeLangIndex&language=en_GB';
		my ($Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		
		if ($Listing_Deadline=~m/^\s*([^>]*?)\s([^>]*?)\s*$/is)
		{
			$Deadline_Date = $1;
			$Deadline_time = $2;
		}
		# print "Deadline Date :: $Deadline_Date\n";
		# print "Deadline Time :: $Deadline_time\n";
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		my ($Tender_content)=&Getcontent1($tender_link);
		sleep(10);
		$Tender_content = decode_utf8($Tender_content);  
		# open(ts,">Tender14_content.html");
		# print ts "$Tender_content";
		# close ts;
		# exit;
		
		if ($Tender_content=~m/<div class="form_question">Project Title<\/div>\s*<div class="form_answer">\s*([^>]*?)\s*<\/div>\s*<\/li>/is)
		{
			my $title_temp = &clean($1);
		}
		if ($Tender_content=~m/<div class="form_question">Project Code<\/div>\s*<div class="form_answer">\s*([^>]*?)\s*<\/div>\s*<\/li>/is)
		{
			$Authority_Reference = &clean($1);
		}
		if ($Tender_content=~m/>\s*Project\s*Description\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Description = &clean($1);
		}
		if ($Tender_content=~m/>\s*Work\s*Category\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Contract_Type = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Listing\s*Deadline)\s*<\/div>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Deadline_Description = ($1);
			my $Deadline_Date_temp = &clean($2);
		}
		if ($Tender_content=~m/>\s*Buyer\s*Details\s*<\/h3>\s*([\w\W]*?)\s*<\/ul>/is)
		{
			$Awarding_Authority = ($1);
			$Awarding_Authority =~s/\s*<\/div>\s*<div[^>]*?>\s*/ : /igs;
			$Awarding_Authority = &clean($Awarding_Authority);
		}
		
		my $tenderblock;
		if ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/table>\s*<\/div>\s*<\/form>/is)
		{
			$tenderblock = ($1);
		}
		elsif ($Tender_content=~m/maintitle\"\s*>([\w\W]*?)<\/form>\s*<\/div>\s*<\/div>/is)
		{
			$tenderblock = ($1);
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# my $tender_type="Normal";
		# &BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
					open RES,">>crown.txt";
			print RES "\t$Tender_ID\tCrown Commercial Service\tWeb\tUK Other\tGB\tCrown Commercial Service\t\t$tender_title\t$Awarding_Authority\t\t$Contract_Type\t\t$Description\t\t$Authority_Reference\t\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\tYou must register or log in at Web: $tender_link\n";
			close RES;
			# sleep(10);
	}

 }	


sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"e-sourcingni.bravosolution.co.uk");
	$req->header("Referer"=>"https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "JSESSIONID=mkA1e1brdcJvMm9YDbp40lwZWmt0xh4-7915C1Jw.ogcadm_lb4; VISITORID=39660515-af69-4dc2-b5fd-ee56c510d588; VISITOR_ET=1517556481531; _tabSessionId=971ee151-2d28-ac28-1778-1ca41b431317");


	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "e-sourcingni.bravosolution.co.uk");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do");
	$req->header("Cookie"=> "JSESSIONID=sI5UCFoLIZ-gKPCItVvyn-zosZNj603b1g0VKdKr.ogcadm_lb2; VISITORID=e0612085-628e-4a06-bc15-2de1b90720a1; VISITOR_ET=1518451311914; _tabSessionId=e1a718e5-7da9-80cc-19d7-b0dab9df5898");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>|<td>|<\/td>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"e-sourcingni.bravosolution.co.uk");
	# $req->header("Referer"=>"https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL");
	# $req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "JSESSIONID=wP4TWzyPuyMk8obSW-3cHxrXyvKjhpt7IlDo8jIw.ogcadm_lb1; VISITORID=a9443424-12c0-484f-bbef-a9721c3f367e; VISITOR_ET=1517495235990; _tabSessionId=254aae41-ee98-ac51-4ada-c74f50468f5e");
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Getcontent2
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"e-sourcingni.bravosolution.co.uk");
	# $req->header("Referer"=>"https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL");
	# $req->header("Content-Type"=> "text/html;charset=UTF-8");
	$req->header("Cookie"=> "JSESSIONID=yEbrYIo8Vau64wkorlpf3dn67O1P2Bsougib5TbJ.ogcadm_lb3; VISITORID=ca70a9e6-85ef-45e7-b806-0fcd08bfa603; VISITOR_ET=1517408117508; _tabSessionId=467febf4-8c37-2a9b-c477-5845cc47f782");
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}


sub Postcontent1()
{
	my $post_url=shift;
	my $tenderno=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "e-sourcingni.bravosolution.co.uk");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://e-sourcingni.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=GLOBAL");
	$req->header("Cookie"=> "JSESSIONID=yEbrYIo8Vau64wkorlpf3dn67O1P2Bsougib5TbJ.ogcadm_lb3; VISITORID=ca70a9e6-85ef-45e7-b806-0fcd08bfa603; VISITOR_ET=1517410974563; _tabSessionId=467febf4-8c37-2a9b-c477-5845cc47f782");
	# $req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}