#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use HTTP::Cookies;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use POSIX 'strftime';
use Date::Parse;
# use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = '22';
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
my $cookie = HTTP::Cookies->new(file=>$0."_cookies.txt", autosave => 1);
$ua->cookie_jar($cookie);
# my $cookie_session = $cookie->get_cookie('JSESSIONID');
# my $session_id = $cookie_session->val;
# print "Session ID :: $session_id\n";

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my ($Tender_content2)=&Getcontent("https://www.great.gov.uk/export-opportunities/");
my $sourcecw="Contrax Weekly";
my $origin="Web";
my $noticeSector="UK Other";
my $cy="GB";
my $country_code="United Kingdom";
my $sourceURL="Exporting Is Great";
my $pubFlagCW="No";
my $authorityDept="";
my $authorityAddress2="";
my $authorityAddress3="";
my $Industry_Sector="Other";
my $authorityCountry="GB";
my $fulldate_temp=strftime "%d_%m_%Y_%A_%H_%M_%S", localtime;
my $filename = "Tender_22_$fulldate_temp.txt";
open(out,">$filename");
print out "sourcecw\ttitle\torigin\tawardingAuthority\tauthorityName\tnoticeSector\tauthorityDept\tcy\tCountry Codes::ISOcountry\tauthorityAddress1\tsourceURL\tauthorityAddress2\tauthorityAddress3\thigh_value\tdefence\tpubFlagCW\tauthorityTown\tauthorityCounty\tonline_only\tprivate_sector\tframeworkAgreement\tauthorityPostcode\tauthorityCountry\tCountry Codes 2::ISOcountry\tauthorityTelephone\tauthorityFax\tauthorityEmail\ttemp_description_export\tauthorityWebAddress\tauthorityProfileURL\tauthorityContactSalutation\tauthorityContactFirstName\tauthorityContactLastName\tauthorityPosition\ttemp_site_export\tawardProcedure\tnc\ttrackerRef\tcontractType\tearliestDate\tpr\tcpvNos\taa\tnuts\tIndustry_Sector\torigPublicationDate\tdescription\tproofed\texported\tsite\tautoModifyDate\tautoModifyTime\tauthorityRefNo\tno_of_months\tcontract_start_date\tcontract_end_date\tvalue\tcurrency\tamount\tamount_min\tamount_max\tcw_RAdeadline\td_RA\td_RAtime\td_RApost\totherInformation\n";
close out;	
while($Tender_content2=~m/<a class="title" href="([^>]*?)">([^>]*?)<\/a>\s*<span class='text'>[^>]*?<\/span>\s*<span class='([^>]*?)'>/igs)
{
	my $tender_link = "https://www.great.gov.uk".$1;
		my $temp_title  = ($2);
		my $temp_source = $3;
		next if($temp_source=~m/source open-opps/is);	
		print "#######################################################\n";
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 
		$Tender_content =~s/<\!\-\-[\w\W]*?\-\->//igs;
		open(ts,">Tender_content_22_c.html");
		print ts "$Tender_content";
		close ts;
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		my ($authorityName,$tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$defence,$Location,$Deadline_Description,$Deadline_Date,$authorityAddress2,$authorityAddress21,$authorityAddress3,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$authorityPostcode,$authorityFax,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$awardProcedure,$nc,$trackerRef,$aa,$nuts,$origPublicationDate,$proofed,$exported,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$currency,$amount_min,$amount_max,$pr,$value1);
		if($Tender_content=~m/<h1[^>]*?>\s*[^>]*?\s*-\s*([\w\W]*?)\s*<\/h1>/is)
		{	
			$tender_title = &clean($1)." - Exporting Opportunity";
		}
         elsif($Tender_content=~m/<h1[^>]*?>([\w\W]*?)\s*<\/h1>/is)
		{	
			$tender_title = &clean($1)." - Exporting Opportunity";
		}
        # if($Tender_content=~m/CPV\s*\:\s*\s*([^>]*?)\s*<\/dd>/is)
		# {	
			# $CPV_Code = &clean($1);
		# }
        if($Tender_content=~m/CPV\s*\:\s*\s*([^>]*?)\s*<\/dd>/is)
		{	
			$CPV_Code = &clean($1);
		}
		# if ($Tender_content=~m/<h4[^>]*?>\s*(Response[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
        if ($Tender_content=~m/<dt class='expiry'>\s*(Opportunity closing date)\s*<\/dt>\s*<dd class='expiry'>\s*([^>]*?)\s*<\/dd>/is)
		# if ($Tender_content=~m/<h4[^>]*?>\s*((?:Response|Expiry)[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}
		elsif($Tender_content=~m/<h4[^>]*?>\s*(Expiry\s*date)[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)	
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}		
		print "TenderTitle :: $tender_title\n";
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<\/div>\s*<\/div>/is)
		{
			$Description = ($1);
			$Description =~s/<[^>]*?block\s*value\">[\w\W]*?$//igs;
			$Description =~s/\s*(<p[^>]*?inline\">[^>]*?<\/p>)\s*/\; /igs;
			$Description =~s/\s*For\s*(?:further|more)?\s*information[^>]*?<[^>]*?>//igs;
			$Description =~s/\s*Please\s*register\s*your\s*interest[^>]*?<[^>]*?>//igs;
			$Description =~s/\&\#8211\;/-/igs;
			$Description = &clean($Description);
			$Description =~s/\s;/;/igs;
		}
		elsif ($Tender_content=~m/<\/h1>([\w\W]*?)<\/dl>\s*<\/div>/is)
		{
			$Description = ($1);
			$Description =~s/<[^>]*?block\s*value\">[\w\W]*?$//igs;
			$Description =~s/\s*(<p[^>]*?inline\">[^>]*?<\/p>)\s*/\; /igs;
			$Description =~s/\s*For\s*(?:further|more)?\s*information[^>]*?<[^>]*?>//igs;
			$Description =~s/\s*Please\s*register\s*your\s*interest[^>]*?<[^>]*?>//igs;
			$Description =~s/\&\#8211\;/-/igs;
			$Description = &clean($Description);
			$Description =~s/\s;/;/igs;
		}
		my $DescriptionTemp;
		if ($Tender_content=~m/Essential\s*information<\/h3>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
		{
			$DescriptionTemp = ($1);
			
			$DescriptionTemp =~s/\&\#8211\;/-/igs;
			$DescriptionTemp = &clean($DescriptionTemp);
			$DescriptionTemp =~s/\s;/;/igs;
		}
		
		
		
		# if ($Tender_content=~m/<h4[^>]*?>\s*Value[^>]*?<\/h4>\s*([\w\W]*?)<\/p>/is)
		if ($Tender_content=~m/<dd class='value'>\s*([\w\W]*?)<\/dd>/is)
		{
			$Value = &clean($1);
		}
		# if ($Tender_content=~m/<dt>Market[^>]*?<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is)
        if ($Tender_content=~m/Your guide to exporting\s*<\/dt>[\w\W]*?href=[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/li>/is)
        
        
		{
			$Location = &clean($1);
		}
		elsif ($Tender_content=~m/<h4[^>]*?>\s*Your\s*guide\s*to\s*exporting[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)
		{
			$Location = &clean($1);
		}
		
		my ($tenderblock);
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<div class='disclaimer'>/is)
		{
			$tenderblock = $1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		$tenderblock = $DescriptionTemp."\n".$tenderblock if ($DescriptionTemp ne '');
        $nuts_code = "UKC, UKD, UKE, UKF, UKG, UKH, UKI, UKJ, UKK, UKL, UKM, UKN";
        $Awarding_Authority="Secretary of State for Business, Innovation & Skills acting on behalf of UK Trade & Investment (UKTI). Web: ".$tender_link;
		my $Sector='UK Other';
		my $tender_type="Normal";
		my $Industry_Sector="Other";
		my $proofed="No";
		my ($Address,$Email,$Telephone);
		my $d_RApost="For further information regarding the above contract notice please visit: ".$tender_link;
		open(out,">>$filename");

		print out "$sourcecw\t$tender_title\t$origin\t\t$Awarding_Authority\t$noticeSector\t$authorityDept\t$cy\t$country_code\t$Address\t$tender_link\t$authorityAddress2\t$authorityAddress3\t\t$defence\t$pubFlagCW\t$authorityTown\t$authorityCounty\t$online_only\t\t$frameworkAgreement\t$authorityPostcode\t$cy\t$country_code\t$Telephone\t$authorityFax\t$Email\t$temp_description_export\t$authorityWebAddress\t$authorityProfileURL\t$authorityContactSalutation\t$authorityContactFirstName\t$authorityContactLastName\t$authorityPosition\t$temp_site_export\t$awardProcedure\t$nc\t$trackerRef\t$Contract_Type\t$Deadline_Date\t$pr\t$CPV_Code\t$aa\t$nuts\t$Industry_Sector\t$origPublicationDate\t$Description\t$proofed\t$exported\t$Location\t$autoModifyDate\t$autoModifyTime\t$Authority_Reference\t$no_of_months\t$contract_start_date\t$contract_end_date\t$value1\t$currency\t$value1\t$amount_min\t$amount_max\t$Deadline_Description\t$Deadline_Date1\t$Deadline_time\t$d_RApost\t$tenderblock\n";
		close out;
		&Insert_Tender_Data_new($dbh,$Tender_ID,$tender_link,$sourcecw,$Awarding_Authority,$tenderblock,$origin,$noticeSector,$authorityDept,$country_code,$cy,$sourceURL,$Industry_Sector,$tender_title,$authorityName,$Address,$authorityAddress2,$authorityAddress3,$defence,$pubFlagCW,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$Telephone,$authorityFax,$Email,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$nc,$trackerRef,$awardProcedure,$Contract_Type,$Deadline_Date1,$pr,$CPV_Code,$aa,$origPublicationDate,$Description,$proofed,$exported,$Location,$Authority_Reference,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$value1,$currency,$amount_min,$amount_max,$Deadline_Description,$Deadline_time,$d_RApost,$nuts,$authorityPostcode);
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
		# exit;
}	
### Ping the first level link page ####
# my ($Tender_content1)=&Getcontent($Tender_Url);
# my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?utf8=%E2%9C%93&isSearchAndFilter=true&filterOpen=false&s=new+opportunities&commit=Find+opportunities");
# my ($Tender_content1)=&Getcontent("https://opportunities.export.great.gov.uk/opportunities?s=&types%5B%5D=aid-funded-business&types%5B%5D=private-sector&types%5B%5D=public-sector&sort_column_name=first_published_at");
my ($Tender_content1)=&Getcontent("https://www.great.gov.uk/export-opportunities/opportunities?s=&sort_column_name=first_published_at");
# All regions and countries([\w\W]*?)<\/label>
# my $block1;
		# if ($Tender_content2=~m/All regions and countries([\w\W]*?)<\/label>/is)
		# {
			# $block1 = $1;
		# }
# while ($block1=~m/<option value='([^>]*?)'>/igs)

# {
# my $country1=$1;
# next if($country1=~m/western_europe/is);
# my $url1="https://opportunities.export.great.gov.uk/opportunities?s=&areas%5B%5D=".$country1."&commit=Find+opportunities";
# my ($Tender_content1)=&Getcontent($url1);

# $Tender_content1 = decode_utf8($Tender_content1);  

		my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
	my @Processed_Title=@$Processed_Title;
	my @Processed_Url  =@$Processed_Url;
	my @Processed_Date  =@$Processed_Date;
	re:
	# while ($Tender_content1=~m/<a[^>]*?href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/dt>/igs)
    while ($Tender_content1=~m/<a class="title" href="([^>]*?)">([^>]*?)<\/a>\s*<span class='text'>[^>]*?<\/span>\s*<span class='([^>]*?)'>/igs)
	{
		my $tender_link = "https://www.great.gov.uk".$1;
		my $temp_title  = ($2);
		my $temp_source = $3;
		print $tender_link;
		next if($temp_source=~m/source open-opps/is);	
		print "#######################################################\n";
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 
		$Tender_content =~s/<\!\-\-[\w\W]*?\-\->//igs;
		open(ts,">Tender_content_22_c.html");
		print ts "$Tender_content";
		close ts;
		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
		my ($authorityName,$tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$defence,$Location,$Deadline_Description,$Deadline_Date,$authorityAddress2,$authorityAddress21,$authorityAddress3,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$authorityPostcode,$authorityFax,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$awardProcedure,$nc,$trackerRef,$aa,$nuts,$origPublicationDate,$proofed,$exported,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$currency,$amount_min,$amount_max,$pr,$value1);
		if($Tender_content=~m/<h1[^>]*?>\s*[^>]*?\s*-\s*([\w\W]*?)\s*<\/h1>/is)
		{	
			$tender_title = &clean($1)." - Exporting Opportunity";
		}
       elsif($Tender_content=~m/<h1[^>]*?>([\w\W]*?)\s*<\/h1>/is)
		{	
			$tender_title = &clean($1)." - Exporting Opportunity";
		}
        # if($Tender_content=~m/CPV\s*\:\s*\s*([^>]*?)\s*<\/dd>/is)
		# {	
			# $CPV_Code = &clean($1);
		# }
        if($Tender_content=~m/CPV\s*\:\s*\s*([^>]*?)\s*<\/dd>/is)
		{	
			$CPV_Code = &clean($1);
		}
		# if ($Tender_content=~m/<h4[^>]*?>\s*(Response[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
        if ($Tender_content=~m/<dt class='expiry'>\s*(Opportunity closing date)\s*<\/dt>\s*<dd class='expiry'>\s*([^>]*?)\s*<\/dd>/is)
		# if ($Tender_content=~m/<h4[^>]*?>\s*((?:Response|Expiry)[^>]*?)<\/h4>\s*([\w\W]*?)<\/p>/is)
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}
		elsif($Tender_content=~m/<h4[^>]*?>\s*(Expiry\s*date)[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)	
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
		}		
		print "TenderTitle :: $tender_title\n";
		#Duplicate check
		my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");


		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<\/div>\s*<\/div>/is)
		{
			$Description = ($1);
			$Description =~s/<[^>]*?block\s*value\">[\w\W]*?$//igs;
			$Description =~s/\s*(<p[^>]*?inline\">[^>]*?<\/p>)\s*/\; /igs;
			$Description =~s/\s*For\s*(?:further|more)?\s*information[^>]*?<[^>]*?>//igs;
			$Description =~s/\s*Please\s*register\s*your\s*interest[^>]*?<[^>]*?>//igs;
			$Description =~s/\&\#8211\;/-/igs;
			$Description = &clean($Description);
			$Description =~s/\s;/;/igs;
		}
		
		elsif ($Tender_content=~m/<\/h1>([\w\W]*?)<\/dl>\s*<\/div>/is)
		{
			$Description = ($1);
			$Description =~s/<[^>]*?block\s*value\">[\w\W]*?$//igs;
			$Description =~s/\s*(<p[^>]*?inline\">[^>]*?<\/p>)\s*/\; /igs;
			$Description =~s/\s*For\s*(?:further|more)?\s*information[^>]*?<[^>]*?>//igs;
			$Description =~s/\s*Please\s*register\s*your\s*interest[^>]*?<[^>]*?>//igs;
			$Description =~s/\&\#8211\;/-/igs;
			$Description = &clean($Description);
			$Description =~s/\s;/;/igs;
		}
		my $DescriptionTemp;
		if ($Tender_content=~m/Essential\s*information<\/h3>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>/is)
		{
			$DescriptionTemp = ($1);
			
			$DescriptionTemp =~s/\&\#8211\;/-/igs;
			$DescriptionTemp = &clean($DescriptionTemp);
			$DescriptionTemp =~s/\s;/;/igs;
		}
		
		
		
		# if ($Tender_content=~m/<h4[^>]*?>\s*Value[^>]*?<\/h4>\s*([\w\W]*?)<\/p>/is)
		if ($Tender_content=~m/<dd class='value'>\s*([\w\W]*?)<\/dd>/is)
		{
			$Value = &clean($1);
		}
		# if ($Tender_content=~m/<dt>Market[^>]*?<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is)
        if ($Tender_content=~m/Your guide to exporting\s*<\/dt>[\w\W]*?href=[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/li>/is)
        
        
		{
			$Location = &clean($1);
		}
		elsif ($Tender_content=~m/<h4[^>]*?>\s*Your\s*guide\s*to\s*exporting[^>]*?<\/h4>\s*([\w\W]*?)\s*<\/p>/is)
		{
			$Location = &clean($1);
		}
		
		my ($tenderblock);
		if ($Tender_content=~m/<\/h1>([\w\W]*?)<div class='disclaimer'>/is)
		{
			$tenderblock = $1;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		$tenderblock = $DescriptionTemp."\n".$tenderblock if ($DescriptionTemp ne '');
        $nuts_code = "UKC, UKD, UKE, UKF, UKG, UKH, UKI, UKJ, UKK, UKL, UKM, UKN";
        $Awarding_Authority="Secretary of State for Business, Innovation & Skills acting on behalf of UK Trade & Investment (UKTI). Web: ".$tender_link;
		my $Sector='UK Other';
		my $tender_type="Normal";
		my $Industry_Sector="Other";
		my $proofed="No";
		my ($Address,$Email,$Telephone);
		my $d_RApost="For further information regarding the above contract notice please visit: ".$tender_link;
		open(out,">>$filename");

		print out "$sourcecw\t$tender_title\t$origin\t\t$Awarding_Authority\t$noticeSector\t$authorityDept\t$cy\t$country_code\t$Address\t$tender_link\t$authorityAddress2\t$authorityAddress3\t\t$defence\t$pubFlagCW\t$authorityTown\t$authorityCounty\t$online_only\t\t$frameworkAgreement\t$authorityPostcode\t$cy\t$country_code\t$Telephone\t$authorityFax\t$Email\t$temp_description_export\t$authorityWebAddress\t$authorityProfileURL\t$authorityContactSalutation\t$authorityContactFirstName\t$authorityContactLastName\t$authorityPosition\t$temp_site_export\t$awardProcedure\t$nc\t$trackerRef\t$Contract_Type\t$Deadline_Date\t$pr\t$CPV_Code\t$aa\t$nuts\t$Industry_Sector\t$origPublicationDate\t$Description\t$proofed\t$exported\t$Location\t$autoModifyDate\t$autoModifyTime\t$Authority_Reference\t$no_of_months\t$contract_start_date\t$contract_end_date\t$value1\t$currency\t$value1\t$amount_min\t$amount_max\t$Deadline_Description\t$Deadline_Date1\t$Deadline_time\t$d_RApost\t$tenderblock\n";
		close out;
		&Insert_Tender_Data_new($dbh,$Tender_ID,$tender_link,$sourcecw,$Awarding_Authority,$tenderblock,$origin,$noticeSector,$authorityDept,$country_code,$cy,$sourceURL,$Industry_Sector,$tender_title,$authorityName,$Address,$authorityAddress2,$authorityAddress3,$defence,$pubFlagCW,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$Telephone,$authorityFax,$Email,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$nc,$trackerRef,$awardProcedure,$Contract_Type,$Deadline_Date1,$pr,$CPV_Code,$aa,$origPublicationDate,$Description,$proofed,$exported,$Location,$Authority_Reference,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$value1,$currency,$amount_min,$amount_max,$Deadline_Description,$Deadline_time,$d_RApost,$nuts_code,$authorityPostcode);
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
		# exit;
	}	

  if($Tender_content1=~m/<a\s* class="next"\s*rel\=\"next\"\s*href\=\"([^>]*?)\">[^>]*?<\/a>/is)
     {
        my $nexturl="https://www.great.gov.uk".$1;
        $nexturl=~s/\&amp\;/&/igs;
		my ($Tender_content_next)=&Getcontent($nexturl);
		$Tender_content_next = decode_utf8($Tender_content_next);  
		$Tender_content1=$Tender_content_next;
        goto re;
     }		

  # }	 
	
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.great.gov.uk");
	$req->header("Content-Type"=> "text/html;charset=utf-8");
	# $req->header("Cookie"=> "JSESSIONID=IQWc8wgu5zuaAg6auxZ7zA__");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;

	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->header("Cookie"=> "JSESSIONID=IQWc8wgu5zuaAg6auxZ7zA__");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	decode_entities($Clean);
	return $Clean;
}


sub Insert_Tender_Data_new()
{
	my $dbh=shift;
my $Tender_ID=shift;
my $tender_link=shift;
my $sourcecw=shift;
my $Buyer=shift;
my $tenderblock=shift;
my $origin=shift;
my $noticeSector=shift;
my $authorityDept=shift;
my $country_code=shift;
my $cy=shift;
my $sourceURL=shift;
my $Industry_Sector=shift;
my $tender_title=shift;
my $authorityName=shift;
my $Address=shift;
my $authorityAddress2=shift;
my $authorityAddress3=shift;
my $defence=shift;
my $pubFlagCW=shift;
my $authorityTown=shift;
my $authorityCounty=shift;
my $online_only=shift;
my $frameworkAgreement=shift;
my $Telephone=shift;
my $authorityFax=shift;
my $Email=shift;
my $temp_description_export=shift;
my $authorityWebAddress=shift;
my $authorityProfileURL=shift;
my $authorityContactSalutation=shift;
my $authorityContactFirstName=shift;
my $authorityContactLastName=shift;
my $authorityPosition=shift;
my $temp_site_export=shift;
my $nc=shift;
my $trackerRef=shift;
my $awardProcedure=shift;
my $Contract_Type=shift;
my $Deadline_Date1=shift;
my $pr=shift;
my $CPV_Code=shift;
my $aa=shift;
my $origPublicationDate=shift;
my $Description=shift;
my $proofed=shift;
my $exported=shift;
my $Location=shift;
my $Authority_Reference=shift;
my $autoModifyDate=shift;
my $autoModifyTime=shift;
my $no_of_months=shift;
my $contract_start_date=shift;
my $contract_end_date=shift;
my $value1=shift;
my $currency=shift;
my $amount_min=shift;
my $amount_max=shift;
my $Deadline_Description=shift;
my $Deadline_time=shift;
my $d_RApost=shift;
my $nuts=shift;
my $authorityPostcode=shift;
$contract_end_date=&BIP_Tender_DB::Date_Formatting($contract_end_date,$Tender_ID);
$contract_start_date=&BIP_Tender_DB::Date_Formatting($contract_start_date,$Tender_ID);
$origPublicationDate=&BIP_Tender_DB::Date_Formatting($origPublicationDate,$Tender_ID);

	# my $bip_non_uk1				= shift;
	# my $tender_type1			=shift;
	my $tender_type1;
	# $InTendFlag1 = 'N' if ($InTendFlag1 eq '');
	# print "\nInTendFlag	:: $InTendFlag1\n";
	my $bip_non_uk1 = 'N';
$sourcecw=~s/\'/\'\'/igs;
$Buyer=~s/\'/\'\'/igs;
$tenderblock=~s/\'/\'\'/igs;
$origin=~s/\'/\'\'/igs;
$noticeSector=~s/\'/\'\'/igs;
$authorityDept=~s/\'/\'\'/igs;
$country_code=~s/\'/\'\'/igs;
$cy=~s/\'/\'\'/igs;
$sourceURL=~s/\'/\'\'/igs;
$Industry_Sector=~s/\'/\'\'/igs;
$tender_title=~s/\'/\'\'/igs;
$authorityName=~s/\'/\'\'/igs;
$Address=~s/\'/\'\'/igs;
$authorityAddress2=~s/\'/\'\'/igs;
$authorityAddress3=~s/\'/\'\'/igs;
$defence=~s/\'/\'\'/igs;
$pubFlagCW=~s/\'/\'\'/igs;
$authorityTown=~s/\'/\'\'/igs;
$authorityCounty=~s/\'/\'\'/igs;
$online_only=~s/\'/\'\'/igs;
$frameworkAgreement=~s/\'/\'\'/igs;
$Telephone=~s/\'/\'\'/igs;
$authorityFax=~s/\'/\'\'/igs;
$Email=~s/\'/\'\'/igs;
$temp_description_export=~s/\'/\'\'/igs;
$authorityWebAddress=~s/\'/\'\'/igs;
$authorityProfileURL=~s/\'/\'\'/igs;
$authorityContactSalutation=~s/\'/\'\'/igs;
$authorityContactFirstName=~s/\'/\'\'/igs;
$authorityContactLastName=~s/\'/\'\'/igs;
$authorityPosition=~s/\'/\'\'/igs;
$temp_site_export=~s/\'/\'\'/igs;
$nc=~s/\'/\'\'/igs;
$trackerRef=~s/\'/\'\'/igs;
$awardProcedure=~s/\'/\'\'/igs;
$Contract_Type=~s/\'/\'\'/igs;
$Deadline_Date1=~s/\'/\'\'/igs;
$pr=~s/\'/\'\'/igs;
$CPV_Code=~s/\'/\'\'/igs;
$aa=~s/\'/\'\'/igs;
$origPublicationDate=~s/\'/\'\'/igs;
$Description=~s/\'/\'\'/igs;
$proofed=~s/\'/\'\'/igs;
$exported=~s/\'/\'\'/igs;
$Location=~s/\'/\'\'/igs;
$Authority_Reference=~s/\'/\'\'/igs;
$autoModifyDate=~s/\'/\'\'/igs;
$autoModifyTime=~s/\'/\'\'/igs;
$no_of_months=~s/\'/\'\'/igs;
$contract_start_date=~s/\'/\'\'/igs;
$contract_end_date=~s/\'/\'\'/igs;
$value1=~s/\'/\'\'/igs;
$currency=~s/\'/\'\'/igs;
$amount_min=~s/\'/\'\'/igs;
$amount_max=~s/\'/\'\'/igs;
$Deadline_Description=~s/\'/\'\'/igs;
$Deadline_time=~s/\'/\'\'/igs;
$d_RApost=~s/\'/\'\'/igs;
$nuts=~s/\'/\'\'/igs;
$authorityPostcode=~s/\'/\'\'/igs;
my $other_information;
	# $tender_type1=~s/\'/\'\'/igs;
	# $tender_type1='';
	if (length($tenderblock)>30000)
	{
		$tenderblock = substr $tenderblock, 0, 30000;
	}	
		if ($Tender_ID==63)
	{
		$other_information = 'Please Click the "View Tenders" Section on the left side of the website for view the above tender. Click the "Further Details" button to view the Contract Notice. Please note you must be a registered user in order to access the tender documentation. Please register and login above website to participate in any available opportunities, download Tender documentation, submit your replies, and communicate with our buyers online. If you have already registered and are a returning user, simply log in on the side panel.';
	}	
	my $Sent_to = &d_Rapost_Value_Concatenation($tender_link,$Tender_ID);
	my $tender_type1="Normal";



		my $insertQuery;
	if($Tender_ID==69)
	{
	#$insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,created_date,nuts_code,award_procedure,supplier,D_award,tendersReceived,authoritypostcode,Site,price,Awarded_Status,InTend,other_information,BIP_Non_UK,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$tenderblock\',\'$Origin\',\'$Sector\',\'$TenderName\',\'$Industry_Sector\',\'$tender_title\',\'$title_check_duplicate\',\'$Awarding_Authority\',\'$Contract_Type\',\'$CPV_Code\',\'$Description\',\'$Location\',\'$Authority_Reference\',\'$Value\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date\')<1 then null else \'$Deadline_Date\' end),\'$Deadline_time\',\'$Sent_to\',NOW(),\'$Nuts_Code\',\'$Award_procedure\',\'$Supplier\',\'$D_award\',\'$tendersReceived\',\'$authoritypostcode\',\'$Site\',\'$price\',\'$Award_status\',\'$InTendFlag1\',\'$other_information\',\'$bip_non_uk1\',\'$tender_type1\')";
	$insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,origPublicationDate,description,proofed,exported,location,authority_reference,autoModifyDate,autoModifyTime,no_of_months,contract_start_date,contract_end_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$Buyer\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$origPublicationDate\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$autoModifyDate\',\'$autoModifyTime\',\'$no_of_months\',\'$contract_start_date\',\'$contract_end_date\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date1\')<1 then null else \'$Deadline_Date1\' end),\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	
	print "Query issue";
	}
	else
		{
	# $insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,created_date,nuts_code,award_procedure,supplier,D_award,tendersReceived,authoritypostcode,Site,price,Awarded_Status,InTend,BIP_Non_UK,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$tenderblock\',\'$Origin\',\'$Sector\',\'$TenderName\',\'$Industry_Sector\',\'$tender_title\',\'$title_check_duplicate\',\'$Awarding_Authority\',\'$Contract_Type\',\'$CPV_Code\',\'$Description\',\'$Location\',\'$Authority_Reference\',\'$Value\',\'$Deadline_Description\',(select case when CHAR_LENGTH(\'$Deadline_Date\')<1 then null else \'$Deadline_Date\' end),\'$Deadline_time\',\'$Sent_to\',NOW(),\'$Nuts_Code\',\'$Award_procedure\',\'$Supplier\',\'$D_award\',\'$tendersReceived\',\'$authoritypostcode\',\'$Site\',\'$price\',\'$Award_status\',\'$InTendFlag1\',\'$bip_non_uk1\',\'$tender_type1\')";
	# $insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,tender,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,origPublicationDate,description,proofed,exported,location,authority_reference,autoModifyDate,autoModifyTime,no_of_months,contract_start_date,contract_end_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$authorityName\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$origPublicationDate\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$autoModifyDate\',\'$autoModifyTime\',\'$no_of_months\',\'$contract_start_date\',\'$contract_end_date\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',\'$Deadline_Date1\',\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	$insertQuery = "insert into tender_data2 (process_id,url,sourcecw,authorityName,other_information,origin,sector,authorityDept,Country_Codes_ISOcountry,country,web_source,industry_Sector,title,title_duplicate_check,awarding_authority,authorityAddress1,authorityAddress2,authorityAddress3,defence,pubFlagCW,authorityTown,authorityCounty,online_only,frameworkAgreement,authorityTelephone,authorityFax,authorityEmail,temp_description_export,authorityWebAddress,authorityProfileURL,authorityContactSalutation,authorityContactFirstName,authorityContactLastName,authorityPosition,temp_site_export,nc,trackerRef,awardProcedure,contact_type,earliestDate,pr,cpv_code,aa,description,proofed,exported,location,authority_reference,no_of_months,contract_start_date,value,currency,amount,amount_min,amount_max,deadline_description,deadline_date,deadline_time,sent_to,nuts_code,authoritypostcode,created_date,Tender_Type) VALUES (\'$Tender_ID\',\'$tender_link\',\'$sourcecw\',\'$Buyer\',\'$tenderblock\',\'$origin\',\'$noticeSector\',\'$authorityDept\',\'$country_code\',\'$cy\',\'$sourceURL\',\'$Industry_Sector\',\'$tender_title\',\'$tender_title\',\'$authorityName\',\'$Address\',\'$authorityAddress2\',\'$authorityAddress3\',\'$defence\',\'$pubFlagCW\',\'$authorityTown\',\'$authorityCounty\',\'$online_only\',\'$frameworkAgreement\',\'$Telephone\',\'$authorityFax\',\'$Email\',\'$temp_description_export\',\'$authorityWebAddress\',\'$authorityProfileURL\',\'$authorityContactSalutation\',\'$authorityContactFirstName\',\'$authorityContactLastName\',\'$authorityPosition\',\'$temp_site_export\',\'$nc\',\'$trackerRef\',\'$awardProcedure\',\'$Contract_Type\',\'$Deadline_Date1\',\'$pr\',\'$CPV_Code\',\'$aa\',\'$Description\',\'$proofed\',\'$exported\',\'$Location\',\'$Authority_Reference\',\'$no_of_months\',\'$Deadline_Date1\',\'$value1\',\'$currency\',\'$value1\',\'$amount_min\',\'$amount_max\',\'$Deadline_Description\',\'$Deadline_Date1\',\'$Deadline_time\',\'$d_RApost\',\'$nuts\',\'$authorityPostcode\',NOW(),\'$tender_type1\')";
	print $insertQuery;
	}

	my $sth = $dbh->prepare($insertQuery);
	if($sth->execute())
	{
		print "tender_data InserQuery :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		open(ERR,">>Failed_Query_InsertAgain.txt");
		print ERR $insertQuery."\n";
		close ERR;
		$dbh=&DbConnection();
		&send_mail_InsertFailed($insertQuery,$Tender_ID);
	}
}
sub d_Rapost_Value_Concatenation()
{
	my $Tender_link	= shift;
	my $Tender_ID	= shift;
	
	my $sendtoo;
	if ($Tender_ID==1)
	{
		$sendtoo="For further information regarding the above contract notice please visit $Tender_link";
	}
		if (($Tender_ID==69) || ($Tender_ID==70) || ($Tender_ID==73) || ($Tender_ID==74))
	{
		$sendtoo="For further information regarding the above contract notice please visit $Tender_link";
	}
	if ($Tender_ID==6)
	{
		$sendtoo="Please log in or register at the following portal Web: $Tender_link to participate";
	}
	if ($Tender_ID==10)
	{
		$sendtoo="Note: This is a CompeteFor opportunity. Opportunities may be with both public and private sector businesses. To apply, please click the following link to register on the CompeteFor portal Web: $Tender_link";
	}
	if ($Tender_ID==14)
	{
		$sendtoo="You must register or log in at Web: $Tender_link to participate in this tender.";
	}
	if (($Tender_ID==16)||($Tender_ID==17)||($Tender_ID==19)||($Tender_ID==20)||($Tender_ID==21)||($Tender_ID==65)||($Tender_ID==27)||($Tender_ID==29)||($Tender_ID==31)||($Tender_ID==34)||($Tender_ID==38)||($Tender_ID==41)||($Tender_ID==42)||($Tender_ID==44)||($Tender_ID==51)||($Tender_ID==54)||($Tender_ID==55)||($Tender_ID==28)||($Tender_ID==67))
	{
		$sendtoo="For further information regarding the above contract notice please visit: $Tender_link";
	}
	if ($Tender_ID==22)
	{
		$sendtoo="To register your interest in the above opportunity please visit: $Tender_link";
	}
	if ($Tender_ID==24)
	{
		$sendtoo="For further information regarding the above contract notice please visit: http://www.finditinbirmingham.com/";
	}
	if ($Tender_ID==26)
	{
		$sendtoo="For further information regarding the above contract notice please visit: http://www.finditinworcestershire.com/";
	}
	if (($Tender_ID==32)||($Tender_ID==36)||($Tender_ID==60))
	{
		$sendtoo="Please log in or register at the following portal Web: $Tender_link to participate.";
	}
	if (($Tender_ID==47)||($Tender_ID==68))
	{
		$sendtoo="For further information/documentation regarding the above contract notice please visit $Tender_link";
	}
	if (($Tender_ID==49)||($Tender_ID==52)||($Tender_ID==58))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
		if (($Tender_ID==4))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
			if (($Tender_ID==63))
	{
		$sendtoo="For further information and instructions on how to register your interest in the above contract notice please visit: $Tender_link";
	}
	if (($Tender_ID==41))
	{
		$sendtoo="For further information regarding the above contract notice please visit: $Tender_link";
	}
	if ($Tender_ID>500)
	{
		$sendtoo="For further information and instructions in the above contract notice please visit website: $Tender_link";
	}
	if ($Tender_ID==513)
	{
		$sendtoo="For further information and instructions in the above contract notice please visit website: https://www.lgtenders.co.nz/";
	}
	return $sendtoo;
}		

sub send_mail_InsertFailed()
{
	my $insertQuery	= shift;
	my $tender_ID	= shift;
	
	my $MAILDETAILS = &ReadIniFile('MAILDETAILS');
	my %MAILDETAILS	= %{$MAILDETAILS};
	
	
	my $fulldate = strftime "%d %m %Y %A %H:%M:%S", localtime;

	my $subject	= "BIP Tender - InsertFailed - $tender_ID - $fulldate";
	my $host   	= $MAILDETAILS{'host'}; 
	my $from 	= $MAILDETAILS{'from'};
	my $user 	= $MAILDETAILS{'user'};
	my $to		= $MAILDETAILS{'to_insert_failed'};
	my $pass	= $MAILDETAILS{'password'};
	my $body	= "Hi team,<br><br>Please find the Below query has not inserted<br><br>Insert Query :: $insertQuery<br><br>"."Thanks,<br>MERIT IT";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
		
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	eval {$msg->send;};
	if($@)
	{
		print "\n\n>>>>>>>>Mail Failed>>>>>>>>\n\n";
	}
	else
	{
		print "\n\n>>>>>>>>Mail Sent>>>>>>>>\n\n";
	}
}

sub ReadIniFile()
{
	my $INIVALUE = shift;
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Tender.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}

sub Date_Formatting()
{
	my $date 		= shift;
	my $Company_ID	= shift;
	$date=~s/([A-Za-z]\s*)\.\s*/$1/igs;
	# print "\nBefore the Formatting : $date\n";
	# if ($Company_ID=~m/\b139\b|\b138\b/is)
	# {
		# print "Step 1\n";
		# if ($date=~m/^\s*([\d]+)(?:\/|\-|\.)([\d]+)(?:\/|\-|\.)([\d]{2,4})/is) #04.31.1990
		# {
			# my $YY = $3;
			# my $MM = $1;
			# my $DD = $2;
			# $DD = "0$DD" if (length($DD)==1);
			# $MM = "0$MM" if (length($MM)==1);
			# if ($MM>12)
			# {
				# my $DD1=$MM;
				# my $MM1=$DD;
				# my $date_formatted = "$YY-$MM1-$DD1";
				# return ($date_formatted);
			# }
			# else
			# {
				# my $date_formatted = "$YY-$MM-$DD";
				# return ($date_formatted);
			# }	
		# }
		
	# }
	if ($date=~m/^\s*([\d]+)\-([\d]+)\-([\d]{4})/is) #27-04-1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 2\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*([\d]+)\.([\d]+)\.([\d]{4})/is) #27.04.1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 3\n";
		return ($date_formatted);
		
	}
	elsif ($date=~m/^\s*([\d]+)\/([\d]+)\/([\d]{4})/is)	#27/04/1990
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 4\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*([\d]{4})\.([\d]+)\.([\d]+)/is) #2015.10.22
	{
		my $YY = $1;
		my $MM = $2;
		my $DD = $3;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 5\n";
		return ($date_formatted);
	}
	elsif ($date=~m/^\s*[^>]*?([\d]+)\/([\d]+)\/([\d]{2})/is) #27-04-16
	{
		my $YY = $3;
		my $MM = $2;
		my $DD = $1;
		$DD = "0$DD" if (length($DD)==1);
		$MM = "0$MM" if (length($MM)==1);
		$YY = "20$YY" if ((length($MM)==2));
		# my $date_formatted = "$3/$2/$1";
		my $date_formatted = "$YY-$MM-$DD";
		# print "Step 6\n";
		return ($date_formatted);
	}
	else #Monday, 23rd december 2015
	{
		my $dt = ParseDate($date);
		my $date_formatted;
		if ($dt=~m/([\d]{4})([\d]{2})([\d]{2})/is)
		{
			my $YY = $1;
			my $MM = $2;
			my $DD = $3;
			$DD = "0$DD" if (length($DD)==1);
			$MM = "0$MM" if (length($MM)==1);
			# $date_formatted = "$1-$2-$3";
			$date_formatted = "$YY-$MM-$DD";
		}
		# print "After the Formatting : $date_formatted\n";
		# print "Step 7\n";
		return ($date_formatted);
	}
	# print "Step never\n";
}