

import requests
import os, sys
import re
import xlwt
from googletrans import Translator
import redis
import time
import json
import pymysql
import xlwt
import imp
import dateparser
import warnings
warnings.filterwarnings('ignore')

# reload(sys)
# sys.setdefaultencoding('utf-8')

# Database_Connector = imp.load_source('Database_Connector','Database_Connector.py')
# connection = Database_Connector.connection_string()
# mysql_cursor = connection.cursor()
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

##Clean Function:::
def reg_clean(desc):
	# desc=desc.decode("utf-8")
	# desc=desc.replace(''b'', '')
	desc=desc.replace("'", "''")	
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc=desc.replace("''''", "''")
	return desc 

	
# conn=pymysql.connect(host='172.27.138.250',port=3306,user='root',password='admin',db='BIP_tender_analysis')
# cur=conn.cursor()
file_name=os.path.basename(__file__)
tender_id1 = file_name.replace("BIP_Tender_","")
Tender_ID = tender_id1.replace(".py","")
main_url_query = u"select tender_link from tender_master where id="+str(Tender_ID)
# Tender_ID = 533
	
##Redis Connection::::
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)

def redis_connection():
	red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
	return red_connec
	
def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = """{'q': '{}',
                               'target': 'en'
                               'source' : 'no'}""".format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'no'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    if isinstance(translated_sentence, str):
        return translated_sentence
    else:	

        return translated_sentence.decode("utf-8")
	
	
def program():

	proxies = {
	"http" :"http://172.27.137.192:3128",
	"https":"https://172.27.137.192:3128"
	}
	main_url = Database_Connector.select_query(mysql_cursor, main_url_query)
	
	# print main_url
	# raw_input("ddddd")
# main_url = "https://www.doffin.no/Notice?query=&PageNumber=1&PageSize=100&OrderingType=0&OrderingDirection=1&RegionId=&CountyId=&MunicipalityId=&IsAdvancedSearch=false&location=&NoticeType=&PublicationType=&IncludeExpired=false&Cpvs=&EpsReferenceNr=&DeadlineFromDate=&DeadlineToDate=&PublishedFromDate=&PublishedToDate="
	main_req = requests.get(main_url[0],proxies=proxies)
	main_cont = main_req.content.decode('utf-8', errors='ignore')
	# print main_cont
	# raw_input("ddddd")
	sub_url_re = '<img class=\"pull-right\"\s*src=\"\/Themes\/Doffin\/content\/Images\/flag_no.gif\"\/>(?:\s*<a[^>]*?>\s*<i\s*class=[^>]*?><\/i>\s*<\/a>|\s*)\s*<a\s*href="([^>]*?)">'
	sub_url = re.findall(sub_url_re, main_cont)
	count = 1
	# while (count < 35):
	for tender_link in sub_url:
		

		base_url = 'http://www.doffin.no' + str(tender_link)
		print (base_url)
		Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
		Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, base_url)
		if Duplicate_Check == "N":
			curr_date = dateparser.parse(time.asctime())
			created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			sub_req = requests.get(base_url,proxies=proxies)
			
			sub_cont = sub_req.content.decode('utf_8', errors='ignore')
			# with open('datacr.html','a') as f:
				# f.write(str(sub_cont))
			# raw_input("testing")

			# title_re = '<h3>Tittel<\/h3>\s*<\/div>\s*<\/div>\s*([\w\W]*?)\s*<div\s*class\=\"eps\-sub\-section\-head\s*clearfix\">'
			title_re = '<h3>Tittel<\/h3>\s*<\/div>\s*<\/div>\s*([\w\W]*?)\s*<\/div>'
			# address_re = '<h3>Navn\s*og\s*adresser<\/h3>\s*([\w\W]*?)\s*<div\s*class\=\"eps\-sub\-section\-head\s*clearfix\">'
			address_re = '<h3>Navn\s*og\s*adresser<\/h3>\s*([\w\W]*?)\s*NUTS-kode'
			type_procedure_re = 'Prosedyre\s*<\/h\d{1}>\s*[\w\W]*?<div\s*class\=\"eps\-sub\-section\-body\">([\w\W]*?)\s*<\/div>'
			contract_type_re = '<h3>Type\s*kontrakt<\/h3>\s*</div>\s*<\/div>\s*<div\s*class\=\"eps\-sub\-section\-body\">([\w\W]*?)\s*<\/div>'
			cpv_no_re = '<h3>Tilleggs\-CPV-kode\(r\)<\/h3>\s*([\w\W]*?)\s*<div\s*class\=\"eps\-sub\-section\-head\s*clearfix\">'
			cpv_no_re1 = '<h3>Hoved-CPV-kode<\/h3>\s*([\w\W]*?)\s*<div\s*class\=\"eps\-sub\-section\-head\s*clearfix\">'
			description_re = '<h3>Kort\s*beskrivelse<\/h3>\s*<\/div>\s*<\/div>\s*<div\s*class\=\"eps\-sub\-section\-body\">\s*<div\s*class\=\"eps\-text\">\s*([\w\W]*?)\s*<\/div>'
			# site_re = '<h3>Sted\s*for\s*gjennomf[\w\W]*?ring<\/h3>\s*<\/div>\s*<\/div>\s*<[^>]*?>\s*<[^>]*?>\s*<div\s*class\=\"nuts\">[^>]*?<\/div>\s*<div class="eps-text">([^>]*?)<\/div>'
			site_re = '<div class="eps-text">\s*<div class="nuts">NUTS-kode:[^>]*?<\/div>\s*<div class="eps-text">[^>]*?<\/div>\s*<\/div>\s*<div class="eps-text">[^>]*?\s*<div class="eps-text">([^>]*?)\s*<\/div>'
			ref_re = '<div\s*class\=\"eps\-text\">Referansenummer\:([^>]*?)<\/div>'
			value_re = '<h3>Estimert\s*totalverdi<\/h3>\s*<\/div>\s*<\/div>\s*<[\w\W]*?>Verdi\s*ekskl\.\s*MVA\:\s*([^>]*?)NOK<\/div>'
			deadline_date_re = '<div\s*class\=\"eps\-text\"\s*xmlns:n[^>]*?\=\"ted/[^>]*?/nuts\">Dato\:([^>]*?)\s*<\/div>'
			time_re = '<div\s*class\=\"eps\-text\"\s*xmlns:n[^>]*?\="ted/[^>]*?/nuts\">Lokal\s*tid\s*\:\s*([^>]*?)\s*<\/div>'


			title = re.findall(title_re, sub_cont, re.I)
			if title:
				title = title[0]
				title = re.sub(r'<[^>]*?>', ' ', title)
				title = re.sub(r"\s+", " ", title)
			else:
				title = ''
			title = translateText(str(title))
			print ("Title::", title)


			awarding_authority = re.findall(address_re, sub_cont, re.I)
			if awarding_authority:
				awarding_authority = awarding_authority[0]
				awarding_authority = re.sub(r"\s+", " ", awarding_authority)
				awarding_authority = re.sub(r'<[^>]*?>', ' ', awarding_authority)
				awarding_authority = awarding_authority.replace('\n', '')
				awarding_authority = re.sub("       ","",awarding_authority)
				awarding_authority = awarding_authority.replace('Del II: Kontrakten','')
			# awarding_authority = translateText (str(awarding_authority))
			#print 'Address::',awarding_authority

			award_procedure = re.findall(type_procedure_re, sub_cont, re.I)
			if award_procedure:
				award_procedure = award_procedure[0]
				award_procedure = re.sub(r'<[^>]*?>', ' ', award_procedure)
			else:
				award_procedure = ''
			award_procedure = translateText (str(award_procedure))
			award_procedure=award_procedure[0:150]
			#print "Award_Procedure::", award_procedure

			contact_type = re.findall(contract_type_re, sub_cont, re.I)
			if contact_type:
				contact_type = contact_type[0]
				contact_type = re.sub(r'<[^>]*?>', ' ', contact_type)
			else:
				contact_type = ''
			contact_type = translateText (str(contact_type))
			#print "contact_type::", contact_type
			main_cpv_code = re.findall(cpv_no_re1, sub_cont, re.I)
			if main_cpv_code:
				main_cpv_code = main_cpv_code[0]
				# main_cpv_code=main_cpv_code.decode("utf-8")
				main_cpv_code = re.sub(r"\s+", " ", main_cpv_code)
				main_cpv_code = re.sub(r"        ","",main_cpv_code)
				main_cpv_code = re.sub(r'<[^>]*?>', ' ', main_cpv_code)
				main_cpv_code = re.sub(r'\\xc2', ' ', main_cpv_code)
				main_cpv_code = re.sub(r'\\xa0', ' ', main_cpv_code)
			else:
				main_cpv_code = ''
			main_cpv_code = translateText(str(main_cpv_code))
			# main_cpv_code = main_cpv_code.decode('ASCII')
			print (main_cpv_code)	
			cpv_code = re.findall(cpv_no_re, sub_cont, re.I)
			if cpv_code:
				cpv_code = cpv_code[0]
				# cpv_code=cpv_code.decode("utf-8")
				cpv_code = re.sub(r"\s+", " ", cpv_code)
				cpv_code = re.sub(r"        ","",cpv_code)
				cpv_code = re.sub(r'<[^>]*?>', ' ', cpv_code)
				cpv_code = re.sub(r'\\xc2', ' ', cpv_code)
				cpv_code = re.sub(r'\\xa0', ' ', cpv_code)
			else:
				cpv_code = ''
			cpv_code = translateText(str(cpv_code))
			# cpv_code = cpv_code.decode('ASCII')
			# 'ASCII'
			print (cpv_code)
			#print "Additional cpv_no::", cpv_code

			description = re.findall(description_re, sub_cont, re.I)
			if description:
				description = description[0]
				description = re.sub(r'<[^>]*?>', ' ', description)
			else:
				description = ''

			description = translateText (str(description))
			# description =description.decode("utf-8")
			# description = reg_clean(description)
			if len(description) > 20000:
				description = description[0:20000]
			#print 'Description::', description

			location = re.findall(site_re, sub_cont, re.I)
			if location:
				location = location[0]
			else:
				location = ''
			# location = reg_clean(location)
			# location = location))
			# print 'Site:::', location

			authority_reference = re.findall(ref_re, sub_cont, re.I)
			if authority_reference:
				authority_reference = authority_reference[0].strip()
			else:
				authority_reference = ''
			authority_reference = translateText (str(authority_reference))
			#print 'Reference No:::', authority_reference.strip()

			value = re.findall(value_re, sub_cont, re.I)
			if value:
				value = value[0]
			else:
				value = ''
			# value = translateText (str(value))
			#print 'Total Value::', value
			
			deadline_description = 'Deadline for receipt of tenders or requests for participation:'

			deadline_date = re.findall(deadline_date_re, sub_cont, re.I)
			if deadline_date:
				deadline_date = deadline_date[0].strip()
				deadline_date = str(deadline_date).replace('&nbsp;', '')
				dt = dateparser.parse(deadline_date)
				FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
			else:
				deadline_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
				FormattedDate = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			FormattedDate = translateText (str(FormattedDate))
			print ('Date:::', FormattedDate.strip())

			deadline_time = re.findall(time_re, sub_cont, re.I)
			if deadline_time:
				deadline_time = deadline_time[0].strip()
			else:
				deadline_time = ''

			#deadline_time = translateText (str(deadline_time))
			print ('time:::', deadline_time.strip())

			summary ='The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website:'
			other_information = summary + str(base_url)
			# other_information = translateText (str(other_information))
			print ('other_information::',other_information)
			
			# cpv_code=cpv_code.decode("utf-8")
			orgin = 'European/Non Defence'
			sector = 'European Translation'
			country = 'NO'
			websource = 'Norway: DOFFIN National Procurement Portal'
			# title=title.replace("'","''").strip()
			# awarding_authority=awarding_authority.replace("'","''").strip()
			# award_procedure=award_procedure.replace("'","''").strip()
			# contact_type=contact_type.replace("'","''").strip()
			# description=description.replace("'","''").strip()
			# main_cpv_code=main_cpv_code.replace("'","''").strip()
			# cpv_code=cpv_code.replace("'","''").strip()
			cpv_code = main_cpv_code + " " + cpv_code
			# location=location.replace("'","''").strip()
			# authority_reference=authority_reference.replace("'","''").strip()
			# value=value.replace("'","''").strip()
			# deadline_description=deadline_description.replace("'","''").strip()
			# title=title.replace("''","'")
			# awarding_authority=awarding_authority.replace("''","'")
			# award_procedure=award_procedure.replace("''","'")
			# contact_type=contact_type.replace("''","'")
			# description=description.replace("''","'")
			# main_cpv_code=main_cpv_code.replace("''","'")
			# cpv_code=cpv_code.replace("''","'")
			# cpv_code = main_cpv_code + " " + cpv_code
			# location=location.replace("''","'")
			# authority_reference=authority_reference.replace("''","'")
			# value=value.replace("'","''").strip()
			# deadline_description=deadline_description.replace("'","''")

			insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(base_url), str(), str(orgin), str(sector), str(country), str(websource), str(title), str(title),str(awarding_authority), str(award_procedure), str(contact_type), str(cpv_code), str(description), str(location), str(authority_reference), str(value), str(deadline_description), str(FormattedDate), str(deadline_time),str(), str(other_information), str('Y'),str(created_date))
			insertQuery = insertQuery.replace("'b'","'")
			insertQuery = insertQuery.replace("'',","',")
			insertQuery = insertQuery.replace(",',",",'',")
			insertQuery = insertQuery.replace(",'',',",",'','',")
			try:
				val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
			# print "insertion status :", val
				if val == 1:
					print ("1 Row Inserted Successfully")
				else:
					print ("Insert Failed")
					print (insertQuery)
			except Exception as e:
			# print '**translate 4 **', e
				next		
			count = count+1
	connection.close()
		
if __name__== "__main__":

	program()
	
	
	

	



