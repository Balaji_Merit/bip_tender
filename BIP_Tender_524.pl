#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;

my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# $Tender_Url 			= "https://biznet.ct.gov/SCP_Search/Default.aspx";
# my ($HomeContent,$CookieeeeeePOST)=&Getcontent5($Tender_Url);
# $CookieeeeeePOST=~s/\;[^>]*?$/;/igs;
# print ("Coo :: $CookieeeeeePOST\n");
# my ($Tender_content1,$Cookie)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
# open(ts,">Tender_content524.html");
# print ts "$Tender_content1";
# close ts;

$Tender_Url 			= "https://biznet.ct.gov/SCP_Search/BidResults.aspx";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####

my ($Tender_content1,$Cookie)=&Getcontent1($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content524.html");
print ts "$Tender_content1";
close ts;
# exit;
my $count=1;
#### To get the tender link ####
top:


	# while($Tender_content1=~m/<td style="[^>]*?"><a href="([^>]*?)">/igs)
	# {
	# my $inner_content=$1;
	# print $inner_content;
	
	# my ($Tender_content2,$Referer)=&Getcontent1($inner_content);

		while($Tender_content1=~m/<td align="center" valign="top" style="[^>]*?">([^>]*?)<br\s*\/>([^>]*?)<\/td>[\w\W]*?<A Href="(BidDetail\.aspx[^>]*?)">[^>]*?<\/A><\/td>/igs)
	{
		print " youre in while loop";
		my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		my $tender_title;
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1);
		$Deadline_time=&clean($2);
		my $tender_link='https://biznet.ct.gov/SCP_Search/'.$3;
		
		
		if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		$Deadline_time=~s/am//igs;
		$Deadline_time=~s/AM//igs;
			
		$Deadline_Description="Due Date";
		
		print $tender_link;
		
			my ($tender_link_content1,$Referer)=&Getcontent2($tender_link);
	# while($tender_link_content1=~m/<table class='ContentAccordionFormat'>([\w\W]*?)<div id="ContentPlaceHolder1" class="HeaderAccordionPlusFormat">/igs)
	# {
	# my $block=$1;

	if($tender_link_content1=~m/<span id="lbSummary">([^>]*?)<\/span><\/td>/is)
	{
	$tender_title=$1;
	}
	if($tender_link_content1=~m/<span id="lbOrganization">([^>]*?)<\/span><\/b><\/td>[\w\W]*?(Contact Name[\w\W]*?)Additional\s*Description/is)
	{
	$Awarding_Authority=$1." ".$2;
	$Awarding_Authority=&BIP_Tender_DB::clean($Awarding_Authority);
	$Awarding_Authority=&clean($Awarding_Authority);
	}
	if($tender_link_content1=~m/<span id="lbBidNumber">([^>]*?)<\/span><\/b><\/td>/is)
	{
	$Authority_Reference=$1;
	}
	if($tender_link_content1=~m/<span id="lbAdditionalSummary">([^>]*?)<\/span><\/td>/is)
	{
	$Description=$1;
	$Description=&BIP_Tender_DB::clean($Description);
	$Description=&clean($Description);
	}

	if($tender_link_content1=~m/<span id="lbBidEndDate">(\d{1,2})\/(\d{1,2})\/(\d{4})<\/span><\/b><\/td>/is)
	{
	$Deadline_Date=&BIP_Tender_DB::Date_Formatting($2."/".$1."/".$3);
	}
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	if($tender_link_content1=~m/(Conference\s*Date[\w\W]*?)<\/table>/is)
	{
	$otherInformation=$1;
	$otherInformation = &BIP_Tender_DB::clean($otherInformation);
	$otherInformation=&clean($otherInformation);
	}
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
		if($tender_link_content1=~m/<span id="lbOrganization">([\w\W]*?)Administered by:<br\s*\/>/is)
	{
	$tenderblock=$1;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	}
	elsif($tender_link_content1=~m/<table id="Page_Header"[^>]*?>([\w\W]*?)<table id="Page_Footer"/is)
	{
	$tenderblock=$1;
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	$tenderblock=&clean($tenderblock);
	}
	$tenderblock=~s/Current User\: Home Biznet Menu Log In\/Out Search Search Results Results Detail SCP Solicitation Details State Contracting Portal Solicitation Details\s*//igs;
	$cy='US';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
}
# }

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $CookieeeeeePOST=shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "biznet.ct.gov");
	$req->header("Referer"=> "https://biznet.ct.gov/SCP_Search/Default.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.223422027.1610741195; _hjid=8a567a0c-c589-4b9c-8414-c76ae6d9f8c9; ASP.NET_SessionId=xlsbmtpkk5r5m2se0zn3jdpt; _hjTLDTest=1; _hjAbsoluteSessionInProgress=0; _gid=GA1.2.1407785966.1623002480; _gat_UA-70999654-1=1");
	$req->header("Cookie"=> $CookieeeeeePOST);

	
	# Cookie: ASP.NET_SessionId=n3mbdynkcfouwinkkzjhhlv4


	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $session = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "biznet.ct.gov");
	$req->header("Referer"=> "https://biznet.ct.gov/SCP_Search/Default.aspx");
	# $req->header("Cookie"=> "_ga=GA1.2.765990050.1509460869; ASP.NET_SessionId=5y4h2lgnqu22l4qcpr2aueia");
	$req->header("Cookie"=> "_ga=GA1.2.223422027.1610741195; ASP.NET_SessionId=xlsbmtpkk5r5m2se0zn3jdpt");
	# $req->header("Cookie"=> "$session");
	
	# Cookie: ASP.NET_SessionId=n3mbdynkcfouwinkkzjhhlv4


	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent2
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "biznet.ct.gov");
	$req->header("Referer"=> "https://biznet.ct.gov/SCP_Search/BidResults.aspx");
	$req->header("Cookie"=> "ASP.NET_SessionId=g2lgtm0h0azop31mzttzd0vk");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Getcontent4
{
	my $url = shift;
	my $Cookie_JSESSIONID = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "biznet.ct.gov");
	$req->header("Referer"=> "https://biznet.ct.gov/SCP_Search/Default.aspx");
	$req->header("Cookie"=> "$Cookie_JSESSIONID");
	# $req->header("Cookie"=> "ASP.NET_SessionId=nd1lv5rl5kqzarabtvr3cxx5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "biznet.ct.gov");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=utf-8");

	$req->header("Referer"=> "https://biznet.ct.gov/SCP_Search/Default.aspx");
	$req->header("Cookie"=> "_ga=GA1.2.1659672016.1505287316; ASP.NET_SessionId=4etexhmc1b4k3hv21zhsxh1t");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	decode_entities($Clean);
	return $Clean;
}
