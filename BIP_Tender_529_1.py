# -*- coding: utf-8 -*-
import os, sys
import requests
import re
import xlwt
import imp
import pymysql
import dateparser
import redis
import time
import warnings
import datetime
from datetime import datetime
from datetime import date
warnings.filterwarnings('ignore')
from googletrans import Translator

# reload(sys)
# sys.setdefaultencoding('utf-8')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()

base_url = 'http://www.przetargi.egospodarka.pl'
block_re = '<tr>([\w\W]*?)<\/tr>'
title_re = 'Nazwa\s*nadana[\w\W]*?\"\>([^>]*?)<\/span>'
dates_re = r'Daty\s*\:\s*publikacji\s*\/\s*zak[^>]*?enia\"\>([^>]*?)\s*\<br\>([^>]*?)\s*<'
sub_url_re = 'Przedmiot zam[\w\W]*?wienia\"\>\s*\<a\s*[\w\W]*?href\=\"([^>]*?)\"\>([^>]*?)\<'
title_attribute_re = 'Nazwa\s*nadana[\w\W]*?">([\w\W]*)<\/span'
name_address_re = '<p\s*[\w\W]*?NAZWA\s*\I\s*ADRES\:\s*([\w\W]*?)<\/p>'
awardProcedure_re = 'zastosowanie_procedury[\w\W]*?\"\>([\w\W]*?)<\/span>'
cpv_code_re = 'kod CPV:[\w\W]*?glowny[\w\W]*?\">([\w\W]*?)\s*<\/TABLE>'
description_re = 'opis\s*przedmiotu[\w\W]*?\:([\w\W]*?)<\/span>'
reference_re = 'numer\_referencyjny\"\>\s*([^>]*?)\s*\<\/span\>'
reference_re1 = '(?:<p><b>Nr\:<\/b>|<span id="[^>]*?Ogloszenia">)\s*([^>]*?)\s*<\/'
value_contract_re = 'wartosc\_zamowienia\_calosc\"\>([^>]*?)<\/span>'
time_read_line = 'Time limit for receipt of tenders or requests to participate:'
d_ra_date_re = 'ctl00\_ContentPlaceHolder1\_IV\_4\_4\_data\"\>([^>]*?)<\/span>'
d_ra_time_re = 'ContentPlaceHolder1\_IV\_4\_4\_godzina\"\>([^>]*?)<\/span\>'
summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
Tender_ID = 529
nxt_re = 'nexpage2[\w\W]*?href\=\"([^>]*?)\"\>nast[\w\W]*?pna[\w\W]*?\<\/a\>'

Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
#print "re----->",Retrive_duplicate


def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()
    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(str(InputText))
                translated_sentence = translated.text

            except Exception as e:
                # print 'Error translate 1**',e
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'pl'
                               }
                    '''Log file to check google translation'''
                    with open('descriptoin1.txt', 'a') as dees:
                        dees.write('\n=================================1============================\n')
                        dees.write(str(payload))
                        dees.write('\n==================================1===========================\n')

                    s = requests.post(url, data=payload)
                    with open('descriptoin1.txt', 'a') as dees:
                        dees.write(str(s.text))
                        dees.write('\n------------------------------1--------------------------------\n')

                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    print ('Error translate 2 **', e)
                    # raw_input('***raw_input*****')
    else:
        # do translate
        try:
            # print "translate 3 ",InputText
            translated = translator.translate(InputText)
            # print "translated_sentence:::",InputText,translated
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                payload = {'q': str(InputText),
                           'target': 'en',
                           'source' : 'pl'
                            }
                with open ('descriptoin1.txt','a') as dees:
                    dees.write('\n=============================2================================\n')
                    dees.write(str(payload))
                    dees.write('\n=============================2================================\n')

                s = requests.post(url, data=payload)

                with open ('descriptoin1.txt','a') as dees:
                    dees.write(str(s.text))
                    dees.write('\n----------------------------2----------------------------------\n')
                value = s.json()
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                redis_con.set(InputText, translated_sentence)
            except Exception as e:
                print ('**translate 4 **', e)
                # raw_input('***raw_input*****')


    return translated_sentence


def reg_clean(desc):
    # desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')
    desc = desc.replace('&quot;', '')
    desc = re.sub(r'<[^<]*?>', ' ', str(desc))
    # desc = re.sub(r'\r\n', '', str(desc), re.I)
    # desc = re.sub(r"\r\n", '', str(desc), re.I)
    # desc = re.sub(r'\t', " ", desc, re.I)
    # desc = re.sub(r'\s\s+', " ", desc, re.I)
    # desc = re.sub(r"^\s+\s*", "", desc, re.I)
    # desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"&rsquo;", "'", desc, re.I)
    desc = re.sub(r"&ndash;", "-", desc, re.I)
    desc = re.sub(r"&quot;", "'", desc, re.I)

    return desc


def req_content(url):
    req_url = requests.get(url)
    url_content = req_url.content.decode('utf-8', errors = 'ignore')
    url_content = req_url.content
    return url_content

def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def main():
    global n
    global main_url
    while True:
        req_url = requests.get(main_url)
        url_content = req_url.content.decode('utf-8', errors = 'ignore')
        url_content = req_url.content.decode('utf-8', errors = 'ignore')
        main_content = url_content
        blocks = regxx(block_re,main_content)

        for block in blocks[1:]:
            print ("NO: ", n)
            n = n + 1
            dates = regxx(dates_re,block)

            if dates:
                publication = dates[0][0]
                completion = dates[0][1]
            else:
                publication = ''
                completion = ''

            sub_url = regxx(sub_url_re,block)
            if sub_url:
                sub_urll = sub_url[0][0]
                Ordered_object = sub_url[0][1]
                sub_url_full = base_url + sub_urll
            else:
                Ordered_object = ''
            req_url = requests.get(sub_url_full)
            url_content = req_url.content.decode('utf-8', errors = 'ignore')
            sub_content = req_url.content.decode('utf-8', errors = 'ignore')
            # sub_content = req_content(sub_url_full)
            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url_full)
            if Duplicate_Check == "N":
                curr_date = dateparser.parse(time.asctime())
                created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                title_attribute = regxx(title_attribute_re,sub_content)
                if title_attribute:
                    title_attribute = title_attribute[0]
                else:
                    title_attribute = ''
                title = regxx(title_re,sub_content)
                if title:
                    title = title[0]
                    title = reg_clean(title)
                    title = translateText(title)
                    title = title.replace("'", "''")
                else:
                    title = ''
                name_address = regxx(name_address_re,sub_content)
                if name_address:
                    name_address1 = name_address[0]
                    name_address1 = reg_clean(name_address)
                    # name_address = translateText(str(name_address))
                    name_address = name_address.replace("'", "''")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("±", "ą")
                    name_address = name_address.replace("ê","ę")
                    name_address = name_address.replace("¿","ż")
                    name_address = name_address.replace("æ","ć")
                    name_address = name_address.replace("ñ","ń")
                    name_address = name_address.replace("³","ł")
					
                else:
                    name_address = ''
                awardProcedure = regxx(awardProcedure_re,sub_content)
                if awardProcedure:
                    awardProcedure = awardProcedure[0]
                    awardProcedure = reg_clean (awardProcedure)
                    awardProcedure = str(awardProcedure)
                    awardProcedure = awardProcedure.replace("'", "''")
                else:
                    awardProcedure = ''
                cpv_code = regxx(cpv_code_re,sub_content)
                if cpv_code:
                    cpv_code = cpv_code[0]
                    cpv_code = reg_clean (cpv_code)
                    cpv_code = translateText(str(cpv_code))
                    cpv_code = cpv_code.replace("'", "''")
                else:
                    cpv_code = ''
                description = regxx(description_re,sub_content)
                if description:
                    description = description[0]
                    description = reg_clean (description)
                    description = translateText(str(description))
                    description = description.replace("'", "''")
                else:
                    description = ''
                reference = regxx(reference_re,sub_content)
                if reference:
                    reference = reference[0]
                    reference = reg_clean (reference)
                    reference = translateText(str(reference))
                    reference = "Reference No: " + reference.replace("'", "''")
                else:
                    reference = ''
                reference1 = regxx(reference_re1,sub_content)
                if reference1:
                    reference1 = reference1[0]
                    reference1 = reg_clean(reference1)
                    reference1 = translateText(str(reference1))
                    reference1 = "Announcement No: " + reference1.replace("'", "''")
                else:
                    reference1 = ''
                reference = str(reference1) + str(reference)
                value_contract = regxx(value_contract_re,sub_content)
                if value_contract:
                    value_contract = value_contract[0]
                    value_contract = reg_clean (value_contract)
                    value_contract = translateText(str(value_contract))
                else:
                    value_contract = ''
                ra_date = regxx(d_ra_date_re,sub_content)
                FormattedDate = ''
                if ra_date:
                    ra_date = ra_date[0]
                    ra_date = reg_clean (ra_date)
                    ra_date = translateText(str(ra_date))
                    T_dead_line_date = str(ra_date)+" 00:00:00"
                    # dt = dateparser.parse(T_dead_line_date)
                    dt = datetime.strptime(T_dead_line_date,'%Y-%m-%d %H:%M:%S')
                    FormattedDate = dt
                    # if dt:
                        # FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                # else:
                    # pass
                d_ra_time = regxx(d_ra_time_re,sub_content)
                if d_ra_time:
                    d_ra_time = d_ra_time[0]
                    d_ra_time = reg_clean (d_ra_time)
                    d_ra_time = translateText(str(d_ra_time))
                    d_ra_time = d_ra_time.replace("'", "''")
                else:
                    d_ra_time = ''
                summary_link = summary + sub_url_full

                summary_cont = summary + sub_url_full
                orgin = 'European/Non Defence'
                sector = 'European Translation'
                country = 'PL'
                websource = 'Poland: Przetargi'

                if FormattedDate:
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, sub_url_full, str(), orgin, sector, country, websource,title, title, name_address, awardProcedure, str(), cpv_code,description, str(), reference, value_contract, time_read_line, FormattedDate,d_ra_time,str(), summary_cont, 'Y',created_date)
                else:
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,sent_to,other_information,BIP_Non_UK) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(Tender_ID), str(sub_url_full), str(), str(orgin), str(sector), str(country), str(websource),str(title), str(title), str(name_address), str(awardProcedure), str(), str(cpv_code),str(description), str(), str(reference), str(value_contract), str(), str(summary_cont), str('Y'))
                val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # print "insertion Status :",val
                if val == 1:
                    print ("1 Row Inserted Successfully")
                else:
                    print ("Insert Failed")
        nxt = regxx(nxt_re, main_content)
        if nxt:
            nxt = base_url + nxt[0]
            main_url = nxt
        else:
            print ("<----Over---->")
            break
if __name__== "__main__":
    main_url = 'http://www.przetargi.egospodarka.pl/search.php?submitted=1&status%5B%5D=1&mode%5B%5D=2&mode%5B%5D=1&mode%5B%5D=4&mode%5B%5D=8&or=&publish_date=0&deadline=0&province=0&sort=relevance'

    n = 1
    main()
