#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = '516';
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");

$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];
# $Tender_Url="https://www.philgeps.gov.ph/GEPS/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&BusCatID=157&type=category&ClickFrom=OpenOpp";
$Tender_Url="https://www.philgeps.gov.ph/GEPS/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&ClickFrom=OpenOpp&Result=3";
print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	
# $Tender_Url="https://philgeps.gov.ph/GEPSNONPILOT/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&ClickFrom=OpenOpp&Result=3";
# $Tender_Url="https://www.philgeps.gov.ph/GEPS/Tender/SplashOpenOpportunitiesUI.aspx?ClickFrom=OpenOpp&menuIndex=3";
#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my $i=6000;
### Ping the first level link page ####
# my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content5181.html");
print ts "$Tender_content1";
close ts;
	# my $Referer='https://www.gebiz.gov.sg/ptn/opportunity/BOListing.xhtml';
# my $post_url='https://www.gebiz.gov.sg/ptn/opportunity/opportunityDetails.xhtml';
# my $Search_po_Content1='contentForm=contentForm&contentForm%3Aj_idt198_listButton2_HIDDEN-INPUT=&contentForm%3Aj_idt488%3Aj_id34%3AdatatableNameLocation%3AdatatableNameLocation_inputHidden=&javax.faces.ViewState=5463701771893953202%3A-6137200045418060473&javax.faces.source=contentForm%3Aj_idt347_ajax_ONLOAD-BUTTON&javax.faces.partial.event=click&javax.faces.partial.execute=contentForm%3Aj_idt347_ajax_ONLOAD-BUTTON%20contentForm%3Aj_idt347&javax.faces.partial.render=contentForm%3Aj_idt347%20contentForm%3AnoticeAttId2&javax.faces.behavior.event=action&javax.faces.partial.ajax=true';
# my ($Result_content1)=&Postcontent($post_url, $Referer, $Search_po_Content1);
	# open(out,">result565.html");
	# print out "$Result_content1";
	# close out;
my $count=1;
my $Tender_content2;
#### To get the tender link ####


my $tender_category_content1;
top:
while($Tender_content1=~m/<td class="GridItemTD">\s*<a id="[^>]*?" href="([^>]*?)">/igs)
			{
			my $tender_link="https://www.philgeps.gov.ph/GEPS/Tender/".$1; 
			my ($tender_link_content1,$Referer);
			$tender_link=~s/amp;//igs;
			$tender_link_content1=&Getcontent($tender_link);
			$tender_link_content1 = decode_utf8($tender_link_content1);  
my ($Origin,$Sector,$cy,$title,$Awarding_Authority,$awardProcedure,$contractType,$cpvNos,$Description,$site,$authorityRefNo,$value,$cw_Radeadline,$d_RA,$d_Ratime,$d_Rapost,$otherInformation,$Authority_Reference,$Industry_Sector,$Contract_Type,$CPV_Code,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure,$other_information,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
		# my $tender_link;	
	my $tender_title;
			if($tender_link_content1=~m/Title<\/span><\/TD>\s*<TD colSpan="2"><span id="[^>]*?" class="[^>]*?">([^>]*?)<\/span><\/TD>/is)
		{
		$tender_title=$1;
		$tender_title=&clean($tender_title);
		}
		if($tender_link_content1=~m/Approved Budget for the Contract:<\/span><\/TD>([\w\W]*?)<\/span><\/TD>/is)
		{
		$Value=&clean($1);
		}
		if($tender_link_content1=~m/Procuring Entity<\/span><\/TD>\s*<TD colSpan="2"><span id="[^>]*?" class="[^>]*?">([^>]*?)<\/span><\/TD>/is)
		{
		$Awarding_Authority=$1;
		}
		if($tender_link_content1=~m/Contact Person:<\/span><\/TD>\s*<TD[\w\W]*?5">([\w\W]*?)<\/span><\/TD/is)
		{
		$Awarding_Authority=$Awarding_Authority.' '.$1;
		}
		if($tender_link_content1=~m/Reference Number<\/span><\/TD>\s*<TD colSpan="2"><span id="[^>]*?" class="[^>]*?">([^>]*?)<\/span><\/TD>/is)
		{
		$Authority_Reference=$1;
		$Authority_Reference=~s/\:\s*//igs;
		$Authority_Reference=&clean($Authority_Reference);
		}

		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=&Trim($Awarding_Authority);
		$Awarding_Authority = &BIP_Tender_DB::clean($Awarding_Authority);
		# $Authority_Reference=&BIP_Tender_DB::clean($Authority_Reference);
		# $Authority_Reference=&clean($Authority_Reference);
		if($tender_link_content1=~m/Procurement Mode:<\/span><\/TD>\s*<TD[^>]*?>\s*<span id="[^>]*?" class="[^>]*?">\s*([^>]*?)\s*<\/span><\/TD>/is)
		{
			$awardProcedure=$1;
			$awardProcedure = &BIP_Tender_DB::clean($awardProcedure);
			$awardProcedure=&clean($awardProcedure);
			print $awardProcedure;
			# exit;
		}
	
		if($tender_link_content1=~m/Description<\/span>([\w\W]*?)<\/TABLE>/is)
		{
		$Description=$1;
		$Description=~s/\-\-\>//igs;
		$Description=&BIP_Tender_DB::clean($Description);
		$Description=&clean($Description);
		$Description=&Trim($Description);
		}
		# elsif($tender_link_content1=~m/Desc:\s*<\/b>[\w\W]*?>([^>]*?)<\/div>/is)
		# {
		# $Description=$1;
		# $Description=~s/mets-field">//igs;
		# $Description=&BIP_Tender_DB::clean($Description);
		# $Description=&clean($Description);
		# $Description=&Trim($Description);
		# }
			if (length($Description)>24000)
	{
		$Description = substr $Description, 0, 24000;
	}
		if($tender_link_content1=~m/Area of Delivery<\/span><\/TD>\s*<TD[\w\W]*?5">([\w\W]*?)<\/span><\/TD>/is)
		{
		$Location=$1;
		$Location=&clean($Location);
		$Location=&Trim($Location);
		$Location=&BIP_Tender_DB::clean($Location);
		}
		# elsif($tender_link_content1=~m/Region\s*<\/span>([\w\W]*?)<\/div>/is)
		# {
		# $Location=$1;
		# $Location=&BIP_Tender_DB::clean($Location);
		# $Location=&clean($Location);
		# }
		if($tender_link_content1=~m/Closing Date\s*\/\s*Time<\/span><\/TD>\s*<TD[\w\W]*?5">([\w\W]*?)<\/span><\/TD>/is)
		{
		my $temp_dt=$1;
		# $Deadline_time=$2;
		# $Deadline_time=~s/\&nbsp\;/ /igs;

		if($temp_dt=~m/(\d{1,2})\/(\d{1,2})\/(\d{4})/is)
		{
		$Deadline_Date=&BIP_Tender_DB::Date_Formatting($1."/".$2."/".$3);
		
		# $Deadline_time=$4;
		}
		
		if($temp_dt=~m/(\d{1,2})(?:\.|\:)(\d{1,2})\s*(?:pm|PM)/is)
		{
		my $hh=$1;
		$Deadline_time=($hh+12).":".$2 if($hh != 12);
		$Deadline_time=$hh.":".$2 if($1 == 12);
		}
		elsif($temp_dt=~m/(\d{1,2})(?:\.|\:)(\d{2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":".$2 if($hh != 12);
		$Deadline_time="00:".$2 if($hh == 12);
		}
		}
		# if($tender_link_content1=~m/Closing Date([\w\W]*?)<\/div>/is)
		# {
		# my $temp_dt=$1;
	
		# if($temp_dt=~m/(\d{4})\-(\d{1,2})\-(\d{1,2})\s*(\d{1,2}\:\d{1,2}:\d{1,2}\s*(?:AM|PM))/is)
		# {
		# $Deadline_Date=&BIP_Tender_DB::Date_Formatting($3."/".$2."/".$1);
		
		# $Deadline_time=$4;
		# }
		# }
		# if($Deadline_time=~m/(\d{1,2})(?:\.|\:)(\d{2})(?:\.|\:)\s*\d{2}\s*(?:pm|PM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=($hh+12).":".$2 if($hh != 12);
		# $Deadline_time=$hh.":".$2 if($1 == 12);
		# }
		# if($Deadline_time=~m/(\d{1,2}):(\d{2})\s*(?:am|AM)/is)
		# {
		# my $hh=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		# }
			# $Deadline_time=~s/am//igs;
		# $Deadline_time=~s/AM//igs;
		
		$Deadline_Description="Closing Date/Time";
		
		print $tender_link;
		
		
# my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
# my @Processed_Title=@$Processed_Title;
# my @Processed_Url  =@$Processed_Url;
# my @Processed_Date  =@$Processed_Date;
	
	#Duplicate check
	my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
	if ($duplicate eq 'Y')
	{
		print "\nAlready Existing this Tender Data\n";
		next;
	}
	else
	{
		print "\nThis is newly Released Tender\n";
	}
	
	my $BIP_Non_UKFlag="Y";
	my ($tenderblock);
	if($tender_link_content1=~m/(Reference Number[\w\W]*?)<input type="submit" name="btnBack4" value="Back" id="btnBack4" class="Button" /is)
	{
	$tenderblock=$1;	
	$tenderblock=~s/\t/ /igs;
	$tenderblock=~s/\|/ /igs;
	$tenderblock=&clean($tenderblock);
	$tenderblock=&Trim($tenderblock);
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);
	}
	if (length($tenderblock)>24000)
	{
		$tenderblock = substr $tenderblock, 0, 24000;
	}
	$cy='PH';

		# open (RE,">>Digital526.txt");
		# print RE "$Tender_ID\t$tender_link\t$tenderblock\t$Origin\t$Sector\t$TenderName\t$Industry_Sector\t$tender_title\t$Awarding_Authority\t$Contract_Type\t$CPV_Code\t$Description\t$Location\t$Authority_Reference\t$Value\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$nuts_code\t$award_procedure\t$other_information\n";
		# close RE;
&BIP_Tender_DB::Insert_Tender_Data1($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$cy,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$awardProcedure,$award_status,$Supplier,$D_award,$tendersReceived,$authoritypostcode,$Site,$price,$InTendFlag,$otherInformation,$BIP_Non_UKFlag);
sleep(1);
# }
}
$count=$count+1;
if($count<=300)
{
# while($Tender_content1=~m/href="javascript[\w\W]*?Next/is
if($Tender_content1=~m/href="javascript[\w\W]*?Next/is)
{
$i=$i+2;
my ($ViewState1,$CSRFToken,$Viewstategenerator1,$Eventvalidation1,$eventtarget1,$Viewstategen);
if($Tender_content1=~m/<input type="hidden" name="__VIEWSTATEGENERATOR" value="([^>]*?)"\s*\/>/is)
{
	$Viewstategenerator1=uri_escape($1);
}

if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewState1=uri_escape($1);
}
if($Tender_content1=~m/<input\s*type\s*=\s*"hidden"\s*name[^>]*?VIEWSTATEGENERATO\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$Viewstategen=uri_escape($1);
}
if($Tender_content1=~m/name="__EVENTVALIDATION" id="__EVENTVALIDATION" value=\s*"([^>]*?)"\s*\/>/is)
{
	$Eventvalidation1=uri_escape($1);
}
my $Referer='https://www.philgeps.gov.ph/GEPS/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&ClickFrom=OpenOpp&Result=3';
my $post_url='https://www.philgeps.gov.ph/GEPS/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&ClickFrom=OpenOpp&Result=3';
# my $Search_po_Content1='__EVENTTARGET=pgCtrlDetailedSearch%24nextLB&__EVENTARGUMENT=&__VIEWSTATEGENERATOR=='.$Viewstategenerator1.'&txtKeyword=&pgCtrlDetailedSearch%3ApageDropDownList='.$i.'1&txtHidden=&__VIEWSTATE='.$ViewState1;
my $Search_po_Content1='__EVENTTARGET=pgCtrlDetailedSearch%24nextLB&__EVENTARGUMENT=&__LASTFOCUS=&txtKeyword=&pgCtrlDetailedSearch%24pageDropDownList='.$i.'1&txtHidden=&__VIEWSTATEGENERATOR='.$Viewstategenerator1.'&__EVENTVALIDATION='.$Eventvalidation1.'&__VIEWSTATE='.$ViewState1;

$Tender_content1=&Postcontent($post_url, $Search_po_Content1);

goto top;
}
}
# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "www.merx.com");
	# $req->header("Referer"=> "http://www.asia-pacific.undp.org/content/rbap/en/home/operations/procurement.html");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.philgeps.gov.ph");
	# $req->header("Referer"=> "https://www.gebiz.gov.sg/ptn/opportunity/opportunityDetails.xhtml");
	# $req->header("Cookie"=> "ASP.NET_SessionId=p35su5b0qkd31m4a4si0v3uk");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	$req->header("Host"=> "www.tenderlink.com");
	$req->header("Referer"=> "https://www.tenderlink.com/webapps/jadehttp.dll?WebTender&tendererhome&tenderer=2744.4");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	# $req->header("Host"=> "sasktenders.ca");
	# $req->header("Content-Type"=> "text/plain; charset=utf-8"); 
	# my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.philgeps.gov.ph");
	# $req->header("Content-Type"=>"text/html; charset=utf-8"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");

	$req->header("Referer"=> "https://www.philgeps.gov.ph/GEPS/Tender/SplashOpportunitiesSearchUI.aspx?menuIndex=3&ClickFrom=OpenOpp&Result=3");
	# $req->header("Cookie"=> "_ga=GA1.3.2125134017.1490703165; HstCfa3550935=1490703165236; HstCla3550935=1493795465339; HstCmu3550935=1493795465339; HstPn3550935=1; HstPt3550935=1; HstCnv3550935=1; HstCns3550935=1; ASP.NET_SessionId=wn2i1favngt1c1my2lgno055; ROUTEID=.web02");
	# $req->header("Cookie"=> "__cfduid=dee8fcad128c3e108d1e8b7f669ff54d11494494981; wlsessionid=cWL216v6piDgyYgnzPqqYmWzn3FtfUy44251NKH6Hi4GIB3JzFVQ!-111085610; BIGipServerPTN2_PRD_Pool=52519072.47873.0000");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>/ /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/nbsp/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\\n/ /igs;
	$Clean=~s/\s\s+/ /igs;
	# decode_entities($Clean);
	return $Clean;
}