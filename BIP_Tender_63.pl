#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;
use Time::Local;
use POSIX qw(strftime);
use Time::Piece;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	
my $date1		= strftime "%d", localtime;
print $date1;
my $month1		= strftime "%m", localtime;
print "$month1 \n";
my $year1		= strftime "%y", localtime;
print $year1;
my $date2=$date1-1;
$date2="0$date2" if(length($date2)==1);
# print $date2;
my $month2=$month1;
# $month1 = "0$month1" if (length($month1)==1);
$month2="0$month2" if(length($month2)==1);
my $year2=$year1;
$year2="20$year2";
#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;
my $url="https://www.e-hub.com/epublic/displayframe.asp?id=143335";
my ($Tender_content001)=&Getcontent($url);
### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content63.html");
print ts "$Tender_content1";
close ts;
# exit;

#### To get the tender link ####
	while ($Tender_content1=~m/onclick="javascript:document.location.href='\s*([^>]*?)\s*';"/igs)
	{
	my $tend_url="https://www.e-hub.com/epublic/".$1;
	my ($Tender_content2)=&Getcontent1($tend_url);
	open(ts,">Tender_content63_1.html");
print ts "$Tender_content2";
close ts;
if($Tender_content2=~m/<a href="([^>]*?)">View Contract Notice<\/a>/is)
{
my $tender_link="https://www.e-hub.com".$1;
print $tender_link;
	my ($Tender_content3)=&Getcontent($tender_link);
	$Tender_content3 = decode_utf8($Tender_content3);  
	open(ts,">Tender_content63_2.html");
print ts "$Tender_content3";
close ts;

my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
if($Tender_content3=~m/<b>Title<\/b>\s*<\/td>\s*<td[^>]*?><b>\s*([^>]*?)\s*<\/b><\/td></is)
{
$tender_title=$1;
}
if($Tender_content3=~m/<b>Description[^>]*?<\/b>\s*([\w\W]*?)\s*<\/textarea>/is)
{
$Description=&BIP_Tender_DB::clean($1);
print $Description;
# exit;
}
if($Tender_content3=~m/Official Name and Address of the Contracting Authority<\/b>([\w\W]*?)\s*<\/table>/is)
{
$Awarding_Authority=&BIP_Tender_DB::clean($1);
}
		if (length($Awarding_Authority)>3000)
	{
		$Awarding_Authority = substr $Awarding_Authority, 0, 3000;
	}	
	my $tenderblock;
	if ($Tender_content3=~m/<html><body>([\w\W]*?)<\/body><\/html>/is)
		{
			$tenderblock = ($1);
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		my $temp_time;
			if ($Tender_content2=~m/<td bgcolor="white" width="130" align="center">\s*([^>]*?)\s*<font size="1">\s*\[([^>]*?)\]\s*<\/font>\s*<\/td>/is)
		{
			$Deadline_Date = ($1);
			$temp_time = $2;
			# print $temp_time;
			# exit;
			
		}
				# my $temp_time;
		# if($block=~m/"dueTime":([^>]*?)"/is)
		# {
		# $temp_time = $1;
		if($temp_time=~m/(\d{1,2})\s*(?:PM|pm|noon|Noon|Midday)/is)
		{
		my $hh=$1;
		print $hh;
		$Deadline_time=($hh+12).":00" if($hh != 12);
		$Deadline_time=$hh.":00" if($1 == 12);
		}
		if($temp_time=~m/(\d{1,2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":00" if($hh != 12);
		$Deadline_time="00:00" if($hh == 12);
		}
		if($temp_time=~m/(\d{1,2})\:(\d{1,2})(?:\:00|\s*)/is)
		{
		$Deadline_time=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		}
		$Deadline_Description="Closing Date";
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		# my $other_info='Please Click the "View Tenders" Section on the left side of the website for view the above tender. Click the "Further Details" button to view the Contract Notice. Please note you must be a registered user in order to access the tender documentation. Please register and login above website to participate in any available opportunities, download Tender documentation, submit your replies, and communicate with our buyers online. If you have already registered and are a returning user, simply log in on the side panel.';
&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);		
}
	}
	my $url2="https://www.e-hub.com/epublic/BuyResults.asp?id=143335&logo=17";
	my ($Tender_content4)=&Getcontent($url2);
# $Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content63.html");
print ts "$Tender_content1";
close ts;
# exit;

#### To get the tender link ####
	while ($Tender_content4=~m/onclick="javascript:document.location.href='\s*([^>]*?)\s*';"/igs)
	{
	my $tender_link="https://www.e-hub.com/epublic/".$1;
	my ($Tender_content3)=&Getcontent1($tender_link);
	open(ts,">Tender_content63_1.html");
print ts "$Tender_content3";
close ts;
# if($Tender_content2=~m/<a href="([^>]*?)">View Contract Notice<\/a>/is)
# {
# my $tender_link="https://www.e-hub.com".$1;
# print $tender_link;
	# my ($Tender_content3)=&Getcontent($tender_link);
	# $Tender_content3 = decode_utf8($Tender_content3);  
	# open(ts,">Tender_content63_2.html");
# print ts "$Tender_content3";
# close ts;

my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
# if($Tender_content3=~m/<td bgcolor="#dcdcdc" width="150">\s*<b><a name="[^>]*?">([^>]*?)<\/a><\/b>/is)
if($Tender_content3=~m/<b><a name="[^>]*?">([^>]*?)<\/a>/is)
{
$tender_title=$1;
}
if($Tender_content3=~m/<b>Details:<\/b>\s*([\w\W]*?)\s*<\/form>/is)
{
$Description=&BIP_Tender_DB::clean($1);
print $Description;
# exit;
}
if($Tender_content3=~m/<b>Delivery Address:<\/b>([\w\W]*?)\s*<\/td>/is)
{
$Awarding_Authority=&BIP_Tender_DB::clean($1);
}
		if (length($Awarding_Authority)>3000)
	{
		$Awarding_Authority = substr $Awarding_Authority, 0, 3000;
	}	
	my $tenderblock;
	if ($Tender_content3=~m/<td bgcolor="#dcdcdc" width="150">([\w\W]*?)<\/form>/is)
		{
			$tenderblock = ($1);
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		my $temp_time;
			if ($Tender_content3=~m/<td bgcolor="white" width="130">\s*([^>]*?)\s*<font size="1">\s*\[([^>]*?)\]\s*<\/font>\s*<\/td>/is)
		{
			$Deadline_Date = ($1);
			$temp_time = $2;
			# print $temp_time;
			# exit;
			
		}
				# my $temp_time;
		# if($block=~m/"dueTime":([^>]*?)"/is)
		# {
		# $temp_time = $1;
		if($temp_time=~m/(\d{1,2})\s*(?:PM|pm|noon|Noon|Midday)/is)
		{
		my $hh=$1;
		print $hh;
		$Deadline_time=($hh+12).":00" if($hh != 12);
		$Deadline_time=$hh.":00" if($1 == 12);
		}
		if($temp_time=~m/(\d{1,2})\s*(?:am|AM)/is)
		{
		my $hh=$1;
		$Deadline_time=$hh.":00" if($hh != 12);
		$Deadline_time="00:00" if($hh == 12);
		}
		if($temp_time=~m/(\d{1,2})\:(\d{1,2})(?:\:00|\s*)/is)
		{
		$Deadline_time=$1;
		# $Deadline_time=$hh.":".$2 if($hh != 12);
		# $Deadline_time="00:".$2 if($hh == 12);
		}
		$Deadline_Description="Closing Date";
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		# my $other_info='Please Click the "View Tenders" Section on the left side of the website for view the above tender. Click the "Further Details" button to view the Contract Notice. Please note you must be a registered user in order to access the tender documentation. Please register and login above website to participate in any available opportunities, download Tender documentation, submit your replies, and communicate with our buyers online. If you have already registered and are a returning user, simply log in on the side panel.';
&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);		
}
	# }
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.e-hub.com");
	$req->header("Referer"=>"https://www.e-hub.com/epublic/Side.asp");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.e-hub.com");
	# $req->header("Referer"=>"https://www.e-hub.com/epublic/Side.asp");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}


sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("X-Requested-With"=>"XMLHttpRequest"); 
	# $req->header("X-MicrosoftAjax"=>"Delta=true"); 
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do");
	$req->header("Cookie"=> "$Cookie_JSESSIONID");
	$req->header("Connection"=> "keep-alive");

	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>|<td>|<\/td>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}

sub Getcontent1
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"crowncommercialservice.bravosolution.co.uk");
	$req->header("Referer"=> "https://crowncommercialservice.bravosolution.co.uk/esop/toolkit/opportunity/opportunityList.do?reset=true&resetstored=true&oppList=CURRENT&_ncp=1517573764198.823-1");
	$req->header("Content-Type"=> "text/html;charset=UTF-8");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}