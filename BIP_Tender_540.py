# -*- coding: utf-8 -*-
import requests
import re
import xlwt
import sys
import imp
import dateparser
import time
import redis
import warnings
from datetime import date, timedelta
# warnings.filterwarnings('ignore')
# reload(sys)
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


from googletrans import Translator


sub_url_id_re = '\{"data\-id"\:\s*([^>]*?)\}'
base_url = 'https://www.enarocanje.si/Obrazci/?id_obrazec='
title_re = '<\/h5>Naslov\:([^>]*?)<'
address_block_re = 'Ime\s*in\s*naslovi<\/h5>([\w\W]*?)<h5>'
address_re = '<label>([\w\W]*?)<\/label>(?:[\w\W]*?)([^>]*?)<\/'
cvp_code_re = '<td class="cpv">([^>]*?)</td>'
bref_des_re = 'Kratek\s*opis[\w\W]*?<\/h5>([\w\W]*?)[<h5>|<h4>]'
place_perform_re = 'Kraj\s*izvedbe[\w\W]*?<\/h5>([\w\W]*?)<h5>'
ref_no_re = 'Referen[\w\W]*?na\s*[\w\W]*tevilka\s*dokumenta\:\s*([^>]*?)<'
read_line_time = 'Time limit for receipt of tenders or requests to participate'
# dead_line_date_re = 'Datum\s*sklenitve\s*pogodbe<\/h5>([\d\.\-\w]*?)<'
# dead_line_date_re = 'Rok\s*za\s*sprejemanje\s*ponudnikovih\s*(?:[\w\W]*?)\s+([\d\.]*?)\s+([\d\:\.]*?)[\.\s+\s*]'
# dead_line_date_re = 'Rok\s*za\s*sprejemanje\s*(?:[\w\W]*?)\s+([\d\.\/]*?)&[\w\W]*?([\d\:\.]*?)[\s+\.\<]'
dead_line_date_re = 'Rok za prejem ponudb ali prijav za sodelovanje<\/h5>(\d{1,2})\.(\d{2})\.(\d{4})&nbsp;&nbsp;&nbsp;(\d{2}\:\d{2})<h5>'
Tender_ID = 540
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate


def reg_clean(desc):
    desc = desc.replace('\r', '').replace('\n', '')
    desc = desc.replace('&#160;', '')

    desc = re.sub(r'<[^<]*?>', ' ', str(desc))
    desc = re.sub(r'\r\n', '', str(desc), re.I)
    desc = re.sub(r"\\r\\n", '', str(desc), re.I)
    desc = re.sub(r'\t', " ", desc, re.I)
    desc = re.sub(r'\s\s+', " ", desc, re.I)
    desc = re.sub(r"^\s+\s*", "", desc, re.I)
    desc = re.sub(r"\s+\s*$", "", desc, re.I)
    desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
    desc = re.sub(r"\&ndash\;", "-", desc, re.I)

    return desc
	
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'si'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'si'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    # if isinstance(translated_sentence, str):
        return translated_sentence
    # else:	

        # return translated_sentence.decode("utf-8")


def req_content(url):
    req_url = requests.get(url,proxies = {
    "http" :"http://172.27.137.199:3128",
    "https":"http://172.27.137.199:3128"
    })
    url_content = req_url.content.decode('utf_8', errors='ignore')
    return url_content


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return  val


def regxx(reg,content):
    value = re.findall(reg,content)
    return value

def start(main_url,n):
	
    main_content = req_content(main_url)

    sub_url_ids = regxx(sub_url_id_re,main_content)
    # print "sub ids :",sub_url_ids
    file = 101
    counter1 = 0
    for id in sub_url_ids:
        time.sleep(5)
        # print "NO: ", n
        sub_url_full = base_url + id
        Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url_full)
        if Duplicate_Check == "N":	
            time.sleep(5)
            curr_date = dateparser.parse(time.asctime())
            created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
            sub_content = req_content(sub_url_full)
            print (sub_url_full)
            # print "id :", id
            sub_content1 = req_content(str(sub_url_full)+"#dodatna_pojasnila")
            title = regxx(title_re,sub_content1)
            if title:
                title = title[0]
                title = translateText(title)
            else:
                title = ''
            # print "title :",title

            address_block = regxx(address_block_re,sub_content)
            # print "address blcokL:",address_block
            address_block = reducer(address_block)
            address = []
            if address_block:
                address_all = regxx(address_re,address_block)
                # print "all :",address_all
                for i in address_all:
                    addrss1 = i[0].strip()
                    addrss2 = i[1].strip()
                    con_address = addrss1 + ': '+addrss2 +'\n'
                    address.append(con_address)
                # print "main lock :",address
                awarding_authority = address
                awarding_authority = ', '.join(awarding_authority)
            else:
                awarding_authority = ''
            # print "main lock :", awarding_authority
            # print "address  :", address

            cvp_code = regxx(cvp_code_re,sub_content)
            if cvp_code:
                cvp_code = cvp_code[0]
            else:
                cvp_code = ''
            # print "CVp :",cvp_code
            bref_des = regxx(bref_des_re,sub_content)
            if bref_des:
                bref_des = bref_des[0]
                description = bref_des
                description = translateText(description)
            else:
                bref_des = ''
                description = ''
            # print "bref_des :",bref_des

            place_perform = regxx(place_perform_re,sub_content)
            if place_perform:
                place_perform = place_perform[0]
                # place_perform = place_perform.replace('<br>','-')
                site = place_perform
            else:
                place_perform = ''
                site = ''
            # print "place_perform :",site

            ref_no = regxx(ref_no_re,sub_content)
            if ref_no:
                ref_no = ref_no[0]
                ref_no = ref_no.replace('<br>','- ')
                authorityRefNo = ref_no
                authorityRefNo = translateText(authorityRefNo)
            else:
                ref_no = ''
                authorityRefNo = ''
            # print "ref_no :",ref_no

            dead_line_date = regxx(dead_line_date_re,sub_content)
            # print "dead lines :",dead_line_date
            if dead_line_date:
                print (dead_line_date)
                # dead_line_date = dead_line_date[0]

                # dead_line_dat = translateText(dead_line_date[0])
                # dt = dateparser.parse(dead_line_dat)
                # dt = dead_line_dat
                # dt = parser.get_date_data(dead_line_dat, '%Y-%m-%d %H:%M:%S')
                # FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                dead_line_dat = dead_line_date[0][2]+"-"+dead_line_date[0][1]+"-"+dead_line_date[0][0]+" 00:00:00"
                print (dead_line_dat)
                dt = dateparser.parse(dead_line_dat)
                FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                dead_line_time = dead_line_date[0][3]
            else:
                dead_line_time = ''
            # print "dead_line_date :",dead_line_date
            # print "dead_line_time :",dead_line_time

            orgin = 'European/Non Defence'
            sector = 'European Translation'
            country = 'SI'
            websource = 'Slovenia: Ministry of Finance (1): eObjave'
            summary = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website:'
            other_information = summary + sub_url_full
            other_information = other_information
            deadline_description = 'Time limit for receipt of tenders or requests to participate'
            # print 'other_information::', other_information

            if dead_line_date:
		# print "FormattedDate",FormattedDate
                insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(
                    Tender_ID, sub_url_full, '', orgin, sector, country,websource,title,
                    title,awarding_authority, '', '', cvp_code, description, site,ref_no,'', deadline_description, FormattedDate, dead_line_time, '',
                    other_information,'Y', created_date)
                val = Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                counter1 = counter1 + 1
                print (insertQuery)
				
            else:
                val = 0
                print ("Deadline Date not available")
                # insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(
                    # Tender_ID, sub_url_full,'', orgin, sector, country, websource, title,
                    # title, awarding_authority, '', '', cvp_code, description, site, ref_no,
                    # '', deadline_description, FormattedDate,  dead_line_time, '', other_information, 'Y',
                    # created_date)
                # Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # counter1 = counter1 + 1                
            if counter1 > 300:
                break;
            # print "insertion status :",val
            if val == 1:
                print ("1 Row Inserted Successfully")
            else:
                print ("Insert Failed")



if __name__== "__main__":
	last_week = date.today() - timedelta(7)
	# last_week = date.today()
	last_week_date1 = (last_week.strftime('%d.%m.%Y'))
	print (last_week_date1)
	# curr_date1 = date.today() - timedelta(1)
	curr_date1 = date.today()
	created_date1 = (curr_date1.strftime('%d.%m.%Y'))
	print (created_date1)
	# raw_input("check")
	main_url = 'https://www.enarocanje.si/default_data.asp?method=getData&draw=2&columns%5B0%5D%5Bdata%5D=Obrazec_Oznaka&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=sys_ObjavaDatum&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=noDoc&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=Naslov&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=id_SifVrstaNarocila_Naziv&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&start=0&length=100&search%5Bvalue%5D=&search%5Bregex%5D=false&SifObrazecTip_ListOfID=99%2C1%2C2%2C3&NoDoc=&sys_ObjavaDatum_od='+str(last_week_date1)+'&sys_ObjavaDatum_do='+str(created_date1)+'&id_SifVrstaNarocila=&SifCpv_ListOfID=&SifCpv_ListOfKode=&id_SifCpv_upostevaj_podrejene=1&id_SifZakon=&id_SifVrstaPostopka=&id_SifObrazec=&id_SifDejavnost=&id_SifVrstaNarocnika=&UpostevanOkoljskiSocialniEticniVidik=&ObjaviLeNaPjn=&RokZaPrejemPonudb_Pogoj=&RokZaPrejemPonudb_Datum=&Narocnik_Maticna=&Narocnik_Davcna=&Narocnik_Naziv=&_=1514384012911'
	n = 1
	start(main_url,n)