import requests
import os, sys
import re
import xlwt
from googletrans import Translator
import redis
import time
import json
import pymysql
import xlwt
import imp
import dateparser
import warnings
import datetime
from datetime import datetime
from datetime import date
warnings.filterwarnings('ignore')

# reload(sys)
# sys.setdefaultencoding('utf-8')

# Database_Connector = imp.load_source('Database_Connector','Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


##Clean Function:::
def reg_clean(desc):
	desc = desc.replace('\r', '')
	desc = desc.replace('\n', '')
	desc = desc.replace('&#160;', '')

	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "'", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	return desc
file_name=os.path.basename(__file__)
tender_id1 = file_name.replace("BIP_Tender_","")
Tender_ID = tender_id1.replace(".py","")
main_url_query = u"select tender_link from tender_master where id="+str(Tender_ID)
# Tender_ID = 530
	
##Redis Connection::::
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
print ("re----->",Retrive_duplicate)

def redis_connection():
	red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
	return red_connec

def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'nl'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'nl'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                    payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fr'}

                # print "payload :",payload
                    s = requests.post(url, data=payload)
                    value = s.json()
                # print "value :",value
                # raw_input("Value s")
                    translated_sentence = value['data']['translations'][0]['translatedText']
                    time.sleep(2)
                # print "1 :", translated_sentence
                    try:
                        redis_con.set(InputText, translated_sentence)
                    except Exception as e:
                    # print 'Redis Err:',e
                        pass
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
    # print "Returning Text :",translated_sentence
    return translated_sentence
def translateText1(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'fr'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fr'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyC_6rqHUX03SENXURrcYJA1ATM_isx4N_k'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                    payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fr'}

                # print "payload :",payload
                    s = requests.post(url, data=payload)
                    value = s.json()
                # print "value :",value
                # raw_input("Value s")
                    translated_sentence = value['data']['translations'][0]['translatedText']
                    time.sleep(2)
                # print "1 :", translated_sentence
                    try:
                        redis_con.set(InputText, translated_sentence)
                    except Exception as e:
                    # print 'Redis Err:',e
                        pass
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
    # print "Returning Text :",translated_sentence
    return translated_sentence
	
def program():
	
    # conn=pymysql.connect(host='172.27.138.250',port=3306,user='root',password='admin',db='BIP_tender_analysis')
    # cur=conn.cursor()
	main_url = Database_Connector.select_query(mysql_cursor, main_url_query)

	
	
##Main & Details page Data::::
	# main_url = 'https://enot.publicprocurement.be/enot-war/searchNotice.do?pageSize=100'
	main_req = requests.get(main_url[0])
	main_cont = main_req.content.decode('utf-8')

	sub_url_re = 'tr\s*class\=\"Zebra\">\s*<td>\s*<a\s*href\=\"([\w\W]*?)\">'
	sub_url = re.findall(sub_url_re, main_cont)

	count = 1
	while (count < 35):	
		for tender_link in sub_url:

			print ("No :", count)

			base_url = 'https://enot.publicprocurement.be/enot-war/' + str(tender_link)
			Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, base_url)
			
			if Duplicate_Check == "N":
				curr_date = dateparser.parse(time.asctime())
				created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))

				sub_req = requests.get(base_url)
				sub_cont = sub_req.content.decode('utf-8',errors='ignore')

				title_re = '<dt>Title<\/dt>\s*<dd>\s*([\w\W]*?)\s*<\/dd>'
				awardingAuthority_re = '<dl>\s*<dt>Awarding\s*authority<\/dt>\s*<dd>([\w\W]*?)<\/dd>'
				type_procedure_re = '<dt>Procedure\s*type<\/dt>\s*<dd>\s*([\w\W]*?)\s*<\/dd>'
				description_re = '<dt>Description<\/dt>\s*<dd>\s*([\w\W]*?)\s*<\/dd>'
				ref_re = '<dt>File\s*reference\s*number<\/dt>\s*<dd>\s*([\w\W]*?)\s*<\/dd>'
				date_re = '<dt>Tenders\s*submission\s*deadline<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>'
				
				
				title = re.findall(title_re, sub_cont, re.I)
				if title:
					title1 = title[0]
					title1 = reg_clean(title1)
					title = title[0]
					title = reg_clean(title)
				else:
					title = ''
					title1 = ''
				title = translateText(title)
				if title == title1:
					title = translateText1(title)
				print ('Title::::',title)

				awarding_authority = re.findall(awardingAuthority_re, sub_cont, re.I)
				if awarding_authority:
					awarding_authority = awarding_authority[0]
					awarding_authority = reg_clean(awarding_authority)
					# .replace('&nbsp;', '')
				else:
					awarding_authority = ''
				# awarding_authority = translateText (str(awarding_authority))	
				#print "Awarding Authority::", awarding_authority

				award_procedure = re.findall(type_procedure_re, sub_cont, re.I)
				if award_procedure:
					award_procedure = award_procedure[0]
					award_procedure = reg_clean(award_procedure)
				else:
					award_procedure = ''
				award_procedure = translateText(award_procedure)	
				#print "Award Procedure::", award_procedure

				description = re.findall(description_re, sub_cont, re.I)
				if description:
					description1 = description[0]
					description1 = reg_clean(description1)
					description = description[0]
					description = reg_clean(description)
				else:
					description = ''
				description = translateText(description)
				if description == description1:
					description = translateText1(description)
				#print 'Description::', description

				authority_reference = re.findall(ref_re, sub_cont, re.I)
				if authority_reference:
					authority_reference = authority_reference[0].strip()
					authority_reference = reg_clean(authority_reference)
				else:
					authority_reference = ''
				authority_reference = translateText(authority_reference)
				#print 'Reference No:::', authority_reference.strip()
				
				deadline_description = "Tenders submission deadline:"
				
				deadline_date = re.findall(date_re, sub_cont, re.I)
				# deadline_date = deadline_date[0].strip()
				# deadline_date = str(deadline_date).replace('&nbsp;', '')
				# FormattedDate = ''
				if deadline_date:
					deadline_date = deadline_date[0].strip()
					# print (deadline_date)
					deadline_date = deadline_date.replace('&nbsp;', '')
					FormattedDate = deadline_date+" 00:00:00"
					dt = dateparser.parse(FormattedDate)
					# print ("Formatted Date:",FormattedDate)
					dt = dt.strftime('%Y-%m-%d %H:%M:%S')
					# FormattedDate = dt.strptime('%Y-%m-%d %H:%M:%S')
					# print (dt)
					# dt = dateparser.parse(FormattedDate)
					FormattedDate = dt
				else:
					pass
					
				

				#print 'Date:::', FormattedDate.strip()
			

				summary ='The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website:'
				other_information = summary + base_url
				# other_information = translateText(other_information)
				print ('other_information::',other_information)
				
				orgin = 'European/Non Defence'
				sector = 'European Translation'
				country = 'BE'
				websource = 'Belgium: Public Procurement Portal'
				
				# title=title.replace("'","''").strip()
				# awarding_authority=awarding_authority.replace("'","''").strip()
				# award_procedure=award_procedure.replace("'","''").strip()
				# description=description.replace("'","''").strip()
				# authority_reference=authority_reference.replace("'","''").strip()
				# deadline_description=deadline_description.replace("'","''").strip()
				if deadline_date:
					insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, base_url, '', orgin, sector, country, websource, title, title,awarding_authority, award_procedure, '', '', description, '', authority_reference, '', deadline_description, FormattedDate, '','', other_information, 'Y',created_date)
				else:
					insertQuery = u"insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, base_url, '', orgin, sector, country, websource, title, title,awarding_authority, award_procedure, '', '', description, '', authority_reference, '', deadline_description, '','', other_information, 'Y',created_date)
				insertQuery = insertQuery.replace("'b'","'")
				insertQuery = insertQuery.replace("'',","',")
				insertQuery = insertQuery.replace(",',",",'',")
				insertQuery = insertQuery.replace(",'',',",",'','',")	
				val = Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
				# print "insertion Status :",val
				if val == 1:
					print ("1 Row Inserted Successfully")
				else:
					print ("Insert Failed")
				count = count + 1
	connection.close()
		
if __name__== "__main__":
	# main_url = 'https://enot.publicprocurement.be/enot-war/searchNotice.do?pageSize=100'
	program()




