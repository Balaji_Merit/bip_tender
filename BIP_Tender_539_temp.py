# -*- coding: utf-8 -*-
import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import urllib.parse
import re
import imp
import pymysql
import time
from urllib.parse import quote 
# import cookielib
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
h = HTMLParser()
# warnings.filterwarnings('ignore')
# reload(sys)
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


from googletrans import Translator


block_re = '(<td\s*valign\=\"top\">[\w\W]*?Voir)'
details_block_re = '(<td[\w\W]*?<\/td>)'
sub_detail_re = '(<p[\w\W]*?<\/p>)'
# boamp_blocks_re ='class\="m[^>]*?corps[^>]*?\">([\w\W]*?)<\/tr>'
boamp_blocks_re ='<article class="result-search-avis">([\w\W]*?)<\/article>'
b_link_re = '<a\s*href\="([^>]*?)"'
title_re = '<label class="col-md-4 col-xs-12 spacing-helper-m">\s*Intitu[\w\W]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*<\/div>'
article_re = '<article\s*role\=\"article\">([\w\W]*?)<\/article>'
awardingAuthority_re = 'Enti[^>]*?publique[^>]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*</div>'
object_contract_re = '<h3>Objet[\w\W]*?<p>([\w\W]*?)</p>'
desc_re = '<label class="col-md-4 col-xs-12 spacing-helper-m">Objet\s*:\s*<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*<\/div>'
autho_ref_re = '<label class="col-md-4 col-xs-12 spacing-helper-m">\s*R[\w\W]*?rence[\w\W]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*<\/div>'
# deadline_date_re = '<h3>Date\s*limite[\w\W]*?<p>([/^\d{2}\/\d{2}\/\d{4}$/]*?)\s+'
deadline_date_re = 'Date et heure limite de remise des plis[^>]*?<\/label>\s*<span class="col-md-8 col-xs-12">\s*<span class="green bold">\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s*'
# deadline_date_re = 'Date\s*limite[^>]*?<\/td>[\w\W]*?<td class="txt">(\d{1,2})\/(\d{1,2})\/(\d{4})\s+'
# deadline_time_re = 'Date\s*limite\s*de[^>]*?[^>]*?<\/td>[\w\W]*?<td class="txt">[\w\W]*?\s*(\d{1,2}h\d{2})'
deadline_time_re = '<span class="green bold">\s*\d{1,2}\/\d{1,2}\/\d{4}\s*(\d{1,2}:\d{2})'
mail_date_re = '[^>]*?\,\s+([\d]{2}\s+[^>]*?\s+[\d]{4})\s+[^>]*?'
cpvcode_re='<span data-code-cpv="[^>]*?">([\w\W]*?)<\/div>'
contract_type_re='Cat[^>]*?gorie\s*principale[^>]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*</div>'
site_re='Lieu[^>]*?cution[^>]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*</div>'
awardprocedure_re='Proc[^>]*?dure[^>]*?<\/label>\s*<div class="col-md-8 col-xs-12">\s*([\w\W]*?)\s*</div>'
Tender_ID = 539
orgin_Boamp = 'European/Non Defence'
orgin_place = 'European/Defence'
sector = 'European Translation'
country = 'FR'
dead_line_W_Boamp = 'Deadline and deadline for submission of bids: (Paris time)'
summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: Tender Page Link'
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate
prado='id\=\"PRADO_PAGESTATE\"\s*value\=\"([\w\W]*?)\"\s*\/>'
id1='<div class="col-md-1 text-right p-0">\s*<a\s*id="([^>]*?)"'
tender_url='<a class="btn btn-primary btn-sm" href="([^>]*?)">\s*<span class="sr-only">Acc'
def reg_clean(cont):
    cont = str(cont).replace("'b'",'')
    cont = str(cont).replace('\r','')
    cont = str(cont).replace('\n','')
    cont = str(cont).replace('\t','')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'","''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'","''")
    cont = cont.replace("î","'")
    cont = cont.replace("l'","l''")
    cont = cont.replace("L'","L''")
    cont = cont.replace("d'","d''")
    cont = cont.replace("D'","D''")
    cont = cont.replace("'s","''s")
    cont = cont.replace("du'","du''")
    cont = cont.replace("N'","N''")
    cont = cont.replace("'acheteur","''acheteur")
    cont = cont.replace("'id","''id")
    cont = cont.replace("'b'","'b''")
    cont = cont.replace("'Esp","''Esp")
    cont = cont.replace("'RD","''RD")
    # cont = str(cont).replace("\’","''")
    # cont = cont.replace("’","''", re.I)
	# cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''","''")
    cont = cont.replace("'''''","''")
    cont = cont.replace("''''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("\xc3\xa9","é")
    cont = cont.replace("&#39;","''")
    cont = cont.replace("&quot;",'"')
    cont = cont.replace("î","'")
    cont = cont.replace("'","''")
    cont = cont.replace("''''","''")
    cont = cont.replace("''''","''")
    return cont
	
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    print (red_connec)
    return red_connec


def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'fr'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fr'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    return translated_sentence


def req_content(url):
	try:
		req_url = requests.get(url,proxies = {
		"http" :"http://172.27.137.199:3128",
		"https":"http://172.27.137.199:3128"
		})
		url_content = req_url.content.decode('utf_8', errors='ignore')
	except Exception as e:
		url_content=''
	return url_content


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return  val


def regxx(regx,content):
    cont_data = re.findall(regx,content)
    if cont_data:
        cont_data = cont_data[0] 
    else:
        cont_data = ''
    return h.unescape(cont_data)

def start(main_url):
	proxies = {
	"http" :"http://172.27.137.192:3128",
	"https":"https://172.27.137.192:3128"
	}
	global awardingAuthority1
	last_week = date.today()- timedelta(0)
	print ("Today******",last_week)
	last_week_date1 = (last_week.strftime('%d'))
	last_week_date2 = (last_week.strftime('%m'))
	last_week_date3 = (last_week.strftime('%Y'))
	s=requests.session()
	# main_content1= requests.get(main_url)
	# main_content4 = main_content1.content.decode('utf_8', errors='ignore')
	# with open ("temp5392.html",'w') as f:
			
	    # f.write(main_content4)
	# viewstate = re.findall(prado,main_content4)
	# if viewstate:
		# viewstate = viewstate[0]
            # viewstate = viewstate.encode(encoding='UTF-8',errors='strict')
            # viewstate = viewstate.decode('UTF-8')
            # print viewstate.decode(encoding='UTF-8',errors='strict')
            # print viewstate
		# viewstate1 = urllib.parse.quote_plus(viewstate)
		# print (viewstate1)
		# viewstate1 = urllib.urlencode(viewstate)
		# VIEWSTATEGENERATOR = urllib.quote_plus(VIEWSTATEGENERATOR[0])
	headers1 = {"Host":"www.marches-publics.gouv.fr","Origin": "https://www.marches-publics.gouv.fr",
				"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0",
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
				"Content-Type":"application/x-www-form-urlencoded","Accept-Encoding": "gzip, deflate, br","Accept-Language": "en-US,en;q=0.9","Cache-Control": "max-age=0","Connection": "keep-alive",
				"Referer": "https://www.marches-publics.gouv.fr/?page=entreprise.EntrepriseAdvancedSearch&searchAnnCons",
				# "Cookie": "PHPSESSID=vv3thup8c750gopo0i458hmqo2; SERVERID=AIFPLAMPVP103|YLUg/|YLUg9; _pk_id.1.1118=4bfc9034ab4b50e1.1619809068.25.1622049336.1622048764.; _pk_ses.1.1118=*"
				"Cookie": "PHPSESSID=vv3thup8c750gopo0i458hmqo2; SERVERID=AIFPLAMPVP103|YLUg/|YLUg9; _pk_id.1.1118=4bfc9034ab4b50e1.1619809068.25.1622049336.1622048764."
				# "Cookie": "PHPSESSID=vv3thup8c750gopo0i458hmqo2; _pk_id.1.1118=4bfc9034ab4b50e1.1619809068.25.1622049336.1622048764.; SERVERID=AIFPLAMPVP202|YLRmV|YLRl1"
				#PHPSESSID=2gp716ng37d34b2pf0oi6ghvh3; _pk_id.1.1118=b88cbb6f0f07b2e5.1585845640.28.1593111502.1593111502.; _pk_ses.1.1118=*; SERVERID=PLACE-MPE-PROD-06|XvTz1|XvTzz
# PHPSESSID=hfijliavdnda569bpvdj33lbc4; _pk_ref.1.1118=%5B%22%22%2C%22%22%2C1585057426%2C%22https%3A%2F%2Fwww.google.com%2Furl%3Fq%3Dhttps%3A%2F%2Fwww.marches-publics.gouv.fr%2F%3Fpage%3Dentreprise.EntrepriseAdvancedSearch%26searchAnnCons%26sa%3DD%26source%3Dhangouts%26ust%3D1565767810673000%26usg%3DAFQjCNE9WCFFwOgTyWx4omlBuwM51Sx31Q%22%5D; _pk_id.1.1118=edc30be10a7057ee.1563865926.157.1585057426.1585057426.; _pk_ses.1.1118=*; SERVERID=PLACE-MPE-PROD-03|XnoOn|XnoOk
				 # PHPSESSID=9pjaogeo50h9cl9j0e4tpjph81; _pk_ref.1.1118=%5B%22%22%2C%22%22%2C1573719398%2C%22https%3A%2F%2Fwww.google.com%2Furl%3Fq%3Dhttps%3A%2F%2Fwww.marches-publics.gouv.fr%2F%3Fpage%3Dentreprise.EntrepriseAdvancedSearch%26searchAnnCons%26sa%3DD%26source%3Dhangouts%26ust%3D1565767810673000%26usg%3DAFQjCNE9WCFFwOgTyWx4omlBuwM51Sx31Q%22%5D; _pk_id.1.1118=edc30be10a7057ee.1563865926.21.1573719398.1573719398.; _pk_ses.1.1118=*; 
# PHPSESSID=0msjghtj3ivvajjb663jq5efr5; _pk_ref.1.1118=%5B%22%22%2C%22%22%2C1573660453%2C%22https%3A%2F%2Fwww.google.com%2Furl%3Fq%3Dhttps%3A%2F%2Fwww.marches-publics.gouv.fr%2F%3Fpage%3Dentreprise.EntrepriseAdvancedSearch%26searchAnnCons%26sa%3DD%26source%3Dhangouts%26ust%3D1565767810673000%26usg%3DAFQjCNE9WCFFwOgTyWx4omlBuwM51Sx31Q%22%5D; _pk_id.1.1118=edc30be10a7057ee.1563865926.20.1573660453.1573660453.; _pk_ses.1.1118=*; SERVERID=PLACE-MPE-PROD-03|XcwnK|XcwnI

# PHPSESSID=779gk8460hu1sl2tgbr7h3qto4; _pk_ref.1.1118=%5B%22%22%2C%22%22%2C1573807979%2C%22https%3A%2F%2Fwww.google.com%2Furl%3Fq%3Dhttps%3A%2F%2Fwww.marches-publics.gouv.fr%2F%3Fpage%3Dentreprise.EntrepriseAdvancedSearch%26searchAnnCons%26sa%3DD%26source%3Dhangouts%26ust%3D1565767810673000%26usg%3DAFQjCNE9WCFFwOgTyWx4omlBuwM51Sx31Q%22%5D;  _pk_ses.1.1118=*; 
				}
	s=requests.session()
	# s=requests.session()
	main_url1='https://www.marches-publics.gouv.fr/?page=entreprise.EntrepriseAdvancedSearch&searchAnnCons'
	data2="PRADO_PAGESTATE=eJztvdty40aaLtqPgqiZNbIjLBdxJsrdPUFRlIpe4qFJqmbcNw6IhCTYIEDjIFd5wg8wN%2BsZ9t1q7R2xr%2Fpu3y292M5MnBLIJBMQM2GVrY5wVxWIxOHD%2F%2F%2F5nzK%2F%2Fo1xa%2FbktWMainmzduS1IhvrXs%2Bybjba2rAUvd%2FXbm9v7Hfyu%2F9y3%2FW%2Bsd8p7%2F4reqe9e7OOvd4b8G%2Bj9u%2Feu%2F%2F6NT8gwwNyeYKWn1CcocAjSq88Rc%2BGYDez3r1ZrkN3F1%2BH3ptvond9892bt6ETRUESrp3o7dr2HH8Tuk74dlj89fI%2FVtV%2Ffe0Ha3t973z9Q%2FTvP%2F7FAK9oqLZs2r21bXytqX1D1WQNXh5c%2FYMbuTee8%2BabG%2FAU5cMa7Gez9AbPtnAcr%2Fp82REOz2iyn1GWmzzkYLN1fTeKQzt2A7%2F6uMRvRz24rLx7c2P7G8dOBneOH6M3kLMn39m%2B410Fd0HlvdCv1WvJ6bXUfIjr%2F%2Bg1GJY9gobd6Sywt7tmtyPkXjGyC03tB%2FcuQfhMEs%2F1XP8uceB5ainrcjmu%2F%2B6NZ%2Ft39p0TkXcGMIxjZzsMEgiOi56Z9UIAh8g5d9aB7zvr2AnhVXV4tgxvBb6ds3TCB3ftTJzYTX%2FGFFUp%2Fq3I796Ezs6xwTWusGENHxM9C7qj%2F%2BMk8IfBdhc75Vcu76gSpkGrnaETZxj5EQYYuRidg5d4k2sF%2FOQbcGCQ%2FADUYHOfuJSrAUVZxp%2FgtWZQJN%2Bs5vA66aFMRuEX%2FxN2%2FE%2Ff39jrH8db8CmRvk2B1Ctq%2FZyNG4LPAoQjO8Gqn3AfhO4vgR%2Fb3sBz7%2FLTtPpp0ToMPO%2FMDqP0DIh19YyfQ3uX%2FabD37LDt67jbaJSAGUV%2FzGA3weN0bDDa8%2BOIqjFGXTwr8B2YyckURxsM8TQA8vYrxs32nn2p%2BxnBO6F%2B9HZvCnuA8Uj8IFw%2FOl7p%2FjauAgN1uvEcb15EMa269UExMQERDYqAwqJw2avPuV7w2%2BMNBe8XWYtVXDorQus08evd%2Fe7f9%2BB7%2FoXG17ua3TR98EWPCq6pZmrfhC7t%2B7aTj9vLm7gDXzsh6h4pFQr9w3WqeairqWkDuF6i1%2Bzod4SM%2FarUrCUQu3VvrD0Q3TqV6E%2FpC%2BZSUqV5N2JDxTh5A1PHQLn%2FmA%2F2BHyBa7sG8eryEKuGivnY4yUHDzpn9OT%2Fxo6t8BfuK8I0hdffvPnt9nv2aMUYgE%2B%2FyoTCOQf7kE5t9jZW2WmBb45UKlW8E%2BPM0XgilN0VzaKFtQrAg1Sq54BR2uhO%2FzWqWeVC5SfvR8vcYLqC5zA2PVsN6ramD8eGDU%2F8FdMo%2BoOyav93GM%2FZfDtdonnnYbu3X3clW9x8Esp4DJD8NgAjmjh%2FJS4IXDj50EUn4GvUV4knb6zEGYETnd2IdAJNH2bua95ZHihZWoE52Sj4sSiCw8zP3%2BDh%2BS%2BvXXIG2EmHmiEJKXeS3ZpE%2Ft42RXG%2Fm0QnYEP0%2FZa%2FfJa2RGrocNeeEPnbrTG30zPXxoo8CCJ78HnBFaH8mA0NIE63HjBGnwD1y8iMXgxd5Nex%2FbjVUD5NF1Yr9Q%2B3Qbh9nSdSlyH3rUM4Y6in4Nw84cEQAF3tgtxSiexsySO06ks86hTGdZ7heMCpRTGGPMwAHK2dQrBqg4ok2AQKjhg7Kduk4td%2F6Ds%2FtphKiR7aoWItunhB35ArR%2FQ6gf0%2BgGjfsCsH%2BjXD1h1syL3iCMycUQhjqjEEY04ohNHDOIIbjKBPIJZYv3j0rHD9T1xKmERZeJlFOJlFOJlFOJlFOJlFOJlFOxloCQi2RiEsbv2HFlVC4uoysRvC2d974AXcgYPtr92yiRKJizF3VnzmcK89MLBvJjqXWixKO0uRQ6uvMvK8cB97PAuzXThV9WZV%2B2lGpihSAiAYhJHiO%2BsYN8Z%2Blt5ZF68%2FYX9AHyzxsG50t9%2FjcFD8%2BukT6cSMqcSMqcSMqcSMqcSMqcSCqQS%2BKkEfiqBn0roiUY8s0Y8s0Y8s4Y9M%2FQ3XeDbjMIQRQO5S6XAVCwIjidOFAF%2F%2B6DTAy7xhp2Mrecf5cP5x19TxzMJvUF1Shp9BJ%2FbL7PImQQTiOpmI5lmptWyq%2FWxB8tg1OrnWNg5Zgbgt0sC%2FmbKlso34U%2F%2FhxvfB0k8cfwEXqZv8XetTYvhWvef7Vr3MYE91rXuE6LeJ0S9T6hnn21AGe53AQzpfhsNnOl0ONCZQRyH7k0SwyrCDPl4q%2BLQEISemf1Pv4yKYmHK78AhtCNn6fiRG7sPRdCGgtWJvQMeYfnMcE7z7LVzH3gbOAekh8bls2YeY%2BzGWbag%2FvOv%2BJXD%2FGYHHMvfv9t8KG54Wd8aQDQJYmnjSPB5HeJjE7%2B%2Ffu2WQdJe4wUfeOlIWCESs2N6MY80C6b6WOpjfzB1uKBjmWRBxykmma%2FL%2BQaWdv7tLoiDv%2FwP9QKe9j%2FUc%2BzEbNIsz%2F83F38URkj3wuK9PqVvAChAmXEEg8FHvY%2FjXfTu7duff%2F756y0McZzodJfceO46%2BvouSB6%2Bvg3fAgnZOtFbFw6N3sIHOQ3CO9t3o61zeheCWf3rH3Z39GaA12CzebD50mLKw%2FER5rD2sUemRMy%2F7RSiZ3oSANcMmFJkI8E%2FQRhphzP%2FAh77wnkAZuCr%2BN6Nvvwmug9%2BPncfvjiBoURUxGILewccoZMvv8l9icDfBknkBA%2FpjAQd%2FL3XhCcYcMiNl6CzVWBD78H1mtyoPvWBCxUnSyE6m5j%2BqOc8YwrEzT79mln8exP7l8Eq%2BBv88gsnSryY8v3l3%2BL7o5sCS2p7h16jLTR4AuFgcgAPyHGVIU6xCp1uZN9%2FfwH9nhQWFsxZcg1Fi3hli525wlG0tPoVCTgsdlOSjF%2BxbhwtAiqrWdWk0e004hNrxCfWiHfSiE%2BsEZ8Yr%2FbwyHDVK0bV%2FHvlCCEmOvGddeK9deK9deK9D2VWyOwImQvJjxjEMxvEMxvEMxvEMxvEMxvEMxvEMxvEMxv4BKwirz64ADFFUbxH8mhk%2Fg728PkhmSmRuNAZBCAmAYhJAGISgJgEICYBiEkAYmKAfP4pv%2FwLKORHaWrKGEm%2F%2FHJ4r3aGpFkglp9Uy%2BcV8Xe9h85oFjrs6xDen1E0aV181KIA%2BG5bx08u7QTcegI8DpfWFNijvNARfQHVu6IbLoO1V35P7K719tvSp8rjIYvS6peKmgewhhpczIBESASzAHbZtu0kYdqBGNVureMiqcJMX5NRhY6xhI%2BIu2DcD%2BJEKOuzPEqMah%2BF%2FY0zybbg87Ku1rgJoV88GYjxXaC263u7lJd6OMjKaMvw2d6zL9jUtkLDnV2t%2FCT4hZSm72ngT0a%2FVrMCH69mGp1QmppcN9QXOe2%2BYecLoBCuQ8cJwfPBYATvYqMqBmr1fM8aYhCPVXp7e0vFRCICiCHKpcx%2BDGrXt3DUnpee4Oeq%2FdFSHHqpHngFGKolkouRD9F0SktZD%2F9K1Tt4Pl5Zgw5ElLgPobPn7LKSm0k1uPrhATIpQ8dEhqlsWxXZ%2Fh6lqlNLty9yhEhkp31PSHs9rMQe9WBgmS8CGGZ6mj8EdlGtAhjq0WeNkLERlLBULbwrJxp9dNbFcHDx%2B9C5zY%2Fl99Tze5Ij6jFbZnnSqqRnJ5ELxGbjwmkZnmJiY3DjZuFDwC3KEXhT4ZFRX%2BVG4P8ebPxGWIDbLqSjxnRKUZqFn%2BmDE0ZwSi3vhhemVfJKByI%2FAi6A8NBz8RmIDA2h2OAA0wccGTlW7pPiW94Hg7ddZEkJLSv4oheaA4AD37c9N3LqzQd4AKoolWE1Hwcf1CpGzRQZu3LhEuDXbBVkYkENLlcqnAyzSTAETw4Fao5ilNrd8FA0L0Slxq66tq0ephZWmTFAxx8UmRQY%2FiyDJMq7H%2FCzjdrly4vDZKTtf6oPqKyUQk7zFWsI%2FjnALc7syDlf16XBJD5Cv8xkwskLmxXOhyPiZEx2UaieTgTZmdjTlN0OWAWLFOo%2BHv%2FouM0AQZjrxNDcrr2EmBSxsmR2wMDvp%2BY2uHCYWNczyUcjlB9P8UJBjMuWtTnQvOG9e3tLzt8WbiCU1J1oOBK3sXBaiZLdLnS36aCFcwtNWX0M7min0eH7JsPIT2MRRhhPpSJLh%2FQuXUCbpgvqV8XFPguv2IMqog8MFlrTuufcignSUjXZf3bdUXPAhER5DiKt9mva7gSe3r5xPTeNJEaHB%2BPfjpISgoIA3T73wks%2BDu%2BDMIlWoe1HD8CS22SqB%2F88qpw9yrL5eI3yNNjXzA8ZpSnoV0zBAPyHKjL1EbXVCJk9wE6vPEb5uTJ9n2ftW0V8lo0k8cS%2FnYIMdOOxcq8mhvPCNMDodd%2BoivapxR2Z45Ta3dIsY%2FCz7wX2Zu8olX435riKV5BP7xMnKvtpt%2FjslQ%2BrxMpIcRoONGrPiYatwLTHGGeSEohHjVC%2FbOBeb8CsnizB94xS5wTN7T%2Bl6%2FL7%2BPUsQhYVMg2OR5hwrVUEBSWK4QWjib2zMZUhoVUU8pHxcDQ%2FpJGHSNVSKh6AVn9ZdO8efnOTvESfPESioJIo4JXF%2FBAeDxkkMHUsVJXEQiVfXCVfXDUoI9HL6fj1yXdTyXfTyHfTyHfTKN8Nr6rVs7Eyni%2FXyJfC4y5os2rZ1moWEwdNq%2BiKScnUHhiL6ws7M5yPwufDJrnhfJxVGwccE%2BAyoX0k5nZYPGZ9nN6rjSvuBpebAjuwBkAnITkOt61yL5sKUXhNnIp%2FTegO%2BMGBk3ExlZFJgY0EYdkBRg7RakAjgzb2N9BJAYjVNt%2FIR1Xc%2F2IYdD7AgwXhZmhvQiyvDeVmk5ly5KHUz6pcnJSb5mMrKRhFxR7MWbqhAx7v3smj8PTRjNqjEadVrl%2BRsD7xbIcHV9Kh2UG89ApTrlmWezKYD%2BrjjYrYaHhSnHp6ZSZGIUFobx2gCnfOPAyA%2FaWIpkFOxO8bjatEkDoMU%2B885wMIyTfVxHN%2Bvl4TVXAf1hCjNiR793TLlm9tbzQnhtTNSInYoVH92qhS42opg8qouhHBNfXAOBM3IvkxPM7cW%2BWpXKXudu0t5VRG1TfkyMWJPgpG4jB0v4TmEZyT7bRDXFXD5ZxaBqqcXhGFPbWeyoCKIFjF5fHwhBhTkQQFv8nBYZUISyludTYbTAi5Ma2aDpT3oJ7frzvm2cmXYZDsHICvS7Mi%2FYpjXrECjIFKDee6JhAD1BrOFO0hxlTsQCm6F2AqSkIqxn19r%2BgeGmXUYKg82RC2QQOvFvjPxMCKJKR1MvagumdRPCAqRKUpODcmVKFfMQpG9d0OD60kTjAlOnOHFG%2B9kiypKtGeARVp0GDPfhIC3yUCknMerInTK7KQ1m0YI%2FZIwsT2gX23Xcrsbe2XhEOjjNoHRd7bALoHwOHZY6Ms0h5cNhmGy0F%2BDC%2Fo6LB%2Fp2iyv44I30npkRUw1ggymaL0alnhtRNmPTnRRflZ6lgplUyKgkxU05H1%2FHuub%2BVwqrVWejqhAo2HksGTUk%2B5oGb%2By9De3edxcuUK%2FZoQup6X5NsCnoMYyN8QQyzypnjRGusHmTo%2FR54Tx%2BQnk%2Bu5y1KSDwxSajJJjFg4G7uuaEo1d6KlotxoIJkWU2QihFXwCnh%2BiAjPFZkIYRUySaGQSQoFT1JQe%2F3wR64kJcDJt%2FZPxCn1aqR9EzoPbroRpBdEETQixCAKFGQaQ1FIKMhMhUJmKhQyU6GQmQqFzFQoeKYiP4SXdsCxy8TdOBFqGNs6G7eevFDUqjsG5Cq1Nt%2Fitdw%2FzFZIItrmqjvqyJXlIfUmpOe3GcHcAuv67dqYDPz67x3PDdDXZG6Yix9ouOwJ6yXEyyj7eoz2txRhhS4RnzKznJkLqMjPhILRPPW6NCxzj%2FMjnAS6X1EY309yW9%2B%2BlRXrxzyQPMWuize0HCi4k716h%2BvthNRz3BBsUmBVr5a3gkqlQsVW%2FgPl9j3FdQIMcmdVTuiA%2BWc4m65G0%2Bvv54NL9ASoVwYIwDa6G4UhHmDLeQK0fhwrSGFt%2B%2FR%2B%2FuzrpmY5rG06U6AI59gbtKZ5ke7xyGX%2BljO4PqdtLsHPgw1CZ4MtzVMruMNvnSmTQVdVtrXN2lVKMTwoYdg6jH7ahCKhkfmHRb17ZEMufYNTiDt%2BckQuP8harZWsp88H36y%2B9XWjJy1HUpRK3AxTLAkHr3m7tLGaebHFB%2FzFxcKBvQtiynlEzmsODXS9Ns3Ve7j3TPjw6R6KTPUy2W7t8BOJ%2BnNVET5R4AVh9r3%2FZWPpqnbbsS42dKnTNynT9igJmW%2F6UGK0cGBZzPXjbJEttJmRE8LqVBgGIXY5UT4GZqdLg4GvIaitu9jjvuuvm6Pu2xwVbfuBrtLdph9ZEzpS1n75MStZBXCFBxe24OKVOVxIFKWienjffer0pcMXW5sYWd9oqswalOcY1WcBb%2BqEH9IrTgaF1clOrrXqFztTRNndj7Ywz%2Fi48KED%2F66br4pOhKs4kPxiMNY02SEqVCkuhQswSOKgNPik9v5O8Wq2Unzfmv7KkYNr8iEiPzqf4N5KL2pvBMoeEwaQhnPbjyTPlsKnx9unx9AB3s5XknfiQilKvKdHKUjAP4ObHxy0zRI4c13x035tv6UC5t9MgjiqlGGKLv5yUZMXpAns3Brc5gfk33TDkdy4FbtxwNuVW07Yu10YrO%2BfHp1n7cgBJHR476x%2FhGJe2xGimndwPtrr%2BIWImL53bxL0lM9BAnvvSr62WOowCTZOcZ%2B0Og%2FPs3KHY5t6wuVp4P%2FK0%2BS9u1%2BBoeg0oAG7otswr9Vg3lIeIWAdv%2BlurHnzf7VjERpf%2BKKeS99wH3sEcJ9%2F82%2Bi3TfpfS006U3trTOY7ElI4NqAVDu2%2F6fz6aLwPOAmf2gnLepyQBRfQ80%2BHDu%2FOnfFfGbBVjL4MZ3uOHX%2BMLt7%2F8HEj5JrSnes6DK2Bfc%2F%2F%2BTbW7huij2J5ytzKCv2UUsUaf3a2Dt4%2BdLLh3avmr9RX8w0R5knXneh3OeKp6yR8BPOoP1cwf5Q%2BFP2qrC%2B%2F6fi4J%2B%2Bt7GPO83wxn%2BPM%2BFRgeienp5KqwCeKwGXUso%2BS1R8l0gCZ6BHNasXebC9JP2a79700J9W9QTHt0GssClKlb3qzxFc5BDnrtpBKFxwAa4vbhjQU7S3N2Duhg46uOsW%2BELA5QL%2B%2BsnY38C%2BDteRvhgOx19KX5h6X%2B9Lp9J8sBgvpaGzcT5KsvnlQVSgJ6NsReOi8MVFBwo4isHjwcYduPYEobO0faipAJvJ0%2BPGXQenUbB27eSj9IVh9XoqQObqu9mUicdHcy0aD5UvHnLPIgABMJQWFkV3J8D%2BOWCiQydIUbIDIaHrJOFXWewXlh59Gg6CQNEPHlAsCIWr19OhcNmhGzExvFXuRWOo8cXQBM8yQSsZnv4ROun723ehuwZOcxKWmNieC%2FErUFE1q1C5nikt50xsbjRfNDY6X2z6MonN03%2BDaSbYug4Un0i6cH1Yl4qkIpWwcDx4BGKkm2ouOblZUpgw2eJhMjirIbRLBE6PmyRd%2FCr56A%2Fbc0qQfnASHzZfIqtVG3mkupqqUgddZ%2Btt70406CZnvTVIzEdJGOwyXCJpcHuLmmilp8c4tP07eF6EEOr1CoTWTRFa9x9EI9TnLJbwZnWI3O3WvUt7Qr%2FKpQfMntVDduRmwppIm6fHB8cLdrtMHgNY5QKwpjODWQOyx3Y7fux5ooG0%2BAIJmXLqOI4hakgvERBy8ynyzjj2%2FWWmO8rZEddI%2B2ZLw2yChK%2BvEnaebXH0H4R75Zzdcp10FWzpW%2BCLu2C6QzhoddPbYwOxUW%2BFA8HZD5cV0jGwJbTEGqVAJegkeMEdDNC%2Bqs9vQHKCexDCwfOgmV45YejGAbTUlHMnDlAwS%2BnpzRVso8XCAeXsyGtmDc9IssPt02M6X%2Bk9qx7dseerO1O4Jy5zdsWJCSuBMvVgu142L422Oy9wc0eo6JyX5mFwC7wpF9JaeMXMde7aQAYTECDCcNCDSFq9Z%2FhGTj8UjiRnx920cCAjqQiIIxQrfyXFOa64IxrtgjCOUEJB1UsXqVl0c98Xb885%2B%2B06OD6D7nW6YD6TMRDiAQRMTdMAAhM79J1Tzz79YHse0MdMathRTKRshMPB2aOGxAbzIHkApjjLr62j1MvrN7a9P6sfhb82ZzdZ0yAnDloCF0GzMQ8dEN2GUr69Qc3RZU8%2FivBAQebs4Jrgsa%2F9fEJOl9Zm6bUT1HQSSfNCHo7VjNj6JBafkhIdK%2BmO%2FerKFVRpUMzKOWefhjCnXqFOx5rm8C1P4O3XxMl%2FuH6lbCECwnCeAAjtyCmqOtgWmJXdRa2sn%2BkTMeIl9TXBHS%2FwqooEUZZuvcCOTz3nNv4tqiy6ZYooswBYJuPpCir13ij3d1diMbOXfiudTZbjg%2B8OfozyCWKNFs4DV%2BoUjWMZOwUuDf%2B8aixWAcz5OcTlfLwYDVfj2XQpnY9AELIaTUbT1eBqtJTAaaMFOHm8BH%2BOR1fg2EFAwGtoaBuez6zK0qtA8hZE9RCX89FqCf5M%2F5E3HcB82c4OYzR%2FwpRvmtLNA5ck97zzJGWaTwtRUZOFnYp2Dfisqiv9Ph26%2BbwZcra0C4M4PwHgtQt2iVeuEGHAZXxmBRdzj6StmqAVSXGZSmkAjvm5lVlIPVRwPVQ61MP%2BZ1YtIfVQwfSQjdyRemh9ZqUTUg%2BVUg9ZaLXTQ7RJymdVDpFVQpjUXA9TeVJ5aWJZMD1C%2FNCmMoKdU84uOSl%2Fail%2FLHTbyp%2FyudVhKAKoVQVQe2ECqH5uFR5SALVSAFnothVA4QEB73INRQD1qgDqL0wAhccNvGtBpADqpQCy0G0rgMLDBN71HYorbOCusNGZK4w2Kfu8yj6kL2xgvjAbuiNVUXjowLsu1NfoeK0mTfCq6GJpzbbO4ZRiipXwuIF3MYkyM5jVmcF8WTODLD5xy7tYRZg%2Bs5wZWOi2nBlk4ZGDwjlyoAhgvyqA%2FRcmgMLDD4Vz%2BEEKYL8UQBa6bQVQeOSg8O4NIwXQqgqg9cIEUHyFhnP4QQqgVQogC922Aig8clB4d5ERAij3KgKY%2FvMFCaDw8EMRXabIEF41QbetAAqPLxTedQpSAOWqAHKrGHISQOEhiSI6JMkQRiEJE95jQhJZeEiicA5JDAIrpSqNh%2BsZvV4vc9EO4qIIjyMU0XGEXNZ4GJi0NmKK8DhC5R1HEBkmuSjyQIC4lXgagCc8QlA5RwhEhinDLtU3NnTHmXNFeMygco4ZSHOuYua8Vb2rpTlXxDcjCe9GkjVcMbmVvhqAJzw2UEW3I2XYpYrJhu5IxRTu6Ku8F4ATiqlhitmqDthWMYW7%2Fap4t79aFJRfWFFQEe72q7zXfBOmrywKMtFt67IJd%2FRV4bUH2agKILeyIB8BVIXHDKrwmMEoBbBVJawBOsJjBk18zGDirgm30lcD8ITHDJrwmMHEXBM2dEeqovCYQRMeM5iYa9KqDtjSNVGFxwya8H4luVoUlF9YUVAVv85BdL%2BSXBYFmei2nRmExxEa734lUgCrRUH5hRUFVeHhh8Y5%2FCAFsCwKMtFtK4DCIweNc%2BRAuiYKvoZL6W4Nl6IKDyw0zoEF4Zoo2CKuBtAdp4qa8ChBEx0lKOUqLiZcLVVREx4l6JyjhD6BDr6KS2myFmldbCCENjbJdu09jJPwgEAXHRAo2JKtRjAdpXXCAwJddECQ4YUCAiZexwQEmvCAQBceECjVJVzKC1vCpQkPCHTRAYFSLuFiott2EhC%2Fzll4QKBUl3ApL2wJlyY8INBFBwRKuYSLiW5bARQeEOi8AwJSAKvVGuWFVWs04WGDLnoRtVJWa5jothRAXXiQoPNeRU1GpPgSLqXDJVy68BjC4B1DEMqLLeFqAN1xqqgLjyUM0UsWlLJuxYSr7b4i4tHhvWSBVEW8bqV0WLfShYddBudQglRFrG7VALojVVF46GXwbkEiwlSsbsXE65gwVRceQxnio4Rq3Up5YXUrXXggZgiPEsq6FRPdtk6a%2BB2nxJcNLHxm4Fa1agCe8BDLEF42sLCZgQ3dkaooPF4yOEcEhk7HC80MxS6MtU0YpdVosRivZuDnpTQZLdjAGMJDJVP4mmZlUJ0GBi9rGjCEB1Qm72ID4ZUMMK%2BEBe8xXokhPFwwha9wVs6q4nj2wsRReFBh8g4qCHE8w8SRBe9R4ig8oDB5r3cmfBS1h%2FkoKrfVzg3AEx5hmLwXONRVOcMO6XED6I5UTOHhgsl776S6YmZ4IcVk4nWUYgoPHkzhwYOK9xypHfYcGeL3qxUdPKhYz1ED6I5UTOHBgym650gte46YcLWM403hEURfeAShVhfBq9w2j%2BbjspnCI4g%2Bb0IFQgDL5fRNgtcGmAiPFPrCCwsqvohe7XARvSk8BuiLLiyo2CL6BtAdqYDC44C%2B6MKCii2iZ%2BJ1jGtmCnf7%2B8L3T1XxRfRqh4voTeExQF%2F0%2Fqkqtoi%2BAXRHKqbwOKAverMiFVtEz8TrKMUU7vb3ha9hVqttWeoLa8syxVNbcI4USHHUMXFs1ZfVUhz74pkuxMcN1SX16gtbUt8XHjdYoluT1LI1iYluy8C1LzyCsMRHEHhrktpha1JfeARhCY8gsNakBtAdqYrCIwhL9JIFtdxZmwlXW1UUHjNY4mOGPq6K3FqRGoAnPGawhMcMfUwV2dAdqYrCYwZL9E5HatmWxYSrrSoKjxIs8VFCdTsB9YVtJ9AXHiVYwusJ5XYCTHRbCqAlnoOiJzww0KqbjGsvbJNxqwMOPNE9SRpWa2bie0ygKn4Jg9wT3pSkVTcd117YpuOW8HBC7onmXdDKGisT3rY2UXj0IPeENyJp%2BM4OWnf8vIp4nmy5J7oTScO2e2iA3ZHaKJ4Cryd65YJWFpyZeLXVRvEkdz3hC5y16hYP2gvb4kE8p7bcE73CWSv3eGDC21YExXPh9YQvcdbwKqvWXZVVFU%2ByLfPmgCYnBKzM2gC7o7RR7YAzmzctNBkvYHVWJmBHxAtqFxTawgsLmo7rJrcyawP0xEcCvAmgSd3UMd1kY3ekboqPDYTTOWvlXiBMvNrNlGoHZNfc%2BZxVq45Ptc6sdVpnbgKy%2BICBN%2B8zKYNlpZmJb1sZFB8w8KZ5pgQMVepi7WVRF6sdMGnzpocmRbCssDLhbSuC4gMG3tTOFBGs7vegvaz9HtQO2LN5E0STIlhWFpnwthRB8fTZMm9yZ4pfjO%2F4oHW344Mqnhlb5k3%2BTPrF2JYPDbA7UhvFBxLCqZy1sszKxKvtxlwdaKPwkoKOr23Wu1vbrIqnCZd50zQT2qhji5sbYHekNooPw3gzNxMZJB2rODMBOyaDJJ4DXObNwUw6a3q14qy%2FrIqzKp5JXObN3UxMD3pZcWbC29ZZEx9O8aZwpohgdVmv%2FrKW9ari2cNl3kTQpAiWZVYmvC1FUDyJuMyb6pkigtUyq%2F6yyqyqeCpymTc9NCmCZZmVCW9bERQfdIlnhNbxMqveYZlVPPW4LJwSWsfKrA2wO1IbxUcVvDmhSW0seQWYeLXVRvExhHgaaL26glV%2FWStYVfGs5LJwHmi9rCYy4W0rguKjBu5E0OSEgFML6N1RC6jiOcZl3hzP5ISAcQs0wO5IbRQfIfCmfSazJgaWNWlVWG2ZNRFPIC7zpnmWe3IdLXxBr364CMhXOcUzjMvcWZ8J5cSW9DLAO145xbOKy7ypn8mpsqw4M%2FFqOVWKJxGXedM2U7y1asVZf2EVZ%2FHk4zJv8mdSBMuKMxPetiIoPmDgTdxM8dbwirPeYcVZPLG4zJvYmZwQsIpzA%2ByO1EbxwQF3MmfCW8NYBpiAHeWtiY8FeDM5k7pp4PVno8P6s3jacJk703NdNw2s%2FtwAu%2BN0UzxvuMyb8pmYKTPAVk3wajlTiicOl3kzPJPOmlGtOBsvrOIsnnRc5s0MTYpgWXFmwttWBMXHC7xZnUmThq3SNQSv0lXFs2DLvImeCXfDwFjZmYAd426IJ8WWeVM2W%2FVVIDlYy2bSxc3ZEM9oL3MndCZdNXyvbaO7vbZV8VzhMm%2BuZtKuYZttN8DuSLsmPjLgTd9MzpNlTwITr7bzpHjXnzdBM0Ub8Z4Eo8OeBPHU4DJvumZSG7GehAbYHaeN4unAZd4MzqSXgS39ZgJ2jJchngtc5s3YTAmjqh0KxgvrUBBPii3zZnkmp4eyQ4EJb8vpQTwHtsyd2JmcHvAOBaPDDgXxrNgyb85mcnrAOhQaYHekNoqPq3jTOJPTA9ahwATsqOlBfCjAm7aZopt4h4LR3ZbjqnjOa5k3iTOpm1iDQgPsjtRN8ZECb3JncqYsGxSYeLWcKcVTX8u8iZkpzlq1QcF4YQ0K4rmvZd6EzqQIlg0KTHjbiqD4eIE3GTNlQsAbFIwOGxTEM1nLvOmZyQkBa1BogN2R2ig%2BNuBNyUxqY7kknolXW20U7%2FzzZmAmJwSzuvO4%2BbJ2HlfFk1zLvJmbCRE0yzo8E962Iig%2BQuDNzUxOCCZObG12R2ytiie2lnkzNRMTgokxWzfA7khtFB8h8CZvJrWxbElg4tVSG8VTW8u8iZcp2ohvu252WDYWz1ot86ZoJrURa%2BhogN1x2iie0lrmTctMamO5HwATr7baKN7d583CTHHPqvsBmC9sPwDxRNcyb%2FZmUgTL2jsT3rabZnUAj%2FAFBSZeezc7rL2LZwaXubM1E%2FqL1d4bYHekNooPlnhTNpPaWO4HwMSr7YQgPljiTbds1nv6zLQSPES62GSr8HVwD5QN%2Fh4Faxccb4CT%2BLCIN9UyqXfYluqNcDpK78SHRbyJkylmHq%2Bhmx3W0MUTe8u8iZVJccNq6A2wO07cxNN0y9zJlOs1dBOroTMBO6aGLp61W%2BbNpEzRTbyGbnZYQxdP2y3zZlomdROroTfA7kjdFO%2Fx82ZXJl2wsobOxKulCyaeuFvmTaZM0UacudvsjrlbFc%2FcLfMmWya1EaPuboDdkdoo3uMXTp1slu0ETLzaaqNwR18RT51sVsm7zZdF3q2KJ%2B9WeFMnkyJY1tCZ8LbNkAl3MBTuZMnEhNDHl%2FX3O1zWL577XOFNg0xMCH1sWX8D7I7TRvEsBgpvYmQidOpj28ozATsmdBJPZK7w5kEmp4d%2BdZF%2F%2F4Ut8u%2FC%2FomOF%2FplRZ0Jb0sPRXwBRRFPndyvbivff2HbyounO1eEUyf3yzIyE962Iig86FLEUyf38dXs%2FQ5Xs4vnPleEUyf3sdXsDbA7UhvFx2TCqZMzwFIPpVVJva2HIj64Ek%2Bd3Mfr6%2F3u6uuaeFpzRTh1ch%2BrrzfA7ijd1MTTmivCqZP7ZX2diVe7mVITT2SucKdOJrURJzLvd0dkroknMld4cyKT2ohV3Rtgd6Q2io8NeLMkkzOljs2UrVb2t5spNfG05gpvumMZ%2FlBFC29R6PMjNZ%2FOJtL5SLo6GU1X49WoAZriYwLe7MikrmItCw2wPFJXxYcBwqmO%2ByXhOxOvtjOneKefO9UxmeaoEr73Xxbhuyae0VzhTYhMimBZmWfC21IExZORK7zpjykiWF3d3n9Zq9s18YzmCm8OZVIEy3I0E962Iig%2BfuBNkkwRwWo5uv%2ByytGaeJZzhTevMimCZTmaCW9bERQfU4hnUraqS7qtl7WkWxNP7a0IZ1K2yiXdTHjbiqD42II7kzKRRbHwJd1Wd0u6NfE03QpvjmQiMrOwJd0NsDtSG8WHHrxZk0ltLAvQTLzaaqP4qIE74XF9zZCVFkfRmiGryaLk9muGNPHk2wpv5mNS77DF241wOkbvxDNpK7zJjknBUjHBalIYfY5giQ8KeHMck4KFFZAb4XSUYIn38HmzHpOCpWGC1aSq9xzBEu%2Fq82YuJgULq342wukowRLvt4vnMrbw8p7VYXlPPFG2IpzL2MLKew2wO1LcxHvyvLmMSce03LebiVdLx1Q8N7bCm73YwMiLL4dXFTwunx79p8cwh2IYePBJ3Yc073AVrMEvh1FR4XKCDkDh7K3r2Nx3ObqogHJXBeXpEVgg%2F84JI8nxpQvw9%2FXheQ5C0kE%2FGW%2FWYQuzO5eXUwDJ8HI22Scul44PrPDWCV1HmiLjAo%2BCMcF2a%2FsbpGHl%2BdIsiUNHmjBq5gA5RfxSToU%2FIbFChe6thP4%2BGg6WOJ7D2eRyND2XLq8H56Or2fV8BA7uw7MYfOVIZx6QvUPXYYplB9U43kzEsmo%2BD9zvBtORdLEYTIeD8bI1xHsuw7aF4iNy7mTGzxPfyWCxGk%2FHf7s%2BUnzL67BtQwfOCe86gWI8D9zvZqvVM5G9zC9ifwrimD1ZdVAB5M2ILGvWc2Cdzq4%2FjK6uRtJwcDU6n03HR8ruNEgeHOAzSUPbe3rcBL7bAG3xKSjulMr6syzEPPA%2B%2BTBB4CC36el%2F227kNAHc2wv4fHb13XS0HLcyyeJjQN4szLL%2BrElvabvA65q7IKJxYLw8cX9KHA84bkcJ%2BXIlzcejxWJ0OlqdTqChvppN2aiLT4jxpnKW1d4zUD9djK6n49n0OJCvbGnx9Jj4YAx7GhQfXPCmge7vMyDj9F%2F5H9PlfDRcQTQBKqMFsNKoRXQA%2F3k%2BWExGC6D60wE8A%2FzGitcVRTx%2FvcKbzdkgoNoTr8NUD1XimAIknipW4U3irKlVVN5Ko%2FkAqhzSTPTX4WgxBPIyGf8nu5lYMcTvUKXw5mU2TRoGb7MXP0UvDo3MSDq7ApMjhMQJgahM3I%2BxAy3NcjavmKEGKInPPvMmZO5TJQWgNLm8SCFZF5igXPPTf6%2BDVKFwbQL%2FvAj8GMyoDnhXO2FhpXWwgaPCm4C57kVjWE1qGiWlIcVo9XdmllTvoLGaN5my3OvXoUinqVJrhqNLKbc4tcP7p36K1qVj2Ea6A7eVd%2BlCaYRh7k9dgpntB%2BSdjvZoIPqdPct3ABV3suV6SEXB6mIA%2FvwwvroaDGfXwKR%2FR5M%2B6knthJG4BNsDFZ%2Fl4s3PLMvEvEBgOR1dXNIwzo%2B3gxWOYmu5%2BJQWd%2BJmwg0hAFtdHgqH9uAFBjHhEk%2B%2BoAilbqbjBSzh8P1gNRpcX42ntDmmekJbZCujmRCLJ9dVeDM3y6rcDOLryWy62otv8esz0EVj2diKz%2F1xp33WjAbYXoC3H4yno7MrIGV0gIlT2qJcuQB7iurAUnAvxGgNoIaCdnU9TBNOjJ%2FbQlwMZsIrnoVP4c0o3QzexWz4fnQxW%2BwxE5Wf28JbDGbD24GDxTuCUpgeLYBgdQ3LL1Roi5%2FawooGsiEVn5DmTUwtyz0mpLNLqiGYPcMDg4PYHpj4KYw3XbXF1PvLceWPVqjBQezGC%2FERKn8WayI5RMC2GlBkLzvaCsS7StwfwaZPP9oFYRxJ9tMjOMroXUQVJ%2FFTEm8W6z5TMnFBa44oU4s7qItwp7RWmMYQA2v%2BLdUu5odbCCcawwK0i70keLNcG%2FQ4nlRfqJCrUiEHmUJCOGVdYRcROlh0w5vQWpb1Ojj7ZW1xOYaVhbkduhCUywXNKO6RuNpl2NNvB3LGuyBDQLm4HPwNwjO4%2BLt0FoQbx04%2BQgCeHu%2FSpvUKXIOfEhfVYepD2Mk18S4fbxprg5h0M7AagIPObA5OB9Mlb1oLwmLl4FxejSctRak6hq144mvm3Fmr9yne4nIyb40WNoSd5xKffOHNWH0ArPmwNVjYEHamSvz0yJu%2FmqyxLy7PFqNVbqEXju8Dp2E%2FWmehE9t3pUlPB7BtVgdQCdxNqgYVG5vs1IboiCdCV7gzU1O0Lkfn8mzaFKRpU4j0DiDizURxQIAuh7mqNQNq2FiUxDsH3BmmiVgaw2nKxGkahHDhkpsC1SAB3sEUx9sRJxK0GELzq3aiVJzPnt%2FEe%2BG8CaVlmeKGv4dB3tnZQBpss%2FB3D07v7QSEyRsnWyx4Kl1A2UINnPZm6%2FrgdiH45wNwGtwIwTlLG%2BOXwXbboHtTfAjInW%2BaqCGimDmPmPfgOE57glIU8zgbjWFrp3hvnTvpNNEmACKU2SL3kCZO%2FMt%2BqK6CMCxC5FV2OluSxM%2BGvCmnDYokpSixkUnPbISMIX6hJG9uaZNisjJkLgdXy%2BYCVJ7OdqfEqxl3EmlK6JLDdDa7Xly2AaocwJYo8VabN300zTnPoRoOmuJUnMuWJvFFMt6k0WRLfgnRxbCNKGVns%2BVIfMMgd25oimWazof5zHbleigVvt8x34BZHzlUQ9uzkcdQDmSXujqQKs6OukW0s%2BWA4cC0Qyy9QjPI9A5kjDtfNEUT54PhoPCf7DByDkvaPAweHOhqDrwd3A3l6Z9wicjJ4JckJK7CFrsOPAjOXrpBiaAzCPcgdpqf0RwXswPZ4r7RKkUfc2Auh8DcD4MwerZgYZdga6ZwL0Llzh19QDPBZLk4XjOrV2FLoHDNVHlzQ%2BsUT2zB0Evw%2B9WnBquJ9Q7g6MCHT%2BG4HFx%2FQK%2B9H5vF%2FdM%2F%2FVySUqCyMex4uQOoeG%2B3SskeI6j%2BDu7eDqliBHvuE27jVd58zxqefdm%2F7RVshQngXlanLP4n1PUnPPOp8qdy1qpAvJWGywkUmHP4x3y0GF9ej67%2FU6pt9bVJpKUTPrhruLTWg5XQEEjOxt7FT4%2B5hOUnRtmJ2%2BJEOzsRCOAcdhfdJQ67cKp1IWbcW%2Fz7e%2BBdwD8qW4LV8d0PWxt84aYBib1xvCDZMefKDraDVPkzQfcOQ4x29Goivs%2BG95Pts6FVhdeyVd4E0AzprewI9mzrsGTACxy92PXhzjZMiMVvbKXyJpGu9H8hhLPpKAN6H6wEWuzJSXjuVuXNES3rDHQu4R%2FUTb7aiONlCG1jE7P7jP2%2FNFV4aV3lTTUtV%2Fak2Yc7ZW8uYcBTtxZjQy%2FeH%2BNPXK0fhh5ZXmAZyx0DW1ne6mim0RCfTFV5k1fLMkN4EQblJl4TILjgL%2BfX0nK0%2BDAewv1PrsarwXgxkgbng%2Fkq35IqHzKYjKGlAf%2B%2BHE9GU3Qm27ESH0ryJrWurBfAcbwanKM%2FB5ej6TBF52R2vVqMTiejBTAL1wsI12R2BnE8DA14IbnfgdPJndFalveAs0w3bj%2F7MKptM12LKtEOyacTJ%2FxKWgZJdIptWg5%2BBfbOjaHPA%2F6a3HjobywgFV385hIqb3JrYy%2BOo%2Fk5tG7nQLuIHeKWUiFvTBMmnltI5U5gjVuwOTRSy8Eq%2FTO17VGlsegWUcbFzvreT0UmowUAMgSmAb%2FRNnFQEdUu5kvuq5A1OlZv0z8tU8ZhGxCwreqwzVPYykU79SAaarcdRQ4YGoZMJ0QVz5Wm8maxlhWTAapyGFRCFveAWsZ2Euqai8v%2BL2bSUfw2ryp3smuZAauKDF%2FqhQzOJ9JqNHyfBtjA2YYHm22FicI98ekG7mTXlT3ZaPhofHS58c6r4vnWVN5s1nKPZRANDiA23GpcE78OXuXNd20Rti%2FKwVqDu4fpqqA99CEYXj626pO4wkHggJXoQHl5L1sgfBYSNvDTcD64qHh1QxBBIa8us3CD6%2F%2BULhaz6Wo8WoyWTJ%2FXVMWntbjTWSt6I6zSn86Hi4t5dVvgIToJClwiLZx1CG6Jgv%2BCOLhsQKerMEv8xPuBvOmrqz0bTEwXw%2FTPQgyBrC1B4DoEocVoBRdyz0crGN0u28284CHE56D5c1tbrbEjoTwfLo1n0Z5CKZ7b0enGOU2715hKb4hfF6jyJre2iGm5AcbL6fkHzCXMZRBFwOez6XQE%2FxxJH2bfDS6ZiRZNEU%2FGqPImvTbLaWU0vELgjGLwvC4IwJDNS8HzJC%2B4A4NRKEEPf6XDO1cgn1l8qp43rbVero%2BYzUdSCk%2BcZY3WhH93Ks120FmJnSRk6pmsdlCz5E1bjZUuECBvpcF0OcSyldfS9BryCsCoKk3sLkfD68V4NZKG4w%2FjBiwDhvi9AVTedNVYX1ABy2owhrjcoU67inu%2FCm038ykGSRxAXyJCBdlIGvu3ob1ms0si778DnHizMdBwWpYw%2BRhMkRS7cQg5Sp8e10kIIWLvWtUTnw7izkhNYDIcLmGSG7hJk%2FFyOU7LJ0PgtI%2Bg3w48eFhNWaHZajqCNRJpPvhuwNgwFbnz4vcOUnkTUZsGAc90MF%2BiypwPe1QL3UIyMygJN%2Beh%2B%2FD0mAbay1SGGlTyNfGt5ipv7mmMbLLAaDi8mGflSzCbQ4ewqlxr8GxxCs7a3u7QSuSMFPjW9WGOEBqoCE30cbMKidFBMYA3HTVWIcmgG02R9qWb6Jd2e5ns0E5dSVjNRDDlSfy2SSpvymkSlPEUqdzYB05gnMSFLEEoMlcwWrvgydzbJr1EingmaZU3k7Sp1kGZXYzhFD%2B7uIDhQ96mkVZsx8B2Xy6QjYYhb3oMWPDsWIPMi3hTzZs6Gku8zBeoPgv%2FGA1X16xcE4BW76AdnDcZNEajhF44%2FaMHa2Lz8OnxFjxabi5OBu7hhDh4hA7MJ2%2FmZ02nIqDQEYgYjZzgITqoCfCmesa2dMcxUKkYeJ7LaHWHIHSg%2B5y9WL1PBUGrgZCuftiku3qUK5GYgIhP%2B%2FFmWtZlKiA6IRVRikWUYsNEQrw3wZsRWdeoSBgUJFLxmNjAU3e3DcAQHwHzZjDW6HpiUo1FuHn6x%2FqerR3iE2u8GYc1kwpDnyYT4Ya9MxJEQbxLyZsJeA8KFl0Y3Kd%2F3LFlQXwChDc9r0a1D3KPikJyw4ZA%2FKpe3ly5eyCgO5LJhg2BeE%2BSN6ctXRdkuif54HwKGQ0mCswGikeBd4KU6jXIaiWWgvm%2F89Ec1anOZtfD97BOdS0t3s%2Bmh9PrEBPxriV3slqqfy3XXctNAteNPNibgDlVqOLdSd5stRpdMOru5NCGxWDm%2B4t3Innz0OrUUFsmnMhEmgduHABZuLdD8FhMU9lBhwtvnll6rCXTfEi7wEHaZl41ExDx3iRvullNoQJCeJNQKtjRdwercnhTxWLMwzgANEfShnujhE%2F%2F%2BIUtCOJdSd48sPSgW6H5UXax78mMKRHid3ZUeRO00u2lQrpT2f4vEdwAJtwGbCzEOxC8KVbprqVCS9ABqQidhLH4EaIg3oPgzZxKtxEK4UYhFM6DcBPcsdO1HRS3eNOb0pP2CpGYSwAIyQ3Tl9TE%2Bw%2B8SUj36AOZkEOSED79k%2B00iKc%2FUXnzg9JjboWaiRsljKVIEALxbgNvDk%2BMNR2HgMzCZRDABoGrwGXPER14DrxdSGohS6l7UBeo3e%2FpHw3EQXwWijdBJtadhWMwIMUh3aEProZPNrB76wd7vXYDpkMpHhLeZJd7Js0zEpK0oMXedxDiID47x5u%2Bkh5hqUSONpEu7XDD1I0OAODtQ1JjbrUeWaRSADAIfLbzJJ4%2BReVNG7lHDoiwAsiBw2iBhgCIjyV40zzukQN6LHHpAjlgZ%2BvF83upvBkd6S6kSgsmTt47oZ14MRMF8aEEby5GemVbJWv8AAVIvwHcpw%2BuBzcpZ4IhPqrgzbVI96BUWlRxMvY3bA9KFx9T8KZUpGddVGpMgTBo6lGL38xf5c2XSC%2FbqNTgYhw1canFb6Su8uZB3DNfEonZRPo2CW0mAOIdaN7chnSroJHF%2FUi6gttssX0G8b4jb9JCnRpXaURWOkGmAJqEJmUKQ7z3yJufkG4SNFqN30ZYME2CId6D5M1ASA8pNYoHmQYTzXAQ70LyZhikO08axYVECJwOYs%2F2Y%2BbqAgiGeE%2BSN5kgPRmpUZLSCAymMy2esU3lzRJI73DQKBX%2Bq4D9%2FuJ9R96sfvRkrEb4juj94STRMMtgiHcdeZP30aNLjeY6wnniF6eBgRTvO%2FIm59sjEJSi%2FgTxXmWhBBsK8V4kbzY%2BukDotBZRuO2Z36BpWjwnocqbbI%2FuPun07oaJHbJtg3iOa5U3jR5dK3S6D5l6UM2gEO9J8qbLo%2BcidXoucmJ%2FchrMFqZ4R5I3IZ5OVwuKIzlxkjC%2BR5ZyEkQOi94IwiHeleRNd7fHStCSkjZEhF2v6mBDDt4EdntAoDiTkyC8ce9tZjO5eA53lTcD3R77QO8TbaoQ4r1J3oRzBjW2QjAs%2F74cD1MqplEU15rsR%2Bk%2B20vIfnAQFrSgXfj0ofGmktsjHnQve%2Bo%2B%2FeOB7VuK5yPUeLPB0VO0OiVFC5lAmQAId6413vxvJjU9iQBI9SOnlKXAUT2HrSXCvQuNN%2BUbPTNjUFenzVi0GVBAhMcdGnc2NzoE1NVpswaudl941KHxJnKj20qjHnVUaIKZMHQwZfAueFNNhUFEHIk0Tz5BJM6bNI%2F2O7AKvLcNok4aBhlzRNL8Ezjg%2B2gnqjKHzZYO4ZGHxpvQjB6IGQe2PEixgdAw4RAeg2jcycfoykLb96DEQZqFbrqhKxsS4RGJxptyjB6WGZQc9xmwpIt79p45feHxiMab%2F4uezDQoq9WgljRDQbzbzZuKa49TQXG7U%2BJcJgTiHW%2FeXFo6tTfCwBzv7xCHFuUo09MWv6xV482IRW%2BeMulJ%2FjSzu7QbiYZ4l5s3A9YeLCgudwrBqROfNir%2BWOJ9b94kVnR7adIz%2FksbZniZKIh3vXmzVu1BgZ7sX9oPAYM8E6Ig3u%2FmTUi1Ry%2FoixhzG9EIC%2FHuNm8eKXowZtbd7XG66XTK%2ByTJrJlD7SCDxZv5id5SdRiJw%2FvVQ4kQP4Pypnjag0M94lg6sF1g0nA%2FCEt8mMGbpYkeeZmUwsfE8RIfMTZlHRSNasWW%2BKCDN%2B%2FSnumDtuHadw%2BO5zbYcM0SH3TwplGi7w9hki01kXTuJB%2BlJSp6sJHowGTyTmhSI%2FE%2B3dleBtsGdkK8l82b1ohe%2BulTGrNXwCywZk65J9635k1ORPen%2BhTfGiLQtOsQgSHexeZNIkTvQO1Tstsf7MPt%2BQgA8d41bzqgPUaBstPaBztZe6zWEYSCeL%2BaN%2BkPvdbTpzfQfHD8zdNjAxzEe5O8uX7oLkOfvk3IB5fZXYZgEO9M8ub22WMi6V00acjZFAzxbiRvAh%2F6cq4%2BzY38EER3DNcJgSDei%2BTN2LMHBOq2vd81myzFO5C8eXl0aqxpUXaEgETebgyTkxCSM8e7DcKYDYl4d5I3DQ%2FdYFrUPolRFDWSDFm8T8mbUoduMC3a1nPQXKb8TA5rUTwCQ7xPyZtKh75Xp7UnbYuyEEvb9WMQf%2FqMFhKEiHgnkzePDj1taVGdTC9d3cJKySAkxDuavPly6I6mRVn0h5Bgt5chHMQ7mrxpdOhLOixatQtuqpLYG8cLkl0DLMR7m7wJdUxqCAawSBc7le3JsD95OQBnnk4Gi9U4Z5lKD52BQ%2B9HV6PJdwcxSjkBxfsevBl39gkMvSQI7Efs%2Bk1oyWRZvEfOnXhnj8DQZ5jF0%2BMd5AK8TD7ZTYyqeOecO%2B8OfXrZUyIEeCS%2By6ASQEh0oCWc3VGD7ocRZbFN5nKczl3gqadVEKgtHgMW8DKy2UETK3cKHmr4ZmVlMqeyrvZTEDO2kIclU%2FE8RBpvAh6Lrib17M5XFYprYDiqNJiR9PS%2FPPD%2F%2F2F7nhtByblI4sQ%2FvJGNhhYCiHdcebP19Knh3fvhwkLJIBjLnGZ0q3bo2nFpYnaQLhxypzs%2BZA395D89Rm5aiH763zbLq4N4yR307fDm9elTZ6b3i6GF8kUIL4kN2DRIHuDKtNOh7T09bgKf0cwB4TI6aAvlzQGklg2Ry0tUkl4DPYxTZC5Re3DIoPaQ370R7%2FHyJv6xTPy930Jmm8HF%2BBQ4sleTMcIhfID8svb63o6%2FklzfDx5SWwTMjRfcgQsiUQFh0rbYnTdNsoBJLTNWTInpi6dM0niTBWEKRkHurXQ2GU0AgGfAVtsJstZbO1zfQ27szcnT40%2BJu8tInhHxM%2Fg1hctj5h2UDlrHeNMIyVi1dy9cp%2BeXy%2BHlEKBW%2BfW09psbgheBIniX62XuUEYF0bi0dh9cLyPVhj%2FdOREaAz%2FEOgRW%2FzDMQJnFuxW8aYrkXq8JyMvFHoTTHwp4s7RYiekiSGKXuVcgpN3uIHHKm%2BPIYgroeHK5R6Hd7Ta4cSG5bJTTuG%2BDT44fFSKafGSJWwdzJmdX3pRZiC3Hw%2BEeyGAvPYBqHfiR4zInVvGRMG9WpL51GJzz8XAG54fzp0fgWN2lc%2BrT%2F4Xmzdsg3BaTLDxmA5y2WxArr9PDLLg6mE05u%2FcWYzY9v5oPvq1Zp0i6cm%2BcMIbiNM981kL%2FBre3Njg3glt7uhv2oj8YSYrnldF4cy6xrNb5ZLCiGPVJsHFC4K4VUpYyO6anZCVD4Pqyd6TowNDzJmeSdRZmi%2FfQ8z0fLAdziZA5IFNRkIRr8Nf7ZAv3%2Forg2UESnW4qKJ%2FY6d%2BjYA2RLFC210CZd%2Fadj9w%2FdM0dmDhgQZLt%2B6kdhKK8GaBkRW8G%2BOLimWgn4Lx1CJ4EIVr4fIUdZdlL8elF3nxSFmN6uRzPpQWcey9DWKxJJS2NyJ7%2B71hCIf5a%2Bj%2F%2FDwj3XTADh7kdWAPFR1tVQX%2Fv%2F%2Fx%2FzMhN66BTgDcHlawwDMByMLpa4tEvlEQUAUdfpTqMxW%2BluOHhcD0cKYNhZv5EVjtI%2FPNmtTIYKr48v8r0VtrU5qISNjYyHeQJeDNdVRzBQZZUoSWXpB0wdHAWruZ84S5Zz8usqHAXf%2FEVAt68WBWXZpBKD%2FpzfIX2hbpYDKbDEcyUzyGW9J%2BqFZZ54EE1ngPEo8POM3RpOoCMd5%2BLuQey6WxxXoCU%2FSPfUOh8cHkBYrQ5E48OZkferdS9PXjMrkfLVQEI%2Bhc72yb%2B%2FXkTZ1l1m3Pg%2FcG%2FFnDCj8pA%2Fe%2BBj5p%2FQBSFJr08pprkwTuKttbMlTkdzGO8qbaMZyJ3jjYHAH8iH%2FZs%2BTemWKniHXneNFz6PnAWo%2BvpGO2WkP4b9cEAgzwdL%2BE2ftnPzMlKPHOhxpuSy9T2QLK8Lg1v%2BneoPjDXhYzvaDqXpk%2F%2F7zYVnjGQGngSM%2FoTz0Sj8ebsMvbZYhIhHCAwMcEp6uJbppER33rIm79L7u1zcSAOwHk%2BX84lROdVBsPnT487O0wj3LLggSXn8wxYRvuFks4M3i%2FUC9DBSmvezF%2BGsR8%2B3EpDNNN%2FS1efAj%2F3ecBsdj6esP1A8dlA3ixge%2Bd9gER1AoPQ5EfOgnADpv2PyBLBnPT86Z9AwmwsDrlNc%2FXbXQzfnhmnKYr4Sjhv9jDzgJ3ag10VOiha4MiQ7VB3AA7v3dT2BBg4LOnfJ078S4bGaXuHUfz0xptjzFLbIpMVfFaoHRRt0%2Bj%2BUBqn07REBpPCDTCDtVZNfLzKm5cMW5dxCLTUTQIILRx3m8YnmzsG%2FQ5EpAPrw5ulrKpgC1aSCC9wZV3FcNNCpgOpi%2Bf51HjzllVSiwCaVFak%2BcVikAlLbePwxegSBiPjq9Hp%2Beg0zQkx%2FSC1g31xuZOZqRRkFjky0tz%2BFDWmc4PRhXhuGo03j5lmHoJgkDw44Z3vnKa7GJ4OvF2DMnAHO0nwZjDTe4dgOAM24y4AOKS7L907sFMWxAxMJMQz1Gi8icwqjZwkEqETw8oru8wqPiriTWGm0exk8eZD8AChA9eynW6cBrsVos%2FfgTvG2VE9%2FPnh7uBByF7Hp3awnylvwjJNPvTmxVrfdP81NgDiNZ87TdnBT583skuDnxI3ZrKAIwzE2wDeJGXqQRswW6%2FBq7Na%2BJEnID73wZuaTDMOvTrmIU5ni8lgej4%2B7BOilUUdoCBuG18aCmHw4AD1Tx2h0%2BHTP2NH2pwMfmFXktUO6hW8uclUqkiMcjQugSncQOohtjp08O68t8PALcGwFk9GRUCZfESLgBL%2FsFusQRDEM8BrvJnIjCoK6P97cGX7cjRcjFaDxXiwki5H09FicCUNZ5PJ9RS1rixWo8lougIHB%2BPDdSuETBfmkrO3WEk45Mgo7ZBZTplxNcCmA6nhne3sU7BRW2FzdTUeLdjgiCd81HkTl%2FVpKqW1A2c%2BWsLUzPvB9Wp0Ol%2FMPowapGgAWsJFSefNb1apD%2Bdo6W3QQiAtTxFobIjEs%2Fno3BnQaJbIaC9QE3DmajxphFIHgsQ7C2pRUDJbobQ4Hw3fN1Az8csLdd4kaZXiXY5Ovy0602kT4RG%2F87LOm0CNOqFZ7eAZjy7ZsmOK33dV502rVukmyMCRe63AuT5rAk0HcsPbfaZB0859vj5vAI34VgudN90azR7L7fznD6PvFoyuuBSdDgSHswdtUgyO3MqDPptdg8kKuInXp4v3swaBhik%2BW6PzZmSjTVtyK196OLj6MDifsactUxEvRby52mjTltzKeR4O4B9scNQOZEcccXAJTiu3efh%2BsAB%2Fa6Baegfw8G7MpdnnVv5yDk8RWLBxEr%2BAWOfN%2B0ad4lt5zsBMszMdpvjebp03DRx1im%2FlNQ9ni8Xo700UrAN0OLvNld0fMnSUVr7hcAaU6%2Fxk1kB6xCdYdd6UcSYlT6a0chAhPsABOhksJo0w6kCGeDvRlAleaeUmgtOulw0UrINEK2%2BGOdoEr7RyDs9ni%2FPZZRPfuYPMKm%2FaOVoBQ2nlHJ7Prs8aOM4dpFR5U9FRsWnlG54vZk0cng4Sqdzp6SgOj9LKMRxdLxpA00EWlTdNHa1YobTyBSE0p6PV6dVs3GDG6iCTypvDjur1tPIJL8bT8XI1aiBC%2FQ6SqbyZ7agiNGjpMy9HMO2zvD5nQaT1Osgc8ua9M1UKRGetS4KnCCg2Qh2knXnz4tFMtNoqI385WDCFB%2BhXB9DwXqVGiSrUVlFXKjwAoNm0gWfY70LBeLdv0MSnVeR1OVqwHcN%2BB3l53ux6tJSG2irguhwDwWlQ0%2Bl3kI7nzblHRadVxPUe%2FHR9tWqATgey00Gfhtoq4BpfXSHv8MP4ajBuYnw6SMvz5uijhV5qq9BrPD1v4hyqHUgQb%2BeZZphbhV4ImzzAaABSBwl53tR%2BVAFqFYSNl42iiw4y8rw5%2Fqgze6vo69vrxYANTQf1Lt68f7RUqtbKZ74agHm9gdvTQa2CNw8gLSrVWnnN0N5Au9Oo2NXvoFzBmxiQZna0Vk5zQ5PcQZmCN1UgLWLXWnnNadDVEKEOahW8SQRp%2FT5aK88ZYXM6WAErtBr%2F7boJTB0IEu%2FcMw2mVi40gqlBfNFB0YI3ryBtrY7WynO%2BmjVBpgOx6cBv1lr5zQAZOHs1zPpocgdzPG%2FqQaputfKbr2Z%2Fb%2BQ4d1DZ4c1GSJWgVo7zBEbsLSKvDmo7vKkJaRKkt3KhJ4NpkxUWVgeFHd6UhTT%2FUG%2FlQk8GiwY5H6uDpDxvbkKaf6i38p1T%2F7AhQh2k5HmzEdIyq3orD3oy%2BG7UpGBhdZBZ5U0%2B2Kd0%2BuitvOfJ6BpyTkP7PJktR1dXDYDqILnKm62QaoRa%2Bc%2BTRu1iVgelHd58hLR2Mb2V%2BzyZLc7G7wfsFSlWB7Ud3gSEVAvUyodurlgdCE8HzrPeynmejkcfGviFVgcZed7MgLSss97KeYbMCw2g6UBuOujUMFq5zLNxE3vcQUKeNwEgFZpWDvOskTfYQajOm%2ByP1sRitPKX5wO0gcRwcDUYs0sWVgf5eN7EfrSYwmjlMc%2Bvv4MYnTdp5LU6yMrz5vHrKxSEWnnN8%2B8W4Ge4uUaRdm4gTB2k53kz8NFaN4zn7EKSI9YApQ4EijejN6W312jlQRcCNVuM0ZEmSHWQqufNoEeLNYxWzvQZMOCL9%2By9tTS5g6mfNxkerUncaOVNQ3VriE8HXiNvSjxaGG%2B0cqgbbQkAsOlAdngvG6TM%2B2YrjzrNJS4HjRDqIFblTXZHq2WYrRxrhE3jWgYAqQMx6mALO7OVi70cwIwiG5wO8q286fGo4LTyrZeDDzPGBrIpOOIlhzcDHtUANXerR4X9aYZQB5lW3jR4VAPUyk9cjmAxtem%2BEgCkDsSIdzqaBlIrFzEFCVY0mpTFNLmDwiFvbjyaH222chS%2F%2BzC6GjfYtg7A04EM8fajaaaolZ94Prr%2Bz9MlzEyzEep10LLAnQmP4kn3W3mLS3CQrVy9DnpeeJPh0RKw%2FVZ%2BIjiHHYD1Omgo4851RxObVt4hhKZNR1mvgywHd9I7Spq638pN%2FDBgNotrvQ4yZbxJ72jzVr9V6vXD4Hp41aAUD%2BARLzi86e1o8UW%2FVbr1w2h6PmoATgdpe%2B5MdzRwWnnPH8ZNOoEAOB1ITgc7cPRbec1p9NUUog6Kh7xJ76jy08pn%2FjBbXjbxBzvIPPOmw6NO7P30z3MhKKmGcJIwnTdbHtVrbhVXfNfM6ekgN8%2BbP69PibmsVhHFarRYjFcwswrJOM9GVxezxaoBVh1oWwd7QFutIozRctlMkjrI0%2FOm4qNlgKzWXdGozwOlghqAJF6EeLP20baptdqlolGabDkAFwFQTdntMFqvg5Q9b3I%2FmmNktYw4rqAoNUsm9jpI3PPm%2FjMp%2FTBWu6gDQnTSpCNP63WQt%2BfNEEhLJ1rtKoeX14Pz0dXset4AoQ4SrrzZA%2BkItbLZQL9W42mjFbxWBwlX3pSCdITarXQeSIvR9XTcgPrB6iDtypt4kJYbstqVxyaD72arBvvSW6JTr%2BB%2F4Hf53Rtw3I2dpZN4xY%2FRO%2F3dm3XsQc8sPwKfxPXXXhI65060dvwNJNuN4Anyu%2F9ywe3QXwBI794M7531j%2FlT%2Flperl%2B9XLQO3V08T8L1vR05U3tLuxx4g1UGv6KAa%2F85HfVXSdoE62QLnv7rOyceeQ7869mn8eaLk437MIIv9Sm%2F9MmXX0fxJ8%2F5euNGO8%2F%2BJP1FOjn5Rvr2i5N%2FAY%2FV%2B344m65G0%2Bvv54PL0feDzQN8s83SscHo73e273jp3we%2BH8B3nsB%2FOBG46r27cb748pu2Fxr7I8hYvQvd9Nnug5%2FBVf78Nnu1NwCzXzEIAHpL%2BPhvgDTDL7aao0uhQ%2FAAlGgkseXxP31%2FY69%2FHG%2FtO%2Bc69FKpVtT6ORs3BOLiBn52glU%2F4T4I3V8CP7a9gefe5adp9dPAcweed2aHuf7062f8HNq77Dcd%2FpYdvnUdb5N%2FdCQ62RfK1ADgDd4aiouKjwJPlF1Mww6vPTvKHiDVi%2Fx4EsXBNkMQ%2FSpjv2Y3zH5Gd50GGYPyIe2J3qngMvGnnbNwgDRAiUBC5wySOFiDMZ4TOyzt6OHaYZXaASGmXBoqOvOauMZpvfyar%2BIERKJjUQL3P%2F%2Fk21t33UCasg8ml0IA4AudWyd0EKl51TLDjEJhSsEz2KlpWgGZqciHkuEwKucNOC%2BAi8AJA0Glp%2BfyZScG405PT6VVkESS50QSFOZI2pxkzymBHw9OgeCVhPMScw7joChkMwR4Ude%2FDcKtnWrD4RcV7gdxDsag4hcv6kjrwI8SL270quqRryqzXpVzWFX9pvmoBi%2Bqif6mnKMjeK95GPzgxPA9gdMUs97Q6sA9zWyditk6IHu7MABeFfBDCWuXT2%2BFbQMvIMC4GXTj5kjwyZ4e4aN1YeCYysDZwOngXQbrdRBuTof2Br6jJEvLp0cvnfQBAJE0yK5lA1cgkr4IbqLAe%2FpH7Hx5EAzwQKpwc8%2FbCkLTsNs5HlCY4PYWvm6QPDghU3Fk0S%2FK2wZqxIuC%2F0D44vrMdxXPHc%2FZDALjMQTWPkjCpp9TuKXnnOWBznTxio0%2FpC76LTlnaqADCt4yDu0YGua5HcbgaUM3%2Fad3MooZMxwYLws3SZzrm%2FBZzl3bC%2B4S6JZtd0%2BPIGp0b1kfV3iqknNtEr4LysE8PUr%2B0%2BNdsHafHlkv2Rf9kpxLjFqf8pJgyj1%2FelyHwE1bgq%2B7dm%2Fdn8C3BsduHT9yfmOfDXgcnF0t%2BKzz0qmyN%2FYufnpkvuexYQb7PXnHjr3qe4axu%2FYcSWWaKOHOBG9qV3gcf9MEzD%2FMtxQ%2B%2B%2FBmaFVqcvvgrHMlRnFyxMr%2Fy72e%2BPwHZ%2BdJq740jIfsKLKL2GCbWrNIikrTdRgIGBeI12XODlZzGJKb6OkRgODHbBjEK7oAD2z5KYqf%2FrGFuZPB%2BqfEjVyEQpoU%2FSlhKr7ophc8yaBVEq9rO3bugtBl5Bc0EemFXpFeAOeiBAN4Gmg8wONEv8fEAniVVWg%2F2MnH3zx25j3bgWe%2BAJGW78bAFhzW8i7CZd67igAVd8IHd81%2BNcFmPK1mg8ujKus50N6Fs3UjZ%2B65aSIwV12o6vobTJfhAaO4TnGoVvneVK64jG2YGKiVZLDqNqyfrWH4GdSepRhJ2JSiJAeGrrJiHPo4hypc8M6BF4TIbsBy0abjEhd4gAv3I7wt4%2FMUKGFVTigvVWBH%2FqYOq94jjmA1MpicogKdXemPBnO1KvwBzLg38Ap5pRmWbtI%2BhNiOk3hYKd2g6awAGSswAqgidP6hyXCKbmDlpy62NutsxrNmz4GS%2Fyr2YNhEDeUnSG689OmPuBdMioXObQh7YRzv7wDYtOckuzP8Nu5mkRZj62ekBXNw1YWzc4AMhvvPw2qy4IZAYO3%2F6XyKoDTqmVF%2Bk36Dwk4CUaj4JfnR9ZtvXGQTsWNhAzuJBkBchkEC5dVFAGS66G4GwLIHVzF4g4W9cYPy9TTq6%2BEnvdh32%2Fvp5apKDD07iZy6SigKJno6YYoMzJpBU4SusYRhnkdrZsJ9R1WA7yiDcWN%2F497ePj3Cb%2FR78xTBt5ol7u%2FNS1RRy9Fv7Bxic7RuEnLeJ45Y9SMGMVEbMnEEb1sBaAB74rlOGIF4GcRbrxrzqjGfp8YYWIsCGBC5NiNx8CrKr6L8QkUZ86%2FBsznRq1F%2BleTPTJLBkyyXQ3jMKr13A%2FPeYet21VcHF9s64doZ%2FZS4sX3jvdrvV6n%2FzKQ%2BE3ODcLoJd97A3HmNUIWxHzkhjH8H69h9cGNntA78ICvYvCrFq1J8hkpBRKsmbWUMLT%2BUnU6EsiYWykKYUh0a%2BQ9uGPg%2BWo1mv84irwrzeSqMqRLyjsUFSl5oy1ZCzpMbz11HS3e789xb18nXFRVjicSpic1SMLm8PXCh37f%2BdL1M6VV%2FOtEfkzm99A6VpbLahJLp2Rns1naGQRzaIDrJ1gNiRTETX1kNrnRDO%2F%2BY%2BhjA13OdZPTRWSd5aUTuYQ9ATLB9Ih3cx%2BZQ%2BB3dTbTMwL10gmktNewn232%2FwrKfAz1SH5Vp6j8r8Fn9H6%2FS55XJV4ddGVP7wb2z43Q5KzjU679784P9YKdLsN%2Ftgt31bunES%2FcX54sT1984H7%2Fe3e%2F%2BfWffOX9Zg1gx8b%2BGN%2FhYIDKELeb%2FBkH6%2BJd%2Fy4XmLydfnfT13u4j%2FLOX%2FvkJrhz%2F5k2Ba1R%2FSz41689rLXUmIUpVaxSaYvwxFjCXy90afqr0ceGi09O7MEh2UvnX02grAY1Iu%2Bo66fWAHXTBxommSRzVTFUfX82nFUXnhXObn4xEG3w%2F8KHA0UobwtqOKoegrsMbgYPz0PXX2C9y9gjgp6WD%2FwC7eZxbAMcQ%2FIxGuTvbq1SiUyOA3Wm%2FPPZe5XGvPGr75bE7USzm0PNga4OvHuVZDUIwMQcXgrBJz89PL5tBetUL4icY%2BfQC5rb8flVxz94k%2B%2FGPau6LTr0r98bxPAeDA2EMf4%2FDxK%2F%2BfHh3mHR%2Fh%2BziceBG88D142js3wZnibcnFP%2FdY%2F2678fL%2Fj6puYD%2B6nn%2ByKVowz2FsPYm6BZ7qUZEbVTiDyYDz5iqUj%2FCj8HnOb3xgnWH0xNAG4So7saOg5A6Rf2BW1gVPVvFG3jAj%2FspcYG3djcPovgMSGQ55FW%2BWfKtIRPTYQAA%2B2HpXtdV7ldjjZV9LD%2BoGHhMUPOxXmx8gPs0f0tsmLxc5yv9ql6mUf2EP%2BEnw59QjyyUCvJa8GcT%2FWxW3KP0pOorvDpA5QSb5poIKMuP4O3BUS3es%2F4TNjljk3WR4HqNF%2FnJCffZuYXk9OCel7dB%2BfXRF5W10g%2BrCsbrTMT8lhmkpzepSeoyDVD4WYQpePWxXn2sz87Hgj8jJ2FwF6J%2BA8LXMCtei42flhoytXaFwsOAb4DVZyp3KFNkno2MID66QXqmh3snf1SnBHMgFIoDcRwez4yAb4IkDvzTaGt7XoeWWS9n08JXbSlSZllGI0T11RX73LTjdXb6ncxOYblUdDj%2FUE47eXQ7ARH24BY4YvcA%2BmLuqSU%2BX1ysn96qeIGJ%2FUMRt%2BcvNtw9IM0za6Z9rylDm%2FJtHAnBVM4P1X4A9Y%2FgnKbojsIwCCdOFCHBqOFTQU42VEgKAGCXYjf2nL%2Bc5CeeSOjB%2F3Kyhpujn7o%2B8v%2Bdk7%2F%2B2d3eSVG4%2FsvJfRzvondv3%2F78889fZy1op7u0B%2B3ruyB5%2BPo2fBvfg%2FkkeutC8xC93bnrODhNr%2BgH8Wnw49d37u2JZHsxuPO9vd1JARh%2FB8IMYC9OpLd%2F%2FfNb%2BHB%2FxT%2Br%2BvpZf1%2BfFcK93j0UFsTBbIyGfgJ2J%2FA3aFvcqg8NfhuEN0GIODVojVrQXx5gbXAz9LVWxaEhmE2yiSk1Qiqa3ii%2Fg09rR87S8eE%2BTA%2BFD4Tmn4m9y9bj52KFYM9D9g%2BJI0WwKzP1GcohTRbml4KvVUwrhCW75st7aSN9abvybY54dSzFnR4w6gfM%2BoF%2B%2FQDRZCcTTXYy0aguvzZVHRMXmbC137HDW%2FejFAY%2Fd9xIhVKNMMpeIjck86KODphbA3F4kkiNe%2Faa7%2FzMx%2BcWIPZyDIIkXDt%2FWCQONgunFtWDjuowqzrJeScNZeeWCnYvcNOW1GlPc1OeE2LdVVlqi9iQoo9vLGVAT%2BjOc0C4ObuJnPDBoVe%2FipUEsoCVBEq2kR%2B%2BmuAFEJ8UbQd2HnmlCAOshl4A96lz6tha7MVSvXz7JzCDhg7ci6viooJryNVvXmzUhs7DWsktpVYdtlTm3fGlWpZWH683fPrsdHxpSi%2FdoWziwqVdV2B2csgd3PL8XeG69uB%2BnW97%2Blulp8j5kS69GTVT6MAH39dPXW0oxVGy24Xu1glHwHddO1%2FE924EW9EzbfPAsfvA2zhhbou%2BkH744e12%2B9YG%2F5O%2BzKxV4SOpFrIZDpgTt26MtjoN0QZsaJ%2FTnedGEuI5k57tNWZfxKSaq9LxSJnTn%2B31A38duOYh9Ph%2Fs69loq8FnvXH9HPA%2Fgm4BmGYPRz6Vl%2Ftpbxjss%2FRpPjky69ONhv4fT%2BB%2F518dXKSCgOYDEHck0ORPkHoplJRDwzwX5%2F1iWGnCLaDH1vTfv%2BRMmZLVPB0V46Uckkh6gJbgp9ScqJYykPfN7j1wlcC9QnrVd8WkWa7evJbWfnd2y6Dabuc%2BGjDZf3xDJcu1nABAX4ZZkvfZ7aoKvZqtGhGq9IoWEURfKB1xkibJh%2BMLOFRrQugQ%2Fh%2BsCZh8rIrNfPbCtvX%2Bz3bvl5m%2B8CnQSbP8SWYeXGOddfyD6L84cweTFeJNHu4EL8I%2B6dq%2B%2BzfYX17NYR07y1XHWx1JJxi6GLQxIv7Q0Sg0NzTLdkxzlv%2BLbQ%2FnhlTOjFjL8WJyz80VptRFYZde3XvGls1RrouR98gPboa%2B8KPzqefgzCTqbMgBi%2F4AkuGhgKNkR9BPMKnxzTju3a%2BkrwTF9K3J97ToxQk4J%2FBDSLaRchV6ZOfJcbFypNJEEdDz4kqEKUTCfq9oJO%2F8ILEKc9K15wAObmtHn9hAMPbFa8g2TtIt3uP6OCO0n48YwIwcD7a6%2FjFYgCLDSUG6FmPfX%2BrPIqVXc4SgIBfkY60P3QIq6EHm5mgbzi6vQVzeZjSF4Vu%2FPSPMOVIDvOHr%2Fg9co%2B8InUjGtR1DLQqXJTXOfAoUDWu0PlIK%2FF7v%2FYMsmrfqL%2B2s0p3nghIrfwcfOAoLpvocjmRicmi7G9IN7PELnL2aQjfpVjqASuji5zy1inKRPmFcN%2B7j3b0Ozi22sD121sG2CpbeeToGYahy35ztCNI5mx1I2j1ba8wKcK3twMXCMK7qb11sE%2BeihTlw4M3GQDDWHYlkw01v2ccbXg%2F%2FtVzTUmr5yXBvO%2F8IiUwtIPOFHClUJAF6W9%2Fh5R4BjgO2w5vslkz354axrWbk7G%2FAV8VBD7SF8Ph%2BEvpC1Pv633pVJoPFuOlNHQ2zkdJNr88iApc96psxfYacN%2FnTwfWbAT353ajKF3mAdFZ2j4UCIDN5Olx466D0wjuZZx8lL4wrF5PBchcfTebMvH4aK5F48GZgQ94bwQgAAZgvWzfjbap8HgnkBwaxrDwBClKdiA0cZ0k%2FCqLQcLSocz40V3fDx5SzlIgXL2eDoXLDt2IieGtci8aQ868tSZ4lokL4EJOavr%2B9l3ogmgfdsMUmIC4Fu3qm6Oialahcj1TWs6Z2NxovmhsOJPZ9mUSm6f%2FTvcDd75CtdIL14cOdiQVIe3CQS46xEg31VxycrOkMGGyxcPEmexWhnaJwOlxk6SOmJRuVGl7TgnSDw6Yy6IIHamPPFJdTVWpg66z9bZ3Jxp0k7PeGiTmoyQMdhkukTQAQSjsuJeeHuPQ9u%2FSIBQi1OsVCK2bIrTuP4hGqM%2BbflYjIXK3W%2FcuRMLyVS49kF64csiO3ExYE2nz9PjgeMFul8ljAFfRA1jTmcGsAdljux0%2F9jzRQFr8ndE6jmOIGtJLBITcfIq8M4S%2Fv8zbGyftmy0NswkSvr5K2Hm2xdF%2FEA4DZ7dcJ10FW%2FoW%2BOIumO4QDlrd9PbYQGzUW%2BFA8Ca2VkjHwJZWwMhmHO%2FQSfCCOxiffVWf34DkBPdPjxE8D5rplROGLlpYFVHOnThAwSylpzdXsI0WCweUsyOvmTU8I8kOt0%2BP6Xyl96x6dMeer%2B5M4Z64zNkVJyasRELM8K6XzUuj7c4L3NwRukAlKChG8zC4Bd4UyhJ4xcx17tpABhMQICJqG4ik1XuGb%2BT0Q%2BFIcnbcTQsHMpKKgDhCsfJXUpzjijui0S4I4wglFFS9dJGaRTf3ffH2nLPfroPjM%2Bhe2zGYx7NgGYTTMUDA1DQNIDCxQ9859ezTD7bnAX3MpIYdxUTKRjgcnD1qWPyeB8kDMMWRlPUwpF5ev7Ht%2FVn9KPy1ObvJGrjZ0gkfXBjFArMxh53AYNbZIv0hHF329KMIDxRkzg6uCR772s8n5Eu4S3eeXjsZrO9t8Jd5IQ%2FHakZsfRKLTyWxX2vuQOWheQLCZztyUIqfrOzoeFUIa0%2BAnQ8o%2B%2FyJcYHXCkG90pKCl26mwQCvWt0zyUN97C75MXz1n4V2HqFVbirXUchWYoUsLyo4zxcAxl6v6VWh6iiVPKSRh%2FTDbXz7itfdVxvzReNYs5iGGpZAAOhLD04I2ZrCc%2Bh%2BOUVlvkTpi7S5q1Ku3FuZb9%2FL8Lqw%2FXkbMSKzgK7SYZ0%2F3yNnMBwG4SYa2ptsTwwNs7oK2RSmkJZAKSwBq20k3ypvOJgtnDhI8gaW8lIWZlRUs6AncPz0%2FAlU%2BkxGowv7AXzacn%2FudGbxmp6c3VLtVYDBtj3SMkNQ3QaptkVcOsdme1oCKVwm260dfiItCo%2FmRHCnf9lYuqrdvtQGRfgmHwowkAeTz34ZRmi%2ByfZrkeGGcSGwW2hHmmqfYw1kZOayj5le4YDJzlpmOprZ%2B9WZXdoGnxy%2FQ1WG22PZyYNzZ4cbh96QlRvlIdxaB3PefutuGegol310xVs8v5sQ27ALV1e1aLWGrneJVrmrQdVX%2FO2hokz1MDpwo%2By5lvfBz%2BfuQ9oZzu4Ah5v9QtDSFz4hHQHwtDkYz%2BhWOtBYrNJ206Sdj2ioKo%2BJvkvFVtcI5oCVj66c6FvIs8eSd%2FklyDt8xxV4aNSQ%2BUP62Md0japqdR4CgMDN%2FZ1o6ewjJvntMbAQBuDmCIWoeNKjgMDceviGIH62IVuBQ%2BmR%2Fc0RgI7u%2BdNj%2FoQSiM92QQiLeelmq0cBgQXa%2BSGjik26ZOB9vKXsP%2FMSdQbMk2mKWUqf%2BXnd%2BXL%2B4tBJoLQr%2F%2FbvqRTvmT3icTHYa8T1nH2jwKXOsX2j0DzJxzWmznkV3%2B1DvjO%2BmOjhs1napMAtbdJdJ%2FNtJ%2F%2B8DrbvVqiy6YEY4q%2FD94PJ%2FPvZ2dX4crCajRejP7%2BtndH9BpXPeUbKDpaR%2FXDIi8czUwD4kR86dyg%2FHnYX8ujAVN3EvgT%2Bg6ym8A%2B4nhTEvVK2e3kZEEu3XmDHp6F7dx93FxT18yznt3z2thdiaDYiDE2VbdO5tRO4HV09s3vU5t5USyYXq9vgQroihIGzWogOlTlaxWiWWWEGc9nWmpgTnB3RiCN61dRUd0GV86V52bLayXxZv6leP3Bo49Aiuzb07CRyrpw7x9849cc2iIc0iSN4kr1YYDgNsue8CBK%2F4kzAIXgKrXiQbEBUjsC28VOI%2FUsVYv9SPAGfHVGriuEHsE8%2BuxFxMo62AbN0UezMgU2F1N6rYEdV0nJpg4h9Ad%2B9kXsHa2RZFukzWq8AhimH36k8Q2zZL%2FvqmNYoOcvJWYDWNF4GcQAFIPv4GiaOtXjFT7bYiXWBI3%2BC0VVq99LVk9iv6cwAh4T4fTFTtHcNI0Duz3%2FGtw8%2FvJU8MIeVs6ubsWjk2X%2FNZ3S4yMT2N2mSM6VrKHbjLn6d2yH4PU4n%2FVxQ4c%2FQ3kRlBrZMvmO2c%2B9jgHf864HnuLKjY58jM2NwqYRjJ4vaxFBFk7YlaSEjtbXpFXuyb9nwq0n5HZgUbJ04DF9pJqW2pD4baNWsR2pVynNL61EaFuJXq2ZbqidkfhCyL7WHeDUxnZmY4kn6hReaioBK%2BDlqbTvcs9iv7dqQnafUnDeV8DlVwucsU3HlI1n1Cxu47OjZFtvf2g92Gr4cFICSeQnI8zblc%2Fjg2hsnIPzIapNFEnqT%2Bag8ZKFD9aEy6rN5CD45IfFTD2renXMV2BtaivU19%2FSckFARt3u7zqQlwGPmZUOx08oRiBsYn4cOdwKY%2BcbXTjgJfIC765D1X6S%2F1vGxIn5Aqx9oEdgd5DBrGEanHICQV%2Fk6tu8La1CyCMLGpvwnNd%2Fz4wdgED44YURw5mZaPwpDJ%2B2mKILudCuanOQlv42SWRjsB%2Fb3Ym15j5k2pfa11PLf9I8llyeorZ%2BmMea4FQSq%2FmIkShTHyPNSI5hHNbUf3LsEzauTxHM9gGviFB%2B4j7Z%2FuYMp0dpHLrmYTbiPkX033t6R%2FsBvUkwHDxh%2F2uXF7ptsY51aZQ5Y1MWzKuBVFwdepKtkLJzc4NdIwOc4Xd8HcBEURN45TZ%2Bik7Yf0pmD%2Fa7hG9xAyJ%2B9bIymHGQDXuS3l430KX472YAdUtV6ZTvqkyx%2BT%2F%2BT0X%2B%2FklQoyjccqFAU1hR4sPG14RyVOuZptWnvlHNU1p45It3iSe2nhvxf8Xaif622E%2F0r2qEuf8NGI9AeafkITWsyArb4A%2FVNsCZipcE41197SeicI7qwDdoUIR9vNLkvNARF%2FW2EHgIuG1gHkCMtLp%2FFfN61oCQWF4H0esyLVPk7%2F5WgqmvxPOSlcuq3Fh%2Bmsj1hmw%2BKb%2BnX4oNSmxuz%2B1qtxudtZdlgvckXrLSYZU%2Fdazaw3oqV3bbJN8f7l1oMw1t72nxRvDEmc%2Bt%2F%2Ff8BwOwnoA%3D%3D&PRADO_POSTBACK_TARGET=ctl0%24CONTENU_PAGE%24AdvancedSearch%24lancerRecherche&PRADO_POSTBACK_PARAMETER=undefined&ctl0%24bandeauEntrepriseWithoutMenu%24identifiantTop=&ctl0%24bandeauEntrepriseWithoutMenu%24passwordTop=&ctl0%24bandeauEntrepriseWithoutMenu%24quickSearch=Recherche+rapide&ctl0%24CONTENU_PAGE%24AdvancedSearch%24keywordSearch=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24orgNameAM=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24organismesNames=g6l&ctl0%24CONTENU_PAGE%24AdvancedSearch%24entityPurchaseNames=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24choixInclusionDescendancesServices=ctl0%24CONTENU_PAGE%24AdvancedSearch%24inclureDescendances&ctl0%24CONTENU_PAGE%24AdvancedSearch%24type_rechercheEntite=exact&ctl0%24CONTENU_PAGE%24AdvancedSearch%24reference=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24procedureType=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24categorie=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24clauseSociales=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24ateliersProteges=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24siae=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24ess=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24clauseSocialesCommerceEquitable=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24clauseSocialesInsertionActiviteEconomique=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24clauseEnvironnementale=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24idsSelectedGeoN2=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24numSelectedGeoN2=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24referentielCPV%24cpvPrincipale=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24referentielCPV%24cpvSecondaires=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24referentielCPV%24rechercheFloue=ctl0%24CONTENU_PAGE%24AdvancedSearch%24referentielCPV%24cpvArborescence&view=on&ctl0%24CONTENU_PAGE%24AdvancedSearch%24dateMiseEnLigneStart=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24dateMiseEnLigneEnd=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24dateMiseEnLigneCalculeStart=27%2F05%2F2021&ctl0%24CONTENU_PAGE%24AdvancedSearch%24dateMiseEnLigneCalculeEnd=31%2F05%2F2021&ctl0%24CONTENU_PAGE%24AdvancedSearch%24keywordSearchBottom=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24rechercheFloue=ctl0%24CONTENU_PAGE%24AdvancedSearch%24floueBottom&ctl0%24CONTENU_PAGE%24AdvancedSearch%24orgNamesRestreinteSearch=0&ctl0%24CONTENU_PAGE%24AdvancedSearch%24refRestreinteSearch=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24accesRestreinteSearch=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24rechercheName=&ctl0%24CONTENU_PAGE%24AdvancedSearch%24RadioGroup=ctl0%24CONTENU_PAGE%24AdvancedSearch%24tousLesJours&ctl0%24CONTENU_PAGE%24AdvancedSearch%24formatAlerte=ctl0%24CONTENU_PAGE%24AdvancedSearch%24formatHtml&ctl0%24atexoUtah%24javaVersion="
		
	# print (data2)
	# main_content2 = s.post(main_url1, headers=headers1,data=data2,proxies = {"http" :"http://172.27.137.199:3128","https":"http://172.27.137.199:3128"},verify=False).text
	# main_content = main_content2.content.decode('utf_8', errors='ignore')
	# with open ("temp539.html",'w') as f:
			
	    # f.write(main_content)
	# tender_link = re.findall(tender_url,main_content)
	tender_link = ["https://www.marches-publics.gouv.fr/app.php/consultation/772531?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/765869?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/772321?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/771718?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/758599?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/590066?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/770653?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/772821?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/754534?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/770108?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/772756?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/773281?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/773366?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/772568?orgAcronyme=g7h","https://www.marches-publics.gouv.fr/app.php/consultation/773153?orgAcronyme=g7h"]
	for j in tender_link:
		Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, j)
		print (j)
		if Duplicate_Check == "N":
			time.sleep(1)
			main_content5 = requests.get(j, headers=headers1,verify=False)
			link_content = main_content5.content.decode('utf_8', errors='ignore')
		
			title = regxx(title_re,link_content)
			title = reg_clean(title)
			title = translateText(title)
			title = reg_clean(title)
			print (title)
                # sheet2.write(row_s2, 1, title)
                
                
			awardingAuthority = regxx(awardingAuthority_re,link_content)
			print (awardingAuthority)
			if awardingAuthority:
		# input("************************")
				awardingAuthority1 = reg_clean(awardingAuthority)
		# print (awardingAuthority)
		# input("************************")
		# sheet2.write(row_s2, 2,h.unescape(awardingAuthority1))
		
		
			desc = regxx(desc_re,link_content)
			if desc:
				desc = reg_clean(desc)
				desc = translateText(desc)
				desc = reg_clean(desc)
		# sheet2.write(row_s2, 3, desc)
		
			autho_ref = regxx(autho_ref_re,link_content)
			autho_ref = reg_clean(autho_ref)
			location = regxx(site_re,link_content)
			if location:
				location=reg_clean(location)
				location=translateText(location)
				location=reg_clean(location)
			cpvcode = regxx(cpvcode_re,link_content)
			if cpvcode:
				cpvcode=reg_clean(cpvcode)
				cpvcode=translateText(cpvcode)
				# cpvcode=str(cpvcode,'utf-8')
				# print (cpvcode)
				cpvcode=re.sub(r"'", "''", str(cpvcode), re.I)				
			contracttype = regxx(contract_type_re,link_content)
			if contracttype:
				contracttype=reg_clean(contracttype)
				contracttype=translateText(contracttype)
				contracttype=reg_clean(contracttype)
				if "Works" in contracttype:
					contracttype="Works"
				if "Supplies" in contracttype:
					contracttype="Supplies"
				if "Services" in contracttype:
					contracttype="Services"						
				# contracttype=str(contracttype,'utf-8')
			award_procedure = regxx(awardprocedure_re,link_content)
			if award_procedure:
				award_procedure=reg_clean(award_procedure)
				award_procedure=translateText(award_procedure)
				award_procedure=reg_clean(award_procedure)
				if "Adapted procedure" in award_procedure:
					award_procedure="Adapted procedure"
				# award_procedure=str(award_procedure,'utf-8')
				# award_procedure=award_procedure.decode('utf-8', errors='ignore')
				# award_procedure = award_procedure.replace("'b'","")	
		# sheet2.write(row_s2, 4, autho_ref)
			award_procedure=re.sub(r"'", "''", award_procedure, re.I)
			deadlinedate = regxx(deadline_date_re,link_content)
			
			# deadlinedate  = deadlinedate[0].strptime('Y-%m-%d %H:%M:%S')
			print (deadlinedate)
			if deadlinedate:
				deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
				dt = dateparser.parse(deadlinedate)
				deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
				# print (deadlinedate)
		# dead_line_time = dead_line_date[0][1]
		# sheet2.write(row_s2, 5, deadlinedate)       
		
			deadlinetime = regxx(deadline_time_re,link_content)
			# deadlinetime = deadlinetime.replace('h',':')
			summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
			summary_cont = summary_cont +' '+ str(j)
			print (summary_cont)
		# sheet2.write(row_s2, 6, deadlinetime)
			curr_date = dateparser.parse(time.asctime())
			created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			
				# print (summary_cont)
			insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, j, str(), orgin_Boamp, sector, country, str(),title, title, str(awardingAuthority1), award_procedure, contracttype, cpvcode,desc, location, autho_ref, str(), dead_line_W_Boamp, deadlinedate, deadlinetime,str(), summary_cont, 'Y',created_date)
			print (insertQuery)
				
			Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
			# row_s2 = row_s2 + 1
			# book.save('my_book_v9.xls')
		else:
			print ("Duplicate data found ")	
				
if __name__== "__main__":
	last_week = date.today()
	# last_week = date.today()
	last_week_date1 = (last_week.strftime('%d/%m/%Y'))
	print (last_week_date1)
	# curr_date1 = date.today() - timedelta(1)
	curr_date1 = date.today()
	created_date1 = (curr_date1.strftime('%d.%m.%Y'))
	print (created_date1)
	# raw_input("check")
	main_url = 'https://www.marches-publics.gouv.fr/?page=entreprise.EntrepriseAdvancedSearch&searchAnnCons'
	n = 1
	start(main_url)