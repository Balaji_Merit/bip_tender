#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX 'strftime';
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use filehandle;
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $INIPath_Reference = &BIP_Tender_DB::ReadIniFile('FILEPATH');
my %INIPath_Reference = %{$INIPath_Reference};


my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content2)=&Getcontent($Tender_Url);
$Tender_content2 = decode_utf8($Tender_content2);  
# open(ts,">Tender_content61_v1.html");
# print ts "$Tender_content2";
# close ts;

#### To get the tender link ####
while ($Tender_content2=~m/<div class="[^>]*?-funding-opportunity"><li><div class="[^>]*?">[\w\W]*?<a[^>]*?href\=\"([^>]*?)\"/igs)
{
	my $link = &Urlcheck($1);
	
	my ($Tender_content2)=&Getcontent($link);
	$Tender_content2 = decode_utf8($Tender_content2);  
	
	my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$nuts_code,$award_procedure);
	
	my ($title,$tenderblock);
	if ($Tender_content2=~m/<h1>\s*([^>]*?)\s*<\/h1>/is)
	{
		$title = &clean($1);
	}
	if ($Tender_content2=~m/<div[^>]*?main_content\">([\w\W]*?)<\/div>\s*<\/div>/is)
	{
		$tenderblock = $1;
	}
	
	$tenderblock = &BIP_Tender_DB::clean($tenderblock);

	while ($Tender_content2=~m/<tr>([\w\W]*?)<\/tr>/igs)
	{
		my $block = $1;
		if ($block=~m/<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>/is)
		{
			my $tender_title		 = &clean($1);
			my $deadlinedatewithtime = &clean($2);
			my $tenderlinkblock		 = $3;
		
			if ($tender_title ne '')
			{
				$tender_title=$title." - ".$tender_title;
			}
			else
			{
				$tender_title=$title;
			}
			
			my $Deadline_time1;
			if ($deadlinedatewithtime=~m/^\s*([^>]*?)\s*(?:\,\s*|by)\s*([^>]*?)\s*$/is)
			{
				$Deadline_Date = $1;
				$Deadline_time1= $2;
			}
			$Deadline_time1=~s/^\s*by\s*([\d]+)([A-Za-z]+)/$1 $2/igs;
			eval{
			my $t = Time::Piece->strptime($Deadline_time1,"%I:%M %p");
			$Deadline_time=$t->time;
			};
			my $tender_link=$link;
			
			#Duplicate check
			my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
			if ($duplicate eq 'Y')
			{
				print "\nAlready Existing this Tender Data\n";
				next;
			}
			else
			{
				print "\nThis is newly Released Tender\n";
			}
			
			##### PDF Downloads #####
			while ($tenderlinkblock=~m/href\=\"([^>]*?)\"/igs)
			{
				my $pdflink = &Urlcheck($1);
				print "PDFLink :: $pdflink\n";
				my $pdflink_1=(split ("\/",$pdflink))[-1];
				
				my $date = strftime '%Y%m%d', localtime;
				my $TenderName_temp = $TenderName;
				$title=~s/\W/\_/igs;
				$title=~s/_+/\_/igs;
				$TenderName_temp=~s/\W/\_/igs;
				$TenderName_temp=~s/_+/\_/igs;
				
				# $tender_title.='.'.'pdf';
				my $pdflink_name=$title."_".$pdflink_1;
				
				my $path_from_ini=$INIPath_Reference{'path'};
				print "\nPAth::$path_from_ini\n";
				my $Filestorepath = $path_from_ini.$date."/".$TenderName_temp;
				# File location creation
				unless ( -d $Filestorepath ) 
				{
					$Filestorepath=~s/\//\\/igs;
					system("mkdir $Filestorepath");
				}
				my $Storefile = "$Filestorepath/$pdflink_name";
				print "\nSTOREPATH  : $Storefile \n";
				$pdflink=decode_entities($pdflink);
				my ($Doc_content,$status_line)=&Getcontent($pdflink);
				my $fh=FileHandle->new("$Storefile",'w');
				binmode($fh);
				$fh->print($Doc_content);
				$fh->close();
			}
			
			
			&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts_code,$award_procedure);
			# exit;
		}	
	}	
}



sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=utf-8");

	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~s/<br>|<br\s*\/>/, /igs;
	$Clean=~s/\,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\:\s*\,/:/igs;
	$Clean=~s/\.\s*\,/./igs;
	$Clean=~s/\s*\,/,/igs;
	$Clean=~s/\,\s*\,/,/igs;
	$Clean=~s/\,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}