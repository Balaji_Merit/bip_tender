import requests
import re
import os,sys
import pymysql
import time
import json
import pymysql
# import xlwt
import imp
import dateparser
import datetime
from datetime import datetime
from datetime import date
import urllib3
import urllib
import requests
import ConfigParser
import dateutil.parser as dparser
from time import sleep
import warnings
import pytz
warnings.warn("ignore")

# import codec
# warnings.filterwarnings('ignore')
reload(sys)
sys.setdefaultencoding('utf-8')


Database_Connector = imp.load_source('Database_Connector','C:\\BIP_Tender\\UK_Scripts\\Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()
##Clean Function:::
def reg_clean(desc):
	desc = desc.replace('\r', '').replace('\n', '')
	desc = desc.replace('&#160;', '')
	desc =  desc.replace("'","''")
	desc = desc.replace('\s+\s+', ' ')
	desc = re.sub(r'<[^<]*?>', ' ', str(desc))
	desc = re.sub(r"&#39;","''", str(desc))
	# desc = re.sub(r"'","''", str(desc))
	desc = re.sub(r'\r\n', '', str(desc), re.I)
	desc = re.sub(r"\\r\\n", '', str(desc), re.I)
	desc = re.sub(r'\t', " ", desc, re.I)
	desc = re.sub(r'\s\s+', " ", desc, re.I)
	desc = re.sub(r"^\s+\s*", "", desc, re.I)
	desc = re.sub(r"\s+\s*$", "", desc, re.I)
	desc = re.sub(r"\&rsquo\;", "''", desc, re.I)
	desc = re.sub(r"\&ndash\;", "-", desc, re.I)
	desc = re.sub(r"&nbsp;"," ",desc, re.I)
	desc = desc.replace('&nbsp;','')
	return desc

def regex_match(regex,content):
	if regex != "None":
		value = re.findall(regex,str(content))
		if len(value) == 0:
			value1 = ""
		else:
			value1 = reg_clean(value[0])
	else:
		value1 = ""
	return (value1)
	
def DateFormat(Input):
	print ("DATE Before Format :: ",Input)
	try:
	
		dt=dparser.parse(Input,fuzzy=True)
		FormattedDate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
	except Exception as e:
		deadline_date1 = dateparser.parse(time.asctime())
		deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
		FormattedDate = deadline_date1
	return (FormattedDate)

def timeFormat(Input):
	print ("Time Before Format :: ",Input)
	Formattedtime = Input
	# time1 = re.findall('(\d{1,2})(?:\:\d{2}\s*|\s*)(?:Noon|noon|pm)',str(Input))
	time1 = re.findall('(\d{1,2})\s*(?:pm|PM)',str(Input))
	print time1;
	if time1:
		if int(time1[0]) < 12:
			Formattedtime = str(int(time1[0])+12) +":00:00"
		else:
			Formattedtime = str(int(time1[0])) +":00:00"
	print (Formattedtime)	
	time2 = re.findall('(\d{1,2})(?:\:|\.)(\d{2})\s*(?:Noon|noon|pm|PM)',str(Input))
	# print time1[0][0]
	if time2:
		if int(time2[0][0]) < 12:
			print (int(time2[0][0]))
			if time2[0][1]:
				Formattedtime = str(int(time2[0][0])+12) +":"+str(int(time2[0][1]))+":00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0]))+":"+str(int(time2[0][1]))+"0:00"
			else:
				Formattedtime = str(int(time2[0][0])+12)  +":00:00"
		else:    
			if int(time2[0][0]) == 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			if int(time2[0][0]) > 12:
				Formattedtime = str(int(time2[0][0])) +":"+str(int(time2[0][1]))+":00"
	else:
		time2 = re.findall('(\d{1,2})(?:\:00\s*|\s*)(?:am|AM\s*)',str(Input))
        
		if time2:
			if int(time2[0][0]) <= 12:
				Formattedtime = str(int(time2[0][0])) +":00:00"
			
		else:
			time2 = re.findall('(\d{1,2})\s*(?:pm|PM\s*)',str(Input))
			print time2;
			if time2:
				if int(time2[0][0]) < 12:
					Formattedtime = str(int(time2[0][0])+12) +":00:00"
			else:		
				time2 = re.findall('(?:Midday|midday|12\s*noon|12.00\s* Noon|12.00\s*noon)',str(Input))
				if time2:
					Formattedtime = "12:00:00"
				else:
					time2 = re.findall('(?:Midnight|midnight)',str(Input))
					if time2:
						Formattedtime = "00:00:00"
					else:
						Formattedtime = Input
					# Formattedtime = str(int(time1[0][0])) +":"+str(int(time1[0][1]))+":00"
	# dt=dparser.parse(Input,fuzzy=True)
	# Formattedtime = (Formattedtime.strftime('%H:%M:%S'))
	print (Formattedtime)
	return (Formattedtime)

def regex_content_block(regex,content):
# used to take the values block by using regex
	value1=content
	if regex != None:
		revenue = re.findall(regex,str(content))
		return revenue
	else:
		return content

def regex_checking_updating(content,url,tender_id):
	print (tender_id+"\n")
	# print (content)
	title1 = regex_match(title,str(content))
	print title1
	title2 = title1
	# print (title2)
	title2 =  title2.replace("''","'")
	if tender_id=='1174':
		title1=title1[:50]
	# print "re----->",Retrive_duplicate with title of the tender
	Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(tender_id))
	Duplicate_Check_Title = Database_Connector.DuplicateCheckTitle(Retrive_duplicate, title2,url)
	if Duplicate_Check_Title == "N":		
		awarding_authority1 = regex_match(awarding_authority,str(content))
		award_procedure1 = regex_match(award_procedure,str(content))
		contact_type1 = regex_match(contact_type,str(content))
		cpv_code1 = regex_match(cpv_code,str(content))
		description1 = regex_match(description,str(content))
		location1 = regex_match(location,str(content))
		authority_reference1 = regex_match(authority_reference,str(content))
		value1 = regex_match(value,str(content))
		# if tender_id=='1052':
			# deadline_date_re[0]=str(deadline_date_re[0])+" 2019"
		if deadline_date != "None":
			print "U r in Loop"
			deadline_date_re=re.findall(deadline_date,str(content))
			# if tender_id=='1052':
				# deadline_date_re[0]=str(deadline_date_re[0])+" 2019"
				# print (deadline_date_re[0])
			if len(deadline_date_re) == 0 or deadline_date_re[0] == '':
				deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
				# deadline_date1 = dateparser.parse(time.asctime())
				deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
				FormattedDate = DateFormat(deadline_date1)
			else:
				deadline_date1 = reg_clean(deadline_date_re[0])
				deadline_date1 = DateFormat(deadline_date1)
				print (deadline_date1)
				FormattedDate = deadline_date1
		else:
			deadline_date1 = datetime.now(pytz.timezone('Europe/London'))
			deadline_date1 = (deadline_date1.strftime('%Y-%m-%d %H:%M:%S'))
			FormattedDate = deadline_date1
		if deadline_time != "None":
			deadline_time_re = re.findall(deadline_time,str(content))
			if len(deadline_time_re) == 0:
				deadline_time1 = ""
			else:
				if tender_id =="1012":
					deadline_time1 = deadline_time_re[0]+" 00:00"
				else:
					deadline_time1 = timeFormat(deadline_time_re[0])
		else:
			deadline_time1 = "00:00:00"
						
		
		other_information1 = other_information + str(url)
		curr_date = datetime.now(pytz.timezone('Europe/London'))
		created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
							
		insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(tender_id), str(url), str(description1), str(origin), str(sector), str(country), str(websource), str(title1), str(title1),str(awarding_authority1), str(award_procedure1), str(contact_type1), str(cpv_code1), str(description1), str(location1), str(authority_reference1), str(value1), str(deadline_description), str(FormattedDate), str(deadline_time1),str(other_information1), str(url), str(created_date))
		print (insertQuery)
		Database_Connector.insert_query(mysql_cursor, connection ,insertQuery)
	
## DB Connections:::

def program():	

	proxies = {"http" :"http://172.27.137.199:3128","https":"http://172.27.137.199:3128"}

	main_url_query = "SELECT id from tender_master where id>1000 and (frequency=DAYNAME(now()) or frequency='Daily')"
	# main_url_query = "SELECT id from tender_master where id=1150"
	mysql_cursor.execute(main_url_query)
	tender_id = mysql_cursor.fetchall()
	print (tender_id)
	# print "re----->",Retrive_duplicate
	config = ConfigParser.ConfigParser()
	config.read('C:\\BIP_Tender\\UK_Scripts\\Bip_Tender_2.ini')
	print (config.sections())
	# for field in config.options(str(Retailer_Name)):
		# globals()[field]= str(config.get(str(Retailer_Name),field))

	# for field in config.options(str(Retailer_Name)):
	# globals()[field]= str(config.get(str(Retailer_Name),field))
	for i in config.sections():
		temp = str(i)
		# print ("i************",temp)
		for field in config.options(str(temp)):
			globals()[field]= str(config.get(str(temp),field))
			# print (field)
		PYTHONHTTPSVERIFY=1
		# print "re----->",Retrive_duplicate with url
		Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,str(temp))
		if home_url_method == "None":	
			time.sleep(5)
			# session = requests.Session()
			# retry = Retry(connect=3, backoff_factor=0.5)
			# adapter = HTTPAdapter(max_retries=retry)
			# session.mount('http://', adapter)
			# session.mount('https://', adapter)

			# session.get(url)
			r=requests.get(eval(home_url),headers=eval(home_url_headers),verify = False)
			html=r.content

			# print html
		
#### if tender url only from the home url content
			if sub_url_regex != "None" and home_url_block_regex == "None":
				sub_url = re.findall(sub_url_regex,str(html))
				# print sub_url
				for j in sub_url:
					print j
					if not "http" in str(j):
						str1 = base_url.replace('"','')
						if temp == '1139':
							j = str(str1)+j+".htm"
						else:	
							j = str(str1)+j
						j = j.replace("&amp;","&")
						print j
						# print "re----->",Retrive_duplicate with url
					Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, j)
					if Duplicate_Check == "N":
						if sub_url_method == "None":
							r1 = requests.get(str(j),headers=eval(home_url_headers),verify = False)
							html1=r1.content.decode('utf-8', errors = 'ignore')
							regex_checking_updating(html1,j,temp)
							
# if tender url is available in a block of home url content								
			if sub_url_regex != "None" and home_url_block_regex != "None":
				home_url_block = re.findall(home_url_block_regex,str(html))
				# print sub_url
				for j in home_url_block:
					sub_url = re.findall(sub_url_regex,str(j))
					if not "http" in str(sub_url):
						str1 = base_url.replace('"','')
						sub_url = sub_url[0].replace("&amp;","&")
						sub_url = sub_url[0].replace("\\/","/")
						sub_url = str(str1)+sub_url
						# print "re----->",Retrive_duplicate with url
					Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, sub_url)
					if Duplicate_Check == "N":
						if sub_url_method == "None":
							r1 = requests.get(str(sub_url),headers=eval(home_url_headers), verify = False)
							html1=r1.content.decode('utf-8', errors = 'ignore')
							regex_checking_updating(html1,sub_url,temp)
							
	# only tender block has been taken from home url content									
			if sub_url_regex == "None" and home_url_block_regex != "None":
				home_url_block1 = re.findall(home_url_block_regex,str(html))
				# print (home_url_block1)
				for html1 in home_url_block1:
					html1=html1.replace("\n", "").strip()
					print (html1)
					regex_checking_updating(html1,home_url,temp)	
					
	connection.close()
		
if __name__== "__main__":

	program()




