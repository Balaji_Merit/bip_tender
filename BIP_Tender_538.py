# -*- coding: utf-8 -*-
import datetime
import email
import imaplib
import mailbox
import re
import redis
import pymysql
import dateparser
import requests
import xlwt
import urllib
import re
import imp
import pymysql
import time
# import cookielib
import urllib.request as ur
from six.moves.html_parser import HTMLParser
from googletrans import Translator
from datetime import date, timedelta
h = HTMLParser()
# warnings.filterwarnings('ignore')
# reload(sys)
# Database_Connector = imp.load_source('Database_Connector','/home/merit/BIP_TENDER/Complete/Database_Connector.py')
# Database_Connector = imp.load_source('Database_Connector','D:/BIP_Tender_files/Database_Connector.py')
Database_Connector = imp.load_source('Database_Connector','C:/BIP/Temp/Database_Connector.py')
connection = Database_Connector.connection_string()
mysql_cursor = connection.cursor()


from googletrans import Translator


block_re = '(<td\s*valign\=\"top\">[\w\W]*?Voir)'
details_block_re = '(<td[\w\W]*?<\/td>)'
sub_detail_re = '(<p[\w\W]*?<\/p>)'
# boamp_blocks_re ='class\="m[^>]*?corps[^>]*?\">([\w\W]*?)<\/tr>'
boamp_blocks_re ='<article class="result-search-avis">([\w\W]*?)<\/article>'
b_link_re = '<a\s*href\="([^>]*?)">'
title_re = '<h1>([^>]*?)<\/h1>\s*<p class="type-avis">'
article_re = '<article\s*role\=\"article\">([\w\W]*?)<\/article>'
awardingAuthority_re = 'class=\"detail-avis detail-main-1\">([\w\W]*?)DESCRIPTION MARCHE'
object_contract_re = '<h3>Objet[\w\W]*?<p>([\w\W]*?)</p>'
desc_re = 'class\="detail\-avis\s*detail\-main\-2\">([\w\W]*?)<\/div>'
autho_ref_re = 'class\=\"avis\-ref\">([^>]*?)<\/p>'
# deadline_date_re = '<h3>Date\s*limite[\w\W]*?<p>([/^\d{2}\/\d{2}\/\d{4}$/]*?)\s+'
deadline_date_re = '<h3>Date\s*limite[^>]*?<\/h3>\s*<p>(\d{1,2})\/(\d{1,2})\/(\d{4})\s+'
# deadline_date_re = 'Date\s*limite[^>]*?<\/td>[\w\W]*?<td class="txt">(\d{1,2})\/(\d{1,2})\/(\d{4})\s+'
# deadline_time_re = 'Date\s*limite\s*de[^>]*?[^>]*?<\/td>[\w\W]*?<td class="txt">[\w\W]*?\s*(\d{1,2}h\d{2})'
deadline_time_re = '<h3>Date limite de r[^>]*?<\/h3>\s*<p>[^>]*?à\s*(\d{1,2}h\d{2})<\/p>'
mail_date_re = '[^>]*?\,\s+([\d]{2}\s+[^>]*?\s+[\d]{4})\s+[^>]*?'
Tender_ID = 538
orgin_Boamp = 'European/Non Defence'
orgin_place = 'European/Defence'
sector = 'European Translation'
country = 'FR'
dead_line_W_Boamp = 'Deadline for receipt of tenders:'
summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: Tender Page Link'
Retrive_duplicate  =  Database_Connector.Retrieve_for_DuplicateCheck(mysql_cursor,Tender_ID)
# print "re----->",Retrive_duplicate


def reg_clean(cont):
    cont = str(cont).replace('\r','')
    cont = str(cont).replace('\n','')
    cont = str(cont).replace('\t','')
    cont = re.sub(r'<[^<]*?>', '', str(cont))
    cont = re.sub(r'\r\n', '', str(cont), re.I)
    cont = re.sub(r"'", "''", str(cont), re.I)
    cont = cont.replace("'","''")
    cont = re.sub(r'\t', " ", cont, re.I)
    cont = re.sub(r'\n', " ", cont, re.I)
    cont = cont.replace("'","''")
    cont = cont.replace("î","'")
    cont = cont.replace("l'","l''")
    cont = cont.replace("L'","L''")
    cont = cont.replace("d'","d''")
    cont = cont.replace("D'","D''")
    cont = cont.replace("'s","''s")
    cont = cont.replace("du'","du''")
    cont = cont.replace("N'","N''")
    cont = cont.replace("'acheteur","''acheteur")
    cont = cont.replace("'id","''id")
    cont = cont.replace("'b'","'b''")
    cont = cont.replace("'Esp","''Esp")
    cont = cont.replace("'RD","''RD")
    # cont = str(cont).replace("\’","''")
    cont = cont.replace("’","''", re.I)
	# cont = cont.replace("'","''", re.I))
    cont = re.sub(r'\n\t', " ", cont, re.I)
    cont = re.sub(r"^\s+\s*", "", cont, re.I)
    cont = re.sub(r"\s+\s*$", "", cont, re.I)
    cont = re.sub(r"\\xa0", "", cont, re.I)
    cont = re.sub(r"\xa0", "", cont, re.I)
    cont = cont.replace("''''''","''")
    cont = cont.replace("'''''","''")
    cont = cont.replace("''''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("'''","''")
    cont = cont.replace("\xc3\xa9","é")
    cont = cont.replace("&#39;","''")
    cont = cont.replace("&quot;",'"')
    cont = cont.replace("î","'")
    cont = cont.replace("'","''")
    cont = cont.replace("''''","''")	
    return cont
	
def redis_connection():
    red_connec = redis.StrictRedis(host='127.0.0.1', port=6379, db=1)
    return red_connec


def translateText(InputText):
    # print " Current Translate Text :",InputText
    redis_con = redis_connection()
    tempText = InputText
    # InputText = InputText.encode('utf8', 'ignore').decode('utf_8')
    check = redis_con.exists(InputText)
    translated_sentence = ''
    translator = Translator()

    if check:
        translated_sentence = redis_con.get(InputText)
        if (translated_sentence == None) or (str(translated_sentence) == "None"):
            try:
                translated = translator.translate(InputText)
                translated_sentence = translated.text
            except Exception as e:
                # print 'Error translate 1**',e
                pass
                try:
                    url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                    payload = {'q': str(InputText),
                               'target': 'en',
                               'source': 'fr'
                               }.format(InputText)
                    s = requests.post(url, data=payload)
                    value = s.json()
                    translated_sentence = value['data']['translations'][0]['translatedText']
                except Exception as e:
                    # print 'Error translate 2 **', e
                    pass

    else:
        # do translate
        try:
            translated = translator.translate(InputText)
            translated_sentence = translated.text
            time.sleep(2)
            redis_con.set(InputText, translated_sentence)
        except Exception as e:
            # print 'Error translate 3 **',e
            pass
            # print "Input EXcp 3 :",InputText
            try:
                url = 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCGSToBHrOi8UW6JFyolUibjNXV9Q0ux4U'
                # payload = """{'q': '{}',
                #            'target': 'en'
                #            'source' : 'si'}""".format(str(InputText))
                payload = {'q': InputText,
                          'target': 'en',
                          'source' : 'fr'}

                # print "payload :",payload
                s = requests.post(url, data=payload)
                value = s.json()
                # print "value :",value
                # raw_input("Value s")
                translated_sentence = value['data']['translations'][0]['translatedText']
                time.sleep(2)
                # print "1 :", translated_sentence
                try:
                    redis_con.set(InputText, translated_sentence)
                except Exception as e:
                    # print 'Redis Err:',e
                    pass
                # print "2 :", translated_sentence
            except Exception as e:
                # print '**translate 4 **', e
                pass
    # print "Returning Text :",translated_sentence
    return translated_sentence


def req_content(url):
	try:
		req_url = requests.get(url,proxies = {
		"http" :"http://172.27.137.192:3128",
		"https":"http://172.27.137.192:3128"
		})
		url_content = req_url.content.decode('utf_8', errors='ignore')
	except Exception as e:
		url_content=''
	return url_content


def reducer(val):
    if val:
        val = val[0]
    else:
        val = ''
    return  val


def regxx(regx,content):
    cont_data = re.findall(regx,content)
    if cont_data:
        cont_data = cont_data[0] 
    else:
        cont_data = ''
    return h.unescape(cont_data)

def start(main_url):
    proxies = {
    "http":"https://172.27.137.192:3128",
    "https":"https://172.27.137.192:3128"
    }
    s = requests.session()
    last_week = date.today()- timedelta(1)
    print ("Today******",last_week)
    last_week_date1 = (last_week.strftime('%d'))
    last_week_date2 = (last_week.strftime('%m'))
    last_week_date3 = (last_week.strftime('%Y'))
	# estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis[]=1&dateparutionmin=28/08/2020&dateparutionmax=28/08/2020&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis[]=1&dateparutionmin=27/08/2020&dateparutionmax=27/08/2020&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",headers={"Host": "www.boamp.fr","Content-Type": "application/x-www-form-urlencoded","Referer": "https://www.boamp.fr/recherche/avancee"},proxies=proxies,verify=False)
    # main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",proxies=proxies,verify=False)
    main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",verify=False)
	# main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",proxies=proxies,verify=False)
    #main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=",headers={"Host": "www.boamp.fr","Referer": "https://www.boamp.fr/avis/liste","Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; eZSESSID1335d27b6a308a5b0435f39d334a4390=q6ugd6ocqf1drpdh7m9t762th5"},proxies=proxies,verify=False)
	# main_content1 = requests.post(main_url, data="estrecherchesimple=0&archive=0&idweb=&nomacheteur=&fulltext=&typeavis%5B%5D=1&dateparutionmin="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&dateparutionmax="+last_week_date1+"%2F"+last_week_date2+"%2F"+last_week_date3+"&datelimitereponsemin=&datelimitereponsemax=&famille=&prestataire=

	
    s = requests.session()
    main_content = main_content1.content.decode('utf_8', errors='ignore')
    main_content = str(main_content).replace('\r','')
    main_content = str(main_content).replace('\n','')
    main_content = str(main_content).replace('\t','')
    # with open ("temp1.html",'w') as f:
			
        # f.write(main_content)
    Tender_ID = 538
    process_id = 538
    global row_s1
    boamp_blocks = re.findall(boamp_blocks_re,main_content)
    print (boamp_blocks)
    for s2,boamp_block in enumerate(boamp_blocks):
        print ("-----boamp-{}-----".format(s2))
        b_link = re.findall(b_link_re,boamp_block)
        if b_link:                         
            b_link = "https://www.boamp.fr"+b_link[0]

            Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, b_link)
            print(Duplicate_Check)
            if Duplicate_Check == "N":
                print (b_link)
                # try:
                # req_url = s.get(b_link,headers={"Host": "www.boamp.fr","Referer": main_url,"Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; boampCookie=cnil; eZSESSID1335d27b6a308a5b0435f39d334a4390=gi9u7fgp7lv687qhj59mvh4g25","Connection": "keep-alive"},proxies = {"http" :"http://172.27.137.192:3128","https":"http://172.27.137.192:3128"},verify=False)
                req_url = s.get(b_link,headers={"Host": "www.boamp.fr","Referer": main_url,"Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; tarteaucitron=!xiti=true; eZSESSID1335d27b6a308a5b0435f39d334a4390=r967sf9q90o4dsritr44knblc4","Connection": "keep-alive","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"},proxies = {"http" :"http://172.27.137.199:3128","https":"http://172.27.137.199:3128"},verify=False)
                link_content = req_url.content.decode('utf_8', errors='ignore')
                # link_content = req_content(b_link)
                # print (link_content)
                # except Exception as e:
                    # next
                # with open ('temp2.html','wb') as f:

                    # f.write(link_content)
                # link_content = str(link_content)
                # sheet2.write(row_s2, 7, b_link)
                # sheet2.write(row_s2, 0, orgin_Boamp)
                # article = regxx(article_re,link_content)
                # article = reg_clean(article)
                
                title = regxx(title_re,link_content)
                # title = reg_clean(title)
                title = translateText(title)
                title = reg_clean(title)
                print (title)
                # sheet2.write(row_s2, 1, title)
                
                
                awardingAuthority = regxx(awardingAuthority_re,link_content)
                print (awardingAuthority)
                if awardingAuthority:
                # input("************************")
                    awardingAuthority1 = reg_clean(awardingAuthority)
					
                # print (awardingAuthority)
                # input("************************")
                # sheet2.write(row_s2, 2,h.unescape(awardingAuthority1))
                
                    desc = regxx(desc_re,link_content)
                    desc = reg_clean(desc)
                    desc = translateText(desc)
                    desc = reg_clean(desc)
                # sheet2.write(row_s2, 3, desc)
                
                    autho_ref = regxx(autho_ref_re,link_content)
                    autho_ref = reg_clean(autho_ref)
                
                # sheet2.write(row_s2, 4, autho_ref)
                
                    deadlinedate = regxx(deadline_date_re,link_content)
                    
                    # deadlinedate  = deadlinedate[0].strptime('Y-%m-%d %H:%M:%S')
                    print (deadlinedate)
                    if deadlinedate:
                        deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
                        dt = dateparser.parse(deadlinedate)
                        deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
                        # print (deadlinedate)
                # dead_line_time = dead_line_date[0][1]
                # sheet2.write(row_s2, 5, deadlinedate)       
                
                    deadlinetime = regxx(deadline_time_re,link_content)
                    deadlinetime = deadlinetime.replace('h',':')
                    summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
                    summary_cont = summary_cont +' '+ str(b_link)
                    print (summary_cont)
                # sheet2.write(row_s2, 6, deadlinetime)
                    curr_date = dateparser.parse(time.asctime())
                    created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
                
					# print (summary_cont)
                    insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link, str(), orgin_Boamp, sector, country, str(),title, title, awardingAuthority1, str(), str(), str(),desc, str(), autho_ref, str(), dead_line_W_Boamp, deadlinedate, deadlinetime,str(), summary_cont, 'Y',created_date)
                    print (insertQuery)
					
                    Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
                # row_s2 = row_s2 + 1
                # book.save('my_book_v9.xls')
                else:
                    print ("Duplicate data found ")
    next_page=re.findall('<p class="pagination-index">Page 1 sur\s*([^>]*?)<\/p>',main_content)
    next_page=(int((next_page[0])))+1
    for page in range(2,next_page):
        main_url1="https://www.boamp.fr/avis/page?page="+str(page)
        print ("next Page:",main_url1)
        s = requests.session()
        # s.cookies = cookielib.LWPCookieJar(filename="cookies.txt")
		
        # main_content2 = s.get(main_url1,headers={"Host": "www.boamp.fr","Referer": "https://www.boamp.fr/avis/liste","Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; boampCookie=cnil; eZSESSID1335d27b6a308a5b0435f39d334a4390=gi9u7fgp7lv687qhj59mvh4g25"},proxies=proxies,verify=False)
        main_content2 = s.get(main_url1,headers={"Host": "www.boamp.fr","Referer": "https://www.boamp.fr/avis/liste","Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; tarteaucitron=!xiti=true; eZSESSID1335d27b6a308a5b0435f39d334a4390=konuf26c3iesjds7bat1hhf6u3"},proxies=proxies,verify=False)
        s = requests.session()    
        main_content = main_content2.content.decode('utf_8', errors='ignore')
        time.sleep(2)
        # try:
            # with open ("temp538.html",'w') as f:
				# f.write(main_content)
        boamp_blocks = re.findall(boamp_blocks_re,main_content)
	
        for s2,boamp_block in enumerate(boamp_blocks):
            print ("-----boamp-{}-----".format(s2))
            b_link = re.findall(b_link_re,boamp_block)
            if b_link:                         
                b_link = "https://www.boamp.fr"+b_link[0]
                print (b_link)
                Duplicate_Check = Database_Connector.DuplicateCheck(Retrive_duplicate, b_link)
                print (Duplicate_Check)
                if Duplicate_Check == "N":
                    # try:
                    s = requests.session()
                    link_content = s.get(b_link,headers={"Host": "www.boamp.fr","Referer": main_url1,"Cookie": "xtvrn=$517208$; xtan517208=10-10; xtant517208=1; eZSESSID1335d27b6a308a5b0435f39d334a4390=9frbpamlg2ctgj9vgfnlt5hnq2","Connection": "keep-alive"},proxies=proxies,verify=False)
                    link_content = link_content.content.decode('utf_8', errors='ignore')
				# print (link_content)
                    # except Exception as e:
                        # pass
				# with open ('temp2.html','wb') as f:

					# f.write(link_content)
			# link_content = str(link_content)
			# sheet2.write(row_s2, 7, b_link)
			# sheet2.write(row_s2, 0, orgin_Boamp)
			# article = regxx(article_re,link_content)
			# article = reg_clean(article)
			
                    title = regxx(title_re,link_content)
                    title = reg_clean(title)
                    title = translateText(title)
                    title = reg_clean(title)
                    print (title)
			# sheet2.write(row_s2, 1, title)
			
			
                    awardingAuthority = regxx(awardingAuthority_re,link_content)
                    print (awardingAuthority)
                    if awardingAuthority:
			# input("************************")
                        awardingAuthority1 = reg_clean(awardingAuthority)
			# print (awardingAuthority)
			# input("************************")
			# sheet2.write(row_s2, 2,h.unescape(awardingAuthority1))
			
                        desc = regxx(desc_re,link_content)
                        desc = reg_clean(desc)
                        desc = translateText(desc)
                        desc = reg_clean(desc)
			# sheet2.write(row_s2, 3, desc)
			
                        autho_ref = regxx(autho_ref_re,link_content)
                        autho_ref = reg_clean(autho_ref)
			
			# sheet2.write(row_s2, 4, autho_ref)
			
                        deadlinedate = regxx(deadline_date_re,link_content)
				
				# deadlinedate  = deadlinedate[0].strptime('Y-%m-%d %H:%M:%S')
                        print (deadlinedate)
                        if deadlinedate:
                            deadlinedate = deadlinedate[2]+"-"+deadlinedate[1]+"-"+deadlinedate[0]+" 00:00:00"
                            dt = dateparser.parse(deadlinedate)
                            deadlinedate = (dt.strftime('%Y-%m-%d %H:%M:%S'))
					# print (deadlinedate)
			# dead_line_time = dead_line_date[0][1]
			# sheet2.write(row_s2, 5, deadlinedate)       
			
                        deadlinetime = regxx(deadline_time_re,link_content)
                        deadlinetime = deadlinetime.replace('h',':')
                        summary_cont = 'The summary of this contract has been created using automated translation software. BiP Solutions does not accept responsibility for any inaccuracies contained therein. The full text of the notice in its native language is available from the following Website: '
                        summary_cont = summary_cont +' '+ str(b_link)
                        print (summary_cont)
			# sheet2.write(row_s2, 6, deadlinetime)
                        curr_date = dateparser.parse(time.asctime())
                        created_date = (curr_date.strftime('%Y-%m-%d %H:%M:%S'))
			
				# print (summary_cont)
                        insertQuery = "insert into tender_data (process_id,url,tender,origin,sector,country,web_source,title,title_duplicate_check,awarding_authority,award_procedure,contact_type,cpv_code,description,location,authority_reference,value,deadline_description,deadline_date,deadline_time,sent_to,other_information,BIP_Non_UK,created_date) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(Tender_ID, b_link, str(), orgin_Boamp, sector, country, str(),title, title, awardingAuthority1, str(), str(), str(),desc, str(), autho_ref, str(), dead_line_W_Boamp, deadlinedate, deadlinetime,str(), summary_cont, 'Y',created_date)
                        print (insertQuery)
				
                        Database_Connector.insert_query(mysql_cursor, connection, insertQuery)
			# row_s2 = row_s2 + 1
			# book.save('my_book_v9.xls')
                else:
                    print ("Duplicate data found ")
                time.sleep(5)
		# except Exception as e:
			# pass					


if __name__== "__main__":
	last_week = date.today()
	# last_week = date.today()
	last_week_date1 = (last_week.strftime('%d/%m/%Y'))
	print (last_week_date1)
	# curr_date1 = date.today() - timedelta(1)
	curr_date1 = date.today()
	created_date1 = (curr_date1.strftime('%d.%m.%Y'))
	print (created_date1)
	# raw_input("check")
	main_url = 'https://www.boamp.fr/avis/liste'
	n = 1
	start(main_url)