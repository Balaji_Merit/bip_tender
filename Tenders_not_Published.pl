
#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use Date::Parse;
use Date::Manip;
use POSIX 'strftime';
use MIME::Lite;
use Cwd;
require BIP_DB;

sub Retrieve_Link_Not_Working_Buyers()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $fulldate = strftime "%d %m %Y %A", localtime;
my $day = (split(" ",$fulldate))[-1];
print "Day :: $day\n";
my $today = time;
	print "Today::$today\n";
my $yesterday = $today - 60 * 24 * 60;
my $previous_date = strftime "%Y%m%d", ( localtime($yesterday));
		# my $yesterday = $today;
		
		
my $today_date = strftime "%Y%m%d", (localtime($today) );
	my $query = "select id,tender_name,tender_link,origin, noticesector, cy from tender_master a left join (select process_id from tender_data where cast(tender_data.created_date as date) >= '".$previous_date."' and cast(tender_data.created_date as date) <= '".$today_date."') b on a.id = b.process_id where a.id is not null and ifnull(b.process_id,'') = '' and a.active = 'Y'";
	print $query;
	my $sth = $dbh->prepare($query);
	$sth->execute();
	sleep(10);
	my (@Tender_id,@Tender_Name,@Tender_link,@origin,@noticesector,@cy);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Tender_id,&Trim($record[0]));
		push(@Tender_Name,&Trim($record[1]));
		push(@Tender_link,&Trim($record[2]));
		push(@origin,&Trim($record[3]));
		push(@noticesector,&Trim($record[4]));
		push(@cy,&Trim($record[5]));
	}
	$sth->finish();
	return (\@Tender_id,\@Tender_Name,\@Tender_link,\@origin,\@noticesector,\@cy);
}

sub DbConnection()
{
	my $DBDETAILS 		= &ReadIniFile('DBDETAILS');
	my %Credentials		= %{$DBDETAILS};
	my $database_name 	= $Credentials{'database_name'};
	my $server_name 	= $Credentials{'server_name'};
	my $database_user 	= $Credentials{'database_user'};
	my $database_pass 	= $Credentials{'database_pass'};
	my $driver = "mysql"; 
	my $dsn = "DBI:$driver:database=$database_name;host=$server_name;";
	my $dbh = DBI->connect($dsn, $database_user, $database_pass,{mysql_enable_utf8 => 1}) or die $DBI::errstr;
	return $dbh;
}

my %Months=("Jan" => "01", "Jeb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

print "Current_Month_txt-->$Current_Month_txt\n";
print "Current_Year-->$Current_Year\n";
print "Current_Date-->$Current_Date\n";
print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
$ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='tender_master';
############Database Initialization########
my $dbh = &DbConnection();
###########################################
my ($Tender_ID,$Tender_Name,$Tender_link,$origin,$noticesector,$cy) = &Retrieve_Link_Not_Working_Buyers($dbh,$Input_Table);

# exit;
my @Tender_ID=@$Tender_ID;
my @Tender_Name=@$Tender_Name;
my @Tender_link=@$Tender_link;
my @origin=@$origin;
my @noticesector=@$noticesector;
my @cy=@$cy;
my $temp='Yes';
my $temp2='No';
# print $Tender_link;
# exit;
my $freqquency;
open(DATA,">Tenders_Not_Published.csv");
print DATA "Tender_ID,Tender_Name,Tender_Source_URL,Origin,Notice_Sector,Country,Frequency,Is Source Live,Is tender not captured\n";
for(my $i=0;$i<@Tender_ID;$i++)
{
	my $Tender_ID=$Tender_ID[$i];
	if ($Tender_ID<=90)
	{
	$freqquency="Daily"
	}
	if ($Tender_ID>100 and $Tender_ID<405)
	{
	$freqquency="Intend Daily"
	}
	if ($Tender_ID>500 and $Tender_ID<700)
	{
	$freqquency="Worldwide"
	}
	if ($Tender_ID>1000)
	{
	$freqquency="Weekly"
	}	
	my $Tender_Name=$Tender_Name[$i];
	my $Tender_link1=$Tender_link[$i];
	my $origin=$origin[$i];
	my $noticesector=$noticesector[$i];
	my $cy=$cy[$i];
	
	my $homeUrl;
	if ($Tender_link1=~m/^\s*(http[^>]*?tendhost[^>]*?aspx)[^>]*?/is)
{
	$homeUrl = $1."/Home";
	# print $homeUrl;

}
else
{
$homeUrl =$Tender_link1;
}
	# my $Link_Status=$Link_Status[$i];
	print DATA "$Tender_ID,$Tender_Name,$homeUrl,$origin,$noticesector,$cy,$freqquency,$temp,$temp2\n";
}
close DATA;
my $fulldate = strftime "%b %d, %Y %A %H:%M:%S", localtime;
&send_mail("BiP Solutions_Tender not Pubslished_$fulldate","Tender_Not_Published.csv");

sub send_mail()
{
	my $subject = shift;
	my $File_Name = shift;
	my $dir=getcwd();
	$dir=~s/\//\\/igs;
	$dir=$dir.'\\'.$File_Name;	

	# my $host ='mail.meritgroup.co.uk'; 
	# my $from='autoemailsender@meritgroup.co.uk';
	# my $user='autoemailsender';
	# my $to ='arul.kumaran@meritgroup.co.uk';
	# my $pass='T!me#123456';	
	
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='bipsolutions.uk@meritgroup.co.uk,prabukannan.mani@meritgroup.co.uk,poornima.elumalai@meritgroup.co.uk';
	my $cc='balaji.ekambaram@meritgroup.co.uk,vinothkumar.rajendren@meritgroup.co.uk';
	my $pass='sXNdrc6JU';
	my $body = "Greetings from Software support..!<br><br>\t\tPlease find the attached BIP tenders not published status file<br><br><br><br>Note : This is an automatically generated email, please don't reply.<br><br>Have a nice day..!<br><br>"."Thanks &amp; Regards,<br>Merit Software Support.";
	print "Mail Part";
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
		Type=> "text/html",
		Data     => $body
	);
	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
	print 	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

sub ReadIniFile()
{
	my $INIVALUE = shift;
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Tender.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}