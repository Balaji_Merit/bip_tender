# -*- coding: utf-8 -*-
import pymysql
# import ConfigParser
import re
import sys
import urllib
import datetime
from datetime import date, timedelta


# reload(sys)
# sys.setdefaultencoding("utf-8")

'''Read/write configuration files'''
# config = ConfigParser.ConfigParser()
# config.read('D:/BIP_Tender_files/config.ini')


'''connection crediantials'''
host = "10.101.53.30"
username = "bipappuser"
password = "BipApp2018!!"
portname = "3306"
db_name = "BIP_tender_analysis"


def connection_string():
    # Connect to the database
    try:
        connection = pymysql.connect(host=str(host), user=str(username), password=str(password), db=str(db_name), charset="utf8")

        return connection
    except Exception as e:
        print ('DB connection_string Error: ',e)
        connection.close()
		
def connection_string1():
    # Connect to the database
    try:
        connection = pymysql.connect(host=str(host), user=str(username), password=str(password), db=str(db_name), charset="latin1")

        return connection
    except Exception as e:
        print ('DB connection_string Error: ',e)
        connection.close()


def select_query(cursor,query):
    try:
        cursor.execute(query)
        # Read a single record
        result = cursor.fetchone()
        return result
    except Exception as e:
        print ('DB select_query Error: ',e)


def insert_query(cursor,connection,query):
    try:

        cursor.execute(query)
        connection.commit()
    except Exception as e:
        print ('DB insert_query Error: ',e)

##### Retrieve from tender_url to Check the Duplicate #####
def Retrieve_for_DuplicateCheck(cursor1, ProcessID):
    Query = u"select url from tender_data where process_id="+str(ProcessID)
    cursor1.execute(Query)
    rows = cursor1.fetchall()
	
    return (rows)
	
def Retrieve_for_DuplicateCheck2(cursor1, ProcessID):
    Query = u"select url from tender_data where Awarded_Status='Y' and process_id="+str(ProcessID)
    cursor1.execute(Query)
    rows = cursor1.fetchall()	
	

    return (rows)
	
def Retrieve_for_DuplicateCheck1(cursor1, ProcessID):
    Query = u"select title,url from tender_data where process_id="+str(ProcessID)
    cursor1.execute(Query)
    rows = cursor1.fetchall()

    return (rows)	

def Retrieve_url(cursor1, ProcessID):
    Query = u"select url from tender_master where id="+str(ProcessID)
    cursor1.execute(Query)
    rows = cursor1.fetchall()

    return (rows)

##### DeDuping process #####
def DuplicateCheck(Records_DuplicateCheck, URL):
    duplicate = ''

    if len(Records_DuplicateCheck) != 0:
        for row in Records_DuplicateCheck:
            if URL in row:
                print ("duplicate data")
                duplicate = 'Y'
                break
            else:
                duplicate = 'N'
    else:
        duplicate = 'N'

    return (duplicate)
def DuplicateCheckTitle(Records_DuplicateCheck, Duplicate_Check_Title):
    duplicate = ''

    if len(Records_DuplicateCheck) != 0:
        for row in Records_DuplicateCheck:
            if Duplicate_Check_Title in row:
                print ("duplicate Title")
                duplicate = 'Y'
                break
            else:
                duplicate = 'N'
    else:
        duplicate = 'N'

    return (duplicate)	