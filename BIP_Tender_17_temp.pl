#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use Encode;
use DateTime;
use Encode;
use POSIX 'strftime';
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = "17";
# $Input_Tender_ID =~ s/\.pl//igs;
# $Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
my ($Tender_content1)=&Getcontent($Tender_Url);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content1.html");
print ts "$Tender_content1";
close ts;

# my $filename="Due_North_ProContract.txt";
# open(out,">>$filename");
# print out "sourcecw\ttitle\torigin\tawardingAuthority\tauthorityName\tnoticeSector\tauthorityDept\tcy\tCountry Codes::ISOcountry\tauthorityAddress1\tsourceURL\tauthorityAddress2\tauthorityAddress3\thigh_value\tdefence\tpubFlagCW\tauthorityTown\tauthorityCounty\tonline_only\tprivate_sector\tframeworkAgreement\tauthorityPostcode\tauthorityCountry\tCountry Codes 2::ISOcountry\tauthorityTelephone\tauthorityFax\tauthorityEmail\ttemp_description_export\tauthorityWebAddress\tauthorityProfileURL\tauthorityContactSalutation\tauthorityContactFirstName\tauthorityContactLastName\tauthorityPosition\ttemp_site_export\tawardProcedure\tnc\ttrackerRef\tcontractType\tearliestDate\tpr\tcpvNos\taa\tnuts\tIndustry_Sector\torigPublicationDate\tdescription\tproofed\texported\tsite\tautoModifyDate\tautoModifyTime\tauthorityRefNo\tno_of_months\tcontract_start_date\tcontract_end_date\tvalue\tcurrency\tamount\tamount_min\tamount_max\tcw_RAdeadline\td_RA\td_RAtime\td_RApost\totherInformation\n";
# close out;
my $sourcecw="Contrax Weekly";
my $origin="Web";
my $noticeSector="UK Other";
my $cy="GB";
my $country_code="United Kingdom";
my $sourceURL="Due North: ProContract";
my $pubFlagCW="No";
my $authorityDept="";
my $authorityAddress2="";
my $authorityAddress3="";
my $Industry_Sector="Other";
my $authorityCountry="GB";
# my $fulldate_temp=strftime "%d_%m_%Y_%A_%H_%M_%S", localtime;
	
my $filename = "Tender_17.txt";
open(out,">$filename");
print out "sourcecw\ttitle\torigin\tawardingAuthority\tauthorityName\tnoticeSector\tauthorityDept\tcy\tCountry Codes::ISOcountry\tauthorityAddress1\tsourceURL\tauthorityAddress2\tauthorityAddress3\thigh_value\tdefence\tpubFlagCW\tauthorityTown\tauthorityCounty\tonline_only\tprivate_sector\tframeworkAgreement\tauthorityPostcode\tauthorityCountry\tCountry Codes 2::ISOcountry\tauthorityTelephone\tauthorityFax\tauthorityEmail\ttemp_description_export\tauthorityWebAddress\tauthorityProfileURL\tauthorityContactSalutation\tauthorityContactFirstName\tauthorityContactLastName\tauthorityPosition\ttemp_site_export\tawardProcedure\tnc\ttrackerRef\tcontractType\tearliestDate\tpr\tcpvNos\taa\tnuts\tIndustry_Sector\torigPublicationDate\tdescription\tproofed\texported\tsite\tautoModifyDate\tautoModifyTime\tauthorityRefNo\tno_of_months\tcontract_start_date\tcontract_end_date\tvalue\tcurrency\tamount\tamount_min\tamount_max\tcw_RAdeadline\td_RA\td_RAtime\td_RApost\totherInformation\n";
close out;	
my @nextnumbers;
#### To get the tender link ####
top:
while ($Tender_content1=~m/<tr[^>]*?gridrow[^>]*?>([\w\W]*?)<\/tr>/igs)
{
	my $block = $1;
	if ($block=~m/href\=\"([^>]*?)\"/is)	
	{
		my $tender_link = &Urlcheck($1);
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content);
		# open(ts,">Tender_content17.html");
	# print ts "$Tender_content";
	# close ts;	
	# exit;
		# my ($tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date,$authorityAddress2,$authorityAddress3,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$private_sector);
	my ($authorityName,$tenderblock,$tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$private_sector,$Contract_Type,$CPV_Code,$Description,$Authority_Reference,$Value,$Deadline_time,$Sent_to,$defence,$Location,$Deadline_Description,$Deadline_Date,$authorityAddress2,$authorityAddress3,$authorityTown,$authorityCounty,$online_only,$frameworkAgreement,$authorityPostcode,$authorityFax,$temp_description_export,$authorityWebAddress,$authorityProfileURL,$authorityContactSalutation,$authorityContactFirstName,$authorityContactLastName,$authorityPosition,$temp_site_export,$awardProcedure,$nc,$trackerRef,$aa,$nuts,$origPublicationDate,$proofed,$exported,$autoModifyDate,$autoModifyTime,$no_of_months,$contract_start_date,$contract_end_date,$currency,$amount_min,$amount_max,$pr,$value1);
		
		if ($Tender_content=~m/From\s*<\/span>[\w\W]*?>\s*to\s*<\/span>([^>]*?)\s([^>]*?)</is)
		{
			$Deadline_Date = $1;
			$Deadline_time = $2;
		}
		if ($Tender_content=~m/<h1>\s*([^>]*?)\s*<\/h1>/is)
		{
			$tender_title = &clean($1);
		}
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/Short\s*description<\/span>\s*(?:<br>)?([^>]*?)\s(?:\-)\s*([^>]*?)<br>/is)
		{
			$Sector = &clean($1);
		}
		my ($Buyer,$Contact,$Email,$Telephone,$Address);
		if ($Tender_content=~m/>\s*Buyer\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Buyer = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Contact)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $contact_keyword = $1;
			$Contact = &clean($2);
			$authorityName=$2;
			$Contact=$contact_keyword.":".$Contact;
		}
		if ($Tender_content=~m/Contact<\/div>\s*<div[^>]*?>\s*((?:Mrs\s*|Mr\s*|Miss\s*|Ms\s*|\s*))\s*([^>]*?)\s+([^>]*?)<\/div>/is)
		{
			$authorityContactSalutation=$1;
			$authorityContactFirstName = $2;
			$authorityContactLastName = $3;
			# $authorityContactFirstName=$2;
			# $Contact=$contact_keyword.":".$Contact;
		}
		if ($Tender_content=~m/>\s*(Email)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $Email_keyword = $1;
			$Email = &clean($2);
			# $Email=$Email_keyword.":".$Email;
		}
		if ($Tender_content=~m/<div class="cell130 aright">Categories<\/div>\s*([\w\W]*?)\s*<div class="row">/is)
		{
			my $cpv_code_block = $1;
			my $temp;
			while($cpv_code_block=~m/<div\s*[^>]*?>([^>]*?)\s*\-/igs)
			{
			$CPV_Code=$CPV_Code.", ".$1;
					# $Email=$Email_keyword.":".$Email;
		}
		}
		# $CPV_Code
		if ($Tender_content=~m/>\s*(Telephone)\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			my $Telephone_keyword = $1;
			$Telephone = &clean($2);
			# $Telephone=$Telephone_keyword.":".$Telephone;
		}
		my $authorityAddress21;
		if ($Tender_content=~m/>\s*Address\s*<\/div>\s*<div[^>]*?>\s*([^>]*?)\s*<br>\s*([^>]*?)\s*<br>\s*([^>]*?)\s*<br>\s*([^>]*?)\s*<br>\s*([^>]*?)\s*<\/div>/is)
		{
			$Address=$1;
			$authorityTown=$2;
			$authorityCounty=$3;
			$authorityPostcode=$4;
			
			$authorityCountry=$5;
			
			$authorityAddress21 = $Address.", ".$authorityTown.", ".$authorityCounty." - ".$authorityPostcode.", ".$authorityCountry;
		}
		$Awarding_Authority=$Buyer.",".$Contact.",".$Email.",".$Telephone.",".$authorityAddress21;
		$Awarding_Authority=~s/^\s*,|,\s*$//igs;
		if ($Tender_content=~m/>\s*Description\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Description = &BIP_Tender_DB::clean($1);
			$Description = &clean($1);
		}
		if ($Tender_content=~m/>\s*(Region\(s\)\s*of\s*supply\s*<\/div>\s*<div[^>]*?>\s*[\w\W]*?)\s*<\/div>/is)
		{
			$Location = &clean($1);
		}
		if ($Tender_content=~m/>\s*Opportunity\s*Id\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Authority_Reference = "Opportunity Id: ".&clean($1);
		}
		my $block;
		if($Tender_content=~m/Categories<\/div><div[^>]*?>\s*([\w\W]*?)\s*<div class="row">/is)
		{
			$block=$1;
			while($block=~m/<div[^>]*?>\s*([^>]*?])\s+[^>]*?]<\/div>/igs)
			{
			$CPV_Code=$CPV_Code.", ".$CPV_Code;
		}
		}
		$CPV_Code=~s/^\s*,\s*//igs;
		if ($Tender_content=~m/>\s*Estimated\s*value\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$Value = $1;
			if($Value="N\/A")
			{
			# $Value ="";
			$value1='';
			$Value = "Estimated Value ".$Value;
			}
			else
			{
			$value1=$Value;
			# $CPV_Code=~s/\,\s*//igs;
			$Value = "Estimated Value ".$Value;
			
			}
		}
				if ($Tender_content=~m/>Start date<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$contract_start_date = &clean($1);
		}
						if ($Tender_content=~m/ <div class="cell aright">Fax<\/div>\s*<div class="cell">\s*([\w\W]*?)\s*<\/div>/is)
		{
			$authorityFax = &clean($1);
		}
						if ($Tender_content=~m/>Start date<\/div>\s*<div[^>]*?>\s*[\w\W]*?End date<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
		{
			$contract_end_date = &clean($1);
		}
		if ($Tender_content=~m/<h2>(Expression\s*of\s*interest\s*window)<\/h2>/is)
		{
			$Deadline_Description = $1;
			$Deadline_Description='Expression of Interest End Date';
		}
		if ($Tender_content=~m/<div class="cell130 aright">(Keywords<\/div>[\w\W]*?)<h2>Expression/is)
		{
			$tenderblock = &clean($1);
		}
		my $d_RApost="For further information regarding the above contract notice please visit: ".$tender_link;
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		$Sector='UK Other';
		my $tender_type="Normal";
		$Industry_Sector="Other";
		open(out,">>$filename");

		print out "$sourcecw\t$tender_title\t$origin\t$Buyer\t$Buyer\t$noticeSector\t$authorityDept\t$cy\t$country_code\t$Address\t$sourceURL\t$authorityAddress2\t$authorityAddress3\t$Value\t$defence\t$pubFlagCW\t$authorityTown\t$authorityCounty\t$online_only\t$private_sector\t$frameworkAgreement\t$authorityPostcode\t$authorityCountry\t$country_code\t$Telephone\t$authorityFax\t$Email\t$temp_description_export\t$authorityWebAddress\t$authorityProfileURL\t$authorityContactSalutation\t$authorityContactFirstName\t$authorityContactLastName\t$authorityPosition\t$temp_site_export\t$awardProcedure\t$nc\t$trackerRef\t$Contract_Type\t$Deadline_Date\t$pr\t$CPV_Code\t$aa\t$nuts\t$Industry_Sector\t$origPublicationDate\t$Description\t$proofed\t$exported\t$Location\t$autoModifyDate\t$autoModifyTime\t$Authority_Reference\t$no_of_months\t$contract_start_date\t$contract_end_date\t$Value\t$currency\t$value1\t$amount_min\t$amount_max\t$Deadline_Description\t$Deadline_Date\t$Deadline_time\t$d_RApost\t$tenderblock\n";
		close out;	
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time);
	}	
}

if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>\s*Next/is)
{
	my $nextpage = &Urlcheck($1);
	
	my $nextnumber =$1 if ($nextpage=~m/Page\=([\d]+)\&/is);
	exit if ($nextnumber ~~ @nextnumbers);
	push (@nextnumbers,$nextnumber);
	($Tender_content1)=&Getcontent($nextpage);
	$Tender_content1 = decode_utf8($Tender_content1); 
	open(ts,">Tender_contentNext17.html");
	print ts "$Tender_content1";
	close ts;
	print "NextPAge :: $nextpage\n";
	goto top;
}

sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/^\s*,|,\s*$//igs;
	$Clean=~s/\\n//\n /igs;
	decode_entities($Clean);
	return $Clean;
}