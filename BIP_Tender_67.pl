#### Modules ####
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use URI::Escape;
use DateTime;
use POSIX qw(strftime);
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use BIP_Tender_DB;

#### Getting the Input Company ID from the Script Name ####
my $Input_Tender_ID = $0;
$Input_Tender_ID =~ s/\.pl//igs;
$Input_Tender_ID =$1 if($Input_Tender_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

#### Database Initialization ####
my $dbh = &BIP_Tender_DB::DbConnection();
my $Input_Table='tender_master';

#### Retrieve Company URL from Company table ####
my ($ID,$Tender_Name,$Tender_weblink) = &BIP_Tender_DB::RetrieveUrl($dbh,$Input_Tender_ID,$Input_Table);
my $Tender_ID 			= @$ID[0];
my $TenderName 			= @$Tender_Name[0];
my $Tender_Url 			= @$Tender_weblink[0];

print "Tender_ID	: $Tender_ID\n";	
print "TenderName	: $TenderName\n";	
print "Tender_Url	: $Tender_Url\n";	

#### Dupe Check for the press release ####
my ($Processed_Title,$Processed_Url,$Processed_Date) = &BIP_Tender_DB::Retrieve_Data_4_DuplicateCheck($dbh,$Tender_ID,"tender_data");
my @Processed_Title=@$Processed_Title;
my @Processed_Url  =@$Processed_Url;
my @Processed_Date  =@$Processed_Date;

### Ping the first level link page ####
# my $tender_link = 'https://uk.eu-supply.com/ctm/Supplier/PublicTenders';

my ($today)  = strftime "%d-%m-%Y", localtime;
# my ($year)  = strftime "%Y", localtime;
# $today=~s/\-/%2F/igs;

my $today			= time;
my $time1			= $today - 60 * 48 * 60;
my $time2			= $today + 60 * 24 * 60;
my $dayB4Yesterday	= strftime "%d-%m-%Y", ( localtime($time1) );
my $tomottow		= strftime "%d-%m-%Y", ( localtime($time2) );
print "\n$dayB4Yesterday to $tomottow\n";

my $nextpage=1;
top:
my $host = 'irl.eu-supply.com';
my $post = 'SearchFilter.SortField=None&SearchFilter.SortDirection=None&SearchFilter.ShortDescription=&SearchFilter.Reference=&SearchFilter.TenderId=0&SearchFilter.OperatorId=1&Branding=ETENDERS_SIMPLE&SearchFilter.PublishType=2&TextFilter=&SearchFilter.FromDate='.$dayB4Yesterday.'&SearchFilter.ToDate='.$tomottow.'&SearchFilter.ShowExpiredRft=false&SearchFilter.PagingInfo.PageNumber='.$nextpage.'&SearchFilter.PagingInfo.PageSize=25&SearchFilter.CategoryId=0&SearchFilter.UnitId=0';
my $post = 'SearchFilter.SortField=None&SearchFilter.SortDirection=None&SearchFilter.ShortDescription=&SearchFilter.Reference=&SearchFilter.TenderId=0&SearchFilter.OperatorId=1&Branding=ETENDERS_SIMPLE&SavedCategoryId=&SavedUnitAndName=&SearchFilter.PublishType=2&TextFilter=&SearchFilter.FromDate='.$dayB4Yesterday.'&SearchFilter.ToDate='.$tomottow.'&CpvContainer.CpvCodes=&CpvContainer.CpvIds=&CpvContainer.CpvMain=&CpvContainer.ContractType=&CpvContainer.CpvIds=&CpvContainer.IsMandatory=True&SearchFilter.ShowExpiredRft=false&SearchFilter.PagingInfo.PageNumber='.$nextpage.'&SearchFilter.PagingInfo.PageSize=25';
my $ref = 'https://irl.eu-supply.com/ctm/Supplier/publictenders/PublicTenders';
my ($Tender_content1)=&Postcontent($Tender_Url,$post,$host,$ref);
$Tender_content1 = decode_utf8($Tender_content1);  
open(ts,">Tender_content67.html");
print ts "$Tender_content1";
close ts;
# exit;

#### To get the tender link ####
&info_collect($Tender_content1);
if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>\s*<i\s*class\=\"icon\-forward/is)
{
	$nextpage = $1;
	print "\nNextPage	:: $nextpage\n";
	goto top;
	
}

sub info_collect()
{
	my $input_content = shift;
	# while ($input_content=~m/<td>\s*([^>]*?)\s*<\/td>\s*<td>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/td>\s*<td>[^>]*?<\/td>\s*<td>\s*([^>]*?)\s([^>]*?)\s*<\/td>/igs)
	# while ($input_content=~m/<td>\s*<div data-toggle="tooltip"[^>]*?>\s*([^>]*?)\s*<\/div>\s*<\/td>\s*<td>\s*<span[^>]*?><\/span>\s*<a[^>]*?href="([^>]*?)"[^>]*?>([^>]*?)<\/a>\s*<\/td>\s*<td>\s*<div[^>]*?>[^>]*?<\/div>\s*<\/td>\s*<td>\s*<div[^>]*?>\s*s*([^>]*?)\s([^>]*?)\s*<\/div>/igs)
	while ($input_content=~m/<td>\s*<div data-toggle="tooltip"[^>]*?>\s*([^>]*?)\s*<\/div>\s*<\/td>\s*<td>\s*<span[^>]*?><\/span>\s*<a[^>]*?href="([^>]*?)"[^>]*?>([^>]*?)<\/a>\s*<\/td>\s*<td>\s*<div[^>]*?>[^>]*?<\/div>\s*<\/td>\s*<td>\s*<div[^>]*?>\s*s*([^>]*?)\s([^>]*?)\s*<\/div>\s*<\/td>\s*<td>\s*<div[^>]*?>\s*([^>]*?)\s*<\/div>/igs)
	{
		my $Authority_Reference		= $1;
		my $tender_link				= &Urlcheck($2);
		my $tender_title1			= $3;
		my $Deaddate_first_table	= $4;
		my $Deadtime_first_table	= $5;
		my $Contract_Type = $6;
		$tender_link=~s/rwlentrance_s\.asp/publicpurchase.asp/igs;
		
		# my ($Deaddate_first_table,$Deadtime_first_table,$Authority_Reference);
		# if ($Tender_content1 =~m/<td>\s*([^>]*?)\s*<\/td>\s*<td>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/td>\s*<td>[^>]*?<\/td>\s*<td>\s*([^>]*?)\s([^>]*?)\s*<\/td>/is)
		# {
			# $Authority_Reference  = $1;
			# $Deaddate_first_table = $4;
			# $Deadtime_first_table = $5;
		# }
		
		# $tender_link=~s/rwlentrance_s\.asp/publicpurchase.asp/igs;
		my ($Tender_content)=&Getcontent($tender_link);
		$Tender_content = decode_utf8($Tender_content); 

		my ($tender_title,$Origin,$Sector,$Industry_Sector,$Awarding_Authority,$CPV_Code,$Description,$Value,$Deadline_time,$Sent_to,$Location,$Deadline_Description,$Deadline_Date);
		
		if ($Tender_content=~m/Short\s*description<\/span>\s*(?:<br>)([^>]*?)(?:<br>|<\/td>)/is)
		{
			$tender_title 		 = &BIP_Tender_DB::clean($1);
		}
		elsif ($Tender_content=~m/Short\s*description<\/span>\s*(?:<br>)?([^>]*?)\s*(?:\-|<br>)/is)
		{
			$tender_title 		 = &BIP_Tender_DB::clean($1);
		}
		else
		{
			$tender_title=$tender_title1;
		}
		if ($Tender_content=~m/(Response\s*deadline[^>]*?)\s*<\/span>(?:<br>)?(?:([^>]*?)\s([^>]*?)\s*<)?/is)
		{
			$Deadline_Description = &clean($1);
			$Deadline_Date = &clean($2);
			$Deadline_time = $3;
		}
		else
		{
			$Deadline_Date=$Deaddate_first_table if ($Deadline_Date eq '');
			$Deadline_time=$Deadtime_first_table if ($Deadline_time eq '');
		}
		
		#Duplicate check
		my ($duplicate,$title_check_duplicate,$tender_link,$Deadline_Date1) = &BIP_Tender_DB::DuplicateCheck(\@Processed_Url,\@Processed_Title,\@Processed_Date,$tender_title,$tender_link,$Deadline_Date,$Tender_ID);
		if ($duplicate eq 'Y')
		{
			print "\nAlready Existing this Tender Data\n";
			next;
		}
		else
		{
			print "\nThis is newly Released Tender\n";
		}
		
		if ($Tender_content=~m/>\s*CPV\s*codes([\w\W]*?)<\/td>/is)
		{
			$CPV_Code = &clean($1);
		}
		my ($address,$contact_telephone,$tenderblock);
		if ($Tender_content=~m/>\s*office([\w\W]*?<\/td>[\w\W]*?)<\/td>/is)
		{
			$address = &clean($1);
		}
		elsif ($Tender_content=~m/>\s*Buyer([\w\W]*?<\/td>[\w\W]*?)<\/td>/is)
		{
			$address = &clean($1);
		}
		elsif ($Tender_content=~m/>\s*office([\w\W]*?)<\/td>/is)
		{
			$address = &clean($1);
		}
		elsif($Tender_content=~m/>\s*Detailed\s*description[\w\W]*?<\/td>([\w\W]*?)<\/td>([\w\W]*?)<\/td>/is)
		{
			$address = &clean($1);
			$contact_telephone = &clean($2);
		}
		$Awarding_Authority=$address.', '.$contact_telephone;
		# print "Awarding1 :: $Awarding_Authority\n";
		# $Awarding_Authority=~s/^\s*,\s*$//igs;
		# $Awarding_Authority=~s/\,\s*\,/,/igs;
		# $Awarding_Authority=~s/,+/,/igs;
		# $Awarding_Authority=~s/^\s*,\s*$//igs;
		$Awarding_Authority=&clean($Awarding_Authority);
		$Awarding_Authority=~s/[^[:print:]]+/ /igs;
		$Awarding_Authority=~s/\s\s+/ /igs;
		
		if ($Tender_content=~m/>\s*Detailed\s*description([\w\W]*?)<\/td>/is)
		{
			$Description = &clean($1);
		}
		if ($Tender_content=~m/Currency<\/span>\s*(?:<br>)?\s*([^>]*?)\s*<\/td>/is)
		{
			$Value = &clean($1);
		}
		my ($nuts,$awrd_procedure,$Supplier);
		while ($Tender_content=~m/>\s*Awarded\s*supplier([\w\W]*?<\/td>[\w\W]*?)<\/td>/igs)
		{
			$Supplier.=&clean($1)."|";
		}
		$Supplier=~s/\|\s*$//igs;
	
		if ($Tender_content=~m/Tender\s*information<\/td>[\w\W]*?<\/table>([\w\W]*?)<\/table>\s*<div/is)
		{
			$tenderblock = $1;
		}
		else
		{
			$tenderblock=$Tender_content;
		}
		$tenderblock = &BIP_Tender_DB::clean($tenderblock);
		# my $tender_type="Awarded";
		my $award_status = 'Y';
		&BIP_Tender_DB::Insert_Tender_Data($dbh,$Tender_ID,$tender_link,$tenderblock,$Origin,$Sector,$TenderName,$Industry_Sector,$tender_title,$title_check_duplicate,$Awarding_Authority,$Contract_Type,$CPV_Code,$Description,$Location,$Authority_Reference,$Value,$Deadline_Description,$Deadline_Date1,$Deadline_time,$nuts,$awrd_procedure,$award_status,$Supplier);
	}
	# if ($Tender_content1=~m/href\=\"([^>]*?)\"[^>]*?>\s*<i\s*class\=\"icon\-forward/is)
	# {
		# my $nextpage = $1;
		# my $ref = 'https://irl.eu-supply.com/ctm/supplier/publictenders';
		# my $hos = 'irl.eu-supply.com';
		
		# my $Today=DateTime->now();
		# my ($YY,$MM,$DD);
		# if ($Today=~m/([\d]+)\-([\d]+)\-([\d]+)T/is)
		# {
			# $YY = $1;
			# $MM = $2;
			# $DD = $3;
		# }
		
		# my $postcontent ='SearchFilter.SortField=None&SearchFilter.SortDirection=None&SearchFilter.ShortDescription=&SearchFilter.Reference=&SearchFilter.TenderId=0&SearchFilter.OperatorId=1&Branding=ETENDERS_SIMPLE&SearchFilter.PublishType=1&TextFilter=&SearchFilter.FromDate=12%2F02%2F2007&SearchFilter.ToDate='.$DD.'%2F'.$MM.'%2F'.$YY.'&SearchFilter.ShowExpiredRft=false&SearchFilter.PagingInfo.PageNumber='.$nextpage.'&SearchFilter.PagingInfo.PageSize=25&SearchFilter.CategoryId=0&SearchFilter.UnitId=0';
		
		# my $post_url = 'https://irl.eu-supply.com/ctm/Supplier/publictenders/PublicTenders';
		# ($Tender_content1)=&Postcontent($post_url,$postcontent,$hos,$ref);
		# $Tender_content1 = decode_utf8($Tender_content1); 
		# open(ts,">Tender_content1.html");
		# print ts "$Tender_content1";
		# close ts;
		# print "\nNextPage	:: $nextpage\n";
		# goto top;
	# }
}
	
sub Urlcheck()
{
	my $News_link = shift;
	if($News_link!~m/^http/is)
	{	
		my $u1=URI::URL->new($News_link,$Tender_Url);
		my $u2=$u1->abs;
		$News_link=$u2;
		$News_link=~s/\&amp\;/&/igs;
	}
	return ($News_link);
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->header("Referer"=> "currentCulture=en%2DGB; EUSSESSION=8576e973-08e5-4169-87f5-9b97537c3bd5; semanticId=1; EUSSTARTPAGE=etenders%5Fsimple%2Easp; EUSBRAND=%2FPROD%2FETENDERS%5FSIMPLE; EUSCUSTOMCSS=0");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $Clean =shift;
	$Clean=~ s/<br>|<br\s*\/>|<dt>|<dd>|<\/td>|<\/tr>/, /igs;
	$Clean=~ s/,+/,/igs;
	$Clean=~s/<[^>]*?>//igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~s/\s\s+/ /igs;
	$Clean=~s/\&nbsp\;/ /igs;
	$Clean=~s/\&amp\;/&/igs;
	$Clean=~s/^\s*|\s*$//igs;
	$Clean=~ s/^\s*,|,\s*$//igs;
	$Clean=~ s/\:\s*\,/:/igs;
	$Clean=~ s/\.\s*\,/./igs;
	$Clean=~ s/\s*\,/,/igs;
	$Clean=~ s/\,\s*\,/,/igs;
	$Clean=~ s/,+/,/igs;
	decode_entities($Clean);
	return $Clean;
}